﻿using System;
using System.Windows.Forms;
using System.Data.OleDb;
using DDJJ_Regalías_Mineras.Ventanas;
using System.IO;
using System.Reflection;
using System.Diagnostics;

namespace DDJJ_Regalías_Mineras
{
    static class Program
    {
        static bool debug = true;

        static OleDbConnection connMineria;
        static OleDbConnection connAlquiler;

        [STAThread]
        static void Main()
        {
            if (RunningInstance() != null)
            {
                MessageBox.Show("YA EXISTE UNA INSTANCIA DEL SOFTWARE ABIERTA Y ACTIVA.\nNo es posible abrir dos instancias del software!");
            }
            else
            {
                if (ConnectToAccess())
                {
                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);
                    Application.Run(new VentanaPrincipal(connMineria, connAlquiler));
                }
            }

        }

        public static Process RunningInstance()
        {
            Process current = Process.GetCurrentProcess();
            Process[] processes = Process.GetProcessesByName(current.ProcessName);

            //Loop through the running processes in with the same name 
            foreach (Process process in processes)
            {
                //Ignore the current process 
                if (process.Id != current.Id)
                {
                    //Make sure that the process is running from the exe file. 
                    if (Assembly.GetExecutingAssembly().Location.
                         Replace("/", "\\") == current.MainModule.FileName)

                    {
                        //Return the other process instance.  
                        return process;

                    }
                }
            }
            //No other instance was found, return null.  
            return null;
        }

        static bool ConnectToAccess()
        {
            /*XmlDocument xml = new XmlDocument();
            xml.Load(actualPath + @"\data\config.xml");
            XmlNodeList xmlNode = xml.SelectNodes("gestionMinera");
            String productor = Convert.ToString(xmlNode[0]["productor"].InnerText);
            String num = Convert.ToString(xmlNode[0]["num"].InnerText);*/

            connMineria = new OleDbConnection();

            connMineria.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;" +
            "Data source=" + getActualPath() + @"\data\database.accdb";

            connAlquiler = new OleDbConnection();

            connAlquiler.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;" +
            "Data source=" + getActualPath() + @"\data\databaseAlquileres.accdb";

            try
            {
                connMineria.Open();
                connAlquiler.Open();
                return true;
            }
            catch (Exception ex)
            {
                connMineria.Close();
                MessageBox.Show("ERROR AL CONECTAR CON LA BASE DE DATOS:\n" + ex.Message, "ERROR");
                return false;
            }
        }

        public static String getActualPath()
        {
            if (debug)
            {
                return @"C:\Users\Mariano\Desktop\Gestion Minera";
            }
            else
            {
                return Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            }
        }
    }
}
