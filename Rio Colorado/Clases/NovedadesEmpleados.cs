﻿using System;

namespace Gestion_Minera.Clases
{
    public class NovedadesEmpleados
    {
        public int ID { get; set; }
        public Empleado empleado { get; set; }
        public Novedad novedad { get; set; }
        public DateTime fecha { get; set; }
        public String valor { get; set; }
        public String valor2 { get; set; }
        public String valor3 { get; set; }
        public DateTime desde { get; set; }
        public DateTime hasta { get; set; }

        public NovedadesEmpleados (int ID, Empleado empleado, Novedad novedad, DateTime fecha, String valor, String valor2, String valor3, DateTime desde, DateTime hasta)
        {
            this.ID = ID;
            this.empleado = empleado;
            this.novedad = novedad;
            this.fecha = fecha;
            this.valor = valor;
            this.valor2 = valor2;
            this.valor3 = valor3;
            this.desde = desde;
            this.hasta = hasta;
        }

        public NovedadesEmpleados()
        {

        }
    }
}
