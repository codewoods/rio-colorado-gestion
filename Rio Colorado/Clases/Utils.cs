﻿using DDJJ_Regalías_Mineras;
using DDJJ_Regalías_Mineras.Clases;
using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.IO;
using System.Windows.Forms;
using Application = Microsoft.Office.Interop.Word.Application;

namespace Gestion_Minera.Clases
{
    class Utils
    {
        public static List<Mineral> getMinerales(OleDbConnection conn)
        {
            List<Mineral> minerales = new List<Mineral>();

            string sql = "SELECT * FROM Mineral";

            OleDbCommand dbCmd = new OleDbCommand(sql, conn);
            OleDbDataReader dbReader = dbCmd.ExecuteReader();

            while (dbReader.Read())
            {
                Mineral min = new Mineral();

                min.ID = int.Parse(dbReader.GetValue(0).ToString());
                min.nombre = dbReader.GetValue(1).ToString();
                min.yacimiento = dbReader.GetValue(2).ToString();
                min.precioUnitario = float.Parse(dbReader.GetValue(3).ToString());
                min.finalidad = dbReader.GetValue(4).ToString();
                min.regalias = float.Parse(dbReader.GetValue(5).ToString());
                min.vencimiento = 0;
                min.tonAM3 = float.Parse(dbReader.GetValue(7).ToString());
                min.coeficiente = float.Parse(dbReader.GetValue(8).ToString());

                minerales.Add(min);
            }

            return minerales;
        }

        public static List<Empleado> getEmpleados(OleDbConnection conn)
        {
            List<Empleado> empleados = new List<Empleado>();

            string sql = "SELECT * FROM Empleado ORDER BY Apellido, Nombre ASC";

            OleDbCommand dbCmd = new OleDbCommand(sql, conn);
            OleDbDataReader dbReader = dbCmd.ExecuteReader();

            while (dbReader.Read())
            {
                Empleado empleado = new Empleado();

                empleado.ID = int.Parse(dbReader.GetValue(0).ToString());
                empleado.Apellido = dbReader.GetValue(1).ToString();
                empleado.Nombre = dbReader.GetValue(2).ToString();
                empleado.CUIL = dbReader.GetValue(3).ToString();
                empleado.FechaIngreso = DateTime.Parse(dbReader.GetValue(4).ToString());
                empleado.Categoria = dbReader.GetValue(5).ToString();
                empleado.Sindicato = getSindicato(conn, int.Parse(dbReader.GetValue(6).ToString()));

                empleados.Add(empleado);
            }

            return empleados;
        }

        public static Empleado getEmpleado(OleDbConnection conn, int idEmpleado)
        {
            Empleado empleado = new Empleado();

            string sql = "SELECT * FROM Empleado WHERE Id=" + idEmpleado;

            OleDbCommand dbCmd = new OleDbCommand(sql, conn);
            OleDbDataReader dbReader = dbCmd.ExecuteReader();

            if (dbReader.Read())
            {
                empleado.ID = int.Parse(dbReader.GetValue(0).ToString());
                empleado.Apellido = dbReader.GetValue(1).ToString();
                empleado.Nombre = dbReader.GetValue(2).ToString();
                empleado.CUIL = dbReader.GetValue(3).ToString();
                empleado.FechaIngreso = DateTime.Parse(dbReader.GetValue(4).ToString());
                empleado.Categoria = dbReader.GetValue(5).ToString();
                empleado.Sindicato = getSindicato(conn, int.Parse(dbReader.GetValue(6).ToString()));
            }

            return empleado;
        }

        public static Sindicato getSindicato(OleDbConnection conn, int id)
        {
            Sindicato sindicato = null;

            string sql = "SELECT * FROM Sindicato WHERE Id=" + id;

            OleDbCommand dbCmd = new OleDbCommand(sql, conn);
            OleDbDataReader dbReader = dbCmd.ExecuteReader();

            if (dbReader.Read())
            {
                sindicato = new Sindicato(id, dbReader.GetValue(1).ToString());
            }

            return sindicato;
        }


        public static List<Sindicato> getSindicatos(OleDbConnection conn)
        {
            List<Sindicato> sindicatos = new List<Sindicato>();

            string sql = "SELECT * FROM Sindicato";

            OleDbCommand dbCmd = new OleDbCommand(sql, conn);
            OleDbDataReader dbReader = dbCmd.ExecuteReader();

            while (dbReader.Read())
            {
                sindicatos.Add(new Sindicato(int.Parse(dbReader.GetValue(0).ToString()), dbReader.GetValue(1).ToString()));
            }

            return sindicatos;
        }

        public static List<Novedad> getNovedades(OleDbConnection conn)
        {
            List<Novedad> novedades = new List<Novedad>();

            string sql = "SELECT * FROM Novedad ORDER BY Id ASC";

            OleDbCommand dbCmd = new OleDbCommand(sql, conn);
            OleDbDataReader dbReader = dbCmd.ExecuteReader();

            while (dbReader.Read())
            {
                Novedad novedad = new Novedad();

                novedad.ID = int.Parse(dbReader.GetValue(0).ToString());
                novedad.novedad = dbReader.GetValue(1).ToString();
                novedad.fecha = Boolean.Parse(dbReader.GetValue(2).ToString());
                novedad.valores = int.Parse(dbReader.GetValue(3).ToString());
                novedad.medida = dbReader.GetValue(4).ToString();

                novedades.Add(novedad);
            }

            return novedades;
        }

        public static Novedad getNovedad(OleDbConnection conn, int idNovedad)
        {
            Novedad novedad = new Novedad();

            string sql = "SELECT * FROM Novedad WHERE Id=" + idNovedad;

            OleDbCommand dbCmd = new OleDbCommand(sql, conn);
            OleDbDataReader dbReader = dbCmd.ExecuteReader();

            if (dbReader.Read())
            {
                novedad.ID = int.Parse(dbReader.GetValue(0).ToString());
                novedad.novedad = dbReader.GetValue(1).ToString();
                novedad.fecha = Boolean.Parse(dbReader.GetValue(2).ToString());
                novedad.valores = int.Parse(dbReader.GetValue(3).ToString());
            }

            return novedad;
        }

        public static int getCountNovedades(OleDbConnection conn, int idEmpleado, int idNovedad, DateTime desde, DateTime hasta)
        {
            string sql = "SELECT COUNT(*) FROM NovedadesEmpleados WHERE ";

            if (idEmpleado > 0)
            {
                sql += "idEmpleado=" + idEmpleado + " AND ";
            }

            if (idNovedad > 0)
            {
                sql += "idNovedad=" + idNovedad + " AND ";
            }

            if (desde == DateTime.MinValue)
            {
                sql += " 1";
            }
            else
            {
                sql += " ((Desde >=#" + desde.ToString("yyyy-MM-dd") + "# AND Desde <=#" + hasta.ToString("yyyy-MM-dd") + "#)" +
                   " OR (Hasta >=#" + desde.ToString("yyyy-MM-dd") + "# AND Hasta <=#" + hasta.ToString("yyyy-MM-dd") + "#)" +
                   " OR (Desde >=#" + desde.ToString("yyyy-MM-dd") + "# AND Hasta <= #" + desde.ToString("yyyy-MM-dd") + "#)" +
                   " OR (Desde >=#" + hasta.ToString("yyyy-MM-dd") + "# AND hasta <= #" + hasta.ToString("yyyy-MM-dd") + "#)" +
                   " OR (Desde <=#" + desde.ToString("yyyy-MM-dd") + "# AND Hasta >= #" + desde.ToString("yyyy-MM-dd") + "#)" +
                   " OR (Desde <=#" + hasta.ToString("yyyy-MM-dd") + "# AND hasta >= #" + desde.ToString("yyyy-MM-dd") + "#))";
            }

            OleDbCommand dbCmd = new OleDbCommand(sql, conn);
            OleDbDataReader dbReader = dbCmd.ExecuteReader();

            if (dbReader.Read())
            {
                return int.Parse(dbReader.GetValue(0).ToString());
            }
            else
            {
                return 0;
            }
        }

        public static List<NovedadesEmpleados> getNovedadesEmpleados(OleDbConnection conn, int idEmpleado, int idNovedad, DateTime desde, DateTime hasta, int lim1, int lim2)
        {
            List<NovedadesEmpleados> novedadesEmpleados = new List<NovedadesEmpleados>();

            string sql;

            if (lim2 == 1)
            {
                sql = "SELECT TOP " + lim1 + " * FROM NovedadesEmpleados WHERE ";

                if (idEmpleado > 0)
                {
                    sql += "idEmpleado=" + idEmpleado + " AND ";
                }

                if (idNovedad > 0)
                {
                    sql += "idNovedad=" + idNovedad + " AND ";
                }

                if (desde == DateTime.MinValue)
                {
                    sql += " 1";
                }
                else
                {
                    sql += " ((Desde >=#" + desde.ToString("yyyy-MM-dd") + "# AND Desde <=#" + hasta.ToString("yyyy-MM-dd") + "#)" +
                       " OR (Hasta >=#" + desde.ToString("yyyy-MM-dd") + "# AND Hasta <=#" + hasta.ToString("yyyy-MM-dd") + "#)" +
                       " OR (Desde >=#" + desde.ToString("yyyy-MM-dd") + "# AND Hasta <= #" + desde.ToString("yyyy-MM-dd") + "#)" +
                       " OR (Desde >=#" + hasta.ToString("yyyy-MM-dd") + "# AND hasta <= #" + hasta.ToString("yyyy-MM-dd") + "#)" +
                       " OR (Desde <=#" + desde.ToString("yyyy-MM-dd") + "# AND Hasta >= #" + desde.ToString("yyyy-MM-dd") + "#)" +
                       " OR (Desde <=#" + hasta.ToString("yyyy-MM-dd") + "# AND hasta >= #" + desde.ToString("yyyy-MM-dd") + "#))";
                }

                sql += " ORDER BY Fecha ASC";

            }
            else
            {
                sql = "SELECT TOP " + lim1 + " * FROM NovedadesEmpleados WHERE Id NOT IN " +
                "(SELECT TOP " + lim2 + " Id FROM NovedadesEmpleados WHERE ";

                if (idEmpleado > 0)
                {
                    sql += "idEmpleado=" + idEmpleado + " AND ";
                }

                if (idNovedad > 0)
                {
                    sql += "idNovedad=" + idNovedad + " AND ";
                }

                if (desde == DateTime.MinValue)
                {
                    sql += " 1";
                }
                else
                {
                    sql += " ((Desde >=#" + desde.ToString("yyyy-MM-dd") + "# AND Desde <=#" + hasta.ToString("yyyy-MM-dd") + "#)" +
                       " OR (Hasta >=#" + desde.ToString("yyyy-MM-dd") + "# AND Hasta <=#" + hasta.ToString("yyyy-MM-dd") + "#)" +
                       " OR (Desde >=#" + desde.ToString("yyyy-MM-dd") + "# AND Hasta <= #" + desde.ToString("yyyy-MM-dd") + "#)" +
                       " OR (Desde >=#" + hasta.ToString("yyyy-MM-dd") + "# AND hasta <= #" + hasta.ToString("yyyy-MM-dd") + "#)" +
                       " OR (Desde <=#" + desde.ToString("yyyy-MM-dd") + "# AND Hasta >= #" + desde.ToString("yyyy-MM-dd") + "#)" +
                       " OR (Desde <=#" + hasta.ToString("yyyy-MM-dd") + "# AND hasta >= #" + desde.ToString("yyyy-MM-dd") + "#))";
                }

                sql += " ORDER BY Fecha ASC) ORDER BY Fecha ASC";
            }

            OleDbCommand dbCmd = new OleDbCommand(sql, conn);
            OleDbDataReader dbReader = dbCmd.ExecuteReader();

            while (dbReader.Read())
            {
                NovedadesEmpleados novedad = new NovedadesEmpleados();

                novedad.ID = int.Parse(dbReader.GetValue(0).ToString());
                novedad.empleado = getEmpleado(conn, int.Parse(dbReader.GetValue(1).ToString()));
                novedad.novedad = getNovedad(conn, int.Parse(dbReader.GetValue(2).ToString()));
                novedad.fecha = DateTime.Parse(dbReader.GetValue(3).ToString());
                novedad.valor = dbReader.GetValue(4).ToString();
                novedad.valor2 = dbReader.GetValue(5).ToString();
                novedad.valor3 = dbReader.GetValue(6).ToString();

                if (dbReader.GetValue(7).ToString() != "")
                {
                    novedad.desde = DateTime.Parse(dbReader.GetValue(7).ToString());
                }
                else
                {
                    novedad.desde = DateTime.MinValue;
                }

                if (dbReader.GetValue(8).ToString() != "")
                {
                    novedad.hasta = DateTime.Parse(dbReader.GetValue(8).ToString());
                }
                else
                {
                    novedad.hasta = DateTime.MinValue;
                }

                novedadesEmpleados.Add(novedad);
            }

            return novedadesEmpleados;
        }

        public static NovedadesEmpleados getNovedadEmpleado(OleDbConnection conn, int idNovedadEmpleado)
        {
            NovedadesEmpleados novedadEmpleado = new NovedadesEmpleados();

            string sql = "SELECT * FROM NovedadesEmpleados WHERE Id=" + idNovedadEmpleado;

            OleDbCommand dbCmd = new OleDbCommand(sql, conn);
            OleDbDataReader dbReader = dbCmd.ExecuteReader();

            if (dbReader.Read())
            {
                novedadEmpleado.ID = int.Parse(dbReader.GetValue(0).ToString());
                novedadEmpleado.empleado = getEmpleado(conn, int.Parse(dbReader.GetValue(1).ToString()));
                novedadEmpleado.novedad = getNovedad(conn, int.Parse(dbReader.GetValue(2).ToString()));
                novedadEmpleado.fecha = DateTime.Parse(dbReader.GetValue(3).ToString());
                novedadEmpleado.valor = dbReader.GetValue(4).ToString();
                novedadEmpleado.valor2 = dbReader.GetValue(5).ToString();
                novedadEmpleado.valor3 = dbReader.GetValue(6).ToString();

                if (dbReader.GetValue(7).ToString() != "")
                {
                    novedadEmpleado.desde = DateTime.Parse(dbReader.GetValue(7).ToString());
                }
                else
                {
                    novedadEmpleado.desde = DateTime.MinValue;
                }

                if (dbReader.GetValue(8).ToString() != "")
                {
                    novedadEmpleado.hasta = DateTime.Parse(dbReader.GetValue(8).ToString());
                }
                else
                {
                    novedadEmpleado.hasta = DateTime.MinValue;
                }
            }

            return novedadEmpleado;
        }

        public static List<NovedadesEmpleados> getNovedadesEmpleadosMes(OleDbConnection conn, int mes, int anio)
        {
            List<NovedadesEmpleados> novedadesEmpleados = new List<NovedadesEmpleados>();

            string sql;

            sql = "SELECT * FROM NovedadesEmpleados WHERE (MONTH(Desde) = " + mes + " AND YEAR(Desde) = " + anio + ") " +
                " OR (MONTH(Hasta) = " + mes + " AND YEAR(Hasta) = " + anio + ") " +
                " ORDER BY Fecha ASC";

            OleDbCommand dbCmd = new OleDbCommand(sql, conn);
            OleDbDataReader dbReader = dbCmd.ExecuteReader();

            while (dbReader.Read())
            {
                NovedadesEmpleados novedad = new NovedadesEmpleados();

                novedad.ID = int.Parse(dbReader.GetValue(0).ToString());
                novedad.empleado = getEmpleado(conn, int.Parse(dbReader.GetValue(1).ToString()));
                novedad.novedad = getNovedad(conn, int.Parse(dbReader.GetValue(2).ToString()));
                novedad.fecha = DateTime.Parse(dbReader.GetValue(3).ToString());
                novedad.valor = dbReader.GetValue(4).ToString();
                novedad.valor2 = dbReader.GetValue(5).ToString();
                novedad.valor3 = dbReader.GetValue(6).ToString();

                if (dbReader.GetValue(7).ToString() != "")
                {
                    novedad.desde = DateTime.Parse(dbReader.GetValue(7).ToString());
                }
                else
                {
                    novedad.desde = DateTime.MinValue;
                }

                if (dbReader.GetValue(8).ToString() != "")
                {
                    novedad.hasta = DateTime.Parse(dbReader.GetValue(8).ToString());
                }
                else
                {
                    novedad.hasta = DateTime.MinValue;
                }

                novedadesEmpleados.Add(novedad);
            }

            return novedadesEmpleados;
        }

        public static String numeroLetra(string num)
        {
            String res, dec = "";
            Int64 entero;
            int decimales;
            double nro;

            try
            {
                nro = Convert.ToDouble(num);
            }
            catch
            {
                return "";
            }

            entero = Convert.ToInt64(Math.Truncate(nro));
            decimales = Convert.ToInt32(Math.Round((nro - entero) * 100, 2));
            if (decimales > 0)
            {
                dec = " CON " + decimales.ToString() + "/100";
            }

            res = toText(Convert.ToDouble(entero)) + dec;
            return res;
        }

        private static String toText(double value)
        {
            string Num2Text = "";
            value = Math.Truncate(value);
            if (value == 0) Num2Text = "CERO";
            else if (value == 1) Num2Text = "UNO";
            else if (value == 2) Num2Text = "DOS";
            else if (value == 3) Num2Text = "TRES";
            else if (value == 4) Num2Text = "CUATRO";
            else if (value == 5) Num2Text = "CINCO";
            else if (value == 6) Num2Text = "SEIS";
            else if (value == 7) Num2Text = "SIETE";
            else if (value == 8) Num2Text = "OCHO";
            else if (value == 9) Num2Text = "NUEVE";
            else if (value == 10) Num2Text = "DIEZ";
            else if (value == 11) Num2Text = "ONCE";
            else if (value == 12) Num2Text = "DOCE";
            else if (value == 13) Num2Text = "TRECE";
            else if (value == 14) Num2Text = "CATORCE";
            else if (value == 15) Num2Text = "QUINCE";
            else if (value < 20) Num2Text = "DIECI" + toText(value - 10);
            else if (value == 20) Num2Text = "VEINTE";
            else if (value < 30) Num2Text = "VEINTI" + toText(value - 20);
            else if (value == 30) Num2Text = "TREINTA";
            else if (value == 40) Num2Text = "CUARENTA";
            else if (value == 50) Num2Text = "CINCUENTA";
            else if (value == 60) Num2Text = "SESENTA";
            else if (value == 70) Num2Text = "SETENTA";
            else if (value == 80) Num2Text = "OCHENTA";
            else if (value == 90) Num2Text = "NOVENTA";
            else if (value < 100) Num2Text = toText(Math.Truncate(value / 10) * 10) + " Y " + toText(value % 10);
            else if (value == 100) Num2Text = "CIEN";
            else if (value < 200) Num2Text = "CIENTO " + toText(value - 100);
            else if ((value == 200) || (value == 300) || (value == 400) || (value == 600) || (value == 800)) Num2Text = toText(Math.Truncate(value / 100)) + "CIENTOS";
            else if (value == 500) Num2Text = "QUINIENTOS";
            else if (value == 700) Num2Text = "SETECIENTOS";
            else if (value == 900) Num2Text = "NOVECIENTOS";
            else if (value < 1000) Num2Text = toText(Math.Truncate(value / 100) * 100) + " " + toText(value % 100);
            else if (value == 1000) Num2Text = "MIL";
            else if (value < 2000) Num2Text = "MIL " + toText(value % 1000);
            else if (value < 1000000)
            {
                Num2Text = toText(Math.Truncate(value / 1000)) + " MIL";
                if ((value % 1000) > 0) Num2Text = Num2Text + " " + toText(value % 1000);
            }

            else if (value == 1000000) Num2Text = "UN MILLON";
            else if (value < 2000000) Num2Text = "UN MILLON " + toText(value % 1000000);
            else if (value < 1000000000000)
            {
                Num2Text = toText(Math.Truncate(value / 1000000)) + " MILLONES ";
                if ((value - Math.Truncate(value / 1000000) * 1000000) > 0) Num2Text = Num2Text + " " + toText(value - Math.Truncate(value / 1000000) * 1000000);
            }

            else if (value == 1000000000000) Num2Text = "UN BILLON";
            else if (value < 2000000000000) Num2Text = "UN BILLON " + toText(value - Math.Truncate(value / 1000000000000) * 1000000000000);

            else
            {
                Num2Text = toText(Math.Truncate(value / 1000000000000)) + " BILLONES";
                if ((value - Math.Truncate(value / 1000000000000) * 1000000000000) > 0) Num2Text = Num2Text + " " + toText(value - Math.Truncate(value / 1000000000000) * 1000000000000);
            }
            return Num2Text;
        }


        public static void generarComprobanteHaberes(int id, String monto, String mes, String nombre)
        {
            int numeroArchivo = 0;

            try
            {
                String path = Path.GetTempPath() + "comprobanteReciboHaberes.docx";
                String _path = Path.GetTempPath() + "comprobanteReciboHaberes";

                do
                {
                    try
                    {
                        File.Copy(Program.getActualPath() + @"\data\ReciboAH.docx", path, true);
                        break;
                    }
                    catch (Exception ex)
                    {
                        path = _path + numeroArchivo + ".docx";
                        numeroArchivo++;
                    }
                } while (numeroArchivo != 12);

                if (numeroArchivo == 12)
                {
                    MessageBox.Show("No se pudo crear el Comprobante porque hay muchos documentos abiertos. Por favor ciérrelos e intente nuevamente.", "Error");
                }

                Application ap = new Application();
                Document document = ap.Documents.Open(path);
                document.Activate();

                FindAndReplace(ap, "{0}", id);
                FindAndReplace(ap, "{1}", numeroLetra(monto));
                FindAndReplace(ap, "{2}", monto);
                FindAndReplace(ap, "{3}", mes);
                FindAndReplace(ap, "{4}", DateTime.Today.Day);
                FindAndReplace(ap, "{5}", DateTime.Today.ToString("MMMM").ToUpper());
                FindAndReplace(ap, "{6}", DateTime.Today.Year);
                FindAndReplace(ap, "{7}", nombre);

                document.Close();

                System.Diagnostics.Process.Start(path);
            }
            catch (Exception ex)
            {
                MessageBox.Show("No se pudo crea el archivo\nError: " + ex.Message, "Error");
            }
        }

        public static void generarComprobanteVacaciones(String i, String nombre, String vacaciones, String desde, String hasta, String reintegra, String cuit)
        {
            int numeroArchivo = 0;

            try
            {
                String path = Path.GetTempPath() + "NotificacionVacaciones.docx";
                String _path = Path.GetTempPath() + "NotificacionVacaciones";

                do
                {
                    try
                    {
                        File.Copy(Program.getActualPath() + @"\data\NotificacionVacaciones.docx", path, true);
                        break;
                    }
                    catch (Exception ex)
                    {
                        path = _path + numeroArchivo + ".docx";
                        numeroArchivo++;
                    }
                } while (numeroArchivo != 12);

                if (numeroArchivo == 12)
                {
                    MessageBox.Show("No se pudo crear el Comprobante porque hay muchos documentos abiertos. Por favor ciérrelos e intente nuevamente.", "Error");
                }

                Application ap = new Application();
                Document document = ap.Documents.Open(path);
                document.Activate();

                FindAndReplace(ap, "{0}", i);
                FindAndReplace(ap, "{1}", DateTime.Today.ToLongDateString());
                FindAndReplace(ap, "{2}", nombre);
                FindAndReplace(ap, "{3}", vacaciones);
                FindAndReplace(ap, "{4}", desde);
                FindAndReplace(ap, "{5}", hasta);
                FindAndReplace(ap, "{6}", reintegra);
                FindAndReplace(ap, "{7}", cuit);

                document.Close();

                System.Diagnostics.Process.Start(path);
            }
            catch (Exception ex)
            {
                MessageBox.Show("No se pudo crea el archivo\nError: " + ex.Message, "Error");
            }
        }

        public static void generarResumenMineria(String mineral, int año, String[] row1, String[] row2, String[] row3, String[] row4)
        {
            int numeroArchivo = 0;

            try
            {
                String path = Path.GetTempPath() + "ResumenMineria.docx";
                String _path = Path.GetTempPath() + "ResumenMineria";

                do
                {
                    try
                    {
                        File.Copy(Program.getActualPath() + @"\data\ResumenMineria.docx", path, true);
                        break;
                    }
                    catch (Exception ex)
                    {
                        path = _path + numeroArchivo + ".docx";
                        numeroArchivo++;
                    }
                } while (numeroArchivo != 12);

                if (numeroArchivo == 12)
                {
                    MessageBox.Show("No se pudo crear el Comprobante porque hay muchos documentos abiertos. Por favor ciérrelos e intente nuevamente.", "Error");
                }

                Application ap = new Application();
                Document document = ap.Documents.Open(path);
                document.Activate();

                FindAndReplace(ap, "{0}", mineral);
                FindAndReplace(ap, "{00}", año.ToString());

                for (int i = 1; i <= 12; i++)
                {
                    FindAndReplace(ap, "{" + i + "}", row1[i]);
                    FindAndReplace(ap, "{" + (12 + i) + "}", row2[i]);
                    FindAndReplace(ap, "{" + (24 + i) + "}", row3[i]);
                    FindAndReplace(ap, "{" + (36 + i) + "}", row4[i]);
                }

                document.Close();

                System.Diagnostics.Process.Start(path);
            }
            catch (Exception ex)
            {
                MessageBox.Show("No se pudo crea el archivo\nError: " + ex.Message, "Error");
            }
        }

        public static void FindAndReplace(Application doc, object findText, object replaceWithText)
        {
            //options
            object matchCase = false;
            object matchWholeWord = true;
            object matchWildCards = false;
            object matchSoundsLike = false;
            object matchAllWordForms = false;
            object forward = true;
            object format = false;
            object matchKashida = false;
            object matchDiacritics = false;
            object matchAlefHamza = false;
            object matchControl = false;
            object read_only = false;
            object visible = true;
            object replace = 2;
            object wrap = 1;
            //execute find and replace
            doc.Selection.Find.Execute(ref findText, ref matchCase, ref matchWholeWord,
                ref matchWildCards, ref matchSoundsLike, ref matchAllWordForms, ref forward, ref wrap, ref format, ref replaceWithText, ref replace,
                ref matchKashida, ref matchDiacritics, ref matchAlefHamza, ref matchControl);
        }

        public static List<Vacaciones> getVacaciones(OleDbConnection conn, int idEmpleado)
        {
            List<Vacaciones> listaVacaciones = new List<Vacaciones>();

            string sql = "SELECT * FROM Vacaciones WHERE idEmpleado = " + idEmpleado + " ORDER BY Anio ASC";

            OleDbCommand dbCmd = new OleDbCommand(sql, conn);
            OleDbDataReader dbReader = dbCmd.ExecuteReader();

            while (dbReader.Read())
            {
                Vacaciones vacaciones = new Vacaciones();

                vacaciones.idVacaciones = int.Parse(dbReader.GetValue(0).ToString());
                vacaciones.anio = int.Parse(dbReader.GetValue(2).ToString());
                vacaciones.dias = int.Parse(dbReader.GetValue(3).ToString());

                listaVacaciones.Add(vacaciones);
            }

            return listaVacaciones;
        }

        public static List<NovedadesEmpleados> getNovedadesVacaciones(OleDbConnection conn, int idEmpleado)
        {
            List<NovedadesEmpleados> novedadesEmpleados = new List<NovedadesEmpleados>();

            string sql = "SELECT * FROM NovedadesEmpleados WHERE idEmpleado=" + idEmpleado + " AND idNovedad=3 ORDER BY Fecha";

            OleDbCommand dbCmd = new OleDbCommand(sql, conn);
            OleDbDataReader dbReader = dbCmd.ExecuteReader();

            while (dbReader.Read())
            {
                NovedadesEmpleados novedad = new NovedadesEmpleados();

                novedad.ID = int.Parse(dbReader.GetValue(0).ToString());
                novedad.empleado = getEmpleado(conn, int.Parse(dbReader.GetValue(1).ToString()));
                novedad.novedad = getNovedad(conn, int.Parse(dbReader.GetValue(2).ToString()));
                novedad.fecha = DateTime.Parse(dbReader.GetValue(3).ToString());
                novedad.valor = dbReader.GetValue(4).ToString();
                novedad.valor2 = dbReader.GetValue(5).ToString();
                novedad.valor3 = dbReader.GetValue(6).ToString();

                if (dbReader.GetValue(7).ToString() != "")
                {
                    novedad.desde = DateTime.Parse(dbReader.GetValue(7).ToString());
                }
                else
                {
                    novedad.desde = DateTime.MinValue;
                }

                if (dbReader.GetValue(8).ToString() != "")
                {
                    novedad.hasta = DateTime.Parse(dbReader.GetValue(8).ToString());
                }
                else
                {
                    novedad.hasta = DateTime.MinValue;
                }

                novedadesEmpleados.Add(novedad);
            }

            return novedadesEmpleados;
        }

        /* 
         * 3 vacaciones
         * 6 adelanto sueldos
         */

        public static int getNumberNovedad(OleDbConnection conn, int id, int novedad)
        {
            string sql = "SELECT COUNT(*) FROM NovedadesEmpleados WHERE Id<=" + id + " AND idNovedad=" + novedad;

            OleDbCommand dbCmd = new OleDbCommand(sql, conn);
            OleDbDataReader dbReader = dbCmd.ExecuteReader();

            if (dbReader.Read())
            {
                return int.Parse(dbReader.GetValue(0).ToString());
            }
            else
            {
                return 0;
            }
        }

        public static int getLastIDNovedad(OleDbConnection conn)
        {
            string sql = "SELECT Id FROM NovedadesEmpleados WHERE Id > 0 ORDER BY Id DESC";

            OleDbCommand dbCmd = new OleDbCommand(sql, conn);
            OleDbDataReader dbReader = dbCmd.ExecuteReader();

            if (dbReader.Read())
            {
                return int.Parse(dbReader.GetValue(0).ToString());
            }
            else
            {
                return 0;
            }
        }

        public static List<Fila> getTablaDDJJ(OleDbConnection conn, int idMineral, int anio)
        {
            List<Fila> lista = new List<Fila>();
            List<Fila> lista2 = new List<Fila>();

            string sql = "SELECT * FROM Filas WHERE idMineral=" + idMineral + " AND anio=" + anio;

            OleDbCommand dbCmd = new OleDbCommand(sql, conn);
            OleDbDataReader dbReader = dbCmd.ExecuteReader();

            while (dbReader.Read())
            {
                Fila fila = new Fila();

                fila.idFila = int.Parse(dbReader.GetValue(0)?.ToString() ?? "");
                fila.idMineral = int.Parse(dbReader.GetValue(1)?.ToString() ?? "");
                fila.numGuia = int.Parse(dbReader.GetValue(2)?.ToString() ?? "");
                fila.finalidad = dbReader.GetValue(3)?.ToString() ?? "";
                fila.m3 = float.Parse(dbReader.GetValue(4)?.ToString() ?? "");
                fila.toneladas = float.Parse(dbReader.GetValue(5)?.ToString() ?? "");
                fila.reg = float.Parse(dbReader.GetValue(6)?.ToString() ?? "");
                fila.regEnTon = float.Parse(dbReader.GetValue(7)?.ToString() ?? "");
                fila.precioUnitario = float.Parse(dbReader.GetValue(8)?.ToString() ?? "");
                fila.totalReg = float.Parse(dbReader.GetValue(9)?.ToString() ?? "");
                fila.anio = int.Parse(dbReader.GetValue(10)?.ToString() ?? "");
                fila.mes = int.Parse(dbReader.GetValue(11)?.ToString() ?? "");

                lista.Add(fila);
            }

            for (int i = 0; i < 12; i++)
            {
                Fila fila = new Fila();

                fila.anio = anio;
                fila.mes = i + 1;
                fila.precioUnitario = fila.precioUnitario;

                foreach (Fila f in lista)
                {
                    fila.reg = f.reg;
                    fila.regEnTon = f.regEnTon;

                    if (f.mes == (i + 1))
                    {
                        fila.m3 += f.m3;
                        fila.toneladas += f.toneladas;
                        fila.totalReg += f.totalReg;
                    }
                }

                lista2.Add(fila);
            }

            return lista2;
        }

        public static List<Combustible> getCombustible(OleDbConnection conn, int anio)
        {
            List<Combustible> lista = new List<Combustible>();
            List<Combustible> lista2 = new List<Combustible>();

            string sql = "SELECT * FROM Combustible WHERE Anio=" + anio;

            OleDbCommand dbCmd = new OleDbCommand(sql, conn);
            OleDbDataReader dbReader = dbCmd.ExecuteReader();

            while (dbReader.Read())
            {
                lista.Add(new Combustible(int.Parse(dbReader.GetValue(0)?.ToString() ?? ""),
                    int.Parse(dbReader.GetValue(2)?.ToString() ?? ""),
                    dbReader.GetValue(3)?.ToString() ?? ""));
            }

            for (int i = 0; i < 12; i++)
            {
                Combustible fila = new Combustible();

                fila.anio = anio;
                fila.mes = i + 1;

                foreach (Combustible f in lista)
                {
                    if (f.mes == (i + 1))
                    {
                        fila.valor = f.valor;
                        fila.ID = f.ID;
                    }
                }

                lista2.Add(fila);
            }

            return lista2;
        }

        public static bool agregarCombustible(OleDbConnection conn, Combustible combustible)
        {
            string sql = "INSERT INTO Combustible (Anio, Mes, Valor) VALUES (" +
                    combustible.anio + ", " +
                    combustible.mes + ", " +
                    combustible.valor.ToString().Replace(",", ".") + ")";

            OleDbCommand oleDbCommand = new OleDbCommand(sql, conn);

            return oleDbCommand.ExecuteNonQuery() != 1;
        }

        public static bool editarCombustible(OleDbConnection conn, Combustible combustible)
        {
            string sql = "UPDATE Combustible SET Valor = " + combustible.valor.ToString().Replace(",", ".") +
              " WHERE Id = " + combustible.ID;

            OleDbDataAdapter oledbAdapter = new OleDbDataAdapter();

            oledbAdapter.UpdateCommand = conn.CreateCommand();
            oledbAdapter.UpdateCommand.CommandText = sql;

            return oledbAdapter.UpdateCommand.ExecuteNonQuery() != 1;
        }
    }
}

