﻿using System;

namespace DDJJ_Regalías_Mineras.Clases
{
    public class Fila
    {
        public int idFila { get; set; }
        public int idMineral { get; set; }
        public int numGuia { get; set; }
        public String finalidad { get; set; }
        public float m3 { get; set; }
        public float toneladas { get; set; }
        public float reg { get; set; }
        public float regEnTon { get; set; }
        public float precioUnitario { get; set; }
        public float totalReg { get; set; }
        public int anio { get; set; }
        public int mes { get; set; }

        public Fila()
        {
            idFila = 0;
            idMineral = 0;
            numGuia = 0;
            finalidad = "";
            m3 = 0;
            toneladas = 0;
            reg = 0;
            regEnTon = 0;
            precioUnitario = 0;
            totalReg = 0;
            anio = 0;
            mes = 0;
        }
    }
}
