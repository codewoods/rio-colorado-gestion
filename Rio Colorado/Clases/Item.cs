﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDJJ_Regalías_Mineras.Clases
{
    public class Item
    {
        public int value { get; set; }
        public String name { get; set; }

        public Item(String name, int value)
        {
            this.value = value;
            this.name = name;
        }

        public override string ToString()
        {
            return name;
        }
    }
}
