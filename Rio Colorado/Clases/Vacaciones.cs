﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gestion_Minera.Clases
{
    class Vacaciones
    {
        public int idVacaciones { get; set; }
        public int dias { get; set; }
        public int anio { get; set; }
    }
}
