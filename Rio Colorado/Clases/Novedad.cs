﻿using System;

namespace Gestion_Minera.Clases
{
    public class Novedad
    {
        public int ID { get; set; }
        public String novedad { get; set; }
        public bool fecha { get; set; }
        public int valores { get; set; }
        public String medida { get; set; }

        public Novedad (int ID, String novedad, bool fecha, int valores, String medida)
        {
            this.ID = ID;
            this.novedad = novedad;
            this.fecha = fecha;
            this.valores = valores;
            this.medida = medida;
        }

        public Novedad()
        { 
        }

        public Novedad(int ID)
        {
            this.ID = ID;
        }

        override
        public String ToString()
        {
            if (ID > 0) {
                return novedad;
            } else
            {
                return "TODAS";
            }
        }
    }
}
