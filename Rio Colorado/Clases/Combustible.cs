﻿using System;

namespace Gestion_Minera.Clases
{
    public class Combustible
    {
        public int ID { get; set; }
        public int anio { get; set; }
        public int mes { get; set; }
        public float valor { get; set; }

        public Combustible()
        {
            valor = 0;
        }

        public Combustible (int ID, int mes, String valor)
        {
            this.ID = ID;
            this.mes = mes;
            this.valor = float.Parse(valor.Replace(".", ","));
        }
    }
}
