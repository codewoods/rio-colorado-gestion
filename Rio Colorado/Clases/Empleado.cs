﻿using System;

namespace Gestion_Minera.Clases
{
    public class Empleado
    {
        public int ID { get; set; }
        public String Apellido { get; set; }
        public String Nombre { get; set; }
        public String CUIL { get; set; }
        public DateTime FechaIngreso { get; set; }
        public String Categoria { get; set; }
        public Sindicato Sindicato { get; set; }

        public Empleado(int ID, String Apellido, String Nombre, String CUIL, DateTime FechaIngreso, String Categoria, Sindicato Sindicato)
        {
            this.ID = ID;
            this.Apellido = Apellido;
            this.Nombre = Nombre;
            this.CUIL = CUIL;
            this.FechaIngreso = FechaIngreso;
            this.Categoria = Categoria;
            this.Sindicato = Sindicato;
        }

        public Empleado()
        {
        }

        public Empleado(int ID)
        {
            this.ID = ID;
        }

        override
        public String ToString()
        {
            if (ID > 0)
            {
                return Apellido + ", " + Nombre;
            }
            else
            {
                return "TODOS";
            }
        }
    }
}
