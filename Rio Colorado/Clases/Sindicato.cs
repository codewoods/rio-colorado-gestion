﻿using System;

namespace Gestion_Minera.Clases
{
    public class Sindicato
    {
        public int ID { get; set; }
        public String Nombre { get; set; }

        public Sindicato(int ID, String Nombre)
        {
            this.ID = ID;
            this.Nombre = Nombre;
        }

        public Sindicato(){
        }

        override
        public String ToString()
        {
            return Nombre;
        }
    }
}
