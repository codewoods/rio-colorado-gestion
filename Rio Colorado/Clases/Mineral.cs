﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDJJ_Regalías_Mineras.Clases
{
    public class Mineral
    {
        public int ID { get; set; }
        public String nombre { get; set; }
        public String yacimiento { get; set; }
        public float precioUnitario { get; set; }
        public String finalidad { get; set; }
        public float regalias { get; set; }
        public float vencimiento { get; set; }
        public float tonAM3 { get; set; }
        public float coeficiente { get; set; }

        public Mineral(int ID, String nombre, String yacimiento, float precioUnitario)
        {
            this.ID = ID;
            this.nombre = nombre;
            this.yacimiento = yacimiento;
            this.precioUnitario = precioUnitario;
        }

        public Mineral()
        {

        }

        public override string ToString()
        {
            return nombre.ToUpper();
        }

    }
}
