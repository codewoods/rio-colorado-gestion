﻿namespace DDJJ_Regalías_Mineras.Ventanas
{
    partial class VentanaPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VentanaPrincipal));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.inicioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mineralesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agregarEditarMineralesYValoresFijosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DDJJToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sueldosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agregarYEditarEmpleadosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agregarYEditarSindicatosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vacacionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agregarYEditarNovedadesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alquileresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agregarYEditarInmueblesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agregarYEditarInquilinosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agregarYEditarContratosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cargarServiciosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generarReciboToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resúmenMineríaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Transparent;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.inicioToolStripMenuItem,
            this.mineralesToolStripMenuItem,
            this.generarToolStripMenuItem,
            this.sueldosToolStripMenuItem,
            this.alquileresToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(953, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // inicioToolStripMenuItem
            // 
            this.inicioToolStripMenuItem.Name = "inicioToolStripMenuItem";
            this.inicioToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.inicioToolStripMenuItem.Text = "Inicio";
            // 
            // mineralesToolStripMenuItem
            // 
            this.mineralesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.agregarEditarMineralesYValoresFijosToolStripMenuItem});
            this.mineralesToolStripMenuItem.Name = "mineralesToolStripMenuItem";
            this.mineralesToolStripMenuItem.Size = new System.Drawing.Size(70, 20);
            this.mineralesToolStripMenuItem.Text = "Minerales";
            // 
            // agregarEditarMineralesYValoresFijosToolStripMenuItem
            // 
            this.agregarEditarMineralesYValoresFijosToolStripMenuItem.Name = "agregarEditarMineralesYValoresFijosToolStripMenuItem";
            this.agregarEditarMineralesYValoresFijosToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.agregarEditarMineralesYValoresFijosToolStripMenuItem.Text = "Agregar y editar minerales";
            this.agregarEditarMineralesYValoresFijosToolStripMenuItem.Click += new System.EventHandler(this.agregarEditarMineralesYValoresFijosToolStripMenuItem_Click);
            // 
            // generarToolStripMenuItem
            // 
            this.generarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.DDJJToolStripMenuItem,
            this.resúmenMineríaToolStripMenuItem});
            this.generarToolStripMenuItem.Name = "generarToolStripMenuItem";
            this.generarToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.generarToolStripMenuItem.Text = "Generar";
            // 
            // DDJJToolStripMenuItem
            // 
            this.DDJJToolStripMenuItem.Name = "DDJJToolStripMenuItem";
            this.DDJJToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.DDJJToolStripMenuItem.Text = "D.D. J.J. regalías mineras";
            this.DDJJToolStripMenuItem.Click += new System.EventHandler(this.DDJJToolStripMenuItem_Click);
            // 
            // sueldosToolStripMenuItem
            // 
            this.sueldosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.agregarYEditarEmpleadosToolStripMenuItem,
            this.agregarYEditarSindicatosToolStripMenuItem,
            this.vacacionesToolStripMenuItem,
            this.agregarYEditarNovedadesToolStripMenuItem});
            this.sueldosToolStripMenuItem.Name = "sueldosToolStripMenuItem";
            this.sueldosToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.sueldosToolStripMenuItem.Text = "Sueldos";
            this.sueldosToolStripMenuItem.Click += new System.EventHandler(this.sueldosToolStripMenuItem_Click);
            // 
            // agregarYEditarEmpleadosToolStripMenuItem
            // 
            this.agregarYEditarEmpleadosToolStripMenuItem.Name = "agregarYEditarEmpleadosToolStripMenuItem";
            this.agregarYEditarEmpleadosToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.agregarYEditarEmpleadosToolStripMenuItem.Text = "Agregar y editar Empleados";
            this.agregarYEditarEmpleadosToolStripMenuItem.Click += new System.EventHandler(this.agregarYEditarEmpleadosToolStripMenuItem_Click);
            // 
            // agregarYEditarSindicatosToolStripMenuItem
            // 
            this.agregarYEditarSindicatosToolStripMenuItem.Name = "agregarYEditarSindicatosToolStripMenuItem";
            this.agregarYEditarSindicatosToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.agregarYEditarSindicatosToolStripMenuItem.Text = "Agregar y editar Sindicatos";
            this.agregarYEditarSindicatosToolStripMenuItem.Click += new System.EventHandler(this.agregarYEditarSindicatosToolStripMenuItem_Click);
            // 
            // vacacionesToolStripMenuItem
            // 
            this.vacacionesToolStripMenuItem.Name = "vacacionesToolStripMenuItem";
            this.vacacionesToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.vacacionesToolStripMenuItem.Text = "Vacaciones";
            this.vacacionesToolStripMenuItem.Click += new System.EventHandler(this.vacacionesToolStripMenuItem_Click);
            // 
            // agregarYEditarNovedadesToolStripMenuItem
            // 
            this.agregarYEditarNovedadesToolStripMenuItem.Name = "agregarYEditarNovedadesToolStripMenuItem";
            this.agregarYEditarNovedadesToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.agregarYEditarNovedadesToolStripMenuItem.Text = "Novedades";
            this.agregarYEditarNovedadesToolStripMenuItem.Click += new System.EventHandler(this.agregarYEditarNovedadesToolStripMenuItem_Click);
            // 
            // alquileresToolStripMenuItem
            // 
            this.alquileresToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.agregarYEditarInmueblesToolStripMenuItem,
            this.agregarYEditarInquilinosToolStripMenuItem,
            this.agregarYEditarContratosToolStripMenuItem,
            this.cargarServiciosToolStripMenuItem,
            this.generarReciboToolStripMenuItem});
            this.alquileresToolStripMenuItem.Name = "alquileresToolStripMenuItem";
            this.alquileresToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.alquileresToolStripMenuItem.Text = "Alquileres";
            // 
            // agregarYEditarInmueblesToolStripMenuItem
            // 
            this.agregarYEditarInmueblesToolStripMenuItem.Name = "agregarYEditarInmueblesToolStripMenuItem";
            this.agregarYEditarInmueblesToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.agregarYEditarInmueblesToolStripMenuItem.Text = "Agregar y editar Inmuebles";
            this.agregarYEditarInmueblesToolStripMenuItem.Click += new System.EventHandler(this.agregarYEditarInmueblesToolStripMenuItem_Click);
            // 
            // agregarYEditarInquilinosToolStripMenuItem
            // 
            this.agregarYEditarInquilinosToolStripMenuItem.Name = "agregarYEditarInquilinosToolStripMenuItem";
            this.agregarYEditarInquilinosToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.agregarYEditarInquilinosToolStripMenuItem.Text = "Agregar y editar Inquilinos";
            this.agregarYEditarInquilinosToolStripMenuItem.Click += new System.EventHandler(this.agregarYEditarInquilinosToolStripMenuItem_Click);
            // 
            // agregarYEditarContratosToolStripMenuItem
            // 
            this.agregarYEditarContratosToolStripMenuItem.Name = "agregarYEditarContratosToolStripMenuItem";
            this.agregarYEditarContratosToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.agregarYEditarContratosToolStripMenuItem.Text = "Agregar y editar Contratos";
            this.agregarYEditarContratosToolStripMenuItem.Click += new System.EventHandler(this.agregarYEditarContratosToolStripMenuItem_Click);
            // 
            // cargarServiciosToolStripMenuItem
            // 
            this.cargarServiciosToolStripMenuItem.Name = "cargarServiciosToolStripMenuItem";
            this.cargarServiciosToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.cargarServiciosToolStripMenuItem.Text = "Cargar servicios";
            this.cargarServiciosToolStripMenuItem.Click += new System.EventHandler(this.cargarServiciosToolStripMenuItem_Click);
            // 
            // generarReciboToolStripMenuItem
            // 
            this.generarReciboToolStripMenuItem.Name = "generarReciboToolStripMenuItem";
            this.generarReciboToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.generarReciboToolStripMenuItem.Text = "Generar recibo";
            this.generarReciboToolStripMenuItem.Click += new System.EventHandler(this.generarReciboToolStripMenuItem_Click);
            // 
            // resúmenMineríaToolStripMenuItem
            // 
            this.resúmenMineríaToolStripMenuItem.Name = "resúmenMineríaToolStripMenuItem";
            this.resúmenMineríaToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.resúmenMineríaToolStripMenuItem.Text = "Resúmen minería";
            this.resúmenMineríaToolStripMenuItem.Click += new System.EventHandler(this.resúmenMineríaToolStripMenuItem_Click);
            // 
            // VentanaPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(953, 485);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "VentanaPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "VentanaPrincipal";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem inicioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mineralesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agregarEditarMineralesYValoresFijosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem generarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem DDJJToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sueldosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agregarYEditarEmpleadosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agregarYEditarSindicatosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agregarYEditarNovedadesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alquileresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agregarYEditarInmueblesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agregarYEditarInquilinosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cargarServiciosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem generarReciboToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agregarYEditarContratosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vacacionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resúmenMineríaToolStripMenuItem;
    }
}