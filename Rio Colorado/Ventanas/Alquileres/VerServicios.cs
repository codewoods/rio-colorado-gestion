﻿using Gestion_Minera.Ventanas.Alquileres.Clases;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Windows.Forms;

namespace Gestion_Minera.Ventanas.Alquileres
{
    public partial class VerServicios : Form
    {
        OleDbConnection conn;
        Inmueble inmueble;

        public VerServicios(OleDbConnection conn, Inmueble inmueble)
        {
            InitializeComponent();

            this.conn = conn;
            this.inmueble = inmueble;

            numAnio.Value = DateTime.Today.Year;

            tbInmueble.Text = inmueble.ToString();
        }

        public void actualizarTabla(List<InmuebleImpuesto> listaImpuestos)
        {
            dgTabla.Rows.Clear();

            string[] row1 = new string[] { "LUZ", "", "", "", "", "", "", "", "", "", "", "", "" };
            string[] row2 = new string[] { "GAS", "", "", "", "", "", "", "", "", "", "", "", "" };
            string[] row3 = new string[] { "IMP. INMOBILIARIO", "", "", "", "", "", "", "", "", "", "", "", "" };
            string[] row4 = new string[] { "IMP. MUNICIPAL", "", "", "", "", "", "", "", "", "", "", "", "" };

            foreach (InmuebleImpuesto impuesto in listaImpuestos)
            {
                switch (impuesto.impuesto.id)
                {
                    case 1:
                        {
                            row1[impuesto.mes] = impuesto.valor;
                            break;
                        }
                    case 2:
                        {
                            row2[impuesto.mes] = impuesto.valor;
                            break;
                        }
                    case 3:
                        {
                            row3[impuesto.mes] = impuesto.valor;
                            break;
                        }
                    case 4:
                        {
                            row4[impuesto.mes] = impuesto.valor;
                            break;
                        }
                }
            }

            dgTabla.Rows.Add(row1);
            dgTabla.Rows.Add(row2);
            dgTabla.Rows.Add(row3);
            dgTabla.Rows.Add(row4);
        }

        private void numAnio_ValueChanged(object sender, EventArgs e)
        {
            actualizarTabla(UtilAlquileres.getImpuestosInmuebleAnual(conn, inmueble.idInmueble, int.Parse(numAnio.Value.ToString())));
        }
    }
}
