﻿using Gestion_Minera.Ventanas.Alquileres.Clases;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Windows.Forms;

namespace Gestion_Minera.Ventanas.Alquileres
{
    public partial class ABMInquilinosContratos : Form
    {
        OleDbConnection conn;

        List<Inmueble> listaInmuebles;
        List<Inquilino> listaInquilinos;

        public ABMInquilinosContratos(OleDbConnection conn)
        {
            InitializeComponent();

            this.conn = conn;

            listaInmuebles = UtilAlquileres.getInmuebles(conn);

            actualizarCombosInquilinos();

            editarEnabled(false);
        }

        private void butAgregarInquilino_Click(object sender, EventArgs e)
        {
            Inquilino inquilino = new Inquilino();

            inquilino.Apellido = tbApellido.Text;
            inquilino.Nombre = tbNombre.Text;

            String msg = "¿Desea agregar el siguiente Inquilino?\n\n" +
                "Apellido:\t\t" + inquilino.Apellido + "\n" +
                "Nombre:\t\t" + inquilino.Nombre + "\n";

            DialogResult result = MessageBox.Show(msg, "Agregar Inquilino",
                MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                string sql = "INSERT INTO Inquilino (Apellido, Nombre) VALUES ('" +
                    inquilino.Apellido + "', " +
                    "'" + inquilino.Nombre + "')";

                OleDbCommand oleDbCommand = new OleDbCommand(sql, conn);

                if (oleDbCommand.ExecuteNonQuery() != 1)
                {
                    MessageBox.Show("No se ha podido agregar el Inquilino", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    tbApellido.Text = "";
                    tbApellido.Focus();

                    tbNombre.Text = "";

                    actualizarCombosInquilinos();
                }
            }
        }

        public void actualizarCombosInquilinos()
        {
            listaInquilinos = UtilAlquileres.getInquilinos(conn);

            cbInquilino.Items.Clear();
            cbVerInquilino.Items.Clear();

            foreach (Inquilino inquilino in listaInquilinos)
            {
                cbInquilino.Items.Add(inquilino);
                cbVerInquilino.Items.Add(inquilino);
            }

            cbInquilino.SelectedIndex = cbVerInquilino.SelectedIndex = -1;
        }

        public void editarEnabled(bool value)
        {
            tbApellidoE.Enabled = tbNombreE.Enabled = buteEditar.Enabled = butCancelar.Enabled = value;
            if (!value) tbApellidoE.Text = tbNombreE.Text = "";
        }

        private void tbApellido_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter || (int)e.KeyChar == (int)Keys.Tab)
            {
                e.Handled = true;
                tbNombre.Focus();
            }
            else
            {
                e.KeyChar = Char.ToUpper(e.KeyChar);
            }
        }

        private void tbNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter || (int)e.KeyChar == (int)Keys.Tab)
            {
                e.Handled = true;
                butAgregarInquilino.Focus();
            }
            else
            {
                e.KeyChar = Char.ToUpper(e.KeyChar);
            }
        }

        private void cbInquilino_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbInquilino.SelectedIndex > -1)
            {
                tbApellidoE.Text = ((Inquilino)cbInquilino.SelectedItem).Apellido;
                tbNombreE.Text = ((Inquilino)cbInquilino.SelectedItem).Nombre;
                editarEnabled(true);
                tbApellidoE.Focus();
            }
        }

        private void butCancelar_Click(object sender, EventArgs e)
        {
            cbInquilino.SelectedIndex = -1;
            editarEnabled(false);
        }

        private void buteEditar_Click(object sender, EventArgs e)
        {
            Inquilino inquilino = (Inquilino)cbInquilino.SelectedItem;

            string msg = "¿Realmente desea confirmar estos cambios?" +
            "\n\n" +
            "Apellido\t\t " + inquilino.Apellido + " por " + tbApellidoE.Text.ToUpper() +
            "\nNombre\t\t " + inquilino.Nombre + " por " + tbNombreE.Text.ToUpper();

            DialogResult result = MessageBox.Show(msg, "Editar Inquilino",
MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                string sql = "UPDATE Inquilino SET Apellido = '" + tbApellidoE.Text.ToUpper() + "'" +
               ", Nombre = '" + tbNombreE.Text.ToUpper() + "'" +
               " WHERE Id = " + inquilino.id;

                OleDbDataAdapter oledbAdapter = new OleDbDataAdapter();

                oledbAdapter.UpdateCommand = conn.CreateCommand();
                oledbAdapter.UpdateCommand.CommandText = sql;

                int i = oledbAdapter.UpdateCommand.ExecuteNonQuery();

                if (i != 1)
                {
                    MessageBox.Show("Los valores no se han podido modificar.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    actualizarCombosInquilinos();
                    editarEnabled(false);
                }
            }
        }

        private void tbApellidoE_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter || (int)e.KeyChar == (int)Keys.Tab)
            {
                e.Handled = true;
                tbNombreE.Focus();
            }
            else
            {
                e.KeyChar = Char.ToUpper(e.KeyChar);
            }
        }

        private void tbNombreE_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter || (int)e.KeyChar == (int)Keys.Tab)
            {
                e.Handled = true;
                buteEditar.Focus();
            }
            else
            {
                e.KeyChar = Char.ToUpper(e.KeyChar);
            }
        }
        
        private void tbComnetarios_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter || (int)e.KeyChar == (int)Keys.Tab)
            {
                e.Handled = true;
                butAgregarInquilino.Focus();
            }
            else
            {
                e.KeyChar = Char.ToUpper(e.KeyChar);
            }
        }
        
        private void buetVerInquilino_Click(object sender, EventArgs e)
        {
            Contrato contrato = UtilAlquileres.getContrato(conn, 0, ((Inquilino)cbVerInquilino.SelectedItem).id);

            String msg;

            if (contrato.id > 0)
            {
                msg = "Inmueble:\t" + contrato.inmueble +
                    "\nInquilino:\t" + contrato.inquilino +
                    "\nFecha inicio:\t" + contrato.fechaInicio.ToShortDateString() +
                    "\nFecha fin:\t" + contrato.fechaFin.ToShortDateString() +
                    "\nValor:\t\t$" + contrato.valor +
                    "\nComentarios:\t" + contrato.comentarios;
            }
            else
            {
                msg = "NO hay ningun contrato vigente de para el Inquilino seleccionado.";
            }

            MessageBox.Show(msg, "DETALLES DEL CONTRATO");
        }

        private void cbVerInquilino_SelectedIndexChanged(object sender, EventArgs e)
        {
            butVerInquilino.Enabled = cbVerInquilino.SelectedIndex > -1;
        }
    }
}
