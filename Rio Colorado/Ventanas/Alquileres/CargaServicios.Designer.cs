﻿namespace Gestion_Minera.Ventanas.Sueldos
{
    partial class CargaServicios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CargaServicios));
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.butCargarServicio = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.butVerServicios = new System.Windows.Forms.Button();
            this.cbInmueble = new System.Windows.Forms.ComboBox();
            this.panel = new System.Windows.Forms.GroupBox();
            this.butBorrarMun = new System.Windows.Forms.Button();
            this.butBorrarInm = new System.Windows.Forms.Button();
            this.butBorrarGas = new System.Windows.Forms.Button();
            this.butBorrarLuz = new System.Windows.Forms.Button();
            this.butCancelar = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.numAnioImpMun = new System.Windows.Forms.NumericUpDown();
            this.numAnioImpMob = new System.Windows.Forms.NumericUpDown();
            this.numAnioGas = new System.Windows.Forms.NumericUpDown();
            this.numAnioLuz = new System.Windows.Forms.NumericUpDown();
            this.cbImpMun = new System.Windows.Forms.ComboBox();
            this.cbImpMob = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbLuz = new System.Windows.Forms.TextBox();
            this.tbGas = new System.Windows.Forms.TextBox();
            this.tbInm = new System.Windows.Forms.TextBox();
            this.tbMun = new System.Windows.Forms.TextBox();
            this.cbLuz = new System.Windows.Forms.ComboBox();
            this.cbGas = new System.Windows.Forms.ComboBox();
            this.butAgregarServicio = new System.Windows.Forms.Button();
            this.groupBox4.SuspendLayout();
            this.panel.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numAnioImpMun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numAnioImpMob)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numAnioGas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numAnioLuz)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.White;
            this.groupBox4.Controls.Add(this.butCargarServicio);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.butVerServicios);
            this.groupBox4.Controls.Add(this.cbInmueble);
            this.groupBox4.Location = new System.Drawing.Point(12, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(560, 89);
            this.groupBox4.TabIndex = 55;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "VER CONTRATO POR INQUILINO";
            // 
            // butCargarServicio
            // 
            this.butCargarServicio.Location = new System.Drawing.Point(192, 54);
            this.butCargarServicio.Name = "butCargarServicio";
            this.butCargarServicio.Size = new System.Drawing.Size(174, 23);
            this.butCargarServicio.TabIndex = 84;
            this.butCargarServicio.Text = "CARGAR SERVICIO";
            this.butCargarServicio.UseVisualStyleBackColor = true;
            this.butCargarServicio.Click += new System.EventHandler(this.butCargarServicio_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(15, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 15);
            this.label1.TabIndex = 80;
            this.label1.Text = "Inmueble";
            // 
            // butVerServicios
            // 
            this.butVerServicios.Location = new System.Drawing.Point(372, 54);
            this.butVerServicios.Name = "butVerServicios";
            this.butVerServicios.Size = new System.Drawing.Size(177, 23);
            this.butVerServicios.TabIndex = 77;
            this.butVerServicios.Text = "VER SERVICIOS CARGADOS";
            this.butVerServicios.UseVisualStyleBackColor = true;
            this.butVerServicios.Click += new System.EventHandler(this.butVerServicios_Click);
            // 
            // cbInmueble
            // 
            this.cbInmueble.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbInmueble.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbInmueble.Location = new System.Drawing.Point(77, 25);
            this.cbInmueble.Name = "cbInmueble";
            this.cbInmueble.Size = new System.Drawing.Size(472, 23);
            this.cbInmueble.TabIndex = 76;
            this.cbInmueble.SelectedIndexChanged += new System.EventHandler(this.cbInmueble_SelectedIndexChanged);
            // 
            // panel
            // 
            this.panel.BackColor = System.Drawing.Color.White;
            this.panel.Controls.Add(this.butBorrarMun);
            this.panel.Controls.Add(this.butBorrarInm);
            this.panel.Controls.Add(this.butBorrarGas);
            this.panel.Controls.Add(this.butBorrarLuz);
            this.panel.Controls.Add(this.butCancelar);
            this.panel.Controls.Add(this.tableLayoutPanel1);
            this.panel.Controls.Add(this.butAgregarServicio);
            this.panel.Location = new System.Drawing.Point(12, 107);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(560, 198);
            this.panel.TabIndex = 86;
            this.panel.TabStop = false;
            this.panel.Text = "VER CONTRATO POR INQUILINO";
            // 
            // butBorrarMun
            // 
            this.butBorrarMun.Location = new System.Drawing.Point(486, 129);
            this.butBorrarMun.Name = "butBorrarMun";
            this.butBorrarMun.Size = new System.Drawing.Size(63, 23);
            this.butBorrarMun.TabIndex = 97;
            this.butBorrarMun.Text = "BORRAR";
            this.butBorrarMun.UseVisualStyleBackColor = true;
            this.butBorrarMun.Click += new System.EventHandler(this.butBorrarMun_Click);
            // 
            // butBorrarInm
            // 
            this.butBorrarInm.Location = new System.Drawing.Point(486, 95);
            this.butBorrarInm.Name = "butBorrarInm";
            this.butBorrarInm.Size = new System.Drawing.Size(63, 23);
            this.butBorrarInm.TabIndex = 96;
            this.butBorrarInm.Text = "BORRAR";
            this.butBorrarInm.UseVisualStyleBackColor = true;
            this.butBorrarInm.Click += new System.EventHandler(this.butBorrarInm_Click);
            // 
            // butBorrarGas
            // 
            this.butBorrarGas.Location = new System.Drawing.Point(486, 62);
            this.butBorrarGas.Name = "butBorrarGas";
            this.butBorrarGas.Size = new System.Drawing.Size(63, 23);
            this.butBorrarGas.TabIndex = 95;
            this.butBorrarGas.Text = "BORRAR";
            this.butBorrarGas.UseVisualStyleBackColor = true;
            this.butBorrarGas.Click += new System.EventHandler(this.butBorrarGas_Click);
            // 
            // butBorrarLuz
            // 
            this.butBorrarLuz.Location = new System.Drawing.Point(486, 29);
            this.butBorrarLuz.Name = "butBorrarLuz";
            this.butBorrarLuz.Size = new System.Drawing.Size(63, 23);
            this.butBorrarLuz.TabIndex = 94;
            this.butBorrarLuz.Text = "BORRAR";
            this.butBorrarLuz.UseVisualStyleBackColor = true;
            this.butBorrarLuz.Click += new System.EventHandler(this.butBorrarLuz_Click);
            // 
            // butCancelar
            // 
            this.butCancelar.Location = new System.Drawing.Point(296, 164);
            this.butCancelar.Name = "butCancelar";
            this.butCancelar.Size = new System.Drawing.Size(89, 23);
            this.butCancelar.TabIndex = 93;
            this.butCancelar.Text = " CANCELAR";
            this.butCancelar.UseVisualStyleBackColor = true;
            this.butCancelar.Click += new System.EventHandler(this.butCancelar_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel1.Controls.Add(this.numAnioImpMun, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.numAnioImpMob, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.numAnioGas, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.numAnioLuz, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.cbImpMun, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.cbImpMob, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tbLuz, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tbGas, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.tbInm, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.tbMun, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.cbLuz, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.cbGas, 2, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(18, 24);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(462, 134);
            this.tableLayoutPanel1.TabIndex = 86;
            // 
            // numAnioImpMun
            // 
            this.numAnioImpMun.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.numAnioImpMun.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numAnioImpMun.Location = new System.Drawing.Point(394, 106);
            this.numAnioImpMun.Maximum = new decimal(new int[] {
            2050,
            0,
            0,
            0});
            this.numAnioImpMun.Minimum = new decimal(new int[] {
            2010,
            0,
            0,
            0});
            this.numAnioImpMun.Name = "numAnioImpMun";
            this.numAnioImpMun.ReadOnly = true;
            this.numAnioImpMun.Size = new System.Drawing.Size(65, 21);
            this.numAnioImpMun.TabIndex = 928;
            this.numAnioImpMun.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numAnioImpMun.Value = new decimal(new int[] {
            2010,
            0,
            0,
            0});
            // 
            // numAnioImpMob
            // 
            this.numAnioImpMob.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.numAnioImpMob.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numAnioImpMob.Location = new System.Drawing.Point(394, 72);
            this.numAnioImpMob.Maximum = new decimal(new int[] {
            2050,
            0,
            0,
            0});
            this.numAnioImpMob.Minimum = new decimal(new int[] {
            2010,
            0,
            0,
            0});
            this.numAnioImpMob.Name = "numAnioImpMob";
            this.numAnioImpMob.ReadOnly = true;
            this.numAnioImpMob.Size = new System.Drawing.Size(65, 21);
            this.numAnioImpMob.TabIndex = 927;
            this.numAnioImpMob.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numAnioImpMob.Value = new decimal(new int[] {
            2010,
            0,
            0,
            0});
            // 
            // numAnioGas
            // 
            this.numAnioGas.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.numAnioGas.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numAnioGas.Location = new System.Drawing.Point(394, 39);
            this.numAnioGas.Maximum = new decimal(new int[] {
            2050,
            0,
            0,
            0});
            this.numAnioGas.Minimum = new decimal(new int[] {
            2010,
            0,
            0,
            0});
            this.numAnioGas.Name = "numAnioGas";
            this.numAnioGas.ReadOnly = true;
            this.numAnioGas.Size = new System.Drawing.Size(65, 21);
            this.numAnioGas.TabIndex = 926;
            this.numAnioGas.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numAnioGas.Value = new decimal(new int[] {
            2010,
            0,
            0,
            0});
            // 
            // numAnioLuz
            // 
            this.numAnioLuz.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.numAnioLuz.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numAnioLuz.Location = new System.Drawing.Point(394, 6);
            this.numAnioLuz.Maximum = new decimal(new int[] {
            2050,
            0,
            0,
            0});
            this.numAnioLuz.Minimum = new decimal(new int[] {
            2010,
            0,
            0,
            0});
            this.numAnioLuz.Name = "numAnioLuz";
            this.numAnioLuz.ReadOnly = true;
            this.numAnioLuz.Size = new System.Drawing.Size(65, 21);
            this.numAnioLuz.TabIndex = 98;
            this.numAnioLuz.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numAnioLuz.Value = new decimal(new int[] {
            2010,
            0,
            0,
            0});
            // 
            // cbImpMun
            // 
            this.cbImpMun.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbImpMun.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbImpMun.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbImpMun.Location = new System.Drawing.Point(233, 105);
            this.cbImpMun.Name = "cbImpMun";
            this.cbImpMun.Size = new System.Drawing.Size(155, 23);
            this.cbImpMun.TabIndex = 925;
            this.cbImpMun.TabStop = false;
            // 
            // cbImpMob
            // 
            this.cbImpMob.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbImpMob.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbImpMob.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbImpMob.Location = new System.Drawing.Point(233, 71);
            this.cbImpMob.Name = "cbImpMob";
            this.cbImpMob.Size = new System.Drawing.Size(155, 23);
            this.cbImpMob.TabIndex = 924;
            this.cbImpMob.TabStop = false;
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(19, 109);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(116, 15);
            this.label7.TabIndex = 87;
            this.label7.Text = "IMP. MUNICIPALES";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(19, 75);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(116, 15);
            this.label4.TabIndex = 86;
            this.label4.Text = "IMP. INMOBILIARIO";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(105, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 15);
            this.label2.TabIndex = 84;
            this.label2.Text = "LUZ";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(104, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 15);
            this.label3.TabIndex = 85;
            this.label3.Text = "GAS";
            // 
            // tbLuz
            // 
            this.tbLuz.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbLuz.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbLuz.Location = new System.Drawing.Point(141, 6);
            this.tbLuz.Name = "tbLuz";
            this.tbLuz.Size = new System.Drawing.Size(86, 21);
            this.tbLuz.TabIndex = 88;
            // 
            // tbGas
            // 
            this.tbGas.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbGas.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbGas.Location = new System.Drawing.Point(141, 39);
            this.tbGas.Name = "tbGas";
            this.tbGas.Size = new System.Drawing.Size(86, 21);
            this.tbGas.TabIndex = 89;
            // 
            // tbInm
            // 
            this.tbInm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbInm.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbInm.Location = new System.Drawing.Point(141, 72);
            this.tbInm.Name = "tbInm";
            this.tbInm.Size = new System.Drawing.Size(86, 21);
            this.tbInm.TabIndex = 90;
            // 
            // tbMun
            // 
            this.tbMun.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbMun.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMun.Location = new System.Drawing.Point(141, 106);
            this.tbMun.Name = "tbMun";
            this.tbMun.Size = new System.Drawing.Size(86, 21);
            this.tbMun.TabIndex = 91;
            // 
            // cbLuz
            // 
            this.cbLuz.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbLuz.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbLuz.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbLuz.Location = new System.Drawing.Point(233, 5);
            this.cbLuz.Name = "cbLuz";
            this.cbLuz.Size = new System.Drawing.Size(155, 23);
            this.cbLuz.TabIndex = 922;
            this.cbLuz.TabStop = false;
            // 
            // cbGas
            // 
            this.cbGas.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbGas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbGas.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbGas.Location = new System.Drawing.Point(233, 38);
            this.cbGas.Name = "cbGas";
            this.cbGas.Size = new System.Drawing.Size(155, 23);
            this.cbGas.TabIndex = 93;
            this.cbGas.TabStop = false;
            // 
            // butAgregarServicio
            // 
            this.butAgregarServicio.Location = new System.Drawing.Point(391, 164);
            this.butAgregarServicio.Name = "butAgregarServicio";
            this.butAgregarServicio.Size = new System.Drawing.Size(89, 23);
            this.butAgregarServicio.TabIndex = 92;
            this.butAgregarServicio.Text = "CARGAR";
            this.butAgregarServicio.UseVisualStyleBackColor = true;
            this.butAgregarServicio.Click += new System.EventHandler(this.butAgregarServicio_Click);
            // 
            // CargaServicios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(581, 311);
            this.Controls.Add(this.panel);
            this.Controls.Add(this.groupBox4);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CargaServicios";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Carga de Servicios";
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.panel.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numAnioImpMun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numAnioImpMob)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numAnioGas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numAnioLuz)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button butVerServicios;
        private System.Windows.Forms.ComboBox cbInmueble;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button butCargarServicio;
        private System.Windows.Forms.GroupBox panel;
        private System.Windows.Forms.Button butAgregarServicio;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbLuz;
        private System.Windows.Forms.TextBox tbGas;
        private System.Windows.Forms.TextBox tbInm;
        private System.Windows.Forms.TextBox tbMun;
        private System.Windows.Forms.ComboBox cbLuz;
        private System.Windows.Forms.ComboBox cbGas;
        private System.Windows.Forms.Button butCancelar;
        private System.Windows.Forms.ComboBox cbImpMun;
        private System.Windows.Forms.ComboBox cbImpMob;
        private System.Windows.Forms.Button butBorrarMun;
        private System.Windows.Forms.Button butBorrarInm;
        private System.Windows.Forms.Button butBorrarGas;
        private System.Windows.Forms.Button butBorrarLuz;
        private System.Windows.Forms.NumericUpDown numAnioLuz;
        private System.Windows.Forms.NumericUpDown numAnioImpMun;
        private System.Windows.Forms.NumericUpDown numAnioImpMob;
        private System.Windows.Forms.NumericUpDown numAnioGas;
    }
}