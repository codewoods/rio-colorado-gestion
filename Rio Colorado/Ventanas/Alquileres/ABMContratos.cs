﻿using DDJJ_Regalías_Mineras;
using Gestion_Minera.Ventanas.Alquileres;
using Gestion_Minera.Ventanas.Alquileres.Clases;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Gestion_Minera.Ventanas.Sueldos
{
    public partial class ABMContratos : Form
    {
        OleDbConnection conn;
        List<Inquilino> listaInquilinos;

        Contrato contratoSeleccionado;
        List<Inmueble> listaInmuebles;

        public object HttpServerUtility { get; private set; }

        String path = "";

        public ABMContratos(OleDbConnection conn)
        {
            InitializeComponent();

            this.conn = conn;

            listaInmuebles = UtilAlquileres.getInmuebles(conn);

            actualizarComboBusqueda();

            actualizarCombosInquilinos();

            butNuevoContrato.Enabled =
                butEditarContrato.Enabled =
                butVerContrato.Enabled =
                panContrato.Enabled =
                panPeriodo.Enabled =
                butEliminarContrato.Enabled = false;
        }

        public void actualizarCombosInquilinos()
        {
            listaInquilinos = UtilAlquileres.getInquilinos(conn);

            cbInquilinoContrato.Items.Clear();

            foreach (Inquilino inquilino in listaInquilinos)
            {
                cbInquilinoContrato.Items.Add(inquilino);
            }

            cbInquilinoContrato.SelectedIndex = -1;
        }

        private void cbInmueble_SelectedIndexChanged(object sender, EventArgs e)
        {
            actualizarTablaContratos();
        }

        public void actualizarTablaContratos()
        {
            if (cbInmueble.SelectedIndex > -1)
            {
                butNuevoContrato.Enabled = dgTablaContratos.Enabled = true;

                List<Contrato> listaContratos = UtilAlquileres.getContratos(conn, ((Inmueble)cbInmueble.SelectedItem).idInmueble);

                dgTablaContratos.Rows.Clear();

                foreach (Contrato contrato in listaContratos)
                {
                    dgTablaContratos.Rows.Add(new string[] { contrato.id.ToString(),
                        contrato.inquilino.ToString(),
                        contrato.fechaInicio.ToShortDateString(),
                        contrato.fechaFin.ToShortDateString(), ""});
                }

                if (listaContratos.Count > 0)
                {
                    dgTablaContratos.Rows[listaContratos.Count - 1].Selected = true;
                }

                butEditarContrato.Enabled = butVerContrato.Enabled = butEliminarContrato.Enabled = listaContratos.Count > 0;
            }
            else
            {
                butNuevoContrato.Enabled = butEditarContrato.Enabled = butVerContrato.Enabled = butEliminarContrato.Enabled = false;
            }
        }

        private void butEditarContrato_Click(object sender, EventArgs e)
        {
            int id = seleccionarContrato();

            for (int i = 0; i < cbInquilinoContrato.Items.Count; i++)
            {
                if (((Inquilino)cbInquilinoContrato.Items[i]).id == contratoSeleccionado.inquilino.id)
                {
                    cbInquilinoContrato.SelectedIndex = i;
                    break;
                }
            }

            dtDesde.Value = contratoSeleccionado.fechaInicio;
            dtHasta.Value = contratoSeleccionado.fechaFin;
            tbComnetarios.Text = contratoSeleccionado.comentarios;

            llenarTablaPeriodo(contratoSeleccionado.listaPeriodo);

            tbLinkContrato.Text = path = getPathContrato(id);

            panContrato.Enabled = true;

            dgTablaContratos.Enabled =
                cbInmueble.Enabled =
                butVerContrato.Enabled =
                butNuevoContrato.Enabled =
                butEditarContrato.Enabled = false;

            cbInquilinoContrato.Focus();
        }

        public int seleccionarContrato()
        {
            int idContrato = int.Parse(dgTablaContratos.Rows[dgTablaContratos.SelectedRows[0].Index].Cells["ID"].Value.ToString());

            contratoSeleccionado = UtilAlquileres.getContrato(conn, idContrato);

            llenarTablaPeriodo(contratoSeleccionado.listaPeriodo);

            return idContrato;
        }

        public void llenarTablaPeriodo(List<Periodo> listaPeriodos)
        {
            dgTablaPeriodo.Rows.Clear();

            foreach (Periodo periodo in listaPeriodos)
            {
                dgTablaPeriodo.Rows.Add(new string[] { periodo.id.ToString(),
                        periodo.fechaInicio.ToShortDateString(),
                        periodo.fechaFin.ToShortDateString(),
                        periodo.valor,
                        periodo.comentarios
                });
            }

            if (listaPeriodos.Count > 0)
            {
                dgTablaPeriodo.Rows[listaPeriodos.Count - 1].Selected = true;
                butModificarPeriodo.Enabled = butEliminarPeriodo.Enabled = true;
            }
            else
            {
                butModificarPeriodo.Enabled = butEliminarPeriodo.Enabled = false;
            }
        }



        private void butNuevoContrato_Click(object sender, EventArgs e)
        {
            contratoSeleccionado = UtilAlquileres.getContrato(conn, ((Inmueble)cbInmueble.SelectedItem).idInmueble, 0);

            if (contratoSeleccionado.fechaFin > DateTime.Today)
            {
                String msg = "EXISTE UN CONTRATO VIGENTE PARA EL INMUEBLE SELECCIONADO!\n\n" +
                     "Inmueble:\t" + contratoSeleccionado.inmueble +
                     "\nInquilino:\t" + contratoSeleccionado.inquilino +
                     "\nFecha inicio:\t" + contratoSeleccionado.fechaInicio.ToShortDateString() +
                     "\nFecha fin:\t" + contratoSeleccionado.fechaFin.ToShortDateString() +
                     "\nComentarios:\t" + contratoSeleccionado.comentarios;


                if (contratoSeleccionado.listaPeriodo.Count > 0)
                {
                    msg += "\nÚltimo valor:\t $" + contratoSeleccionado.listaPeriodo.Last().valor;
                }
                else
                {
                    msg += "\nÚltimo valor:\tNO se encontraron períodos.";
                }

                MessageBox.Show(msg, "DETALLES DEL CONTRATO");
            }
            else
            {
                contratoSeleccionado = new Contrato();

                panContrato.Enabled = true;

                cbInmueble.Enabled = butVerContrato.Enabled = butNuevoContrato.Enabled = butEditarContrato.Enabled = false;
            }
        }

        private void tbBuscar_TextChanged(object sender, EventArgs e)
        {

        }

        public void buscar(String val)
        {
            Inquilino inquilino;

            for (int i = 0; i < cbInquilinoContrato.Items.Count; i++)
            {
                inquilino = (Inquilino)cbInquilinoContrato.Items[i];

                if (inquilino.Apellido.Contains(val.ToUpper()) || inquilino.Nombre.Contains(val.ToUpper()))
                {
                    cbInquilinoContrato.SelectedIndex = i;
                    break;
                }
            }
        }

        private void tbBuscar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter || (int)e.KeyChar == (int)Keys.Tab)
            {
                e.Handled = true;
                cbInquilinoContrato.Focus();
            }
            else
            {
                e.KeyChar = Char.ToUpper(e.KeyChar);
            }
        }

        private void tbValor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter || (int)e.KeyChar == (int)Keys.Tab)
            {
                e.Handled = true;
                tbComnetarios.Focus();
            }
            else
            {
                e.KeyChar = Char.ToUpper(e.KeyChar);
            }
        }

        private void actualizarComboBusqueda()
        {
            cbInmueble.Items.Clear();

            foreach (Inmueble inmueble in listaInmuebles)
            {
                cbInmueble.Items.Add(inmueble);
            }

            cbInmueble.SelectedIndex = -1;
        }

        private void guardarContrato_Click(object sender, EventArgs e)
        {
            if(cbInquilinoContrato.SelectedIndex == -1)
            {
                MessageBox.Show("Debe seleccionar un Inquilino", "CUIDADO");

                return;
            }

            if (contratoSeleccionado.id <= 0)
            {
                contratoSeleccionado.inmueble = (Inmueble)cbInmueble.SelectedItem;
                contratoSeleccionado.inquilino = (Inquilino)cbInquilinoContrato.SelectedItem;
                contratoSeleccionado.fechaInicio = dtDesde.Value;
                contratoSeleccionado.fechaFin = dtHasta.Value;
                contratoSeleccionado.comentarios = tbComnetarios.Text;

                String msg = "¿Desea agregar el siguiente Contrato?\n\n" +
                    "Inmueble:\t" + contratoSeleccionado.inmueble + "\n" +
                    "Inquilino:\t" + contratoSeleccionado.inquilino + "\n" +
                    "Fecha inicio:\t" + contratoSeleccionado.fechaInicio.ToShortDateString() + "\n" +
                    "Fecha fin:\t" + contratoSeleccionado.fechaFin.ToShortDateString() + "\n" +
                    "Comentario:\t" + contratoSeleccionado.comentarios + "\n" +
                    "Contrato:\t" + tbLinkContrato.Text;

                DialogResult result = MessageBox.Show(msg, "Agregar Contrato",
                    MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    string sql = "INSERT INTO Contrato (idInmueble, idInquilino, FechaInicio, FechaFin, Comentario) VALUES (" +
                        contratoSeleccionado.inmueble.idInmueble + ", " +
                        contratoSeleccionado.inquilino.id + ", " +
                        "'" + contratoSeleccionado.fechaInicio.ToShortDateString() + "', " +
                        "'" + contratoSeleccionado.fechaFin.ToShortDateString() + "', " +
                        "'" + contratoSeleccionado.comentarios + "')";

                    OleDbCommand oleDbCommand = new OleDbCommand(sql, conn);

                    if (oleDbCommand.ExecuteNonQuery() != 1)
                    {
                        MessageBox.Show("No se ha podido agregar el Contrato", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        if (tbLinkContrato.Text != "")
                        {
                            int id = UtilAlquileres.getLastIDContrato(conn);

                            string myFilePath = tbLinkContrato.Text;
                            string ext = Path.GetExtension(myFilePath);

                            bool res = adjuntarContrato2(myFilePath,
                                Program.getActualPath() + @"\data\contratos\" + id + ext);

                            if (!res)
                            {
                                MessageBox.Show("No se ha podido adjuntar el Contrato", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }

                        actualizarTablaContratos();
                        panEnabled();
                    }
                }
            }
            else
            {
                Contrato contrato2 = new Contrato();

                contrato2.inmueble = (Inmueble)cbInmueble.SelectedItem;
                contrato2.inquilino = (Inquilino)cbInquilinoContrato.SelectedItem;
                contrato2.fechaInicio = dtDesde.Value;
                contrato2.fechaFin = dtHasta.Value;
                contrato2.comentarios = tbComnetarios.Text;

                String path = getPathContrato(contratoSeleccionado.id);

                String msg = "¿Desea editar el siguiente Contrato?\n\n" +
                    "Inmueble:\t" + contratoSeleccionado.inmueble + " por " + contrato2.inmueble + "\n" +
                    "Inquilino:\t" + contratoSeleccionado.inquilino + " por " + contrato2.inquilino + "\n" +
                    "Fecha inicio:\t" + contratoSeleccionado.fechaInicio.ToShortDateString() + " por " + contrato2.fechaInicio.ToShortDateString() + "\n" +
                    "Fecha fin:\t" + contratoSeleccionado.fechaFin.ToShortDateString() + " por " + contrato2.fechaFin.ToShortDateString() + "\n" +
                    "Comentario:\t'" + contratoSeleccionado.comentarios + "' por '" + contrato2.comentarios + "'\n" +
                    "Contrato:\t" + path + " por " + tbLinkContrato.Text + "\n";

                DialogResult result = MessageBox.Show(msg, "Editar Contrato",
                    MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    string sql = "UPDATE Contrato SET idInmueble = " + contrato2.inmueble.idInmueble +
                   ", idInquilino = " + contrato2.inquilino.id +
                   ", FechaInicio = '" + contrato2.fechaInicio.ToShortDateString() + "'" +
                   ", FechaFin = '" + contrato2.fechaFin.ToShortDateString() + "'" +
                   ", Comentario = '" + contrato2.comentarios + "'" +
                   " WHERE Id = " + contratoSeleccionado.id;

                    OleDbDataAdapter oledbAdapter = new OleDbDataAdapter();

                    oledbAdapter.UpdateCommand = conn.CreateCommand();
                    oledbAdapter.UpdateCommand.CommandText = sql;

                    int i = oledbAdapter.UpdateCommand.ExecuteNonQuery();

                    if (i != 1)
                    {
                        MessageBox.Show("Los valores no se han podido modificar.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        if (!path.Equals(tbLinkContrato.Text))
                        {
                            string myFilePath = tbLinkContrato.Text;
                            string ext = Path.GetExtension(myFilePath);

                            bool res = adjuntarContrato2(myFilePath,
                                Program.getActualPath() + @"\data\contratos\" + contratoSeleccionado.id + ext);

                            if(res) File.Delete(@path);
                        }

                        actualizarTablaContratos();
                        panEnabled();
                    }
                }
            }
        }

        public bool adjuntarContrato2(String path1, String path2)
        {
            try
            {
                File.Copy(path1, path2, true);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public void panEnabled()
        {
            tbComnetarios.Text = tbLinkContrato.Text = "";
            cbInquilinoContrato.SelectedIndex = -1;
            dtDesde.Value = dtHasta.Value = DateTime.Today;

            panContrato.Enabled = false;
            cbInmueble.Enabled = true;

            cbInmueble.Enabled =
            butNuevoContrato.Enabled = true;

            butEditarContrato.Enabled =
                butVerContrato.Enabled =
                dgTablaContratos.SelectedRows.Count > 0;

            dgTablaContratos.Enabled = true;

            cbInmueble.Focus();
        }

        private void CancelarContrato_Click(object sender, EventArgs e)
        {
            panEnabled();
        }

        private void butVerContrato_Click(object sender, EventArgs e)
        {
            int idContrato = int.Parse(dgTablaContratos.Rows[dgTablaContratos.SelectedRows[0].Index].Cells["ID"].Value.ToString());

            Contrato contrato = UtilAlquileres.getContrato(conn, idContrato);

            String msg;

            if (contrato.id > 0)
            {
                msg = "Inmueble:\t" + contrato.inmueble +
                    "\nInquilino:\t" + contrato.inquilino +
                    "\nFecha inicio:\t" + contrato.fechaInicio.ToShortDateString() +
                    "\nFecha fin:\t" + contrato.fechaFin.ToShortDateString() +
                    "\nComentarios:\t" + contrato.comentarios;

                if (contrato.listaPeriodo.Count > 0)
                {
                    msg += "\nÚltimo valor:\t $" + contrato.listaPeriodo.Last().valor;
                }
                else
                {
                    msg += "\nÚltimo valor:\tNO se encontraron períodos.";
                }
            }
            else
            {
                msg = "NO hay ningun contrato vigente de para el Inmueble seleccionado.";
            }



            MessageBox.Show(msg, "DETALLES DEL CONTRATO");
        }

        private void rbInmueble_CheckedChanged(object sender, EventArgs e)
        {
            //actualizarComboBusqueda();
        }

        private void butAgregarPeriodo_Click(object sender, EventArgs e)
        {
            int idContrato = int.Parse(dgTablaContratos.Rows[dgTablaContratos.SelectedRows[0].Index].Cells["ID"].Value.ToString());

            ABMPeriodo aBMPeriodo = new ABMPeriodo(conn,
                new Periodo(),
                idContrato,
                contratoSeleccionado.fechaInicio,
                contratoSeleccionado.fechaFin);

            aBMPeriodo.FormClosing += aBMPeriodo_FormClosing;
            aBMPeriodo.Show();
        }

        void aBMPeriodo_FormClosing(object sender, FormClosingEventArgs e)
        {
            int idContrato = int.Parse(dgTablaContratos.Rows[dgTablaContratos.SelectedRows[0].Index].Cells["ID"].Value.ToString());

            contratoSeleccionado = UtilAlquileres.getContrato(conn, idContrato);

            llenarTablaPeriodo(contratoSeleccionado.listaPeriodo);
        }

        private void butModificarPeriodo_Click(object sender, EventArgs e)
        {
            int idPeriodo = int.Parse(dgTablaPeriodo.Rows[dgTablaPeriodo.SelectedRows[0].Index].Cells["IDD"].Value.ToString());

            Periodo periodo = UtilAlquileres.getPeriodoContrato(conn, idPeriodo);

            ABMPeriodo aBMPeriodo = new ABMPeriodo(conn,
                periodo,
                0,
                contratoSeleccionado.fechaInicio,
                contratoSeleccionado.fechaFin);

            aBMPeriodo.FormClosing += aBMPeriodo_FormClosing;
            aBMPeriodo.Show();
        }

        private void dgTablaPeriodo_SelectionChanged(object sender, EventArgs e)
        {

        }

        private void butEliminarPeriodo_Click(object sender, EventArgs e)
        {
            int idPeriodo = int.Parse(dgTablaPeriodo.Rows[dgTablaPeriodo.SelectedRows[0].Index].Cells["IDD"].Value.ToString());

            string msg = "¿Realmente desea eliminar el siguiente período?" +
                "\n\n" +
                "Fecha inicio:\t" + dgTablaPeriodo.Rows[dgTablaPeriodo.SelectedRows[0].Index].Cells[1].Value.ToString() +
                "\nFecha fin:\t" + dgTablaPeriodo.Rows[dgTablaPeriodo.SelectedRows[0].Index].Cells[2].Value.ToString() +
                "\nValor:\t\t" + dgTablaPeriodo.Rows[dgTablaPeriodo.SelectedRows[0].Index].Cells[3].Value.ToString() +
                "\nComentario:\t" + dgTablaPeriodo.Rows[dgTablaPeriodo.SelectedRows[0].Index].Cells[4].Value.ToString();

            DialogResult result = MessageBox.Show(msg, "Eliminar Período",
MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                OleDbCommand delcmd = new OleDbCommand("DELETE FROM ContratoPeriodo WHERE Id = " + idPeriodo, conn);

                if (delcmd.ExecuteNonQuery() != 1)
                {
                    MessageBox.Show("No se ha podido eliminar el período.", "Error");
                }
                else
                {
                    int idContrato = int.Parse(dgTablaContratos.Rows[dgTablaContratos.SelectedRows[0].Index].Cells["ID"].Value.ToString());

                    contratoSeleccionado = UtilAlquileres.getContrato(conn, idContrato);

                    llenarTablaPeriodo(contratoSeleccionado.listaPeriodo);
                }
            }
        }

        private void dgTablaContratos_SelectionChanged(object sender, EventArgs e)
        {
            if (dgTablaContratos.SelectedRows.Count > 0)
            {
                butEditarContrato.Enabled =
                    butVerContrato.Enabled =
                    panPeriodo.Enabled =
                    butEliminarContrato.Enabled = true;

                seleccionarContrato();
            }
            else
            {
                butEditarContrato.Enabled =
                    butVerContrato.Enabled =
                    panPeriodo.Enabled = false;
            }
        }

        private void butPeriodo_Click(object sender, EventArgs e)
        {
            panPeriodo.Enabled = true;

            seleccionarContrato();
        }

        private void butCancelar_Click(object sender, EventArgs e)
        {
            panPeriodo.Enabled = false;

            dgTablaPeriodo.Rows.Clear();
        }

        private void dgTablaPeriodo_SelectionChanged_1(object sender, EventArgs e)
        {
            butEliminarPeriodo.Enabled = butModificarPeriodo.Enabled = dgTablaPeriodo.SelectedRows.Count > 0;
        }

        private void adjuntarContrato_Click(object sender, EventArgs e)
        {
            OpenFileDialog file = new OpenFileDialog();
            // file.Filter = "Pdf Files|*.pdf";
            //file.Filter = "PDF Files|*.pdf|image files|*.png;*.jpg;*.gif";

            if (file.ShowDialog() == DialogResult.OK)
            {
                tbLinkContrato.Text = file.FileName;
            }
        }

        private void linkVer_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            String path = Uri.EscapeDataString(tbLinkContrato.Text);

            Process.Start("chrome.exe", path);
        }

        private void tbLinkContrato_TextChanged(object sender, EventArgs e)
        {
            linkVer.Enabled = tbLinkContrato.Text != "";
        }

        private void butEliminarContrato_Click(object sender, EventArgs e)
        {
            string msg = "¿Realmente desea eliminar el contrato? Se eliminará la información del mismo y todos los periodos cargados.";

            DialogResult result = MessageBox.Show(msg, "Eliminar Contrato",
MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                int id = seleccionarContrato();

                OleDbCommand delcmd = new OleDbCommand("DELETE FROM Contrato WHERE Id = " + id, conn);

                if (delcmd.ExecuteNonQuery() != 1)
                {
                    MessageBox.Show("No se ha podido eliminar el contrato", "Error");
                }
                else
                {
                    OleDbCommand delcmd2 = new OleDbCommand("DELETE FROM ContratoPeriodo WHERE idContrato = " + id, conn);

                    delcmd2.ExecuteNonQuery();

                    actualizarTablaContratos();
                    dgTablaPeriodo.Rows.Clear();

                    butModificarPeriodo.Enabled = butEliminarPeriodo.Enabled = false;
                }
            }
        }

        public String getPathContrato(int id)
        {
            String path = "";

            var files = Directory.GetFiles(Program.getActualPath() + @"\data\contratos\", id + ".*");

            if (files.Length > 0)
            {
                return files[0];
            }
            else
            {
                return "";
            }
        }
    }
}
