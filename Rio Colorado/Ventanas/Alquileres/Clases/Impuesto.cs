﻿using System;

namespace Gestion_Minera.Ventanas.Alquileres.Clases
{
    public class Impuesto
    {
        public int id { get; set; }
        public String nombre { get; set; }
        public int periodo { get; set; }

        public Impuesto(int id, String nombre, int perdiodo)
        {
            this.id = id;
            this.nombre = nombre;
            this.periodo = periodo;
        }

        public Impuesto()
        {

        }

        public override string ToString()
        {
            return nombre;
        }
    }
}
