﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gestion_Minera.Ventanas.Alquileres.Clases
{
    class Pago
    {
        public String valor { get; set; }
        public DateTime fechaCarga { get; set; }
        public int mes { get; set; }
        public int anio { get; set; }

        public Pago(String valor, int mes, DateTime fechaCarga)
        {
            this.valor = valor;
            this.fechaCarga = fechaCarga;
            this.mes = mes;
        }
    }
}
