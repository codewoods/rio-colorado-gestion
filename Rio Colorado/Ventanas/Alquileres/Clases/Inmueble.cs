﻿using System;

namespace Gestion_Minera.Ventanas.Alquileres.Clases
{
    public class Inmueble
    {
        public int idInmueble { get; set; }
        public String direccion { get; set; }
        public String medidorLuz { get; set; }
        public String medidorGas { get; set; }

        public Inquilino inquilino { get; set; }

        public override string ToString()
        {
            return direccion.ToUpper();
        }
    }
}
