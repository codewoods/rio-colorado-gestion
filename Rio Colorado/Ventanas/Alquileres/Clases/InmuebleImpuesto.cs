﻿using System;
namespace Gestion_Minera.Ventanas.Alquileres.Clases
{
    public class InmuebleImpuesto
    {
        public int id { get; set; }
        public Inmueble inmueble { get; set; }
        public Impuesto impuesto { get; set; }
        public DateTime fechaCarga { get; set; }
        public int mes { get; set; }
        public int año { get; set; }
        public string valor { get; set; }
    }
}
