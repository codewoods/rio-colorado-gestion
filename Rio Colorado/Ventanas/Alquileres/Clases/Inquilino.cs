﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gestion_Minera.Ventanas.Alquileres.Clases
{
    public class Inquilino
    {
        public int id { get; set; }
        public String Apellido { get; set; }
        public String Nombre { get; set; }

        public override string ToString()
        {
            return Apellido.ToUpper() + ", " + Nombre.ToUpper();
        }
    }
}
