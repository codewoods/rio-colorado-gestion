﻿using System;
using System.Collections.Generic;

namespace Gestion_Minera.Ventanas.Alquileres.Clases
{
    public class Contrato
    {
        public int id { get; set; }
        public Inmueble inmueble { get; set; }
        public Inquilino inquilino { get; set; }
        public DateTime fechaInicio { get; set; }
        public DateTime fechaFin { get; set; }
        public String valor { get; set; }
        public String comentarios { get; set; }
        public bool stringInmueble { get; set; }
        public List<Periodo> listaPeriodo { get; set; }

        public override string ToString()
        {
            if (stringInmueble)
            {
                return inmueble + " - " + inquilino;
            }
            else
            {
                return inquilino + " - " + inmueble;
            }
        }
    }
}
