﻿using System;

namespace Gestion_Minera.Ventanas.Alquileres.Clases
{
    public class Periodo
    {
        public int id { get; set; }
        public DateTime fechaInicio { get; set; }
        public DateTime fechaFin { get; set; }
        public String valor { get; set; }
        public String comentarios { get; set; }
    }
}
