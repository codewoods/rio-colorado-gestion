﻿using DDJJ_Regalías_Mineras;
using Gestion_Minera.Ventanas.Alquileres.Clases;
using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Gestion_Minera.Clases;
using Application = Microsoft.Office.Interop.Word.Application;


namespace Gestion_Minera.Ventanas.Alquileres
{
    public partial class GenerarRecibo : Form
    {
        OleDbConnection conn;
        List<Contrato> listaContratos;
        double luz, gas, inm, mun, valor;

        int numeroArchivo = 2;

        public GenerarRecibo(OleDbConnection conn)
        {
            InitializeComponent();

            this.conn = conn;

            listaContratos = UtilAlquileres.getContratosVigentes(conn);

            rbInmueble.Checked = true;

            cbMes.Items.Add("ENERO");
            cbMes.Items.Add("FEBRERO");
            cbMes.Items.Add("MARZO");
            cbMes.Items.Add("ABRIL");
            cbMes.Items.Add("MAYO");
            cbMes.Items.Add("JUNIO");
            cbMes.Items.Add("JULIO");
            cbMes.Items.Add("AGOSTO");
            cbMes.Items.Add("SEPTIEMBRE");
            cbMes.Items.Add("OCTUBRE");
            cbMes.Items.Add("NOVIEMBRE");
            cbMes.Items.Add("DICIEMBRE");

            cbMes.SelectedIndex = DateTime.Now.AddMonths(-1).Month - 1;

            numAnio.Value = DateTime.Now.AddMonths(-1).Year;

            butVerPagos.Enabled = butVolver.Enabled = false;
        }

        public void actualizarComboContratos()
        {
            cbContratos.Items.Clear();
            List<Contrato> nuevaListaContrato;

            if (rbInmueble.Checked)
            {
                nuevaListaContrato = listaContratos.OrderBy(o => o.inmueble.ToString()).ToList();

            }
            else
            {
                nuevaListaContrato = listaContratos.OrderBy(o => o.inquilino.ToString()).ToList();
            }

            foreach (Contrato contrato in nuevaListaContrato)
            {
                contrato.stringInmueble = rbInmueble.Checked;
                cbContratos.Items.Add(contrato);
            }

            actualizarValores();
        }

        private void rbInmueble_CheckedChanged(object sender, EventArgs e)
        {
            actualizarComboContratos();
        }

        public void actualizarValores()
        {
            panel1.Enabled = panel2.Enabled = butGenerarRecibo.Enabled = tbTotal.Enabled = (cbMes.SelectedIndex > -1 && cbContratos.SelectedIndex > -1);

            tbLuz.Text = tbGas.Text = tbInm.Text = tbMun.Text = tbValorInicial.Text = "";

            try
            {
                int año = int.Parse(numAnio.Value.ToString());

                if (cbMes.SelectedIndex > -1 && cbContratos.SelectedIndex > -1)
                {
                    List<InmuebleImpuesto> listaImpuestos = UtilAlquileres.getImpuestosInmueble(conn,
                        ((Contrato)cbContratos.SelectedItem).inmueble.idInmueble,
                        cbMes.SelectedIndex + 1,
                        año);

                    foreach (InmuebleImpuesto inmuebleImpuesto in listaImpuestos)
                    {
                        switch (inmuebleImpuesto.impuesto.id)
                        {
                            case 1:
                                {
                                    tbLuz.Text = inmuebleImpuesto.valor;
                                    break;
                                }
                            case 2:
                                {
                                    tbGas.Text = inmuebleImpuesto.valor;
                                    break;
                                }
                            case 3:
                                {
                                    tbInm.Text = inmuebleImpuesto.valor;
                                    break;
                                }
                            case 4:
                                {
                                    tbMun.Text = inmuebleImpuesto.valor;
                                    break;
                                }
                        }
                    }

                    Periodo ultimoPeriodo = ((Contrato)cbContratos.SelectedItem).listaPeriodo.Last();

                    tbValorInicial.Text = ultimoPeriodo.valor;

                    tbInfo.Text = "Valor correspondiente al período " +
                        ultimoPeriodo.fechaInicio.ToShortDateString() +
                        " al " +
                        ultimoPeriodo.fechaFin.ToShortDateString() + ".";
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void tbGas_TextChanged(object sender, EventArgs e)
        {
            actualizarTotal();
        }

        private void tbInm_TextChanged(object sender, EventArgs e)
        {
            actualizarTotal();
        }

        private void tbMun_TextChanged(object sender, EventArgs e)
        {
            actualizarTotal();
        }

        private void tbValorInicial_TextChanged(object sender, EventArgs e)
        {
            actualizarTotal();
        }

        private void tbValorActual_TextChanged(object sender, EventArgs e)
        {
            actualizarTotal();
        }

        private void butGenerarRecibo_Click(object sender, EventArgs e)
        {
            int mes = cbMes.SelectedIndex + 1;

            butGenerarRecibo.Enabled = false;
            butVolver.Enabled = true;

            String sql = "INSERT INTO Pagos (idInmueble, Valor, Mes, Anio, FechaPago) VALUES (" +
                           ((Contrato)cbContratos.SelectedItem).inmueble.idInmueble + ", '" +
                           tbTotal.Text + "', " +
                           mes + ", " +
                           numAnio.Value + ", " +
                           "'" + DateTime.Today.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + "')";

            OleDbCommand oleDbCommand = new OleDbCommand(sql, conn);

            if (oleDbCommand.ExecuteNonQuery() != 1)
            {
                MessageBox.Show("No se ha podido cargar el pago", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                generarCOmprobantePagoAlquiler();
            }
        }

        public void generarCOmprobantePagoAlquiler()
        {
            WIP wip = new WIP("Generando recibo de pago...");
            wip.Show();

            Document document = new Document();

            try
            {
                String path = Path.GetTempPath() + "ReciboAlquiler.docx";
                String _path = Path.GetTempPath() + "ReciboAlquiler";

                do
                {
                    try
                    {
                        if (tbLuz.Text == "" && tbGas.Text == "" && tbInm.Text == "" && tbMun.Text == "")
                        {
                            File.Copy(Program.getActualPath() + @"\data\ReciboAlquiler.docx", path, true);
                        }
                        else
                        {
                            File.Copy(Program.getActualPath() + @"\data\ReciboAlquiler2.docx", path, true);
                        }

                        break;
                    }
                    catch (Exception ex)
                    {
                        path = _path + numeroArchivo + ".docx";
                        numeroArchivo++;
                    }
                } while (numeroArchivo != 12);

                if (numeroArchivo == 12)
                {
                    MessageBox.Show("No se pudo crear la Constancia de Pago porque hay muchos documentos abiertos. Por favor ciérrelos e intente nuevamente.", "Error");
                }

                Application ap = new Application();
                Document doc = ap.Documents.Open(path);
                document.Activate();

                int coun = UtilAlquileres.getNumberPagos(conn, UtilAlquileres.getLastIDNovedad(conn));

                Utils.FindAndReplace(ap, "{0}", coun);
                Utils.FindAndReplace(ap, "{1}", ((Contrato)cbContratos.SelectedItem).inquilino.ToString());
                Utils.FindAndReplace(ap, "{3}", Utils.numeroLetra(tbTotal.Text.Replace(".", ",")));
                Utils.FindAndReplace(ap, "{4}", tbTotal.Text.Replace(".", ","));
                Utils.FindAndReplace(ap, "{5}", cbMes.Text);
                Utils.FindAndReplace(ap, "{6}", numAnio.Value.ToString());
                Utils.FindAndReplace(ap, "{7}", ((Contrato)cbContratos.SelectedItem).inmueble.ToString());

                if (tbLuz.Text != "" || tbGas.Text != "" || tbInm.Text != "" || tbMun.Text != "")
                {
                    String gas, luz;

                    if(tbLuz.Text == "")
                    {
                        luz = "0";
                    } else
                    {
                        luz = tbLuz.Text;
                    }

                    if (tbGas.Text == "")
                    {
                        gas = "0";
                    }
                    else
                    {
                        gas = tbLuz.Text;
                    }

                    Utils.FindAndReplace(ap, "{8}", tbValorInicial.Text);
                    Utils.FindAndReplace(ap, "{9}", luz);
                    Utils.FindAndReplace(ap, "{10}", gas);

                    double dou = 0;

                    try
                    {
                        dou = double.Parse(tbInm.Text) + double.Parse(tbMun.Text);
                    }
                    catch (Exception ex) { }

                    Utils.FindAndReplace(ap, "{11}", dou);
                    Utils.FindAndReplace(ap, "{12}", DateTime.Today.Day);
                    Utils.FindAndReplace(ap, "{13}", DateTime.Today.ToString("MMMM").ToUpper());
                    Utils.FindAndReplace(ap, "{14}", DateTime.Today.Year);
                    Utils.FindAndReplace(ap, "{15}", ((Contrato)cbContratos.SelectedItem).inquilino.ToString());
                }
                else
                {
                    Utils.FindAndReplace(ap, "{8}", DateTime.Today.Day);
                    Utils.FindAndReplace(ap, "{9}", DateTime.Today.ToString("MMMM").ToUpper());
                    Utils.FindAndReplace(ap, "{10}", DateTime.Today.Year);
                    Utils.FindAndReplace(ap, "{11}", ((Contrato)cbContratos.SelectedItem).inquilino.ToString());
                }

                document.Close();

                System.Diagnostics.Process.Start(path);
            }
            catch (Exception ex)
            {
                MessageBox.Show("No se pudo crear el archivo\n Error: " + ex.Message, "Error");
                document.Close();
            }

            wip.Close();
        }




        public void actualizarTotal()
        {
            luz = gas = inm = mun = valor = 0;

            try
            {
                if (tbLuz.Text != "") luz = double.Parse(tbLuz.Text.Replace(".", ","));
                if (tbGas.Text != "") gas = double.Parse(tbGas.Text.Replace(".", ","));
                if (tbInm.Text != "") inm = double.Parse(tbInm.Text.Replace(".", ","));
                if (tbMun.Text != "") mun = double.Parse(tbMun.Text.Replace(".", ","));
                if (tbValorInicial.Text != "") valor = double.Parse(tbValorInicial.Text.Replace(".", ","));

                tbTotal.Text = luz + gas + inm + mun + valor + "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "ERROR");
            }
        }

        private void butVerPagos_Click(object sender, EventArgs e)
        {
            if (cbContratos.SelectedIndex > -1)
            {
                VerPagos verPagos = new VerPagos(conn, ((Contrato)cbContratos.SelectedItem).inmueble);
                verPagos.Show();
            }
        }

        private void butVolver_Click(object sender, EventArgs e)
        {
            butGenerarRecibo.Enabled = true;
            butVolver.Enabled = false;
        }

        private void numAnio_ValueChanged(object sender, EventArgs e)
        {
            actualizarValores();
        }

        private void cbContratos_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbContratos.SelectedIndex > -1)
            {
                butVerPagos.Enabled = true;
                actualizarValores();
            }
            else
            {
                butVerPagos.Enabled = false;
            }
        }

        private void cbMes_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbMes.SelectedIndex > -1)
            {
                actualizarValores();
            }
        }

        private void tbAño_TextChanged(object sender, EventArgs e)
        {
            actualizarValores();
        }

        private void tbLuz_TextChanged(object sender, EventArgs e)
        {
            actualizarTotal();
        }
    }
}
