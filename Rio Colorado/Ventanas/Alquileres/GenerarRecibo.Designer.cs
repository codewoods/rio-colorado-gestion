﻿namespace Gestion_Minera.Ventanas.Alquileres
{
    partial class GenerarRecibo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GenerarRecibo));
            this.label1 = new System.Windows.Forms.Label();
            this.butGenerarRecibo = new System.Windows.Forms.Button();
            this.cbContratos = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cbMes = new System.Windows.Forms.ComboBox();
            this.rbInmueble = new System.Windows.Forms.RadioButton();
            this.rbInquilino = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tbInfo = new System.Windows.Forms.TextBox();
            this.tbValorInicial = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tbMun = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbInm = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbGas = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbLuz = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbTotal = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.numAnio = new System.Windows.Forms.NumericUpDown();
            this.butVerPagos = new System.Windows.Forms.Button();
            this.butVolver = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numAnio)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 15);
            this.label1.TabIndex = 87;
            this.label1.Text = "Contratos vigentes";
            // 
            // butGenerarRecibo
            // 
            this.butGenerarRecibo.Location = new System.Drawing.Point(15, 232);
            this.butGenerarRecibo.Name = "butGenerarRecibo";
            this.butGenerarRecibo.Size = new System.Drawing.Size(163, 24);
            this.butGenerarRecibo.TabIndex = 86;
            this.butGenerarRecibo.Text = "CARGAR PAGO Y GENERAR RECIBO";
            this.butGenerarRecibo.UseVisualStyleBackColor = true;
            this.butGenerarRecibo.Click += new System.EventHandler(this.butGenerarRecibo_Click);
            // 
            // cbContratos
            // 
            this.cbContratos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbContratos.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbContratos.Location = new System.Drawing.Point(125, 47);
            this.cbContratos.Name = "cbContratos";
            this.cbContratos.Size = new System.Drawing.Size(323, 23);
            this.cbContratos.TabIndex = 85;
            this.cbContratos.SelectedIndexChanged += new System.EventHandler(this.cbContratos_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 15);
            this.label2.TabIndex = 90;
            this.label2.Text = "Ordenar por:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 15);
            this.label3.TabIndex = 91;
            this.label3.Text = "Mes";
            // 
            // cbMes
            // 
            this.cbMes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMes.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbMes.Location = new System.Drawing.Point(125, 77);
            this.cbMes.Name = "cbMes";
            this.cbMes.Size = new System.Drawing.Size(218, 23);
            this.cbMes.TabIndex = 92;
            this.cbMes.SelectedIndexChanged += new System.EventHandler(this.cbMes_SelectedIndexChanged);
            // 
            // rbInmueble
            // 
            this.rbInmueble.AutoSize = true;
            this.rbInmueble.Location = new System.Drawing.Point(94, 19);
            this.rbInmueble.Name = "rbInmueble";
            this.rbInmueble.Size = new System.Drawing.Size(68, 17);
            this.rbInmueble.TabIndex = 93;
            this.rbInmueble.TabStop = true;
            this.rbInmueble.Text = "Inmueble";
            this.rbInmueble.UseVisualStyleBackColor = true;
            this.rbInmueble.CheckedChanged += new System.EventHandler(this.rbInmueble_CheckedChanged);
            // 
            // rbInquilino
            // 
            this.rbInquilino.AutoSize = true;
            this.rbInquilino.Location = new System.Drawing.Point(168, 19);
            this.rbInquilino.Name = "rbInquilino";
            this.rbInquilino.Size = new System.Drawing.Size(64, 17);
            this.rbInquilino.TabIndex = 94;
            this.rbInquilino.TabStop = true;
            this.rbInquilino.Text = "Inquilino";
            this.rbInquilino.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Honeydew;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.tbInfo);
            this.panel1.Controls.Add(this.tbValorInicial);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Location = new System.Drawing.Point(401, 114);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(147, 109);
            this.panel1.TabIndex = 95;
            // 
            // tbInfo
            // 
            this.tbInfo.BackColor = System.Drawing.Color.Honeydew;
            this.tbInfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbInfo.Location = new System.Drawing.Point(15, 56);
            this.tbInfo.Margin = new System.Windows.Forms.Padding(0);
            this.tbInfo.Multiline = true;
            this.tbInfo.Name = "tbInfo";
            this.tbInfo.ReadOnly = true;
            this.tbInfo.Size = new System.Drawing.Size(114, 48);
            this.tbInfo.TabIndex = 92;
            this.tbInfo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbValorInicial
            // 
            this.tbValorInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbValorInicial.Location = new System.Drawing.Point(15, 28);
            this.tbValorInicial.Name = "tbValorInicial";
            this.tbValorInicial.Size = new System.Drawing.Size(114, 22);
            this.tbValorInicial.TabIndex = 91;
            this.tbValorInicial.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbValorInicial.TextChanged += new System.EventHandler(this.tbValorInicial_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(119, 15);
            this.label4.TabIndex = 88;
            this.label4.Text = "Último valor contrato";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(12, 12);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(172, 15);
            this.label6.TabIndex = 92;
            this.label6.Text = "Impuestos y servicios del mes:";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Info;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.tbMun);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.tbInm);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.tbGas);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.tbLuz);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Location = new System.Drawing.Point(15, 114);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(378, 109);
            this.panel2.TabIndex = 96;
            // 
            // tbMun
            // 
            this.tbMun.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMun.Location = new System.Drawing.Point(276, 73);
            this.tbMun.Name = "tbMun";
            this.tbMun.Size = new System.Drawing.Size(88, 22);
            this.tbMun.TabIndex = 96;
            this.tbMun.TextChanged += new System.EventHandler(this.tbMun_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(171, 75);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(94, 15);
            this.label9.TabIndex = 95;
            this.label9.Text = "Im. municipales";
            // 
            // tbInm
            // 
            this.tbInm.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbInm.Location = new System.Drawing.Point(276, 42);
            this.tbInm.Name = "tbInm";
            this.tbInm.Size = new System.Drawing.Size(88, 22);
            this.tbInm.TabIndex = 94;
            this.tbInm.TextChanged += new System.EventHandler(this.tbInm_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(171, 44);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(99, 15);
            this.label10.TabIndex = 93;
            this.label10.Text = "Imp. Inmobiliario";
            // 
            // tbGas
            // 
            this.tbGas.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbGas.Location = new System.Drawing.Point(58, 73);
            this.tbGas.Name = "tbGas";
            this.tbGas.Size = new System.Drawing.Size(88, 22);
            this.tbGas.TabIndex = 91;
            this.tbGas.TextChanged += new System.EventHandler(this.tbGas_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(11, 74);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 15);
            this.label7.TabIndex = 90;
            this.label7.Text = "Gas";
            // 
            // tbLuz
            // 
            this.tbLuz.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbLuz.Location = new System.Drawing.Point(58, 42);
            this.tbLuz.Name = "tbLuz";
            this.tbLuz.Size = new System.Drawing.Size(88, 22);
            this.tbLuz.TabIndex = 89;
            this.tbLuz.TextChanged += new System.EventHandler(this.tbLuz_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(11, 43);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(27, 15);
            this.label8.TabIndex = 88;
            this.label8.Text = "Luz";
            // 
            // tbTotal
            // 
            this.tbTotal.BackColor = System.Drawing.Color.LavenderBlush;
            this.tbTotal.Font = new System.Drawing.Font("Noto Mono", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbTotal.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.tbTotal.Location = new System.Drawing.Point(401, 230);
            this.tbTotal.Name = "tbTotal";
            this.tbTotal.Size = new System.Drawing.Size(147, 26);
            this.tbTotal.TabIndex = 98;
            this.tbTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(336, 236);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(57, 16);
            this.label11.TabIndex = 97;
            this.label11.Text = "TOTAL";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(349, 81);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(28, 15);
            this.label12.TabIndex = 99;
            this.label12.Text = "Año";
            // 
            // numAnio
            // 
            this.numAnio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.numAnio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numAnio.Location = new System.Drawing.Point(383, 79);
            this.numAnio.Maximum = new decimal(new int[] {
            2050,
            0,
            0,
            0});
            this.numAnio.Minimum = new decimal(new int[] {
            2010,
            0,
            0,
            0});
            this.numAnio.Name = "numAnio";
            this.numAnio.ReadOnly = true;
            this.numAnio.Size = new System.Drawing.Size(65, 21);
            this.numAnio.TabIndex = 100;
            this.numAnio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numAnio.Value = new decimal(new int[] {
            2010,
            0,
            0,
            0});
            this.numAnio.ValueChanged += new System.EventHandler(this.numAnio_ValueChanged);
            // 
            // butVerPagos
            // 
            this.butVerPagos.Location = new System.Drawing.Point(456, 40);
            this.butVerPagos.Name = "butVerPagos";
            this.butVerPagos.Size = new System.Drawing.Size(92, 37);
            this.butVerPagos.TabIndex = 101;
            this.butVerPagos.Text = "VER TODOS LOS PAGOS";
            this.butVerPagos.UseVisualStyleBackColor = true;
            this.butVerPagos.Click += new System.EventHandler(this.butVerPagos_Click);
            // 
            // butVolver
            // 
            this.butVolver.Location = new System.Drawing.Point(184, 232);
            this.butVolver.Name = "butVolver";
            this.butVolver.Size = new System.Drawing.Size(132, 24);
            this.butVolver.TabIndex = 102;
            this.butVolver.Text = "VOLVER A GENERAR";
            this.butVolver.UseVisualStyleBackColor = true;
            this.butVolver.Click += new System.EventHandler(this.butVolver_Click);
            // 
            // GenerarRecibo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(560, 268);
            this.Controls.Add(this.butVolver);
            this.Controls.Add(this.butVerPagos);
            this.Controls.Add(this.numAnio);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.tbTotal);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.rbInquilino);
            this.Controls.Add(this.rbInmueble);
            this.Controls.Add(this.cbMes);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.butGenerarRecibo);
            this.Controls.Add(this.cbContratos);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "GenerarRecibo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Generar Recibo";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numAnio)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button butGenerarRecibo;
        private System.Windows.Forms.ComboBox cbContratos;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbMes;
        private System.Windows.Forms.RadioButton rbInmueble;
        private System.Windows.Forms.RadioButton rbInquilino;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox tbValorInicial;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox tbMun;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbInm;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbGas;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbLuz;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbTotal;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tbInfo;
        private System.Windows.Forms.NumericUpDown numAnio;
        private System.Windows.Forms.Button butVerPagos;
        private System.Windows.Forms.Button butVolver;
    }
}