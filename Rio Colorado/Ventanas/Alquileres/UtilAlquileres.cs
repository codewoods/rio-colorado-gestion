﻿using Gestion_Minera.Ventanas.Alquileres.Clases;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gestion_Minera.Ventanas.Alquileres
{
    class UtilAlquileres
    {
        public static List<Impuesto> getImpuestos(OleDbConnection conn)
        {
            List<Impuesto> listaImpuesto = new List<Impuesto>();

            string sql = "SELECT * FROM Impuesto";

            OleDbCommand dbCmd = new OleDbCommand(sql, conn);
            OleDbDataReader dbReader = dbCmd.ExecuteReader();

            while (dbReader.Read())
            {
                Impuesto impuesto = new Impuesto();

                impuesto.id = int.Parse(dbReader.GetValue(0).ToString());
                impuesto.nombre = dbReader.GetValue(1).ToString();
                impuesto.periodo = int.Parse(dbReader.GetValue(2).ToString());

                listaImpuesto.Add(impuesto);
            }

            return listaImpuesto;
        }

        public static Impuesto getImpuesto(OleDbConnection conn, int id)
        {
            Impuesto impuesto = new Impuesto();

            string sql = "SELECT * FROM Impuesto WHERE Id=" + id;

            OleDbCommand dbCmd = new OleDbCommand(sql, conn);
            OleDbDataReader dbReader = dbCmd.ExecuteReader();

            if (dbReader.Read())
            {
                impuesto.id = int.Parse(dbReader.GetValue(0).ToString());
                impuesto.nombre = dbReader.GetValue(1).ToString();
                impuesto.periodo = int.Parse(dbReader.GetValue(2).ToString());
            }

            return impuesto;
        }

        public static List<Inquilino> getInquilinos(OleDbConnection conn)
        {
            List<Inquilino> listaInquilinos = new List<Inquilino>();

            string sql = "SELECT * FROM Inquilino ORDER BY Apellido, Nombre";

            OleDbCommand dbCmd = new OleDbCommand(sql, conn);
            OleDbDataReader dbReader = dbCmd.ExecuteReader();

            while (dbReader.Read())
            {
                Inquilino inquilino = new Inquilino();

                inquilino.id = int.Parse(dbReader.GetValue(0).ToString());
                inquilino.Apellido = dbReader.GetValue(1).ToString();

                inquilino.Nombre = dbReader.GetValue(2).ToString();

                listaInquilinos.Add(inquilino);
            }

            return listaInquilinos;
        }

        public static List<Inmueble> getInmuebles(OleDbConnection conn)
        {
            List<Inmueble> listaInmueble = new List<Inmueble>();

            string sql = "SELECT * FROM Inmueble ORDER BY Direccion";

            OleDbCommand dbCmd = new OleDbCommand(sql, conn);
            OleDbDataReader dbReader = dbCmd.ExecuteReader();

            while (dbReader.Read())
            {
                Inmueble inmueble = new Inmueble();

                inmueble.idInmueble = int.Parse(dbReader.GetValue(0).ToString());
                inmueble.direccion = dbReader.GetValue(1).ToString();
                inmueble.medidorLuz = dbReader.GetValue(2).ToString();
                inmueble.medidorGas = dbReader.GetValue(3).ToString();

                listaInmueble.Add(inmueble);
            }

            return listaInmueble;
        }

        public static Inquilino getInquilino(OleDbConnection conn, int id)
        {
            Inquilino inquilino = new Inquilino();

            string sql = "SELECT * FROM Inquilino WHERE Id=" + id;

            OleDbCommand dbCmd = new OleDbCommand(sql, conn);
            OleDbDataReader dbReader = dbCmd.ExecuteReader();

            if (dbReader.Read())
            {
                inquilino.id = int.Parse(dbReader.GetValue(0).ToString());
                inquilino.Apellido = dbReader.GetValue(1).ToString();
                inquilino.Nombre = dbReader.GetValue(2).ToString();
            }

            return inquilino;
        }

        public static Inmueble getInmueble(OleDbConnection conn, int id)
        {
            Inmueble inmueble = new Inmueble();

            string sql = "SELECT * FROM Inmueble WHERE Id=" + id;

            OleDbCommand dbCmd = new OleDbCommand(sql, conn);
            OleDbDataReader dbReader = dbCmd.ExecuteReader();

            if (dbReader.Read())
            {
                inmueble.idInmueble = int.Parse(dbReader.GetValue(0).ToString());
                inmueble.direccion = dbReader.GetValue(1).ToString();
                inmueble.medidorLuz = dbReader.GetValue(2).ToString();
                inmueble.medidorGas = dbReader.GetValue(3).ToString();
            }

            return inmueble;
        }

        public static Contrato getUltimoContrato(OleDbConnection conn, int idInmueble)
        {
            Contrato contrato = new Contrato();
            String sql = "SELECT * FROM Contrato WHERE idInmueble=" + idInmueble + " ORDER BY FechaFin DESC LIMIT 1";

            OleDbCommand dbCmd = new OleDbCommand(sql, conn);
            OleDbDataReader dbReader = dbCmd.ExecuteReader();

            if (dbReader.Read())
            {
                contrato.id = int.Parse(dbReader.GetValue(0).ToString());

                if (int.Parse(dbReader.GetValue(1).ToString()) > 0)
                {
                    contrato.inmueble = getInmueble(conn, int.Parse(dbReader.GetValue(1).ToString()));
                }
                else
                {
                    contrato.inmueble = new Inmueble();
                }

                if (int.Parse(dbReader.GetValue(2).ToString()) > 0)
                {
                    contrato.inquilino = getInquilino(conn, int.Parse(dbReader.GetValue(2).ToString()));
                }
                else
                {
                    contrato.inquilino = new Inquilino();
                }

                contrato.fechaInicio = DateTime.Parse(dbReader.GetValue(3).ToString());
                contrato.fechaFin = DateTime.Parse(dbReader.GetValue(4).ToString());
                contrato.valor = dbReader.GetValue(5).ToString();
                contrato.comentarios = dbReader.GetValue(6).ToString();
            }

            return contrato;
        }

        public static List<Contrato> getContratos(OleDbConnection conn, int idInmueble)
        {
            List<Contrato> listaContratos = new List<Contrato>();

            string sql = "SELECT * FROM Contrato WHERE idInmueble=" + idInmueble;

            OleDbCommand dbCmd = new OleDbCommand(sql, conn);
            OleDbDataReader dbReader = dbCmd.ExecuteReader();

            while (dbReader.Read())
            {
                Contrato contrato = new Contrato();

                contrato.id = int.Parse(dbReader.GetValue(0).ToString());

                if (int.Parse(dbReader.GetValue(1).ToString()) > 0)
                {
                    contrato.inmueble = getInmueble(conn, int.Parse(dbReader.GetValue(1).ToString()));
                }
                else
                {
                    contrato.inmueble = new Inmueble();
                }

                if (int.Parse(dbReader.GetValue(2).ToString()) > 0)
                {
                    contrato.inquilino = getInquilino(conn, int.Parse(dbReader.GetValue(2).ToString()));
                }
                else
                {
                    contrato.inquilino = new Inquilino();
                }

                contrato.fechaInicio = DateTime.Parse(dbReader.GetValue(3).ToString());
                contrato.fechaFin = DateTime.Parse(dbReader.GetValue(4).ToString());
                contrato.comentarios = dbReader.GetValue(5).ToString();

                listaContratos.Add(contrato);
            }

            return listaContratos;
        }

        public static Contrato getContrato(OleDbConnection conn, int idContrato)
        {
            Contrato contrato = new Contrato();

            string sql = "SELECT * FROM Contrato WHERE Id = " + idContrato;

            OleDbCommand dbCmd = new OleDbCommand(sql, conn);
            OleDbDataReader dbReader = dbCmd.ExecuteReader();

            if (dbReader.Read())
            {
                contrato.id = int.Parse(dbReader.GetValue(0).ToString());

                if (int.Parse(dbReader.GetValue(1).ToString()) > 0)
                {
                    contrato.inmueble = getInmueble(conn, int.Parse(dbReader.GetValue(1).ToString()));
                }
                else
                {
                    contrato.inmueble = new Inmueble();
                }

                if (int.Parse(dbReader.GetValue(2).ToString()) > 0)
                {
                    contrato.inquilino = getInquilino(conn, int.Parse(dbReader.GetValue(2).ToString()));
                }
                else
                {
                    contrato.inquilino = new Inquilino();
                }

                contrato.fechaInicio = DateTime.Parse(dbReader.GetValue(3).ToString());
                contrato.fechaFin = DateTime.Parse(dbReader.GetValue(4).ToString());
                contrato.comentarios = dbReader.GetValue(5).ToString();

                contrato.listaPeriodo = UtilAlquileres.getPeriodosContrato(conn, contrato.id);
            }

            return contrato;
        }

        public static Periodo getPeriodoContrato(OleDbConnection conn, int idPeriodo)
        {
            Periodo periodo = new Periodo();

            string sql = "SELECT * FROM ContratoPeriodo WHERE Id = " + idPeriodo;

            OleDbCommand dbCmd = new OleDbCommand(sql, conn);
            OleDbDataReader dbReader = dbCmd.ExecuteReader();

            if (dbReader.Read())
            {
                periodo.id = int.Parse(dbReader.GetValue(0).ToString());
                periodo.fechaInicio = DateTime.Parse(dbReader.GetValue(2).ToString());
                periodo.fechaFin = DateTime.Parse(dbReader.GetValue(3).ToString());
                periodo.valor = dbReader.GetValue(4).ToString();
                periodo.comentarios = dbReader.GetValue(5).ToString();
            }

            return periodo;
        }

        public static List<Periodo> getPeriodosContrato(OleDbConnection conn, int idContrato)
        {
            List<Periodo> listaPeriodos = new List<Periodo>();

            string sql = "SELECT * FROM ContratoPeriodo WHERE idContrato = " + idContrato;

            OleDbCommand dbCmd = new OleDbCommand(sql, conn);
            OleDbDataReader dbReader = dbCmd.ExecuteReader();

            while (dbReader.Read())
            {
                Periodo periodo = new Periodo();

                periodo.id = int.Parse(dbReader.GetValue(0).ToString());
                periodo.fechaInicio = DateTime.Parse(dbReader.GetValue(2).ToString());
                periodo.fechaFin = DateTime.Parse(dbReader.GetValue(3).ToString());
                periodo.valor = dbReader.GetValue(4).ToString();
                periodo.comentarios = dbReader.GetValue(5).ToString();

                listaPeriodos.Add(periodo);
            }

            return listaPeriodos;
        }

        public static Contrato getContrato(OleDbConnection conn, int idInmueble, int idInquilino)
        {
            Contrato contrato = new Contrato();
            string sql;

            if (idInmueble > 0)
            {
                sql = "SELECT * FROM Contrato WHERE idInmueble=" + idInmueble + " AND FechaFin > " + DateTime.Now.ToString("yyyy-MM-dd");
            }
            else
            {
                sql = "SELECT * FROM Contrato WHERE idInquilino=" + idInquilino + " AND FechaFin > " + DateTime.Now.ToString("yyyy-MM-dd");
            }

            OleDbCommand dbCmd = new OleDbCommand(sql, conn);
            OleDbDataReader dbReader = dbCmd.ExecuteReader();

            if (dbReader.Read())
            {
                contrato.id = int.Parse(dbReader.GetValue(0).ToString());

                if (int.Parse(dbReader.GetValue(1).ToString()) > 0)
                {
                    contrato.inmueble = getInmueble(conn, int.Parse(dbReader.GetValue(1).ToString()));
                }
                else
                {
                    contrato.inmueble = new Inmueble();
                }

                if (int.Parse(dbReader.GetValue(2).ToString()) > 0)
                {
                    contrato.inquilino = getInquilino(conn, int.Parse(dbReader.GetValue(2).ToString()));
                }
                else
                {
                    contrato.inquilino = new Inquilino();
                }

                contrato.fechaInicio = DateTime.Parse(dbReader.GetValue(3).ToString());
                contrato.fechaFin = DateTime.Parse(dbReader.GetValue(4).ToString());
                contrato.comentarios = dbReader.GetValue(5).ToString();

                contrato.listaPeriodo = UtilAlquileres.getPeriodosContrato(conn, contrato.id);
            }

            return contrato;
        }

        public static List<Contrato> getContratosVigentes(OleDbConnection conn)
        {
            List<Contrato> listaContratos = new List<Contrato>();

            string sql = "SELECT * FROM Contrato WHERE FechaFin > " + DateTime.Now.ToString("yyyy-MM-dd");

            OleDbCommand dbCmd = new OleDbCommand(sql, conn);
            OleDbDataReader dbReader = dbCmd.ExecuteReader();

            while (dbReader.Read())
            {
                Contrato contrato = new Contrato();

                contrato.id = int.Parse(dbReader.GetValue(0).ToString());

                if (int.Parse(dbReader.GetValue(1).ToString()) > 0)
                {
                    contrato.inmueble = getInmueble(conn, int.Parse(dbReader.GetValue(1).ToString()));
                }
                else
                {
                    contrato.inmueble = new Inmueble();
                }

                if (int.Parse(dbReader.GetValue(2).ToString()) > 0)
                {
                    contrato.inquilino = getInquilino(conn, int.Parse(dbReader.GetValue(2).ToString()));
                }
                else
                {
                    contrato.inquilino = new Inquilino();
                }

                contrato.fechaInicio = DateTime.Parse(dbReader.GetValue(3).ToString());
                contrato.fechaFin = DateTime.Parse(dbReader.GetValue(4).ToString());
                contrato.comentarios = dbReader.GetValue(5).ToString();

                contrato.listaPeriodo = getPeriodosContrato(conn, contrato.id);

                listaContratos.Add(contrato);
            }

            return listaContratos;
        }

        public static InmuebleImpuesto getInmuebleImpuesto(OleDbConnection conn, int idInmueble, int idImpuesto, int mes, int anio)
        {
            InmuebleImpuesto inmuebleImpuesto = new InmuebleImpuesto();

            inmuebleImpuesto.mes = mes;
            inmuebleImpuesto.año = anio;

            string sql = "SELECT * FROM InmuebleImpuesto WHERE idInmueble=" + idInmueble +
                " AND idImpuesto=" + idImpuesto +
                " AND Mes='" + mes + "'" +
                " AND Anio='" + anio + "'";

            Console.WriteLine(sql);

            OleDbCommand dbCmd = new OleDbCommand(sql, conn);
            OleDbDataReader dbReader = dbCmd.ExecuteReader();

            if (dbReader.Read())
            {
                inmuebleImpuesto.id = int.Parse(dbReader.GetValue(0).ToString());
                inmuebleImpuesto.fechaCarga = DateTime.Parse(dbReader.GetValue(3).ToString());
                inmuebleImpuesto.valor = dbReader.GetValue(6).ToString();
            }

            return inmuebleImpuesto;
        }

        public static List<InmuebleImpuesto> getImpuestosInmuebleAnual(OleDbConnection conn, int idInmueble, int anio)
        {
            List<InmuebleImpuesto> listaImpuestos = new List<InmuebleImpuesto>();

            string sql = "SELECT * FROM InmuebleImpuesto WHERE idInmueble=" + idInmueble +
                " AND Anio='" + anio + "'";

            OleDbCommand dbCmd = new OleDbCommand(sql, conn);
            OleDbDataReader dbReader = dbCmd.ExecuteReader();

            while (dbReader.Read())
            {
                InmuebleImpuesto inmuebleImpuesto = new InmuebleImpuesto();
                inmuebleImpuesto.año = anio;

                inmuebleImpuesto.id = int.Parse(dbReader.GetValue(0).ToString());
                inmuebleImpuesto.impuesto = getImpuesto(conn, int.Parse(dbReader.GetValue(2).ToString()));
                inmuebleImpuesto.fechaCarga = DateTime.Parse(dbReader.GetValue(3).ToString());
                inmuebleImpuesto.mes = int.Parse(dbReader.GetValue(4).ToString());
                inmuebleImpuesto.valor = dbReader.GetValue(6).ToString();

                listaImpuestos.Add(inmuebleImpuesto);
            }

            return listaImpuestos;
        }

        public static List<InmuebleImpuesto> getImpuestosInmueble(OleDbConnection conn, int idInmueble, int mes, int anio)
        {
            List<InmuebleImpuesto> listaImpuestos = new List<InmuebleImpuesto>();

            string sql = "SELECT * FROM InmuebleImpuesto WHERE idInmueble=" + idInmueble +
                " AND Anio='" + anio + "' AND Mes='" + mes + "'";

            OleDbCommand dbCmd = new OleDbCommand(sql, conn);
            OleDbDataReader dbReader = dbCmd.ExecuteReader();

            while (dbReader.Read())
            {
                InmuebleImpuesto inmuebleImpuesto = new InmuebleImpuesto();
                inmuebleImpuesto.año = anio;

                inmuebleImpuesto.id = int.Parse(dbReader.GetValue(0).ToString());
                inmuebleImpuesto.impuesto = getImpuesto(conn, int.Parse(dbReader.GetValue(2).ToString()));
                inmuebleImpuesto.fechaCarga = DateTime.Parse(dbReader.GetValue(3).ToString());
                inmuebleImpuesto.mes = int.Parse(dbReader.GetValue(4).ToString());
                inmuebleImpuesto.valor = dbReader.GetValue(6).ToString();

                listaImpuestos.Add(inmuebleImpuesto);
            }

            return listaImpuestos;
        }

        public static List<Pago> getPagos(OleDbConnection conn, int idInmueble, int anio)
        {
            List<Pago> listaPagos = new List<Pago>();

            string sql = "SELECT Valor, Mes, FechaPago FROM Pagos WHERE IdInmueble=" + idInmueble + " AND Anio=" + anio + " ORDER BY Mes ASC";

            OleDbCommand dbCmd = new OleDbCommand(sql, conn);
            OleDbDataReader dbReader = dbCmd.ExecuteReader();

            while (dbReader.Read())
            {
                listaPagos.Add(new Pago(dbReader.GetValue(0).ToString(), 
                    int.Parse(dbReader.GetValue(1).ToString()),
                    DateTime.Parse(dbReader.GetValue(2).ToString())));
            }

            return listaPagos;
        }

        public static int getLastIDContrato(OleDbConnection conn)
        {
            string sql = "SELECT Id FROM Contrato ORDER BY Id DESC";

            OleDbCommand dbCmd = new OleDbCommand(sql, conn);
            OleDbDataReader dbReader = dbCmd.ExecuteReader();

            if (dbReader.Read())
            {
                return int.Parse(dbReader.GetValue(0).ToString());
            }
            else
            {
                return 0;
            }
        }

        public static int getLastIDNovedad(OleDbConnection conn)
        {
            string sql = "SELECT Id FROM Pagos WHERE Id > 0 ORDER BY Id DESC";

            OleDbCommand dbCmd = new OleDbCommand(sql, conn);
            OleDbDataReader dbReader = dbCmd.ExecuteReader();

            if (dbReader.Read())
            {
                return int.Parse(dbReader.GetValue(0).ToString());
            }
            else
            {
                return 0;
            }
        }

        public static int getNumberPagos(OleDbConnection conn, int id)
        {
            string sql = "SELECT COUNT(*) FROM Pagos WHERE Id <=" + id;

            OleDbCommand dbCmd = new OleDbCommand(sql, conn);
            OleDbDataReader dbReader = dbCmd.ExecuteReader();

            if (dbReader.Read())
            {
                return int.Parse(dbReader.GetValue(0).ToString());
            }
            else
            {
                return 0;
            }
        }
    }
}
