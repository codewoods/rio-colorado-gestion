﻿using Gestion_Minera.Ventanas.Alquileres.Clases;
using System;
using System.Collections.Generic;
using System.Data.OleDb;

using System.Windows.Forms;

namespace Gestion_Minera.Ventanas.Alquileres
{
    public partial class VerPagos : Form
    {
        OleDbConnection conn;
        Inmueble inmueble;

        public VerPagos(OleDbConnection conn, Inmueble inmueble)
        {
            InitializeComponent();

            this.conn = conn;
            this.inmueble = inmueble;

            numAnio.Value = DateTime.Today.Year;

            tbInmueble.Text = inmueble.ToString();
        }

        private void numAnio_ValueChanged(object sender, EventArgs e)
        {
            dgTabla.Rows.Clear();

            String[] row1 = new string[] { "", "", "", "", "", "", "", "", "", "", "", "" };
            String[] row2 = new string[] { "", "", "", "", "", "", "", "", "", "", "", "" };

            List<Pago> listaPagos = UtilAlquileres.getPagos(conn, inmueble.idInmueble, int.Parse(numAnio.Value.ToString()));

            for (int i = 1; i <= 12; i++)
            {
                foreach (Pago pago in listaPagos)
                {
                    if (pago.mes == i)
                    {
                        row1[i - 1] = "$" + pago.valor;
                        row2[i - 1] = pago.fechaCarga.ToShortDateString() + "\n" + pago.fechaCarga.ToShortTimeString();
                    }
                }
            }

            dgTabla.Rows.Add(row1);
            dgTabla.Rows.Add(row2);
        }
    }
}
