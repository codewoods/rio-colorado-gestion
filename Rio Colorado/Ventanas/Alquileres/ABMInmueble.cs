﻿using Gestion_Minera.Ventanas.Alquileres.Clases;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Windows.Forms;

namespace Gestion_Minera.Ventanas.Alquileres
{
    public partial class ABMInmueble : Form
    {
        OleDbConnection conn;

        public ABMInmueble(OleDbConnection conn)
        {
            InitializeComponent();

            this.conn = conn;

            actualizarComboInmuebles();
            editarEnabled(false);
        }

        private void tbDireccion_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter || (int)e.KeyChar == (int)Keys.Tab)
            {
                e.Handled = true;
                tbMedidorLuz.Focus();
            }
            else
            {
                e.KeyChar = Char.ToUpper(e.KeyChar);
            }
        }

        private void tbMedidorLuz_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter || (int)e.KeyChar == (int)Keys.Tab)
            {
                e.Handled = true;
                tbMedidorGas.Focus();
            }
            else
            {
                e.KeyChar = Char.ToUpper(e.KeyChar);
            }
        }

        private void tbMedidorGas_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter || (int)e.KeyChar == (int)Keys.Tab)
            {
                e.Handled = true;
                butAgregar.Focus();
            }
            else
            {
                e.KeyChar = Char.ToUpper(e.KeyChar);
            }
        }

        private void butAgregar_Click(object sender, EventArgs e)
        {
            String msg = "¿Desea agregar el siguiente Inmueble?\n\n" +
                "Dirección:\t\t" + tbDireccion.Text + "\n" +
                "N° medidor LUZ:\t\t" + tbMedidorLuz.Text + "\n" +
                "N° medidor GAS:\t\t" + tbMedidorGas.Text + "\n";

            DialogResult result = MessageBox.Show(msg, "Agregar Inmueble",
                MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                string sql = "INSERT INTO Inmueble (Direccion, MedidorLuz, MedidorGas) VALUES ('" +
                    tbDireccion.Text + "', '" +
                    tbMedidorLuz.Text + "', " +
                    "'" + tbMedidorGas.Text + "')";

                OleDbCommand oleDbCommand = new OleDbCommand(sql, conn);

                if (oleDbCommand.ExecuteNonQuery() != 1)
                {
                    MessageBox.Show("No se ha podido agregar el Inquilino", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    tbDireccion.Text = "";
                    tbMedidorLuz.Text = "";
                    tbMedidorGas.Text = "";

                    tbDireccion.Focus();

                    actualizarComboInmuebles();
                }
            }
        }

        public void actualizarComboInmuebles()
        {
            List<Inmueble> listaMuebles = UtilAlquileres.getInmuebles(conn);

            cbInmueble.Items.Clear();

            foreach (Inmueble inmueble in listaMuebles)
            {
                cbInmueble.Items.Add(inmueble);
            }

            cbInmueble.SelectedIndex = -1;
        }

        private void cbInmueble_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbInmueble.SelectedIndex > -1)
            {
                tbDireccionE.Text = ((Inmueble)cbInmueble.SelectedItem).direccion;
                tbMedidorLuzE.Text = ((Inmueble)cbInmueble.SelectedItem).medidorLuz;
                tbMedidorGasE.Text = ((Inmueble)cbInmueble.SelectedItem).medidorGas;
                editarEnabled(true);
                tbDireccionE.Focus();
            }
        }

        private void butCancelar_Click(object sender, EventArgs e)
        {
            cbInmueble.SelectedIndex = -1;
            editarEnabled(false);
        }

        public void editarEnabled(bool value)
        {
            tbDireccionE.Enabled = tbMedidorLuzE.Enabled = tbMedidorGasE.Enabled = value;
            if (!value) tbDireccionE.Text = tbMedidorLuzE.Text = tbMedidorGasE.Text = "";
        }

        private void tbDireccionE_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter || (int)e.KeyChar == (int)Keys.Tab)
            {
                e.Handled = true;
                tbMedidorLuzE.Focus();
            }
            else
            {
                e.KeyChar = Char.ToUpper(e.KeyChar);
            }
        }

        private void tbMedidorLuzE_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter || (int)e.KeyChar == (int)Keys.Tab)
            {
                e.Handled = true;
                tbMedidorGasE.Focus();
            }
            else
            {
                e.KeyChar = Char.ToUpper(e.KeyChar);
            }
        }

        private void tbMedidorGasE_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter || (int)e.KeyChar == (int)Keys.Tab)
            {
                e.Handled = true;
                buteEditar.Focus();
            }
            else
            {
                e.KeyChar = Char.ToUpper(e.KeyChar);
            }
        }

        private void buteEditar_Click(object sender, EventArgs e)
        {
            Inmueble inmueble = (Inmueble)cbInmueble.SelectedItem;

            string msg = "¿Realmente desea confirmar estos cambios?" +
            "\n\n" +
            "Dirección\t\t " + inmueble.direccion + " por " + tbDireccionE.Text.ToUpper() +
            "\nN° medidor LUZ\t\t " + inmueble.medidorLuz + " por " + tbMedidorLuzE.Text.ToUpper() +
            "\nN° medidor GAS\t\t " + inmueble.medidorGas + " por " + tbMedidorGasE.Text.ToUpper();

            DialogResult result = MessageBox.Show(msg, "Editar Inmueble",
MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                string sql = "UPDATE Inmueble SET Direccion = '" + tbDireccionE.Text.ToUpper() + "'" +
               ", MedidorLuz = '" + tbMedidorLuzE.Text.ToUpper() + "'" +
               ", MedidorGas = '" + tbMedidorGasE.Text.ToUpper() + "'" +
               " WHERE Id = " + inmueble.idInmueble;

                OleDbDataAdapter oledbAdapter = new OleDbDataAdapter();

                oledbAdapter.UpdateCommand = conn.CreateCommand();
                oledbAdapter.UpdateCommand.CommandText = sql;

                int i = oledbAdapter.UpdateCommand.ExecuteNonQuery();

                if (i != 1)
                {
                    MessageBox.Show("Los valores no se han podido modificar.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    actualizarComboInmuebles();
                    editarEnabled(false);
                }
            }
        }
    }
}
