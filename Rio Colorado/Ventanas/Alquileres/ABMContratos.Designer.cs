﻿namespace Gestion_Minera.Ventanas.Sueldos
{
    partial class ABMContratos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ABMContratos));
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.butVerContrato = new System.Windows.Forms.Button();
            this.butNuevoContrato = new System.Windows.Forms.Button();
            this.butEditarContrato = new System.Windows.Forms.Button();
            this.cbInmueble = new System.Windows.Forms.ComboBox();
            this.dgTablaContratos = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INQUILINO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DESDE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HASTA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CancelarContrato = new System.Windows.Forms.Button();
            this.tbComnetarios = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.guardarContrato = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.cbInquilinoContrato = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.dtHasta = new System.Windows.Forms.DateTimePicker();
            this.dtDesde = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.panContrato = new System.Windows.Forms.GroupBox();
            this.linkVer = new System.Windows.Forms.LinkLabel();
            this.adjuntarContrato = new System.Windows.Forms.Button();
            this.tbLinkContrato = new System.Windows.Forms.TextBox();
            this.panPeriodo = new System.Windows.Forms.GroupBox();
            this.dgTablaPeriodo = new System.Windows.Forms.DataGridView();
            this.IDD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COMENTARIO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.butAgregarPeriodo = new System.Windows.Forms.Button();
            this.butEliminarPeriodo = new System.Windows.Forms.Button();
            this.butModificarPeriodo = new System.Windows.Forms.Button();
            this.butEliminarContrato = new System.Windows.Forms.Button();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTablaContratos)).BeginInit();
            this.panContrato.SuspendLayout();
            this.panPeriodo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTablaPeriodo)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.White;
            this.groupBox3.Controls.Add(this.butEliminarContrato);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.butVerContrato);
            this.groupBox3.Controls.Add(this.butNuevoContrato);
            this.groupBox3.Controls.Add(this.butEditarContrato);
            this.groupBox3.Controls.Add(this.cbInmueble);
            this.groupBox3.Controls.Add(this.dgTablaContratos);
            this.groupBox3.Location = new System.Drawing.Point(12, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(456, 435);
            this.groupBox3.TabIndex = 54;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "CONTRATOS";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 75;
            this.label1.Text = "Inmueble:";
            // 
            // butVerContrato
            // 
            this.butVerContrato.Location = new System.Drawing.Point(188, 405);
            this.butVerContrato.Name = "butVerContrato";
            this.butVerContrato.Size = new System.Drawing.Size(100, 23);
            this.butVerContrato.TabIndex = 9;
            this.butVerContrato.Text = "VER DETALLES";
            this.butVerContrato.UseVisualStyleBackColor = true;
            this.butVerContrato.Click += new System.EventHandler(this.butVerContrato_Click);
            // 
            // butNuevoContrato
            // 
            this.butNuevoContrato.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butNuevoContrato.Location = new System.Drawing.Point(6, 405);
            this.butNuevoContrato.Name = "butNuevoContrato";
            this.butNuevoContrato.Size = new System.Drawing.Size(176, 23);
            this.butNuevoContrato.TabIndex = 10;
            this.butNuevoContrato.Text = "CREAR NUEVO CONTRATO";
            this.butNuevoContrato.UseVisualStyleBackColor = true;
            this.butNuevoContrato.Click += new System.EventHandler(this.butNuevoContrato_Click);
            // 
            // butEditarContrato
            // 
            this.butEditarContrato.Location = new System.Drawing.Point(294, 405);
            this.butEditarContrato.Name = "butEditarContrato";
            this.butEditarContrato.Size = new System.Drawing.Size(68, 23);
            this.butEditarContrato.TabIndex = 11;
            this.butEditarContrato.Text = "EDITAR";
            this.butEditarContrato.UseVisualStyleBackColor = true;
            this.butEditarContrato.Click += new System.EventHandler(this.butEditarContrato_Click);
            // 
            // cbInmueble
            // 
            this.cbInmueble.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbInmueble.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbInmueble.Location = new System.Drawing.Point(6, 46);
            this.cbInmueble.Name = "cbInmueble";
            this.cbInmueble.Size = new System.Drawing.Size(440, 23);
            this.cbInmueble.TabIndex = 8;
            this.cbInmueble.SelectedIndexChanged += new System.EventHandler(this.cbInmueble_SelectedIndexChanged);
            // 
            // dgTablaContratos
            // 
            this.dgTablaContratos.AllowUserToAddRows = false;
            this.dgTablaContratos.AllowUserToDeleteRows = false;
            this.dgTablaContratos.AllowUserToResizeRows = false;
            this.dgTablaContratos.BackgroundColor = System.Drawing.Color.White;
            this.dgTablaContratos.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgTablaContratos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgTablaContratos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgTablaContratos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.INQUILINO,
            this.DESDE,
            this.HASTA});
            this.dgTablaContratos.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.dgTablaContratos.Location = new System.Drawing.Point(6, 75);
            this.dgTablaContratos.MultiSelect = false;
            this.dgTablaContratos.Name = "dgTablaContratos";
            this.dgTablaContratos.ReadOnly = true;
            this.dgTablaContratos.RowHeadersVisible = false;
            this.dgTablaContratos.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgTablaContratos.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgTablaContratos.RowTemplate.ReadOnly = true;
            this.dgTablaContratos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgTablaContratos.Size = new System.Drawing.Size(440, 324);
            this.dgTablaContratos.TabIndex = 74;
            this.dgTablaContratos.SelectionChanged += new System.EventHandler(this.dgTablaContratos_SelectionChanged);
            // 
            // ID
            // 
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.ID.Visible = false;
            // 
            // INQUILINO
            // 
            this.INQUILINO.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.INQUILINO.HeaderText = "INQUILINO";
            this.INQUILINO.Name = "INQUILINO";
            this.INQUILINO.ReadOnly = true;
            // 
            // DESDE
            // 
            this.DESDE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.DESDE.HeaderText = "DESDE";
            this.DESDE.Name = "DESDE";
            this.DESDE.ReadOnly = true;
            this.DESDE.Width = 67;
            // 
            // HASTA
            // 
            this.HASTA.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.HASTA.HeaderText = "HASTA";
            this.HASTA.Name = "HASTA";
            this.HASTA.ReadOnly = true;
            this.HASTA.Width = 67;
            // 
            // CancelarContrato
            // 
            this.CancelarContrato.Location = new System.Drawing.Point(221, 211);
            this.CancelarContrato.Name = "CancelarContrato";
            this.CancelarContrato.Size = new System.Drawing.Size(90, 23);
            this.CancelarContrato.TabIndex = 75;
            this.CancelarContrato.Text = "CANCELAR";
            this.CancelarContrato.UseVisualStyleBackColor = true;
            this.CancelarContrato.Click += new System.EventHandler(this.CancelarContrato_Click);
            // 
            // tbComnetarios
            // 
            this.tbComnetarios.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbComnetarios.Location = new System.Drawing.Point(16, 104);
            this.tbComnetarios.Multiline = true;
            this.tbComnetarios.Name = "tbComnetarios";
            this.tbComnetarios.Size = new System.Drawing.Size(393, 70);
            this.tbComnetarios.TabIndex = 18;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(18, 84);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 15);
            this.label10.TabIndex = 74;
            this.label10.Text = "Comentarios";
            // 
            // guardarContrato
            // 
            this.guardarContrato.Location = new System.Drawing.Point(317, 211);
            this.guardarContrato.Name = "guardarContrato";
            this.guardarContrato.Size = new System.Drawing.Size(90, 23);
            this.guardarContrato.TabIndex = 19;
            this.guardarContrato.Text = "GUARDAR";
            this.guardarContrato.UseVisualStyleBackColor = true;
            this.guardarContrato.Click += new System.EventHandler(this.guardarContrato_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(18, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 15);
            this.label6.TabIndex = 56;
            this.label6.Text = "Inquilino";
            // 
            // cbInquilinoContrato
            // 
            this.cbInquilinoContrato.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbInquilinoContrato.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbInquilinoContrato.Location = new System.Drawing.Point(79, 22);
            this.cbInquilinoContrato.Name = "cbInquilinoContrato";
            this.cbInquilinoContrato.Size = new System.Drawing.Size(330, 23);
            this.cbInquilinoContrato.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(17, 55);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 15);
            this.label7.TabIndex = 60;
            this.label7.Text = "Fecha inicio";
            // 
            // dtHasta
            // 
            this.dtHasta.CustomFormat = "";
            this.dtHasta.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtHasta.Location = new System.Drawing.Point(301, 53);
            this.dtHasta.Name = "dtHasta";
            this.dtHasta.Size = new System.Drawing.Size(106, 20);
            this.dtHasta.TabIndex = 15;
            this.dtHasta.TabStop = false;
            // 
            // dtDesde
            // 
            this.dtDesde.CustomFormat = "";
            this.dtDesde.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDesde.Location = new System.Drawing.Point(99, 53);
            this.dtDesde.Name = "dtDesde";
            this.dtDesde.Size = new System.Drawing.Size(106, 20);
            this.dtDesde.TabIndex = 14;
            this.dtDesde.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(235, 56);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(57, 15);
            this.label8.TabIndex = 70;
            this.label8.Text = "Fecha fin";
            // 
            // panContrato
            // 
            this.panContrato.BackColor = System.Drawing.Color.White;
            this.panContrato.Controls.Add(this.linkVer);
            this.panContrato.Controls.Add(this.adjuntarContrato);
            this.panContrato.Controls.Add(this.tbLinkContrato);
            this.panContrato.Controls.Add(this.CancelarContrato);
            this.panContrato.Controls.Add(this.label6);
            this.panContrato.Controls.Add(this.tbComnetarios);
            this.panContrato.Controls.Add(this.label8);
            this.panContrato.Controls.Add(this.label10);
            this.panContrato.Controls.Add(this.dtDesde);
            this.panContrato.Controls.Add(this.guardarContrato);
            this.panContrato.Controls.Add(this.dtHasta);
            this.panContrato.Controls.Add(this.label7);
            this.panContrato.Controls.Add(this.cbInquilinoContrato);
            this.panContrato.Location = new System.Drawing.Point(474, 206);
            this.panContrato.Name = "panContrato";
            this.panContrato.Size = new System.Drawing.Size(418, 241);
            this.panContrato.TabIndex = 82;
            this.panContrato.TabStop = false;
            this.panContrato.Text = "INFORMACIÓN BÁSICA CONTRATO";
            // 
            // linkVer
            // 
            this.linkVer.AutoSize = true;
            this.linkVer.Location = new System.Drawing.Point(282, 184);
            this.linkVer.Name = "linkVer";
            this.linkVer.Size = new System.Drawing.Size(23, 13);
            this.linkVer.TabIndex = 78;
            this.linkVer.TabStop = true;
            this.linkVer.Text = "Ver";
            this.linkVer.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkVer_LinkClicked);
            // 
            // adjuntarContrato
            // 
            this.adjuntarContrato.Location = new System.Drawing.Point(311, 179);
            this.adjuntarContrato.Name = "adjuntarContrato";
            this.adjuntarContrato.Size = new System.Drawing.Size(98, 23);
            this.adjuntarContrato.TabIndex = 77;
            this.adjuntarContrato.Text = "Adjuntar contrato";
            this.adjuntarContrato.UseVisualStyleBackColor = true;
            this.adjuntarContrato.Click += new System.EventHandler(this.adjuntarContrato_Click);
            // 
            // tbLinkContrato
            // 
            this.tbLinkContrato.Location = new System.Drawing.Point(16, 181);
            this.tbLinkContrato.Name = "tbLinkContrato";
            this.tbLinkContrato.ReadOnly = true;
            this.tbLinkContrato.Size = new System.Drawing.Size(260, 20);
            this.tbLinkContrato.TabIndex = 76;
            this.tbLinkContrato.TextChanged += new System.EventHandler(this.tbLinkContrato_TextChanged);
            // 
            // panPeriodo
            // 
            this.panPeriodo.BackColor = System.Drawing.Color.White;
            this.panPeriodo.Controls.Add(this.dgTablaPeriodo);
            this.panPeriodo.Controls.Add(this.butAgregarPeriodo);
            this.panPeriodo.Controls.Add(this.butEliminarPeriodo);
            this.panPeriodo.Controls.Add(this.butModificarPeriodo);
            this.panPeriodo.Location = new System.Drawing.Point(474, 12);
            this.panPeriodo.Name = "panPeriodo";
            this.panPeriodo.Size = new System.Drawing.Size(418, 188);
            this.panPeriodo.TabIndex = 83;
            this.panPeriodo.TabStop = false;
            this.panPeriodo.Text = "PERÍODOS CONTRATO";
            // 
            // dgTablaPeriodo
            // 
            this.dgTablaPeriodo.AllowUserToAddRows = false;
            this.dgTablaPeriodo.AllowUserToDeleteRows = false;
            this.dgTablaPeriodo.AllowUserToResizeRows = false;
            this.dgTablaPeriodo.BackgroundColor = System.Drawing.Color.White;
            this.dgTablaPeriodo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgTablaPeriodo.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgTablaPeriodo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgTablaPeriodo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IDD,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.COMENTARIO});
            this.dgTablaPeriodo.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.dgTablaPeriodo.Location = new System.Drawing.Point(6, 19);
            this.dgTablaPeriodo.MultiSelect = false;
            this.dgTablaPeriodo.Name = "dgTablaPeriodo";
            this.dgTablaPeriodo.ReadOnly = true;
            this.dgTablaPeriodo.RowHeadersVisible = false;
            this.dgTablaPeriodo.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgTablaPeriodo.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgTablaPeriodo.RowTemplate.ReadOnly = true;
            this.dgTablaPeriodo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgTablaPeriodo.Size = new System.Drawing.Size(403, 132);
            this.dgTablaPeriodo.TabIndex = 75;
            this.dgTablaPeriodo.SelectionChanged += new System.EventHandler(this.dgTablaPeriodo_SelectionChanged_1);
            // 
            // IDD
            // 
            this.IDD.HeaderText = "IDD";
            this.IDD.Name = "IDD";
            this.IDD.ReadOnly = true;
            this.IDD.Visible = false;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn3.HeaderText = "DESDE";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 67;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn4.HeaderText = "HASTA";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 67;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn5.HeaderText = "VALOR";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 67;
            // 
            // COMENTARIO
            // 
            this.COMENTARIO.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.COMENTARIO.HeaderText = "COMENTARIO";
            this.COMENTARIO.Name = "COMENTARIO";
            this.COMENTARIO.ReadOnly = true;
            // 
            // butAgregarPeriodo
            // 
            this.butAgregarPeriodo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butAgregarPeriodo.Location = new System.Drawing.Point(6, 157);
            this.butAgregarPeriodo.Name = "butAgregarPeriodo";
            this.butAgregarPeriodo.Size = new System.Drawing.Size(186, 23);
            this.butAgregarPeriodo.TabIndex = 77;
            this.butAgregarPeriodo.Text = "AGREGAR NUEVO PERÍODO";
            this.butAgregarPeriodo.UseVisualStyleBackColor = true;
            this.butAgregarPeriodo.Click += new System.EventHandler(this.butAgregarPeriodo_Click);
            // 
            // butEliminarPeriodo
            // 
            this.butEliminarPeriodo.ForeColor = System.Drawing.Color.Red;
            this.butEliminarPeriodo.Location = new System.Drawing.Point(331, 157);
            this.butEliminarPeriodo.Name = "butEliminarPeriodo";
            this.butEliminarPeriodo.Size = new System.Drawing.Size(78, 23);
            this.butEliminarPeriodo.TabIndex = 79;
            this.butEliminarPeriodo.Text = "ELIMINAR";
            this.butEliminarPeriodo.UseVisualStyleBackColor = true;
            this.butEliminarPeriodo.Click += new System.EventHandler(this.butEliminarPeriodo_Click);
            // 
            // butModificarPeriodo
            // 
            this.butModificarPeriodo.Location = new System.Drawing.Point(247, 157);
            this.butModificarPeriodo.Name = "butModificarPeriodo";
            this.butModificarPeriodo.Size = new System.Drawing.Size(78, 23);
            this.butModificarPeriodo.TabIndex = 78;
            this.butModificarPeriodo.Text = "MODIFICAR";
            this.butModificarPeriodo.UseVisualStyleBackColor = true;
            this.butModificarPeriodo.Click += new System.EventHandler(this.butModificarPeriodo_Click);
            // 
            // butEliminarContrato
            // 
            this.butEliminarContrato.ForeColor = System.Drawing.Color.Red;
            this.butEliminarContrato.Location = new System.Drawing.Point(368, 405);
            this.butEliminarContrato.Name = "butEliminarContrato";
            this.butEliminarContrato.Size = new System.Drawing.Size(78, 23);
            this.butEliminarContrato.TabIndex = 80;
            this.butEliminarContrato.Text = "ELIMINAR";
            this.butEliminarContrato.UseVisualStyleBackColor = true;
            this.butEliminarContrato.Click += new System.EventHandler(this.butEliminarContrato_Click);
            // 
            // ABMContratos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(904, 459);
            this.Controls.Add(this.panPeriodo);
            this.Controls.Add(this.panContrato);
            this.Controls.Add(this.groupBox3);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ABMContratos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Agregar y editar Contratos";
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTablaContratos)).EndInit();
            this.panContrato.ResumeLayout(false);
            this.panContrato.PerformLayout();
            this.panPeriodo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgTablaPeriodo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button CancelarContrato;
        private System.Windows.Forms.TextBox tbComnetarios;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button guardarContrato;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbInquilinoContrato;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker dtHasta;
        private System.Windows.Forms.DateTimePicker dtDesde;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button butVerContrato;
        private System.Windows.Forms.Button butNuevoContrato;
        private System.Windows.Forms.Button butEditarContrato;
        private System.Windows.Forms.ComboBox cbInmueble;
        private System.Windows.Forms.DataGridView dgTablaContratos;
        private System.Windows.Forms.GroupBox panContrato;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox panPeriodo;
        private System.Windows.Forms.DataGridView dgTablaPeriodo;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDD;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn COMENTARIO;
        private System.Windows.Forms.Button butAgregarPeriodo;
        private System.Windows.Forms.Button butEliminarPeriodo;
        private System.Windows.Forms.Button butModificarPeriodo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn INQUILINO;
        private System.Windows.Forms.DataGridViewTextBoxColumn DESDE;
        private System.Windows.Forms.DataGridViewTextBoxColumn HASTA;
        private System.Windows.Forms.LinkLabel linkVer;
        private System.Windows.Forms.Button adjuntarContrato;
        private System.Windows.Forms.TextBox tbLinkContrato;
        private System.Windows.Forms.Button butEliminarContrato;
    }
}