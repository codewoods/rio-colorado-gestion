﻿using Gestion_Minera.Ventanas.Alquileres;
using Gestion_Minera.Ventanas.Alquileres.Clases;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Windows.Forms;

namespace Gestion_Minera.Ventanas.Sueldos
{
    public partial class CargaServicios : Form
    {
        OleDbConnection conn;

        List<Impuesto> listaImpuestos;
        List<InmuebleImpuesto> listaInmuebleImpuestoCargados;

        List<TextBox> listaValores;
        List<ComboBox> listaMes;
        List<NumericUpDown> listaAnio;
        List<Button> listaBotones;

        public CargaServicios(OleDbConnection conn)
        {
            InitializeComponent();

            this.conn = conn;

            listaValores = new List<TextBox>();

            listaValores.Add(tbLuz);
            listaValores.Add(tbGas);
            listaValores.Add(tbInm);
            listaValores.Add(tbMun);

            listaMes = new List<ComboBox>();

            listaMes.Add(cbLuz);
            listaMes.Add(cbGas);
            listaMes.Add(cbImpMob);
            listaMes.Add(cbImpMun);

            listaAnio = new List<NumericUpDown>();

            listaAnio.Add(numAnioLuz);
            listaAnio.Add(numAnioGas);
            listaAnio.Add(numAnioImpMob);
            listaAnio.Add(numAnioImpMun);

            if (DateTime.Today.Month == 1)
            {
                numAnioLuz.Value = numAnioGas.Value = numAnioImpMun.Value = numAnioImpMob.Value = DateTime.Today.Year - 1;
            }
            else
            {
                numAnioLuz.Value = numAnioGas.Value = numAnioImpMun.Value = numAnioImpMob.Value = DateTime.Today.Year;
            }

            foreach (ComboBox bm in listaMes)
            {
                bm.Items.Add("ENERO");
                bm.Items.Add("FEBRERO");
                bm.Items.Add("MARZO");
                bm.Items.Add("ABRIL");
                bm.Items.Add("MAYO");
                bm.Items.Add("JUNIO");
                bm.Items.Add("JULIO");
                bm.Items.Add("AGOSTO");
                bm.Items.Add("SEPTIEMBRE");
                bm.Items.Add("OCTUBRE");
                bm.Items.Add("NOVIEMBRE");
                bm.Items.Add("DICIEMBRE");
            }

            listaImpuestos = UtilAlquileres.getImpuestos(conn);

            foreach (Inmueble inmueble in UtilAlquileres.getInmuebles(conn))
            {
                cbInmueble.Items.Add(inmueble);
            }

            listaBotones = new List<Button>();

            listaBotones.Add(butBorrarLuz);
            listaBotones.Add(butBorrarGas);
            listaBotones.Add(butBorrarInm);
            listaBotones.Add(butBorrarMun);

            cbLuz.SelectedIndex = cbGas.SelectedIndex = cbImpMob.SelectedIndex = cbImpMun.SelectedIndex = DateTime.Today.AddMonths(-1).Month - 1;

            cbLuz.SelectedIndexChanged += new EventHandler(cbLuz_SelectedIndexChanged);
            cbGas.SelectedIndexChanged += new EventHandler(cbGas_SelectedIndexChanged);
            cbImpMob.SelectedIndexChanged += new EventHandler(cbImpMob_SelectedIndexChanged);
            cbImpMun.SelectedIndexChanged += new EventHandler(cbImpMun_SelectedIndexChanged);

            numAnioLuz.ValueChanged += new EventHandler(numAnioLuz_ValueChanged);
            numAnioGas.ValueChanged += new EventHandler(numAnioGas_ValueChanged);
            numAnioImpMun.ValueChanged += new EventHandler(numAnioImpMun_ValueChanged);
            numAnioImpMob.ValueChanged += new EventHandler(numAnioImpMob_ValueChanged);

            butCargarServicio.Enabled = butVerServicios.Enabled = panel.Enabled = cbInmueble.SelectedIndex > -1;
        }

        private void butAgregarServicio_Click(object sender, EventArgs e)
        {

            listaInmuebleImpuestoCargados[0].inmueble = (Inmueble)cbInmueble.SelectedItem;
            listaInmuebleImpuestoCargados[0].impuesto = listaImpuestos[0];
            listaInmuebleImpuestoCargados[0].mes = cbLuz.SelectedIndex + 1;
            listaInmuebleImpuestoCargados[0].año = int.Parse(numAnioLuz.Text);
            listaInmuebleImpuestoCargados[0].valor = tbLuz.Text;

            if (listaInmuebleImpuestoCargados[0].id > 0)
            {
                if (listaInmuebleImpuestoCargados[0].valor != tbLuz.Text ||
                  listaInmuebleImpuestoCargados[0].año != int.Parse(numAnioLuz.Text))
                {
                    listaInmuebleImpuestoCargados[0].fechaCarga = DateTime.Today;
                }
            }
            else
            {
                listaInmuebleImpuestoCargados[0].fechaCarga = DateTime.Today;
            }

            listaInmuebleImpuestoCargados[1].inmueble = (Inmueble)cbInmueble.SelectedItem;
            listaInmuebleImpuestoCargados[1].impuesto = listaImpuestos[1];
            listaInmuebleImpuestoCargados[1].mes = (cbGas.SelectedIndex * 2) + 1;
            listaInmuebleImpuestoCargados[1].año = int.Parse(numAnioGas.Text);
            listaInmuebleImpuestoCargados[1].valor = tbGas.Text;

            if (listaInmuebleImpuestoCargados[1].id > 0)
            {
                if (listaInmuebleImpuestoCargados[1].valor != tbGas.Text ||
                  listaInmuebleImpuestoCargados[1].año != int.Parse(numAnioGas.Text))
                {
                    listaInmuebleImpuestoCargados[1].fechaCarga = DateTime.Today;
                }
            }
            else
            {
                listaInmuebleImpuestoCargados[1].fechaCarga = DateTime.Today;
            }

            listaInmuebleImpuestoCargados[2].inmueble = (Inmueble)cbInmueble.SelectedItem;
            listaInmuebleImpuestoCargados[2].impuesto = listaImpuestos[2];
            listaInmuebleImpuestoCargados[2].mes = 1;
            listaInmuebleImpuestoCargados[2].año = int.Parse(numAnioImpMob.Text);
            listaInmuebleImpuestoCargados[2].valor = tbInm.Text;

            if (listaInmuebleImpuestoCargados[2].id > 0)
            {
                if (listaInmuebleImpuestoCargados[2].valor != tbInm.Text ||
                  listaInmuebleImpuestoCargados[2].año != int.Parse(numAnioImpMob.Text))
                {
                    listaInmuebleImpuestoCargados[2].fechaCarga = DateTime.Today;
                }
            }
            else
            {
                listaInmuebleImpuestoCargados[2].fechaCarga = DateTime.Today;
            }

            listaInmuebleImpuestoCargados[3].inmueble = (Inmueble)cbInmueble.SelectedItem;
            listaInmuebleImpuestoCargados[3].impuesto = listaImpuestos[3];
            listaInmuebleImpuestoCargados[3].mes = 1;
            listaInmuebleImpuestoCargados[3].año = int.Parse(numAnioImpMun.Text);
            listaInmuebleImpuestoCargados[3].valor = tbMun.Text;

            if (listaInmuebleImpuestoCargados[3].id > 0)
            {
                if (listaInmuebleImpuestoCargados[3].valor != tbMun.Text ||
                  listaInmuebleImpuestoCargados[3].año != int.Parse(numAnioImpMun.Text))
                {
                    listaInmuebleImpuestoCargados[3].fechaCarga = DateTime.Today;
                }
            }
            else
            {
                listaInmuebleImpuestoCargados[3].fechaCarga = DateTime.Today;
            }

            List<String> listaEventos = new List<string>();

            foreach (InmuebleImpuesto inmuebleImpuesto in listaInmuebleImpuestoCargados)
            {
                if (inmuebleImpuesto.id > 0)
                {
                    if (editarImpuesto(inmuebleImpuesto))
                    {
                        listaEventos.Add("El impuesto '" + inmuebleImpuesto.impuesto.nombre + "' ha sido modificado exitosamente.\n");
                    }
                    else
                    {
                        listaEventos.Add("ERROR: El impuesto '" + inmuebleImpuesto.impuesto.nombre + "' no se ha podido modificar.\n");
                    }
                }
                else
                {
                    if (inmuebleImpuesto.valor != "")
                    {
                        if (agregarImpuesto(inmuebleImpuesto))
                        {
                            listaEventos.Add("El impuesto '" + inmuebleImpuesto.impuesto.nombre + "' ha sido cargado exitosamente.\n");
                        }
                        else
                        {
                            listaEventos.Add("ERROR: El impuesto '" + inmuebleImpuesto.impuesto.nombre + "' no se ha podido cargar.\n");
                        }
                    }
                }
            }

            if (listaEventos.Count > 0)
            {
                String msj = "";

                foreach (String cad in listaEventos)
                {
                    msj += cad;
                }

                MessageBox.Show(msj, "RESULTADO DE LA OPERACIÓN");
            }

            buscarYCargarImpuestos();
        }

        public bool agregarImpuesto(InmuebleImpuesto inmuebleImpuesto)
        {
            string sql = "INSERT INTO InmuebleImpuesto (idInmueble, idImpuesto, FechaCarga, Mes, Valor, Anio) VALUES (" +
                inmuebleImpuesto.inmueble.idInmueble + ", " +
                inmuebleImpuesto.impuesto.id + ", " +
                "'" + inmuebleImpuesto.fechaCarga.ToString() + "', " +
                "'" + inmuebleImpuesto.mes + "', " +
                "'" + inmuebleImpuesto.valor + "', " +
                "'" + inmuebleImpuesto.año + "')";

            OleDbCommand oleDbCommand = new OleDbCommand(sql, conn);

            if (oleDbCommand.ExecuteNonQuery() != 1)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool editarImpuesto(InmuebleImpuesto inmuebleImpuesto)
        {
            string sql = "UPDATE InmuebleImpuesto SET FechaCarga = '" + inmuebleImpuesto.fechaCarga.ToString() + "'" +
               ", Mes = '" + inmuebleImpuesto.mes + "'" +
               ", Anio = '" + inmuebleImpuesto.año + "'" +
               ", Valor = '" + inmuebleImpuesto.valor + "'" +
               " WHERE Id = " + inmuebleImpuesto.id;

            OleDbDataAdapter oledbAdapter = new OleDbDataAdapter();

            oledbAdapter.UpdateCommand = conn.CreateCommand();
            oledbAdapter.UpdateCommand.CommandText = sql;

            if (oledbAdapter.UpdateCommand.ExecuteNonQuery() != 1)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private void cbInmueble_SelectedIndexChanged(object sender, EventArgs e)
        {
            butCargarServicio.Enabled = butVerServicios.Enabled = cbInmueble.SelectedIndex > -1;
        }

        private void butCargarServicio_Click(object sender, EventArgs e)
        {
            listaInmuebleImpuestoCargados = new List<InmuebleImpuesto>();

            listaInmuebleImpuestoCargados.Add(new InmuebleImpuesto());
            listaInmuebleImpuestoCargados.Add(new InmuebleImpuesto());
            listaInmuebleImpuestoCargados.Add(new InmuebleImpuesto());
            listaInmuebleImpuestoCargados.Add(new InmuebleImpuesto());

            buscarYCargarImpuestos();
        }

        public void cargarImpuestos(int i)
        {
            listaInmuebleImpuestoCargados[i] = UtilAlquileres.getInmuebleImpuesto(conn,
                ((Inmueble)cbInmueble.SelectedItem).idInmueble,
                i + 1,
                listaMes[i].SelectedIndex + 1,
                int.Parse(listaAnio[i].Value.ToString()));

            if (listaInmuebleImpuestoCargados[i].id > 0)
            {
                listaValores[i].Text = listaInmuebleImpuestoCargados[0].valor;
                listaBotones[i].Enabled = true;
            }
            else
            {
                listaValores[i].Text = "";
                listaBotones[i].Enabled = false;
            }
        }

        public void buscarYCargarImpuestos()
        {
            cbInmueble.Enabled = butCargarServicio.Enabled = false;
            panel.Enabled = true;

            cargarImpuestos(0);
            cargarImpuestos(1);
            cargarImpuestos(2);
            cargarImpuestos(3);
        }

        private void butCancelar_Click(object sender, EventArgs e)
        {
            cbGas.SelectedIndex = cbLuz.SelectedIndex = cbImpMob.SelectedIndex = cbImpMun.SelectedIndex = -1;
            tbGas.Text = tbLuz.Text = tbMun.Text = tbInm.Text = "";

            cbInmueble.Enabled = butCargarServicio.Enabled = true;
            panel.Enabled = false;
        }

        private void cbLuz_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargarImpuestos(0);
        }

        private void cbGas_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargarImpuestos(1);
        }

        private void cbImpMob_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargarImpuestos(2);
        }

        private void cbImpMun_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargarImpuestos(3);
        }

        private void numAnioLuz_ValueChanged(object sender, EventArgs e)
        {
            cargarImpuestos(0);
        }

        private void numAnioGas_ValueChanged(object sender, EventArgs e)
        {
            cargarImpuestos(1);
        }

        private void numAnioImpMun_ValueChanged(object sender, EventArgs e)
        {
            cargarImpuestos(2);
        }

        private void numAnioImpMob_ValueChanged(object sender, EventArgs e)
        {
            cargarImpuestos(3);
        }

        private void butVerServicios_Click(object sender, EventArgs e)
        {
            VerServicios verServicios = new VerServicios(conn, (Inmueble)cbInmueble.SelectedItem);
            verServicios.Show();
        }

        public void borrarServicio(int i)
        {
            string sql = "DELETE FROM InmuebleImpuesto " +
               "WHERE Id = " + listaInmuebleImpuestoCargados[i].id;

            OleDbDataAdapter oledbAdapter = new OleDbDataAdapter();

            oledbAdapter.UpdateCommand = conn.CreateCommand();
            oledbAdapter.UpdateCommand.CommandText = sql;

            if (oledbAdapter.UpdateCommand.ExecuteNonQuery() != 1)
            {
                MessageBox.Show("El valor no se ha podido eliminar.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("El valor se ha eliminado correctamente.", "Eliminar valores", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cargarImpuestos(i);
            }
        }

        private void butBorrarLuz_Click(object sender, EventArgs e)
        {
            borrarServicio(0);
        }

        private void butBorrarGas_Click(object sender, EventArgs e)
        {
            borrarServicio(1);
        }

        private void butBorrarInm_Click(object sender, EventArgs e)
        {
            borrarServicio(2);
        }

        private void butBorrarMun_Click(object sender, EventArgs e)
        {
            borrarServicio(3);
        }
    }
}
