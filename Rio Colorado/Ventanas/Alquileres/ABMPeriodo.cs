﻿using Gestion_Minera.Ventanas.Alquileres.Clases;
using System;
using System.Data.OleDb;
using System.Windows.Forms;

namespace Gestion_Minera.Ventanas.Alquileres
{
    public partial class ABMPeriodo : Form
    {
        OleDbConnection conn;
        Periodo periodo;
        int idContrato;

        public ABMPeriodo(OleDbConnection conn, Periodo periodo, int idContrato, DateTime min, DateTime max)
        {
            InitializeComponent();

            this.conn = conn;
            this.periodo = periodo;
            this.idContrato = idContrato;

            dtDesde.MinDate = dtDesde.Value = min;
            dtHasta.MaxDate = dtHasta.Value = max;

            if (periodo.id > 0)
            {
                butAgregar.Text = "MODIFICAR";
                this.Text = "Modificar período";

                try
                {
                    dtDesde.Value = periodo.fechaInicio;
                }
                catch (Exception ex)
                {
                }

                try
                {
                    dtHasta.Value = periodo.fechaFin;
                }
                catch (Exception ex)
                {
                }

                tbValor.Text = periodo.valor;
                tbComentario.Text = periodo.comentarios;
            }
        }

        private void butAgregar_Click(object sender, System.EventArgs e)
        {
            if (periodo.id > 0)
            {
                string msg = "¿Realmente desea confirmar estos cambios?" +
                "\n\n" +
                "Fecha inicio:\t" + periodo.fechaInicio + " por " + dtDesde.Value.ToShortDateString() +
                "\nFecha fin:\t" + periodo.fechaFin + " por " + dtHasta.Value.ToShortDateString() +
                "\nValor:\t\t" + periodo.valor + " por " + tbValor.Text +
                "\nComentario:\t" + periodo.comentarios + " por " + tbComentario.Text;

                DialogResult result = MessageBox.Show(msg, "Editar Período",
    MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    string sql = "UPDATE ContratoPeriodo SET Inicio = '" + dtDesde.Value.ToShortDateString() + "'" +
                   ", Fin = '" + dtHasta.Value.ToShortDateString() + "'" +
                   ", Valor = '" + tbValor.Text + "'" +
                   ", Comentario = '" + tbComentario.Text + "'" +
                   " WHERE Id = " + periodo.id;

                    OleDbDataAdapter oledbAdapter = new OleDbDataAdapter();

                    oledbAdapter.UpdateCommand = conn.CreateCommand();
                    oledbAdapter.UpdateCommand.CommandText = sql;

                    int i = oledbAdapter.UpdateCommand.ExecuteNonQuery();

                    if (i != 1)
                    {
                        MessageBox.Show("Los valores no se han podido modificar.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show("Los valores se han modificado correctamente.", "Modificar Período", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Close();
                    }
                }
            }
            else
            {
                string msg = "¿Realmente desea agregar el siguiente período?" +
                "\n\n" +
                "Fecha inicio:\t" + dtDesde.Value.ToShortDateString() +
                "\nFecha fin:\t" + dtHasta.Value.ToShortDateString() +
                "\nValor:\t\t" + tbValor.Text +
                "\nComentario:\t" + tbComentario.Text;

                DialogResult result = MessageBox.Show(msg, "Editar Período",
                    MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    string sql = "INSERT INTO ContratoPeriodo (idContrato, Inicio, Fin, Valor, Comentario) VALUES (" +
                        idContrato + ", " +
                        "'" + dtDesde.Value.ToShortDateString() + "', " +
                        "'" + dtHasta.Value.ToShortDateString() + "', " +
                        "'" + tbValor.Text + "', " +
                        "'" + tbComentario.Text + "')";

                    OleDbCommand oleDbCommand = new OleDbCommand(sql, conn);

                    if (oleDbCommand.ExecuteNonQuery() != 1)
                    {
                        MessageBox.Show("No se ha podido agregar el Período", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show("El período se ha agregado correctamente.", "Agregar Período", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Close();
                    }
                }
            }
        }
    }
}
