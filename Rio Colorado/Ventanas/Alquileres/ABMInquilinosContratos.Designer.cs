﻿namespace Gestion_Minera.Ventanas.Alquileres
{
    partial class ABMInquilinosContratos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ABMInquilinosContratos));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbNombre = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.butAgregarInquilino = new System.Windows.Forms.Button();
            this.tbApellido = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.buteEditar = new System.Windows.Forms.Button();
            this.butCancelar = new System.Windows.Forms.Button();
            this.cbInquilino = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbNombreE = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbApellidoE = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.butVerInquilino = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.cbVerInquilino = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.tbNombre);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.butAgregarInquilino);
            this.groupBox1.Controls.Add(this.tbApellido);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(276, 141);
            this.groupBox1.TabIndex = 45;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "AGREGAR INQUILINO";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(13, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 15);
            this.label2.TabIndex = 52;
            this.label2.Text = "Nombre";
            // 
            // tbNombre
            // 
            this.tbNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNombre.Location = new System.Drawing.Point(74, 48);
            this.tbNombre.Name = "tbNombre";
            this.tbNombre.Size = new System.Drawing.Size(189, 21);
            this.tbNombre.TabIndex = 2;
            this.tbNombre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbNombre_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 15);
            this.label1.TabIndex = 46;
            this.label1.Text = "Apellido";
            // 
            // butAgregarInquilino
            // 
            this.butAgregarInquilino.Location = new System.Drawing.Point(188, 77);
            this.butAgregarInquilino.Name = "butAgregarInquilino";
            this.butAgregarInquilino.Size = new System.Drawing.Size(75, 23);
            this.butAgregarInquilino.TabIndex = 3;
            this.butAgregarInquilino.Text = "AGREGAR";
            this.butAgregarInquilino.UseVisualStyleBackColor = true;
            this.butAgregarInquilino.Click += new System.EventHandler(this.butAgregarInquilino_Click);
            // 
            // tbApellido
            // 
            this.tbApellido.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbApellido.Location = new System.Drawing.Point(74, 21);
            this.tbApellido.Name = "tbApellido";
            this.tbApellido.Size = new System.Drawing.Size(189, 21);
            this.tbApellido.TabIndex = 1;
            this.tbApellido.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbApellido_KeyPress);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.White;
            this.groupBox2.Controls.Add(this.buteEditar);
            this.groupBox2.Controls.Add(this.butCancelar);
            this.groupBox2.Controls.Add(this.cbInquilino);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.tbNombreE);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.tbApellidoE);
            this.groupBox2.Location = new System.Drawing.Point(294, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(273, 141);
            this.groupBox2.TabIndex = 53;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "EDITAR INQUILINO";
            // 
            // buteEditar
            // 
            this.buteEditar.Location = new System.Drawing.Point(132, 107);
            this.buteEditar.Name = "buteEditar";
            this.buteEditar.Size = new System.Drawing.Size(130, 23);
            this.buteEditar.TabIndex = 7;
            this.buteEditar.Text = "CONFIRMAR CAMBIOS";
            this.buteEditar.UseVisualStyleBackColor = true;
            this.buteEditar.Click += new System.EventHandler(this.buteEditar_Click);
            // 
            // butCancelar
            // 
            this.butCancelar.Location = new System.Drawing.Point(50, 107);
            this.butCancelar.Name = "butCancelar";
            this.butCancelar.Size = new System.Drawing.Size(75, 23);
            this.butCancelar.TabIndex = 54;
            this.butCancelar.Text = "CANCELAR";
            this.butCancelar.UseVisualStyleBackColor = true;
            this.butCancelar.Click += new System.EventHandler(this.butCancelar_Click);
            // 
            // cbInquilino
            // 
            this.cbInquilino.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbInquilino.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbInquilino.Location = new System.Drawing.Point(15, 19);
            this.cbInquilino.Name = "cbInquilino";
            this.cbInquilino.Size = new System.Drawing.Size(247, 23);
            this.cbInquilino.TabIndex = 4;
            this.cbInquilino.SelectedIndexChanged += new System.EventHandler(this.cbInquilino_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(16, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 15);
            this.label3.TabIndex = 52;
            this.label3.Text = "Nombre";
            // 
            // tbNombreE
            // 
            this.tbNombreE.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNombreE.Location = new System.Drawing.Point(73, 77);
            this.tbNombreE.Name = "tbNombreE";
            this.tbNombreE.Size = new System.Drawing.Size(189, 21);
            this.tbNombreE.TabIndex = 6;
            this.tbNombreE.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbNombreE_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(15, 54);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 15);
            this.label4.TabIndex = 46;
            this.label4.Text = "Apellido";
            // 
            // tbApellidoE
            // 
            this.tbApellidoE.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbApellidoE.Location = new System.Drawing.Point(72, 51);
            this.tbApellidoE.Name = "tbApellidoE";
            this.tbApellidoE.Size = new System.Drawing.Size(189, 21);
            this.tbApellidoE.TabIndex = 5;
            this.tbApellidoE.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbApellidoE_KeyPress);
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.White;
            this.groupBox4.Controls.Add(this.butVerInquilino);
            this.groupBox4.Controls.Add(this.button1);
            this.groupBox4.Controls.Add(this.cbVerInquilino);
            this.groupBox4.Location = new System.Drawing.Point(12, 159);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(555, 56);
            this.groupBox4.TabIndex = 54;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "VER CONTRATO POR INQUILINO";
            // 
            // butVerInquilino
            // 
            this.butVerInquilino.Location = new System.Drawing.Point(418, 20);
            this.butVerInquilino.Name = "butVerInquilino";
            this.butVerInquilino.Size = new System.Drawing.Size(130, 23);
            this.butVerInquilino.TabIndex = 77;
            this.butVerInquilino.Text = "VER CONTRATO";
            this.butVerInquilino.UseVisualStyleBackColor = true;
            this.butVerInquilino.Click += new System.EventHandler(this.buetVerInquilino_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(188, 77);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "AGREGAR";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // cbVerInquilino
            // 
            this.cbVerInquilino.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbVerInquilino.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbVerInquilino.Location = new System.Drawing.Point(16, 20);
            this.cbVerInquilino.Name = "cbVerInquilino";
            this.cbVerInquilino.Size = new System.Drawing.Size(390, 23);
            this.cbVerInquilino.TabIndex = 76;
            this.cbVerInquilino.SelectedIndexChanged += new System.EventHandler(this.cbVerInquilino_SelectedIndexChanged);
            // 
            // ABMInquilinosContratos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(578, 222);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ABMInquilinosContratos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Agregar y editar Inquilinos y Contratos";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbNombre;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button butAgregarInquilino;
        private System.Windows.Forms.TextBox tbApellido;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbNombreE;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbApellidoE;
        private System.Windows.Forms.ComboBox cbInquilino;
        private System.Windows.Forms.Button buteEditar;
        private System.Windows.Forms.Button butCancelar;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button butVerInquilino;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox cbVerInquilino;
    }
}