﻿namespace Gestion_Minera.Ventanas.Alquileres
{
    partial class VerServicios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VerServicios));
            this.dgTabla = new System.Windows.Forms.DataGridView();
            this.IMPUESTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ene = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Feb = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Abr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.May = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Jun = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Jul = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ago = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Sep = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Oct = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nov = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Dic = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.tbInmueble = new System.Windows.Forms.TextBox();
            this.numAnio = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.dgTabla)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numAnio)).BeginInit();
            this.SuspendLayout();
            // 
            // dgTabla
            // 
            this.dgTabla.AllowUserToAddRows = false;
            this.dgTabla.AllowUserToDeleteRows = false;
            this.dgTabla.BackgroundColor = System.Drawing.Color.White;
            this.dgTabla.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgTabla.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgTabla.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgTabla.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IMPUESTO,
            this.Ene,
            this.Feb,
            this.Mar,
            this.Abr,
            this.May,
            this.Jun,
            this.Jul,
            this.Ago,
            this.Sep,
            this.Oct,
            this.Nov,
            this.Dic});
            this.dgTabla.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.dgTabla.Location = new System.Drawing.Point(12, 36);
            this.dgTabla.MultiSelect = false;
            this.dgTabla.Name = "dgTabla";
            this.dgTabla.ReadOnly = true;
            this.dgTabla.RowHeadersVisible = false;
            this.dgTabla.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgTabla.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgTabla.RowTemplate.ReadOnly = true;
            this.dgTabla.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgTabla.Size = new System.Drawing.Size(994, 149);
            this.dgTabla.TabIndex = 0;
            // 
            // IMPUESTO
            // 
            this.IMPUESTO.HeaderText = "IMPUESTO";
            this.IMPUESTO.Name = "IMPUESTO";
            this.IMPUESTO.ReadOnly = true;
            this.IMPUESTO.Width = 150;
            // 
            // Ene
            // 
            this.Ene.HeaderText = "ENE";
            this.Ene.Name = "Ene";
            this.Ene.ReadOnly = true;
            this.Ene.Width = 70;
            // 
            // Feb
            // 
            this.Feb.HeaderText = "FEB";
            this.Feb.Name = "Feb";
            this.Feb.ReadOnly = true;
            this.Feb.Width = 70;
            // 
            // Mar
            // 
            this.Mar.HeaderText = "MAR";
            this.Mar.Name = "Mar";
            this.Mar.ReadOnly = true;
            this.Mar.Width = 70;
            // 
            // Abr
            // 
            this.Abr.HeaderText = "ABR";
            this.Abr.Name = "Abr";
            this.Abr.ReadOnly = true;
            this.Abr.Width = 70;
            // 
            // May
            // 
            this.May.HeaderText = "MAY";
            this.May.Name = "May";
            this.May.ReadOnly = true;
            this.May.Width = 70;
            // 
            // Jun
            // 
            this.Jun.HeaderText = "JUN";
            this.Jun.Name = "Jun";
            this.Jun.ReadOnly = true;
            this.Jun.Width = 70;
            // 
            // Jul
            // 
            this.Jul.HeaderText = "JUL";
            this.Jul.Name = "Jul";
            this.Jul.ReadOnly = true;
            this.Jul.Width = 70;
            // 
            // Ago
            // 
            this.Ago.HeaderText = "AGO";
            this.Ago.Name = "Ago";
            this.Ago.ReadOnly = true;
            this.Ago.Width = 70;
            // 
            // Sep
            // 
            this.Sep.HeaderText = "SEP";
            this.Sep.Name = "Sep";
            this.Sep.ReadOnly = true;
            this.Sep.Width = 70;
            // 
            // Oct
            // 
            this.Oct.HeaderText = "OCT";
            this.Oct.Name = "Oct";
            this.Oct.ReadOnly = true;
            this.Oct.Width = 70;
            // 
            // Nov
            // 
            this.Nov.HeaderText = "NOV";
            this.Nov.Name = "Nov";
            this.Nov.ReadOnly = true;
            this.Nov.Width = 70;
            // 
            // Dic
            // 
            this.Dic.HeaderText = "DIC";
            this.Dic.Name = "Dic";
            this.Dic.ReadOnly = true;
            this.Dic.Width = 70;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(14, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 15);
            this.label1.TabIndex = 99;
            this.label1.Text = "Inmueble";
            // 
            // tbInmueble
            // 
            this.tbInmueble.Enabled = false;
            this.tbInmueble.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbInmueble.Location = new System.Drawing.Point(79, 8);
            this.tbInmueble.Name = "tbInmueble";
            this.tbInmueble.Size = new System.Drawing.Size(403, 22);
            this.tbInmueble.TabIndex = 101;
            this.tbInmueble.TabStop = false;
            // 
            // numAnio
            // 
            this.numAnio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.numAnio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numAnio.Location = new System.Drawing.Point(488, 8);
            this.numAnio.Maximum = new decimal(new int[] {
            2050,
            0,
            0,
            0});
            this.numAnio.Minimum = new decimal(new int[] {
            2010,
            0,
            0,
            0});
            this.numAnio.Name = "numAnio";
            this.numAnio.ReadOnly = true;
            this.numAnio.Size = new System.Drawing.Size(65, 21);
            this.numAnio.TabIndex = 102;
            this.numAnio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numAnio.Value = new decimal(new int[] {
            2010,
            0,
            0,
            0});
            this.numAnio.ValueChanged += new System.EventHandler(this.numAnio_ValueChanged);
            // 
            // VerServicios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1018, 162);
            this.Controls.Add(this.numAnio);
            this.Controls.Add(this.tbInmueble);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgTabla);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "VerServicios";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Servicios";
            ((System.ComponentModel.ISupportInitialize)(this.dgTabla)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numAnio)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridViewTextBoxColumn IMPUESTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ene;
        private System.Windows.Forms.DataGridViewTextBoxColumn Feb;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mar;
        private System.Windows.Forms.DataGridViewTextBoxColumn Abr;
        private System.Windows.Forms.DataGridViewTextBoxColumn May;
        private System.Windows.Forms.DataGridViewTextBoxColumn Jun;
        private System.Windows.Forms.DataGridViewTextBoxColumn Jul;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ago;
        private System.Windows.Forms.DataGridViewTextBoxColumn Sep;
        private System.Windows.Forms.DataGridViewTextBoxColumn Oct;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nov;
        private System.Windows.Forms.DataGridViewTextBoxColumn Dic;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbInmueble;
        private System.Windows.Forms.DataGridView dgTabla;
        private System.Windows.Forms.NumericUpDown numAnio;
    }
}