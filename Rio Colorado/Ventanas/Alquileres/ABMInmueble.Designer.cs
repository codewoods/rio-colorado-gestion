﻿namespace Gestion_Minera.Ventanas.Alquileres
{
    partial class ABMInmueble
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ABMInmueble));
            this.butCancelar = new System.Windows.Forms.Button();
            this.cbInmueble = new System.Windows.Forms.ComboBox();
            this.buteEditar = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbMedidorGas = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.tbMedidorLuz = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.butAgregar = new System.Windows.Forms.Button();
            this.tbDireccion = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tbMedidorGasE = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbMedidorLuzE = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tbDireccionE = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // butCancelar
            // 
            this.butCancelar.Location = new System.Drawing.Point(148, 149);
            this.butCancelar.Name = "butCancelar";
            this.butCancelar.Size = new System.Drawing.Size(75, 23);
            this.butCancelar.TabIndex = 42;
            this.butCancelar.Text = "CANCELAR";
            this.butCancelar.UseVisualStyleBackColor = true;
            this.butCancelar.Click += new System.EventHandler(this.butCancelar_Click);
            // 
            // cbInmueble
            // 
            this.cbInmueble.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbInmueble.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbInmueble.Location = new System.Drawing.Point(16, 25);
            this.cbInmueble.Name = "cbInmueble";
            this.cbInmueble.Size = new System.Drawing.Size(343, 23);
            this.cbInmueble.TabIndex = 41;
            this.cbInmueble.SelectedIndexChanged += new System.EventHandler(this.cbInmueble_SelectedIndexChanged);
            // 
            // buteEditar
            // 
            this.buteEditar.Location = new System.Drawing.Point(229, 149);
            this.buteEditar.Name = "buteEditar";
            this.buteEditar.Size = new System.Drawing.Size(130, 23);
            this.buteEditar.TabIndex = 52;
            this.buteEditar.Text = "CONFIRMAR CAMBIOS";
            this.buteEditar.UseVisualStyleBackColor = true;
            this.buteEditar.Click += new System.EventHandler(this.buteEditar_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.tbMedidorGas);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.tbMedidorLuz);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.butAgregar);
            this.groupBox1.Controls.Add(this.tbDireccion);
            this.groupBox1.Location = new System.Drawing.Point(8, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(374, 183);
            this.groupBox1.TabIndex = 44;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "AGREGAR INMUEBLE";
            // 
            // tbMedidorGas
            // 
            this.tbMedidorGas.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMedidorGas.Location = new System.Drawing.Point(250, 82);
            this.tbMedidorGas.Name = "tbMedidorGas";
            this.tbMedidorGas.Size = new System.Drawing.Size(109, 21);
            this.tbMedidorGas.TabIndex = 3;
            this.tbMedidorGas.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbMedidorGas_KeyPress);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(133, 85);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(112, 15);
            this.label15.TabIndex = 49;
            this.label15.Text = "N° MEDIDOR GAS:";
            // 
            // tbMedidorLuz
            // 
            this.tbMedidorLuz.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMedidorLuz.Location = new System.Drawing.Point(250, 55);
            this.tbMedidorLuz.Name = "tbMedidorLuz";
            this.tbMedidorLuz.Size = new System.Drawing.Size(109, 21);
            this.tbMedidorLuz.TabIndex = 2;
            this.tbMedidorLuz.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbMedidorLuz_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(133, 58);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(111, 15);
            this.label9.TabIndex = 47;
            this.label9.Text = "N° MEDIDOR LUZ:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 15);
            this.label1.TabIndex = 46;
            this.label1.Text = "DIRECCIÓN";
            // 
            // butAgregar
            // 
            this.butAgregar.Location = new System.Drawing.Point(284, 149);
            this.butAgregar.Name = "butAgregar";
            this.butAgregar.Size = new System.Drawing.Size(75, 23);
            this.butAgregar.TabIndex = 4;
            this.butAgregar.Text = "AGREGAR";
            this.butAgregar.UseVisualStyleBackColor = true;
            this.butAgregar.Click += new System.EventHandler(this.butAgregar_Click);
            // 
            // tbDireccion
            // 
            this.tbDireccion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbDireccion.Location = new System.Drawing.Point(92, 21);
            this.tbDireccion.Name = "tbDireccion";
            this.tbDireccion.Size = new System.Drawing.Size(267, 21);
            this.tbDireccion.TabIndex = 1;
            this.tbDireccion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbDireccion_KeyPress);
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.White;
            this.groupBox4.Controls.Add(this.tbMedidorGasE);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.tbMedidorLuzE);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.buteEditar);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.tbDireccionE);
            this.groupBox4.Controls.Add(this.butCancelar);
            this.groupBox4.Controls.Add(this.cbInmueble);
            this.groupBox4.Location = new System.Drawing.Point(388, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(374, 183);
            this.groupBox4.TabIndex = 51;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "EDITAR INMUEBLE";
            // 
            // tbMedidorGasE
            // 
            this.tbMedidorGasE.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMedidorGasE.Location = new System.Drawing.Point(229, 120);
            this.tbMedidorGasE.Name = "tbMedidorGasE";
            this.tbMedidorGasE.Size = new System.Drawing.Size(130, 21);
            this.tbMedidorGasE.TabIndex = 7;
            this.tbMedidorGasE.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbMedidorGasE_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(107, 123);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 15);
            this.label3.TabIndex = 49;
            this.label3.Text = "N° MEDIDOR GAS:";
            // 
            // tbMedidorLuzE
            // 
            this.tbMedidorLuzE.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMedidorLuzE.Location = new System.Drawing.Point(229, 93);
            this.tbMedidorLuzE.Name = "tbMedidorLuzE";
            this.tbMedidorLuzE.Size = new System.Drawing.Size(130, 21);
            this.tbMedidorLuzE.TabIndex = 6;
            this.tbMedidorLuzE.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbMedidorLuzE_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(107, 96);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(111, 15);
            this.label5.TabIndex = 47;
            this.label5.Text = "N° MEDIDOR LUZ:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(16, 62);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 15);
            this.label6.TabIndex = 46;
            this.label6.Text = "DIRECCIÓN";
            // 
            // tbDireccionE
            // 
            this.tbDireccionE.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbDireccionE.Location = new System.Drawing.Point(92, 59);
            this.tbDireccionE.Name = "tbDireccionE";
            this.tbDireccionE.Size = new System.Drawing.Size(267, 21);
            this.tbDireccionE.TabIndex = 5;
            this.tbDireccionE.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbDireccionE_KeyPress);
            // 
            // ABMInmueble
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(772, 201);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ABMInmueble";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Crear y editar Inmuebles";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button butCancelar;
        private System.Windows.Forms.ComboBox cbInmueble;
        private System.Windows.Forms.Button buteEditar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button butAgregar;
        private System.Windows.Forms.TextBox tbDireccion;
        private System.Windows.Forms.TextBox tbMedidorGas;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox tbMedidorLuz;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox tbMedidorGasE;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbMedidorLuzE;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbDireccionE;
    }
}