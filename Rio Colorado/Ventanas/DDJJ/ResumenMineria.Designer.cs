﻿namespace Gestion_Minera.Ventanas.DDJJ
{
    partial class ResumenMineria
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ResumenMineria));
            this.numAnio = new System.Windows.Forms.NumericUpDown();
            this.dgTabla = new System.Windows.Forms.DataGridView();
            this.IMPUESTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ene = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Feb = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Abr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.May = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Jun = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Jul = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ago = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Sep = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Oct = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nov = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Dic = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cbMineral = new System.Windows.Forms.ComboBox();
            this.butImprimirTabla = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.butGuardarValores = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tbDic = new System.Windows.Forms.TextBox();
            this.tbNov = new System.Windows.Forms.TextBox();
            this.tbOct = new System.Windows.Forms.TextBox();
            this.tbSep = new System.Windows.Forms.TextBox();
            this.tbAgo = new System.Windows.Forms.TextBox();
            this.tbJul = new System.Windows.Forms.TextBox();
            this.tbJun = new System.Windows.Forms.TextBox();
            this.tbMay = new System.Windows.Forms.TextBox();
            this.tbAbr = new System.Windows.Forms.TextBox();
            this.tbMar = new System.Windows.Forms.TextBox();
            this.tbFeb = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbEne = new System.Windows.Forms.TextBox();
            this.numCombustibleAnio = new System.Windows.Forms.NumericUpDown();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tbTotalTon = new System.Windows.Forms.TextBox();
            this.tbTotalM3 = new System.Windows.Forms.TextBox();
            this.tbTotalMineria = new System.Windows.Forms.TextBox();
            this.tbTotalEnte = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numAnio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgTabla)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numCombustibleAnio)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // numAnio
            // 
            this.numAnio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numAnio.Location = new System.Drawing.Point(212, 13);
            this.numAnio.Maximum = new decimal(new int[] {
            2050,
            0,
            0,
            0});
            this.numAnio.Minimum = new decimal(new int[] {
            2010,
            0,
            0,
            0});
            this.numAnio.Name = "numAnio";
            this.numAnio.ReadOnly = true;
            this.numAnio.Size = new System.Drawing.Size(80, 21);
            this.numAnio.TabIndex = 104;
            this.numAnio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numAnio.Value = new decimal(new int[] {
            2010,
            0,
            0,
            0});
            this.numAnio.ValueChanged += new System.EventHandler(this.numAnio_ValueChanged);
            // 
            // dgTabla
            // 
            this.dgTabla.AllowUserToAddRows = false;
            this.dgTabla.AllowUserToDeleteRows = false;
            this.dgTabla.BackgroundColor = System.Drawing.Color.White;
            this.dgTabla.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgTabla.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgTabla.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgTabla.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IMPUESTO,
            this.Ene,
            this.Feb,
            this.Mar,
            this.Abr,
            this.May,
            this.Jun,
            this.Jul,
            this.Ago,
            this.Sep,
            this.Oct,
            this.Nov,
            this.Dic});
            this.dgTabla.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.dgTabla.Location = new System.Drawing.Point(12, 40);
            this.dgTabla.MultiSelect = false;
            this.dgTabla.Name = "dgTabla";
            this.dgTabla.ReadOnly = true;
            this.dgTabla.RowHeadersVisible = false;
            this.dgTabla.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgTabla.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgTabla.RowTemplate.ReadOnly = true;
            this.dgTabla.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgTabla.Size = new System.Drawing.Size(1161, 111);
            this.dgTabla.TabIndex = 103;
            // 
            // IMPUESTO
            // 
            this.IMPUESTO.HeaderText = "";
            this.IMPUESTO.Name = "IMPUESTO";
            this.IMPUESTO.ReadOnly = true;
            this.IMPUESTO.Width = 200;
            // 
            // Ene
            // 
            this.Ene.HeaderText = "ENE";
            this.Ene.Name = "Ene";
            this.Ene.ReadOnly = true;
            this.Ene.Width = 80;
            // 
            // Feb
            // 
            this.Feb.HeaderText = "FEB";
            this.Feb.Name = "Feb";
            this.Feb.ReadOnly = true;
            this.Feb.Width = 80;
            // 
            // Mar
            // 
            this.Mar.HeaderText = "MAR";
            this.Mar.Name = "Mar";
            this.Mar.ReadOnly = true;
            this.Mar.Width = 80;
            // 
            // Abr
            // 
            this.Abr.HeaderText = "ABR";
            this.Abr.Name = "Abr";
            this.Abr.ReadOnly = true;
            this.Abr.Width = 80;
            // 
            // May
            // 
            this.May.HeaderText = "MAY";
            this.May.Name = "May";
            this.May.ReadOnly = true;
            this.May.Width = 80;
            // 
            // Jun
            // 
            this.Jun.HeaderText = "JUN";
            this.Jun.Name = "Jun";
            this.Jun.ReadOnly = true;
            this.Jun.Width = 80;
            // 
            // Jul
            // 
            this.Jul.HeaderText = "JUL";
            this.Jul.Name = "Jul";
            this.Jul.ReadOnly = true;
            this.Jul.Width = 80;
            // 
            // Ago
            // 
            this.Ago.HeaderText = "AGO";
            this.Ago.Name = "Ago";
            this.Ago.ReadOnly = true;
            this.Ago.Width = 80;
            // 
            // Sep
            // 
            this.Sep.HeaderText = "SEP";
            this.Sep.Name = "Sep";
            this.Sep.ReadOnly = true;
            this.Sep.Width = 80;
            // 
            // Oct
            // 
            this.Oct.HeaderText = "OCT";
            this.Oct.Name = "Oct";
            this.Oct.ReadOnly = true;
            this.Oct.Width = 80;
            // 
            // Nov
            // 
            this.Nov.HeaderText = "NOV";
            this.Nov.Name = "Nov";
            this.Nov.ReadOnly = true;
            this.Nov.Width = 80;
            // 
            // Dic
            // 
            this.Dic.HeaderText = "DIC";
            this.Dic.Name = "Dic";
            this.Dic.ReadOnly = true;
            this.Dic.Width = 80;
            // 
            // cbMineral
            // 
            this.cbMineral.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMineral.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbMineral.Location = new System.Drawing.Point(12, 12);
            this.cbMineral.Name = "cbMineral";
            this.cbMineral.Size = new System.Drawing.Size(194, 23);
            this.cbMineral.TabIndex = 105;
            this.cbMineral.SelectedIndexChanged += new System.EventHandler(this.cbMineral_SelectedIndexChanged);
            // 
            // butImprimirTabla
            // 
            this.butImprimirTabla.Location = new System.Drawing.Point(12, 157);
            this.butImprimirTabla.Name = "butImprimirTabla";
            this.butImprimirTabla.Size = new System.Drawing.Size(113, 53);
            this.butImprimirTabla.TabIndex = 107;
            this.butImprimirTabla.Text = "IMPRIMIR TABLA";
            this.butImprimirTabla.UseVisualStyleBackColor = true;
            this.butImprimirTabla.Click += new System.EventHandler(this.butImprimirTabla_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox1.Controls.Add(this.butGuardarValores);
            this.groupBox1.Controls.Add(this.tableLayoutPanel1);
            this.groupBox1.Controls.Add(this.numCombustibleAnio);
            this.groupBox1.Location = new System.Drawing.Point(12, 216);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1159, 142);
            this.groupBox1.TabIndex = 108;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Precio combustible";
            // 
            // butGuardarValores
            // 
            this.butGuardarValores.Location = new System.Drawing.Point(6, 109);
            this.butGuardarValores.Name = "butGuardarValores";
            this.butGuardarValores.Size = new System.Drawing.Size(129, 23);
            this.butGuardarValores.TabIndex = 111;
            this.butGuardarValores.Text = "GUARDAR VALORES";
            this.butGuardarValores.UseVisualStyleBackColor = true;
            this.butGuardarValores.Click += new System.EventHandler(this.butGuardarValores_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 12;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333335F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333335F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333335F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333335F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333335F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333335F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333335F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333335F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333335F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333335F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333335F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333335F));
            this.tableLayoutPanel1.Controls.Add(this.tbDic, 11, 1);
            this.tableLayoutPanel1.Controls.Add(this.tbNov, 10, 1);
            this.tableLayoutPanel1.Controls.Add(this.tbOct, 9, 1);
            this.tableLayoutPanel1.Controls.Add(this.tbSep, 8, 1);
            this.tableLayoutPanel1.Controls.Add(this.tbAgo, 7, 1);
            this.tableLayoutPanel1.Controls.Add(this.tbJul, 6, 1);
            this.tableLayoutPanel1.Controls.Add(this.tbJun, 5, 1);
            this.tableLayoutPanel1.Controls.Add(this.tbMay, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.tbAbr, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.tbMar, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.tbFeb, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label12, 11, 0);
            this.tableLayoutPanel1.Controls.Add(this.label11, 10, 0);
            this.tableLayoutPanel1.Controls.Add(this.label10, 9, 0);
            this.tableLayoutPanel1.Controls.Add(this.label9, 8, 0);
            this.tableLayoutPanel1.Controls.Add(this.label8, 7, 0);
            this.tableLayoutPanel1.Controls.Add(this.label7, 6, 0);
            this.tableLayoutPanel1.Controls.Add(this.label6, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.label5, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.label4, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.label3, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tbEne, 0, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(6, 46);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1147, 57);
            this.tableLayoutPanel1.TabIndex = 110;
            // 
            // tbDic
            // 
            this.tbDic.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbDic.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbDic.Location = new System.Drawing.Point(1051, 32);
            this.tbDic.Name = "tbDic";
            this.tbDic.Size = new System.Drawing.Size(89, 21);
            this.tbDic.TabIndex = 23;
            this.tbDic.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbNov
            // 
            this.tbNov.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbNov.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNov.Location = new System.Drawing.Point(953, 32);
            this.tbNov.Name = "tbNov";
            this.tbNov.Size = new System.Drawing.Size(89, 21);
            this.tbNov.TabIndex = 22;
            this.tbNov.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbOct
            // 
            this.tbOct.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbOct.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbOct.Location = new System.Drawing.Point(858, 32);
            this.tbOct.Name = "tbOct";
            this.tbOct.Size = new System.Drawing.Size(89, 21);
            this.tbOct.TabIndex = 21;
            this.tbOct.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbSep
            // 
            this.tbSep.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbSep.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbSep.Location = new System.Drawing.Point(763, 32);
            this.tbSep.Name = "tbSep";
            this.tbSep.Size = new System.Drawing.Size(89, 21);
            this.tbSep.TabIndex = 20;
            this.tbSep.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbAgo
            // 
            this.tbAgo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbAgo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAgo.Location = new System.Drawing.Point(668, 32);
            this.tbAgo.Name = "tbAgo";
            this.tbAgo.Size = new System.Drawing.Size(89, 21);
            this.tbAgo.TabIndex = 19;
            this.tbAgo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbJul
            // 
            this.tbJul.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbJul.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbJul.Location = new System.Drawing.Point(573, 32);
            this.tbJul.Name = "tbJul";
            this.tbJul.Size = new System.Drawing.Size(89, 21);
            this.tbJul.TabIndex = 18;
            this.tbJul.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbJun
            // 
            this.tbJun.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbJun.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbJun.Location = new System.Drawing.Point(478, 32);
            this.tbJun.Name = "tbJun";
            this.tbJun.Size = new System.Drawing.Size(89, 21);
            this.tbJun.TabIndex = 17;
            this.tbJun.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbMay
            // 
            this.tbMay.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbMay.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMay.Location = new System.Drawing.Point(383, 32);
            this.tbMay.Name = "tbMay";
            this.tbMay.Size = new System.Drawing.Size(89, 21);
            this.tbMay.TabIndex = 16;
            this.tbMay.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbAbr
            // 
            this.tbAbr.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbAbr.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAbr.Location = new System.Drawing.Point(288, 32);
            this.tbAbr.Name = "tbAbr";
            this.tbAbr.Size = new System.Drawing.Size(89, 21);
            this.tbAbr.TabIndex = 15;
            this.tbAbr.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbMar
            // 
            this.tbMar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbMar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMar.Location = new System.Drawing.Point(193, 32);
            this.tbMar.Name = "tbMar";
            this.tbMar.Size = new System.Drawing.Size(89, 21);
            this.tbMar.TabIndex = 14;
            this.tbMar.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbFeb
            // 
            this.tbFeb.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbFeb.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbFeb.Location = new System.Drawing.Point(98, 32);
            this.tbFeb.Name = "tbFeb";
            this.tbFeb.Size = new System.Drawing.Size(89, 21);
            this.tbFeb.TabIndex = 13;
            this.tbFeb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(1082, 6);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(27, 15);
            this.label12.TabIndex = 11;
            this.label12.Text = "DIC";
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(981, 6);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(32, 15);
            this.label11.TabIndex = 10;
            this.label11.Text = "NOV";
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(887, 6);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(31, 15);
            this.label10.TabIndex = 9;
            this.label10.Text = "OCT";
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(792, 6);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(31, 15);
            this.label9.TabIndex = 8;
            this.label9.Text = "SEP";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(696, 6);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(32, 15);
            this.label8.TabIndex = 7;
            this.label8.Text = "AGO";
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(603, 6);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 15);
            this.label7.TabIndex = 6;
            this.label7.Text = "JUL";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(507, 6);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 15);
            this.label6.TabIndex = 5;
            this.label6.Text = "JUN";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(411, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 15);
            this.label5.TabIndex = 4;
            this.label5.Text = "MAY";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(317, 6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 15);
            this.label4.TabIndex = 3;
            this.label4.Text = "ABR";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(220, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "MAR";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(127, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "FEB";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(31, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "ENE";
            // 
            // tbEne
            // 
            this.tbEne.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbEne.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbEne.Location = new System.Drawing.Point(3, 32);
            this.tbEne.Name = "tbEne";
            this.tbEne.Size = new System.Drawing.Size(89, 21);
            this.tbEne.TabIndex = 12;
            this.tbEne.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numCombustibleAnio
            // 
            this.numCombustibleAnio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numCombustibleAnio.Location = new System.Drawing.Point(6, 19);
            this.numCombustibleAnio.Maximum = new decimal(new int[] {
            2050,
            0,
            0,
            0});
            this.numCombustibleAnio.Minimum = new decimal(new int[] {
            2010,
            0,
            0,
            0});
            this.numCombustibleAnio.Name = "numCombustibleAnio";
            this.numCombustibleAnio.ReadOnly = true;
            this.numCombustibleAnio.Size = new System.Drawing.Size(80, 21);
            this.numCombustibleAnio.TabIndex = 109;
            this.numCombustibleAnio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numCombustibleAnio.Value = new decimal(new int[] {
            2010,
            0,
            0,
            0});
            this.numCombustibleAnio.ValueChanged += new System.EventHandler(this.numCombustibleAnio_ValueChanged);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.Controls.Add(this.tbTotalTon, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.tbTotalM3, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.tbTotalMineria, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.tbTotalEnte, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label13, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label14, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.label15, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.label16, 3, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(131, 157);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(615, 53);
            this.tableLayoutPanel2.TabIndex = 109;
            // 
            // tbTotalTon
            // 
            this.tbTotalTon.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbTotalTon.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbTotalTon.Location = new System.Drawing.Point(3, 29);
            this.tbTotalTon.Name = "tbTotalTon";
            this.tbTotalTon.ReadOnly = true;
            this.tbTotalTon.Size = new System.Drawing.Size(147, 23);
            this.tbTotalTon.TabIndex = 19;
            this.tbTotalTon.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbTotalM3
            // 
            this.tbTotalM3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbTotalM3.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbTotalM3.Location = new System.Drawing.Point(156, 29);
            this.tbTotalM3.Name = "tbTotalM3";
            this.tbTotalM3.ReadOnly = true;
            this.tbTotalM3.Size = new System.Drawing.Size(147, 23);
            this.tbTotalM3.TabIndex = 18;
            this.tbTotalM3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbTotalMineria
            // 
            this.tbTotalMineria.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbTotalMineria.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbTotalMineria.Location = new System.Drawing.Point(309, 29);
            this.tbTotalMineria.Name = "tbTotalMineria";
            this.tbTotalMineria.ReadOnly = true;
            this.tbTotalMineria.Size = new System.Drawing.Size(147, 23);
            this.tbTotalMineria.TabIndex = 17;
            this.tbTotalMineria.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbTotalEnte
            // 
            this.tbTotalEnte.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbTotalEnte.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbTotalEnte.Location = new System.Drawing.Point(462, 29);
            this.tbTotalEnte.Name = "tbTotalEnte";
            this.tbTotalEnte.ReadOnly = true;
            this.tbTotalEnte.Size = new System.Drawing.Size(150, 23);
            this.tbTotalEnte.TabIndex = 16;
            this.tbTotalEnte.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(17, 5);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(118, 15);
            this.label13.TabIndex = 1;
            this.label13.Text = "TOTAL TONELADAS";
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(197, 5);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(65, 15);
            this.label14.TabIndex = 2;
            this.label14.Text = "TOTAL M3";
            // 
            // label15
            // 
            this.label15.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(311, 5);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(143, 15);
            this.label15.TabIndex = 3;
            this.label15.Text = "TOTAL PAGO A MINERÍA";
            // 
            // label16
            // 
            this.label16.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(474, 5);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(125, 15);
            this.label16.TabIndex = 4;
            this.label16.Text = "TOTAL PAGO A ENTE";
            // 
            // ResumenMineria
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1183, 368);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.butImprimirTabla);
            this.Controls.Add(this.cbMineral);
            this.Controls.Add(this.numAnio);
            this.Controls.Add(this.dgTabla);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ResumenMineria";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Resumen minería";
            ((System.ComponentModel.ISupportInitialize)(this.numAnio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgTabla)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numCombustibleAnio)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.NumericUpDown numAnio;
        private System.Windows.Forms.DataGridView dgTabla;
        private System.Windows.Forms.ComboBox cbMineral;
        private System.Windows.Forms.Button butImprimirTabla;
        private System.Windows.Forms.DataGridViewTextBoxColumn IMPUESTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ene;
        private System.Windows.Forms.DataGridViewTextBoxColumn Feb;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mar;
        private System.Windows.Forms.DataGridViewTextBoxColumn Abr;
        private System.Windows.Forms.DataGridViewTextBoxColumn May;
        private System.Windows.Forms.DataGridViewTextBoxColumn Jun;
        private System.Windows.Forms.DataGridViewTextBoxColumn Jul;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ago;
        private System.Windows.Forms.DataGridViewTextBoxColumn Sep;
        private System.Windows.Forms.DataGridViewTextBoxColumn Oct;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nov;
        private System.Windows.Forms.DataGridViewTextBoxColumn Dic;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button butGuardarValores;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox tbDic;
        private System.Windows.Forms.TextBox tbNov;
        private System.Windows.Forms.TextBox tbOct;
        private System.Windows.Forms.TextBox tbSep;
        private System.Windows.Forms.TextBox tbAgo;
        private System.Windows.Forms.TextBox tbJul;
        private System.Windows.Forms.TextBox tbJun;
        private System.Windows.Forms.TextBox tbMay;
        private System.Windows.Forms.TextBox tbAbr;
        private System.Windows.Forms.TextBox tbMar;
        private System.Windows.Forms.TextBox tbFeb;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbEne;
        private System.Windows.Forms.NumericUpDown numCombustibleAnio;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox tbTotalTon;
        private System.Windows.Forms.TextBox tbTotalM3;
        private System.Windows.Forms.TextBox tbTotalMineria;
        private System.Windows.Forms.TextBox tbTotalEnte;
        private System.Windows.Forms.Label label16;
    }
}