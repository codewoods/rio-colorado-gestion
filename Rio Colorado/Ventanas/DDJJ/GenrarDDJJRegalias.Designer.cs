﻿namespace DDJJ_Regalías_Mineras
{
    partial class GenerarDDJJ
    {

        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GenerarDDJJ));
            this.comboBoxMineral = new System.Windows.Forms.ComboBox();
            this.comboBoxMes = new System.Windows.Forms.ComboBox();
            this.dataGridViewTabla = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonGenerarDDJJ = new System.Windows.Forms.Button();
            this.buttonBuscar = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonVolverBuscar = new System.Windows.Forms.Button();
            this.textBoxAnio = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.groupBoxAgregarFila = new System.Windows.Forms.GroupBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.textBoxPrecioUnitario = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.buttonAgregarFila = new System.Windows.Forms.Button();
            this.textBoxTotal = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxRegEnTon = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxPorReg = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxFInalidad = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxToneladas = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxM3 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxNumGuia = new System.Windows.Forms.TextBox();
            this.buttonEditarFila = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.textBoxValorFinal = new System.Windows.Forms.TextBox();
            this.groupBoxOtrosCampos = new System.Windows.Forms.GroupBox();
            this.dateTimePickerFecha = new System.Windows.Forms.DateTimePicker();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radioButtonBolSi = new System.Windows.Forms.RadioButton();
            this.radioButtonBolNo = new System.Windows.Forms.RadioButton();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.radioButtonGuiaSi = new System.Windows.Forms.RadioButton();
            this.radioButtonGuiaNo = new System.Windows.Forms.RadioButton();
            this.label19 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.comboBoxOCBoletaMes = new System.Windows.Forms.ComboBox();
            this.textBoxOCBoletaAnio = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.comboBoxOCGuiaMes = new System.Windows.Forms.ComboBox();
            this.textBoxLugar = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.textBoxOCTotal = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.textBoxOCGuiaAnio = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.buttonEliminarFila = new System.Windows.Forms.Button();
            this.labFecha = new System.Windows.Forms.Label();
            this.groupBoxEditarFila = new System.Windows.Forms.GroupBox();
            this.buttonCancelarEdición = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.textBoxPrecioUnitarioE = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.buttonConfirmarEdicion = new System.Windows.Forms.Button();
            this.textBoxTotalE = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.textBoxRegEnTonE = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.textBoxPorRegE = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.textBoxFInalidadE = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.textBoxToneladasE = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.textBoxM3E = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.textBoxNumGuiaE = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTabla)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBoxAgregarFila.SuspendLayout();
            this.groupBoxOtrosCampos.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBoxEditarFila.SuspendLayout();
            this.SuspendLayout();
            // 
            // comboBoxMineral
            // 
            this.comboBoxMineral.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMineral.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxMineral.FormattingEnabled = true;
            this.comboBoxMineral.Location = new System.Drawing.Point(16, 19);
            this.comboBoxMineral.Name = "comboBoxMineral";
            this.comboBoxMineral.Size = new System.Drawing.Size(128, 23);
            this.comboBoxMineral.TabIndex = 0;
            // 
            // comboBoxMes
            // 
            this.comboBoxMes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMes.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxMes.FormattingEnabled = true;
            this.comboBoxMes.Location = new System.Drawing.Point(248, 19);
            this.comboBoxMes.Name = "comboBoxMes";
            this.comboBoxMes.Size = new System.Drawing.Size(112, 23);
            this.comboBoxMes.TabIndex = 2;
            // 
            // dataGridViewTabla
            // 
            this.dataGridViewTabla.AllowUserToAddRows = false;
            this.dataGridViewTabla.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridViewTabla.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewTabla.BackgroundColor = System.Drawing.Color.MintCream;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Gold;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Cornsilk;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTabla.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewTabla.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewTabla.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Consolas", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTabla.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewTabla.Location = new System.Drawing.Point(12, 74);
            this.dataGridViewTabla.MultiSelect = false;
            this.dataGridViewTabla.Name = "dataGridViewTabla";
            this.dataGridViewTabla.ReadOnly = true;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridViewTabla.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewTabla.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewTabla.Size = new System.Drawing.Size(935, 173);
            this.dataGridViewTabla.TabIndex = 4;
            this.dataGridViewTabla.RowStateChanged += new System.Windows.Forms.DataGridViewRowStateChangedEventHandler(this.dataGridViewTabla_RowStateChanged);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Nº DE GUIAS MINERAS";
            this.Column1.MinimumWidth = 100;
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "FINALIDAD DE DESPACHO / GRADO DE IND.";
            this.Column2.MinimumWidth = 100;
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 170;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column3.HeaderText = "M3";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 47;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "TOTAL TONELADAS";
            this.Column4.MinimumWidth = 100;
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 110;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "% REG. A APLICAR";
            this.Column5.MinimumWidth = 80;
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // Column6
            // 
            this.Column6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column6.HeaderText = "REG. EN TON.";
            this.Column6.MinimumWidth = 80;
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 96;
            // 
            // Column7
            // 
            this.Column7.HeaderText = "PRECIO UNITARIO ($/TON)";
            this.Column7.MinimumWidth = 100;
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 135;
            // 
            // Column8
            // 
            this.Column8.HeaderText = "TOTAL REG. ($)";
            this.Column8.MinimumWidth = 100;
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            // 
            // buttonGenerarDDJJ
            // 
            this.buttonGenerarDDJJ.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonGenerarDDJJ.Location = new System.Drawing.Point(710, 532);
            this.buttonGenerarDDJJ.Name = "buttonGenerarDDJJ";
            this.buttonGenerarDDJJ.Size = new System.Drawing.Size(236, 42);
            this.buttonGenerarDDJJ.TabIndex = 5;
            this.buttonGenerarDDJJ.Text = "GENERAR D.D. J.J.";
            this.buttonGenerarDDJJ.UseVisualStyleBackColor = true;
            this.buttonGenerarDDJJ.Click += new System.EventHandler(this.buttonGenerarDDJJ_Click);
            // 
            // buttonBuscar
            // 
            this.buttonBuscar.Location = new System.Drawing.Point(366, 19);
            this.buttonBuscar.Name = "buttonBuscar";
            this.buttonBuscar.Size = new System.Drawing.Size(75, 24);
            this.buttonBuscar.TabIndex = 6;
            this.buttonBuscar.Text = "BUSCAR";
            this.buttonBuscar.UseVisualStyleBackColor = true;
            this.buttonBuscar.Click += new System.EventHandler(this.buttonBuscar_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.buttonVolverBuscar);
            this.groupBox1.Controls.Add(this.textBoxAnio);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.comboBoxMes);
            this.groupBox1.Controls.Add(this.buttonBuscar);
            this.groupBox1.Controls.Add(this.comboBoxMineral);
            this.groupBox1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(590, 56);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "BUSCAR";
            // 
            // buttonVolverBuscar
            // 
            this.buttonVolverBuscar.Location = new System.Drawing.Point(451, 19);
            this.buttonVolverBuscar.Name = "buttonVolverBuscar";
            this.buttonVolverBuscar.Size = new System.Drawing.Size(130, 24);
            this.buttonVolverBuscar.TabIndex = 21;
            this.buttonVolverBuscar.Text = "VOLVER A BUSCAR";
            this.buttonVolverBuscar.UseVisualStyleBackColor = true;
            this.buttonVolverBuscar.Click += new System.EventHandler(this.buttonVolverBuscar_Click);
            // 
            // textBoxAnio
            // 
            this.textBoxAnio.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAnio.Location = new System.Drawing.Point(189, 20);
            this.textBoxAnio.Name = "textBoxAnio";
            this.textBoxAnio.Size = new System.Drawing.Size(53, 23);
            this.textBoxAnio.TabIndex = 20;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(153, 24);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(30, 13);
            this.label18.TabIndex = 17;
            this.label18.Text = "AÑO";
            // 
            // groupBoxAgregarFila
            // 
            this.groupBoxAgregarFila.BackColor = System.Drawing.Color.White;
            this.groupBoxAgregarFila.Controls.Add(this.label32);
            this.groupBoxAgregarFila.Controls.Add(this.label31);
            this.groupBoxAgregarFila.Controls.Add(this.label30);
            this.groupBoxAgregarFila.Controls.Add(this.label29);
            this.groupBoxAgregarFila.Controls.Add(this.textBoxPrecioUnitario);
            this.groupBoxAgregarFila.Controls.Add(this.label16);
            this.groupBoxAgregarFila.Controls.Add(this.buttonAgregarFila);
            this.groupBoxAgregarFila.Controls.Add(this.textBoxTotal);
            this.groupBoxAgregarFila.Controls.Add(this.label7);
            this.groupBoxAgregarFila.Controls.Add(this.label4);
            this.groupBoxAgregarFila.Controls.Add(this.textBoxRegEnTon);
            this.groupBoxAgregarFila.Controls.Add(this.label5);
            this.groupBoxAgregarFila.Controls.Add(this.textBoxPorReg);
            this.groupBoxAgregarFila.Controls.Add(this.label6);
            this.groupBoxAgregarFila.Controls.Add(this.textBoxFInalidad);
            this.groupBoxAgregarFila.Controls.Add(this.label3);
            this.groupBoxAgregarFila.Controls.Add(this.textBoxToneladas);
            this.groupBoxAgregarFila.Controls.Add(this.label2);
            this.groupBoxAgregarFila.Controls.Add(this.textBoxM3);
            this.groupBoxAgregarFila.Controls.Add(this.label1);
            this.groupBoxAgregarFila.Controls.Add(this.textBoxNumGuia);
            this.groupBoxAgregarFila.Location = new System.Drawing.Point(13, 253);
            this.groupBoxAgregarFila.Name = "groupBoxAgregarFila";
            this.groupBoxAgregarFila.Size = new System.Drawing.Size(412, 163);
            this.groupBoxAgregarFila.TabIndex = 8;
            this.groupBoxAgregarFila.TabStop = false;
            this.groupBoxAgregarFila.Text = "AGREGAR FILA";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(283, 126);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(13, 13);
            this.label32.TabIndex = 21;
            this.label32.Text = "$";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(283, 100);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(13, 13);
            this.label31.TabIndex = 20;
            this.label31.Text = "$";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(381, 48);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(15, 13);
            this.label30.TabIndex = 19;
            this.label30.Text = "%";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(209, 100);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(68, 13);
            this.label29.TabIndex = 18;
            this.label29.Text = "PREC. UNI.*";
            // 
            // textBoxPrecioUnitario
            // 
            this.textBoxPrecioUnitario.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPrecioUnitario.Location = new System.Drawing.Point(302, 92);
            this.textBoxPrecioUnitario.Name = "textBoxPrecioUnitario";
            this.textBoxPrecioUnitario.Size = new System.Drawing.Size(100, 23);
            this.textBoxPrecioUnitario.TabIndex = 0;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(331, 146);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(69, 13);
            this.label16.TabIndex = 16;
            this.label16.Text = "* valores fijos";
            // 
            // buttonAgregarFila
            // 
            this.buttonAgregarFila.Location = new System.Drawing.Point(16, 100);
            this.buttonAgregarFila.Name = "buttonAgregarFila";
            this.buttonAgregarFila.Size = new System.Drawing.Size(169, 23);
            this.buttonAgregarFila.TabIndex = 4;
            this.buttonAgregarFila.Text = "AGREGAR";
            this.buttonAgregarFila.UseVisualStyleBackColor = true;
            this.buttonAgregarFila.Click += new System.EventHandler(this.buttonAgregarFila_Click_4);
            this.buttonAgregarFila.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.buttonAgregarFila_KeyPress_2);
            // 
            // textBoxTotal
            // 
            this.textBoxTotal.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxTotal.Location = new System.Drawing.Point(302, 118);
            this.textBoxTotal.Name = "textBoxTotal";
            this.textBoxTotal.Size = new System.Drawing.Size(100, 23);
            this.textBoxTotal.TabIndex = 0;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(209, 126);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "TOTAL";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(209, 74);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "REG. EN TON.";
            // 
            // textBoxRegEnTon
            // 
            this.textBoxRegEnTon.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxRegEnTon.Location = new System.Drawing.Point(302, 66);
            this.textBoxRegEnTon.Name = "textBoxRegEnTon";
            this.textBoxRegEnTon.Size = new System.Drawing.Size(100, 23);
            this.textBoxRegEnTon.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(209, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "REGALÍAS*";
            // 
            // textBoxPorReg
            // 
            this.textBoxPorReg.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPorReg.Location = new System.Drawing.Point(302, 40);
            this.textBoxPorReg.Name = "textBoxPorReg";
            this.textBoxPorReg.Size = new System.Drawing.Size(75, 23);
            this.textBoxPorReg.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(209, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "FINALIDAD*";
            // 
            // textBoxFInalidad
            // 
            this.textBoxFInalidad.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxFInalidad.Location = new System.Drawing.Point(302, 14);
            this.textBoxFInalidad.Name = "textBoxFInalidad";
            this.textBoxFInalidad.Size = new System.Drawing.Size(100, 23);
            this.textBoxFInalidad.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "TONELADAS";
            // 
            // textBoxToneladas
            // 
            this.textBoxToneladas.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxToneladas.Location = new System.Drawing.Point(87, 66);
            this.textBoxToneladas.Name = "textBoxToneladas";
            this.textBoxToneladas.Size = new System.Drawing.Size(100, 23);
            this.textBoxToneladas.TabIndex = 3;
            this.textBoxToneladas.TextChanged += new System.EventHandler(this.textBoxToneladas_TextChanged);
            this.textBoxToneladas.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxToneladas_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(22, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "M3";
            // 
            // textBoxM3
            // 
            this.textBoxM3.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxM3.Location = new System.Drawing.Point(87, 40);
            this.textBoxM3.Name = "textBoxM3";
            this.textBoxM3.Size = new System.Drawing.Size(100, 23);
            this.textBoxM3.TabIndex = 2;
            this.textBoxM3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxM3_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Nº DE GUÍA";
            // 
            // textBoxNumGuia
            // 
            this.textBoxNumGuia.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNumGuia.Location = new System.Drawing.Point(87, 14);
            this.textBoxNumGuia.Name = "textBoxNumGuia";
            this.textBoxNumGuia.Size = new System.Drawing.Size(100, 23);
            this.textBoxNumGuia.TabIndex = 0;
            this.textBoxNumGuia.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxNumGuia_KeyPress);
            // 
            // buttonEditarFila
            // 
            this.buttonEditarFila.Location = new System.Drawing.Point(846, 253);
            this.buttonEditarFila.Name = "buttonEditarFila";
            this.buttonEditarFila.Size = new System.Drawing.Size(101, 23);
            this.buttonEditarFila.TabIndex = 9;
            this.buttonEditarFila.Text = "EDITAR FILA";
            this.buttonEditarFila.UseVisualStyleBackColor = true;
            this.buttonEditarFila.Click += new System.EventHandler(this.buttonEditarFila_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(764, 51);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(76, 13);
            this.label15.TabIndex = 17;
            this.label15.Text = "VALOR FINAL";
            // 
            // textBoxValorFinal
            // 
            this.textBoxValorFinal.Font = new System.Drawing.Font("Consolas", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxValorFinal.Location = new System.Drawing.Point(846, 43);
            this.textBoxValorFinal.Name = "textBoxValorFinal";
            this.textBoxValorFinal.Size = new System.Drawing.Size(100, 27);
            this.textBoxValorFinal.TabIndex = 18;
            this.textBoxValorFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxValorFinal.TextChanged += new System.EventHandler(this.textBoxValorFinal_TextChanged);
            // 
            // groupBoxOtrosCampos
            // 
            this.groupBoxOtrosCampos.BackColor = System.Drawing.Color.White;
            this.groupBoxOtrosCampos.Controls.Add(this.dateTimePickerFecha);
            this.groupBoxOtrosCampos.Controls.Add(this.groupBox2);
            this.groupBoxOtrosCampos.Controls.Add(this.groupBox4);
            this.groupBoxOtrosCampos.Controls.Add(this.label19);
            this.groupBoxOtrosCampos.Controls.Add(this.label28);
            this.groupBoxOtrosCampos.Controls.Add(this.label27);
            this.groupBoxOtrosCampos.Controls.Add(this.comboBoxOCBoletaMes);
            this.groupBoxOtrosCampos.Controls.Add(this.textBoxOCBoletaAnio);
            this.groupBoxOtrosCampos.Controls.Add(this.label21);
            this.groupBoxOtrosCampos.Controls.Add(this.label26);
            this.groupBoxOtrosCampos.Controls.Add(this.comboBoxOCGuiaMes);
            this.groupBoxOtrosCampos.Controls.Add(this.textBoxLugar);
            this.groupBoxOtrosCampos.Controls.Add(this.label20);
            this.groupBoxOtrosCampos.Controls.Add(this.textBoxOCTotal);
            this.groupBoxOtrosCampos.Controls.Add(this.label22);
            this.groupBoxOtrosCampos.Controls.Add(this.textBoxOCGuiaAnio);
            this.groupBoxOtrosCampos.Controls.Add(this.label23);
            this.groupBoxOtrosCampos.Controls.Add(this.label24);
            this.groupBoxOtrosCampos.Location = new System.Drawing.Point(12, 422);
            this.groupBoxOtrosCampos.Name = "groupBoxOtrosCampos";
            this.groupBoxOtrosCampos.Size = new System.Drawing.Size(692, 154);
            this.groupBoxOtrosCampos.TabIndex = 17;
            this.groupBoxOtrosCampos.TabStop = false;
            this.groupBoxOtrosCampos.Text = "OTROS CAMPOS";
            // 
            // dateTimePickerFecha
            // 
            this.dateTimePickerFecha.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePickerFecha.Location = new System.Drawing.Point(62, 122);
            this.dateTimePickerFecha.Name = "dateTimePickerFecha";
            this.dateTimePickerFecha.Size = new System.Drawing.Size(264, 23);
            this.dateTimePickerFecha.TabIndex = 101;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.White;
            this.groupBox2.Controls.Add(this.radioButtonBolSi);
            this.groupBox2.Controls.Add(this.radioButtonBolNo);
            this.groupBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox2.Location = new System.Drawing.Point(16, 69);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(157, 44);
            this.groupBox2.TabIndex = 101;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "¿BOLETA DE DEPÓSITO?";
            // 
            // radioButtonBolSi
            // 
            this.radioButtonBolSi.AutoSize = true;
            this.radioButtonBolSi.Checked = true;
            this.radioButtonBolSi.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonBolSi.Location = new System.Drawing.Point(51, 19);
            this.radioButtonBolSi.Name = "radioButtonBolSi";
            this.radioButtonBolSi.Size = new System.Drawing.Size(36, 19);
            this.radioButtonBolSi.TabIndex = 21;
            this.radioButtonBolSi.TabStop = true;
            this.radioButtonBolSi.Text = "SÍ";
            this.radioButtonBolSi.UseVisualStyleBackColor = true;
            // 
            // radioButtonBolNo
            // 
            this.radioButtonBolNo.AutoSize = true;
            this.radioButtonBolNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonBolNo.Location = new System.Drawing.Point(6, 19);
            this.radioButtonBolNo.Name = "radioButtonBolNo";
            this.radioButtonBolNo.Size = new System.Drawing.Size(43, 19);
            this.radioButtonBolNo.TabIndex = 22;
            this.radioButtonBolNo.Text = "NO";
            this.radioButtonBolNo.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.White;
            this.groupBox4.Controls.Add(this.radioButtonGuiaSi);
            this.groupBox4.Controls.Add(this.radioButtonGuiaNo);
            this.groupBox4.Location = new System.Drawing.Point(16, 20);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(157, 41);
            this.groupBox4.TabIndex = 102;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "¿GUÍAS MINERAS?";
            // 
            // radioButtonGuiaSi
            // 
            this.radioButtonGuiaSi.AutoSize = true;
            this.radioButtonGuiaSi.Checked = true;
            this.radioButtonGuiaSi.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonGuiaSi.Location = new System.Drawing.Point(51, 15);
            this.radioButtonGuiaSi.Name = "radioButtonGuiaSi";
            this.radioButtonGuiaSi.Size = new System.Drawing.Size(36, 19);
            this.radioButtonGuiaSi.TabIndex = 17;
            this.radioButtonGuiaSi.TabStop = true;
            this.radioButtonGuiaSi.Text = "SÍ";
            this.radioButtonGuiaSi.UseVisualStyleBackColor = true;
            this.radioButtonGuiaSi.CheckedChanged += new System.EventHandler(this.radioButtonGuiaSi_CheckedChanged);
            // 
            // radioButtonGuiaNo
            // 
            this.radioButtonGuiaNo.AutoSize = true;
            this.radioButtonGuiaNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonGuiaNo.Location = new System.Drawing.Point(6, 15);
            this.radioButtonGuiaNo.Name = "radioButtonGuiaNo";
            this.radioButtonGuiaNo.Size = new System.Drawing.Size(43, 19);
            this.radioButtonGuiaNo.TabIndex = 18;
            this.radioButtonGuiaNo.Text = "NO";
            this.radioButtonGuiaNo.UseVisualStyleBackColor = true;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(332, 126);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(44, 13);
            this.label19.TabIndex = 28;
            this.label19.Text = "LUGAR";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(14, 126);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(42, 13);
            this.label28.TabIndex = 27;
            this.label28.Text = "FECHA";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(579, 88);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(30, 13);
            this.label27.TabIndex = 26;
            this.label27.Text = "AÑO";
            // 
            // comboBoxOCBoletaMes
            // 
            this.comboBoxOCBoletaMes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxOCBoletaMes.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxOCBoletaMes.FormattingEnabled = true;
            this.comboBoxOCBoletaMes.Location = new System.Drawing.Point(461, 83);
            this.comboBoxOCBoletaMes.Name = "comboBoxOCBoletaMes";
            this.comboBoxOCBoletaMes.Size = new System.Drawing.Size(112, 23);
            this.comboBoxOCBoletaMes.TabIndex = 25;
            // 
            // textBoxOCBoletaAnio
            // 
            this.textBoxOCBoletaAnio.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxOCBoletaAnio.Location = new System.Drawing.Point(615, 83);
            this.textBoxOCBoletaAnio.Name = "textBoxOCBoletaAnio";
            this.textBoxOCBoletaAnio.Size = new System.Drawing.Size(54, 23);
            this.textBoxOCBoletaAnio.TabIndex = 24;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(280, 88);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(175, 13);
            this.label21.TabIndex = 23;
            this.label21.Text = "CORRESPONDIENTE AL MES DE";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(579, 39);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(30, 13);
            this.label26.TabIndex = 20;
            this.label26.Text = "AÑO";
            // 
            // comboBoxOCGuiaMes
            // 
            this.comboBoxOCGuiaMes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxOCGuiaMes.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxOCGuiaMes.FormattingEnabled = true;
            this.comboBoxOCGuiaMes.Location = new System.Drawing.Point(461, 35);
            this.comboBoxOCGuiaMes.Name = "comboBoxOCGuiaMes";
            this.comboBoxOCGuiaMes.Size = new System.Drawing.Size(112, 23);
            this.comboBoxOCGuiaMes.TabIndex = 19;
            // 
            // textBoxLugar
            // 
            this.textBoxLugar.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxLugar.Location = new System.Drawing.Point(380, 122);
            this.textBoxLugar.Name = "textBoxLugar";
            this.textBoxLugar.Size = new System.Drawing.Size(290, 23);
            this.textBoxLugar.TabIndex = 14;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(178, 90);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(13, 13);
            this.label20.TabIndex = 12;
            this.label20.Text = "$";
            // 
            // textBoxOCTotal
            // 
            this.textBoxOCTotal.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxOCTotal.Location = new System.Drawing.Point(197, 84);
            this.textBoxOCTotal.Name = "textBoxOCTotal";
            this.textBoxOCTotal.Size = new System.Drawing.Size(77, 23);
            this.textBoxOCTotal.TabIndex = 11;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(280, 41);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(175, 13);
            this.label22.TabIndex = 8;
            this.label22.Text = "CORRESPONDIENTE AL MES DE";
            // 
            // textBoxOCGuiaAnio
            // 
            this.textBoxOCGuiaAnio.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxOCGuiaAnio.Location = new System.Drawing.Point(615, 35);
            this.textBoxOCGuiaAnio.Name = "textBoxOCGuiaAnio";
            this.textBoxOCGuiaAnio.Size = new System.Drawing.Size(54, 23);
            this.textBoxOCGuiaAnio.TabIndex = 7;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(14, 98);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(137, 13);
            this.label23.TabIndex = 6;
            this.label23.Text = "¿BOLETA DE DEPÓSITO?";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(13, 48);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(0, 13);
            this.label24.TabIndex = 4;
            // 
            // buttonEliminarFila
            // 
            this.buttonEliminarFila.ForeColor = System.Drawing.Color.Red;
            this.buttonEliminarFila.Location = new System.Drawing.Point(846, 282);
            this.buttonEliminarFila.Name = "buttonEliminarFila";
            this.buttonEliminarFila.Size = new System.Drawing.Size(101, 23);
            this.buttonEliminarFila.TabIndex = 19;
            this.buttonEliminarFila.Text = "ELIMINAR FILA";
            this.buttonEliminarFila.UseVisualStyleBackColor = true;
            this.buttonEliminarFila.Click += new System.EventHandler(this.buttonEliminarFila_Click);
            this.buttonEliminarFila.MouseClick += new System.Windows.Forms.MouseEventHandler(this.buttonEliminarFila_MouseClick);
            // 
            // labFecha
            // 
            this.labFecha.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labFecha.Location = new System.Drawing.Point(608, 12);
            this.labFecha.Name = "labFecha";
            this.labFecha.Size = new System.Drawing.Size(338, 23);
            this.labFecha.TabIndex = 100;
            this.labFecha.Text = "HOY";
            this.labFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBoxEditarFila
            // 
            this.groupBoxEditarFila.BackColor = System.Drawing.Color.White;
            this.groupBoxEditarFila.Controls.Add(this.buttonCancelarEdición);
            this.groupBoxEditarFila.Controls.Add(this.label8);
            this.groupBoxEditarFila.Controls.Add(this.label9);
            this.groupBoxEditarFila.Controls.Add(this.label10);
            this.groupBoxEditarFila.Controls.Add(this.label11);
            this.groupBoxEditarFila.Controls.Add(this.textBoxPrecioUnitarioE);
            this.groupBoxEditarFila.Controls.Add(this.label12);
            this.groupBoxEditarFila.Controls.Add(this.buttonConfirmarEdicion);
            this.groupBoxEditarFila.Controls.Add(this.textBoxTotalE);
            this.groupBoxEditarFila.Controls.Add(this.label13);
            this.groupBoxEditarFila.Controls.Add(this.label14);
            this.groupBoxEditarFila.Controls.Add(this.textBoxRegEnTonE);
            this.groupBoxEditarFila.Controls.Add(this.label17);
            this.groupBoxEditarFila.Controls.Add(this.textBoxPorRegE);
            this.groupBoxEditarFila.Controls.Add(this.label33);
            this.groupBoxEditarFila.Controls.Add(this.textBoxFInalidadE);
            this.groupBoxEditarFila.Controls.Add(this.label34);
            this.groupBoxEditarFila.Controls.Add(this.textBoxToneladasE);
            this.groupBoxEditarFila.Controls.Add(this.label35);
            this.groupBoxEditarFila.Controls.Add(this.textBoxM3E);
            this.groupBoxEditarFila.Controls.Add(this.label36);
            this.groupBoxEditarFila.Controls.Add(this.textBoxNumGuiaE);
            this.groupBoxEditarFila.Location = new System.Drawing.Point(431, 253);
            this.groupBoxEditarFila.Name = "groupBoxEditarFila";
            this.groupBoxEditarFila.Size = new System.Drawing.Size(412, 163);
            this.groupBoxEditarFila.TabIndex = 22;
            this.groupBoxEditarFila.TabStop = false;
            this.groupBoxEditarFila.Text = "EDITAR FILA";
            // 
            // buttonCancelarEdición
            // 
            this.buttonCancelarEdición.Location = new System.Drawing.Point(16, 100);
            this.buttonCancelarEdición.Name = "buttonCancelarEdición";
            this.buttonCancelarEdición.Size = new System.Drawing.Size(75, 23);
            this.buttonCancelarEdición.TabIndex = 22;
            this.buttonCancelarEdición.Text = "CANCELAR";
            this.buttonCancelarEdición.UseVisualStyleBackColor = true;
            this.buttonCancelarEdición.Click += new System.EventHandler(this.buttonCancelarEdición_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(283, 126);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(13, 13);
            this.label8.TabIndex = 21;
            this.label8.Text = "$";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(283, 100);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(13, 13);
            this.label9.TabIndex = 20;
            this.label9.Text = "$";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(381, 48);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(15, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "%";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(209, 100);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(68, 13);
            this.label11.TabIndex = 18;
            this.label11.Text = "PREC. UNI.*";
            // 
            // textBoxPrecioUnitarioE
            // 
            this.textBoxPrecioUnitarioE.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPrecioUnitarioE.Location = new System.Drawing.Point(300, 92);
            this.textBoxPrecioUnitarioE.Name = "textBoxPrecioUnitarioE";
            this.textBoxPrecioUnitarioE.Size = new System.Drawing.Size(100, 23);
            this.textBoxPrecioUnitarioE.TabIndex = 0;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(331, 146);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(69, 13);
            this.label12.TabIndex = 16;
            this.label12.Text = "* valores fijos";
            // 
            // buttonConfirmarEdicion
            // 
            this.buttonConfirmarEdicion.Location = new System.Drawing.Point(97, 100);
            this.buttonConfirmarEdicion.Name = "buttonConfirmarEdicion";
            this.buttonConfirmarEdicion.Size = new System.Drawing.Size(88, 23);
            this.buttonConfirmarEdicion.TabIndex = 4;
            this.buttonConfirmarEdicion.Text = "CONFIRMAR";
            this.buttonConfirmarEdicion.UseVisualStyleBackColor = true;
            this.buttonConfirmarEdicion.Click += new System.EventHandler(this.buttonConfirmarEdicion_Click);
            // 
            // textBoxTotalE
            // 
            this.textBoxTotalE.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxTotalE.Location = new System.Drawing.Point(300, 118);
            this.textBoxTotalE.Name = "textBoxTotalE";
            this.textBoxTotalE.Size = new System.Drawing.Size(100, 23);
            this.textBoxTotalE.TabIndex = 0;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(209, 126);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(42, 13);
            this.label13.TabIndex = 13;
            this.label13.Text = "TOTAL";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(209, 74);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(80, 13);
            this.label14.TabIndex = 12;
            this.label14.Text = "REG. EN TON.";
            // 
            // textBoxRegEnTonE
            // 
            this.textBoxRegEnTonE.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxRegEnTonE.Location = new System.Drawing.Point(300, 66);
            this.textBoxRegEnTonE.Name = "textBoxRegEnTonE";
            this.textBoxRegEnTonE.Size = new System.Drawing.Size(100, 23);
            this.textBoxRegEnTonE.TabIndex = 0;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(209, 48);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(64, 13);
            this.label17.TabIndex = 10;
            this.label17.Text = "REGALÍAS*";
            // 
            // textBoxPorRegE
            // 
            this.textBoxPorRegE.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPorRegE.Location = new System.Drawing.Point(300, 40);
            this.textBoxPorRegE.Name = "textBoxPorRegE";
            this.textBoxPorRegE.Size = new System.Drawing.Size(75, 23);
            this.textBoxPorRegE.TabIndex = 0;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(209, 22);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(67, 13);
            this.label33.TabIndex = 8;
            this.label33.Text = "FINALIDAD*";
            // 
            // textBoxFInalidadE
            // 
            this.textBoxFInalidadE.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxFInalidadE.Location = new System.Drawing.Point(300, 14);
            this.textBoxFInalidadE.Name = "textBoxFInalidadE";
            this.textBoxFInalidadE.Size = new System.Drawing.Size(100, 23);
            this.textBoxFInalidadE.TabIndex = 0;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(13, 74);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(72, 13);
            this.label34.TabIndex = 6;
            this.label34.Text = "TONELADAS";
            // 
            // textBoxToneladasE
            // 
            this.textBoxToneladasE.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxToneladasE.Location = new System.Drawing.Point(85, 66);
            this.textBoxToneladasE.Name = "textBoxToneladasE";
            this.textBoxToneladasE.Size = new System.Drawing.Size(100, 23);
            this.textBoxToneladasE.TabIndex = 3;
            this.textBoxToneladasE.TextChanged += new System.EventHandler(this.textBoxToneladasE_TextChanged);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(13, 48);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(22, 13);
            this.label35.TabIndex = 4;
            this.label35.Text = "M3";
            // 
            // textBoxM3E
            // 
            this.textBoxM3E.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxM3E.Location = new System.Drawing.Point(85, 40);
            this.textBoxM3E.Name = "textBoxM3E";
            this.textBoxM3E.Size = new System.Drawing.Size(100, 23);
            this.textBoxM3E.TabIndex = 2;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(13, 22);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(66, 13);
            this.label36.TabIndex = 2;
            this.label36.Text = "Nº DE GUÍA";
            // 
            // textBoxNumGuiaE
            // 
            this.textBoxNumGuiaE.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNumGuiaE.Location = new System.Drawing.Point(85, 14);
            this.textBoxNumGuiaE.Name = "textBoxNumGuiaE";
            this.textBoxNumGuiaE.Size = new System.Drawing.Size(100, 23);
            this.textBoxNumGuiaE.TabIndex = 0;
            // 
            // GenerarDDJJ
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(964, 585);
            this.Controls.Add(this.groupBoxEditarFila);
            this.Controls.Add(this.labFecha);
            this.Controls.Add(this.buttonEliminarFila);
            this.Controls.Add(this.groupBoxOtrosCampos);
            this.Controls.Add(this.textBoxValorFinal);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.buttonEditarFila);
            this.Controls.Add(this.groupBoxAgregarFila);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.buttonGenerarDDJJ);
            this.Controls.Add(this.dataGridViewTabla);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(980, 624);
            this.Name = "GenerarDDJJ";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DDJJ Regalías Mineras";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTabla)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBoxAgregarFila.ResumeLayout(false);
            this.groupBoxAgregarFila.PerformLayout();
            this.groupBoxOtrosCampos.ResumeLayout(false);
            this.groupBoxOtrosCampos.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBoxEditarFila.ResumeLayout(false);
            this.groupBoxEditarFila.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxMineral;
        private System.Windows.Forms.ComboBox comboBoxMes;
        private System.Windows.Forms.DataGridView dataGridViewTabla;
        private System.Windows.Forms.Button buttonGenerarDDJJ;
        private System.Windows.Forms.Button buttonBuscar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBoxAgregarFila;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxRegEnTon;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxPorReg;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxFInalidad;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxToneladas;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxM3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxNumGuia;
        private System.Windows.Forms.Button buttonAgregarFila;
        private System.Windows.Forms.TextBox textBoxTotal;
        private System.Windows.Forms.Button buttonEditarFila;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBoxValorFinal;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBoxOtrosCampos;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.ComboBox comboBoxOCGuiaMes;
        private System.Windows.Forms.RadioButton radioButtonGuiaNo;
        private System.Windows.Forms.RadioButton radioButtonGuiaSi;
        private System.Windows.Forms.TextBox textBoxLugar;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBoxOCTotal;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox textBoxOCGuiaAnio;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.RadioButton radioButtonBolNo;
        private System.Windows.Forms.RadioButton radioButtonBolSi;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.ComboBox comboBoxOCBoletaMes;
        private System.Windows.Forms.TextBox textBoxOCBoletaAnio;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button buttonEliminarFila;
        private System.Windows.Forms.TextBox textBoxAnio;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label labFecha;
        private System.Windows.Forms.Button buttonVolverBuscar;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox textBoxPrecioUnitario;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.GroupBox groupBoxEditarFila;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBoxPrecioUnitarioE;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button buttonConfirmarEdicion;
        private System.Windows.Forms.TextBox textBoxTotalE;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBoxRegEnTonE;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBoxPorRegE;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox textBoxFInalidadE;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox textBoxToneladasE;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox textBoxM3E;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox textBoxNumGuiaE;
        private System.Windows.Forms.Button buttonCancelarEdición;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DateTimePicker dateTimePickerFecha;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
    }
}

