﻿using DDJJ_Regalías_Mineras.Clases;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Globalization;
using System.Windows.Forms;

namespace DDJJ_Regalías_Mineras
{
    public partial class EditarValoresFijos : Form
    {
        List<Mineral> minerales;
        OleDbConnection conn;
        int idMineral;

        public EditarValoresFijos(List<Mineral> minerales, OleDbConnection conn)
        {
            InitializeComponent();

            this.minerales = minerales;
            this.conn = conn;

            foreach(Mineral min in minerales)
            {
                comboBoxMineral.Items.Add(min);
            }

            comboBoxMineral.SelectedIndex = 0;
        }

        private void comboBoxMineral_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            idMineral = ((Mineral)comboBoxMineral.SelectedItem).ID;

            string sql = "SELECT * FROM Filas WHERE idMineral="+idMineral+ " AND anio=1";

            OleDbCommand dbCmd = new OleDbCommand(sql, conn);
            OleDbDataReader dbReader = dbCmd.ExecuteReader();

            while (dbReader.Read())
            {
                textBoxFinalidad.Text = dbReader.GetValue(3)?.ToString() ?? "";
                textBoxPorcReg.Text = dbReader.GetValue(6)?.ToString() ?? "";
                textBoxPrecioUnitario.Text = dbReader.GetValue(8)?.ToString() ?? "";
            }
        }

        private void buttonGuardarCambios_Click(object sender, System.EventArgs e)
        {
            float var;

            string sql = "UPDATE Filas SET Finalidad = '" + textBoxFinalidad.Text + "'";

            if (!textBoxPorcReg.Text.Equals(""))
            {
                try
                {
                    var = float.Parse(textBoxPorcReg.Text.Replace(".", ","));
                    sql += ", Reg = " + var.ToString().Replace(",", "."); ;
                } catch(Exception ex)
                {
                    MessageBox.Show("Error en el campo 'Porc. Reg.'");
                }
            }

            if (!textBoxPrecioUnitario.Text.Equals(""))
            {
                try
                {
                    var = float.Parse(textBoxPrecioUnitario.Text.Replace(".", ","));
                    sql += ", PrecioUnitario = " + var.ToString().Replace(",", ".");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error en el campo 'Precio Uni.'");
                }
            }

            sql += " WHERE idMineral = " + idMineral + " AND anio = 1";

            OleDbDataAdapter oledbAdapter = new OleDbDataAdapter();

            oledbAdapter.UpdateCommand = conn.CreateCommand();
            oledbAdapter.UpdateCommand.CommandText = sql;

            int i = oledbAdapter.UpdateCommand.ExecuteNonQuery();

            Close();

            if (i == 1)
            {
                MessageBox.Show("Los valores se han fijado correctamente.", "Editar valores fijos");
            }
            else
            {
                MessageBox.Show("Los valores no se han podido fijar.", "Error");
            }

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void EditarValoresFijos_Load(object sender, EventArgs e)
        {

        }

        private void textBoxPrecioUnitario_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
