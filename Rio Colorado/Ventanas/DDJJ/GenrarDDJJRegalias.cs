﻿using DDJJ_Regalías_Mineras.Clases;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Globalization;
using System.Windows.Forms;
using Application = Microsoft.Office.Interop.Word.Application;
using System.IO;
using System.Xml;
using Gestion_Minera.Clases;
using Gestion_Minera.Ventanas;
using Microsoft.Office.Interop.Word;

namespace DDJJ_Regalías_Mineras
{
    public partial class GenerarDDJJ : Form
    {
        OleDbConnection conn;
        List<Mineral> minerales = new List<Mineral>();
        List<Fila> filas = new List<Fila>();
        DateTimeFormatInfo dtinfo = new CultureInfo("es-ES", false).DateTimeFormat;
        int numeroArchivo = 2;

        int idMineral = -1;
        int anio = -1;
        int mes = -1;

        public GenerarDDJJ(OleDbConnection conn)
        {
            InitializeComponent();

            this.conn = conn;

            List<ComboBox> combos = new List<ComboBox>();
            combos.Add(comboBoxMes);
            combos.Add(comboBoxOCBoletaMes);
            combos.Add(comboBoxOCGuiaMes);

            foreach (ComboBox combo in combos)
            {
                combo.Items.Add(new Item("ENERO", 1));
                combo.Items.Add(new Item("FEBRERO", 2));
                combo.Items.Add(new Item("MARZO", 3));
                combo.Items.Add(new Item("ABRIL", 4));
                combo.Items.Add(new Item("MAYO", 5));
                combo.Items.Add(new Item("JUNIO", 6));
                combo.Items.Add(new Item("JULIO", 7));
                combo.Items.Add(new Item("AGOSTO", 8));
                combo.Items.Add(new Item("SEPTIEMBRE", 9));
                combo.Items.Add(new Item("OCTUBRE", 10));
                combo.Items.Add(new Item("NOVIEMBRE", 11));
                combo.Items.Add(new Item("DICIEMBRE", 12));
            }

            groupBoxAgregarFila.Enabled = false;
            groupBoxEditarFila.Enabled = false;
            buttonEditarFila.Enabled = false;
            buttonEliminarFila.Enabled = false;
            groupBoxOtrosCampos.Enabled = false;
            buttonGenerarDDJJ.Enabled = false;

            string sql = "SELECT * FROM Mineral";

            OleDbCommand dbCmd = new OleDbCommand(sql, conn);
            OleDbDataReader dbReader = dbCmd.ExecuteReader();

            foreach (Mineral minerales in Utils.getMinerales(conn))
            {
                comboBoxMineral.Items.Add(minerales);
            }

            try { comboBoxMes.SelectedIndex = 0; } catch (Exception ex) { }
            try { comboBoxMineral.SelectedIndex = 0; } catch (Exception ex) { }

            textBoxAnio.Text = DateTime.Now.Year.ToString();
            textBoxLugar.Text = "25 de Mayo (La Pampa)";

            labFecha.Text = "Hoy es " + dtinfo.GetDayName(DateTime.Now.DayOfWeek) + " " + DateTime.Now.Day.ToString() + " de " + dtinfo.GetMonthName(DateTime.Now.Month) + " de " + DateTime.Now.Year.ToString();

            buttonVolverBuscar.Enabled = false;
        }

        private void configurarVarolesFijosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EditarValoresFijos pantallaEditar = new EditarValoresFijos(minerales, conn);

            pantallaEditar.Show();
        }

        private void buttonBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                idMineral = ((Mineral)comboBoxMineral.SelectedItem).ID;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en en el campo MINERAL");
            }

            try
            {
                anio = int.Parse(textBoxAnio.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en en el campo AÑO");
            }

            try
            {
                mes = ((Item)comboBoxMes.SelectedItem).value;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en en el campo MES");
            }


            if (idMineral != -1 && anio != -1 && mes != -1)
            {
                buttonVolverBuscar.Enabled = true;
                buttonBuscar.Enabled = false;

                comboBoxMineral.Enabled = false;
                textBoxAnio.Enabled = false;
                comboBoxMes.Enabled = false;

                groupBoxAgregarFila.Enabled = true;
            }

            // Buscar datos tabla
            buscar();

            actualizarTabla();

            // Buscar y setear valores fijos.
            setearValoresFijos();

            groupBoxOtrosCampos.Enabled = true;

            comboBoxOCBoletaMes.SelectedIndex = comboBoxMes.SelectedIndex;
            textBoxOCBoletaAnio.Text = textBoxAnio.Text;

            comboBoxOCGuiaMes.SelectedIndex = comboBoxMes.SelectedIndex;
            textBoxOCGuiaAnio.Text = textBoxAnio.Text;
        }

        void buscar()
        {
            filas = new List<Fila>();

            string sql = "SELECT * FROM Filas WHERE idMineral=" + idMineral + " AND anio=" + anio + " AND mes=" + mes;

            OleDbCommand dbCmd = new OleDbCommand(sql, conn);
            OleDbDataReader dbReader = dbCmd.ExecuteReader();

            while (dbReader.Read())
            {
                Fila fila = new Fila();

                fila.idFila = int.Parse(dbReader.GetValue(0)?.ToString() ?? "");
                fila.idMineral = int.Parse(dbReader.GetValue(1)?.ToString() ?? "");
                fila.numGuia = int.Parse(dbReader.GetValue(2)?.ToString() ?? "");
                fila.finalidad = dbReader.GetValue(3)?.ToString() ?? "";
                fila.m3 = float.Parse(dbReader.GetValue(4)?.ToString() ?? "");
                fila.toneladas = float.Parse(dbReader.GetValue(5)?.ToString() ?? "");
                fila.reg = float.Parse(dbReader.GetValue(6)?.ToString() ?? "");
                fila.regEnTon = float.Parse(dbReader.GetValue(7)?.ToString() ?? "");
                fila.precioUnitario = float.Parse(dbReader.GetValue(8)?.ToString() ?? "");
                fila.totalReg = float.Parse(dbReader.GetValue(9)?.ToString() ?? "");
                fila.anio = int.Parse(dbReader.GetValue(10)?.ToString() ?? "");
                fila.mes = int.Parse(dbReader.GetValue(11)?.ToString() ?? "");

                filas.Add(fila);
            }

            buttonGenerarDDJJ.Enabled = filas.Count > 0;
        }

        public void actualizarTabla()
        {
            dataGridViewTabla.Rows.Clear();

            float sumaTotal = 0;

            foreach (Fila fila in filas)
            {
                string[] row = new string[] {fila.numGuia.ToString(),
                fila.finalidad,
                fila.m3.ToString(),
                fila.toneladas.ToString(),
                fila.reg.ToString(),
                fila.regEnTon.ToString(),
                fila.precioUnitario.ToString(),
                fila.totalReg.ToString()};

                sumaTotal += fila.totalReg;

                dataGridViewTabla.Rows.Add(row);
            }

            textBoxValorFinal.Text = sumaTotal + "";
        }

        public void setearValoresFijos()
        {
            limpiarTextBoxAgregar();

            Mineral mineral = ((Mineral)comboBoxMineral.SelectedItem);

            textBoxFInalidad.Text = mineral.finalidad;
            textBoxPorReg.Text = mineral.regalias.ToString();
            textBoxPrecioUnitario.Text = mineral.precioUnitario.ToString();

            textBoxRegEnTon.Text = "0";
            textBoxTotal.Text = "0";

            textBoxNumGuia.Focus();
        }

        public void limpiarTextBoxAgregar()
        {
            textBoxNumGuia.Text = "";
            textBoxM3.Text = "";
            textBoxM3.Text = "";
            textBoxToneladas.Text = "";
            textBoxFInalidad.Text = "";
            textBoxPorReg.Text = "";
            textBoxRegEnTon.Text = "";
            textBoxPrecioUnitario.Text = "";
            textBoxTotal.Text = "";
        }

        private void buttonVolverBuscar_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Realmente desea volver a buscar? Se borrarán todas las moficiaciones aún no guardadas.", "Volver a buscar",
MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                limpiarTextBoxAgregar();
                groupBoxAgregarFila.Enabled = false;

                limpiarTextBoxEditar();
                groupBoxEditarFila.Enabled = false;

                buttonEliminarFila.Enabled = false;
                buttonEditarFila.Enabled = false;

                dataGridViewTabla.Rows.Clear();
                textBoxValorFinal.Text = "";

                groupBoxOtrosCampos.Enabled = false;
                buttonGenerarDDJJ.Enabled = false;

                // Habilito para buscar.
                buttonVolverBuscar.Enabled = false;
                buttonBuscar.Enabled = true;

                comboBoxMineral.Enabled = true;
                textBoxAnio.Enabled = true;
                comboBoxMes.Enabled = true;

                comboBoxMineral.Focus();
            }
        }

        private void textBoxNumGuia_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter || (int)e.KeyChar == (int)Keys.Tab)
            {
                e.Handled = true;
                textBoxM3.Focus();
            }
        }

        private void textBoxM3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter || (int)e.KeyChar == (int)Keys.Tab)
            {
                e.Handled = true;
                textBoxToneladas.Focus();
            }
        }

        private void textBoxToneladas_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter || (int)e.KeyChar == (int)Keys.Tab)
            {
                e.Handled = true;
                buttonAgregarFila.Focus();
            }
        }

        private void dataGridViewTabla_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridViewTabla.SelectedRows.Count > 0)
            {
                buttonEditarFila.Enabled = true;
                buttonEliminarFila.Enabled = true;
            }
            else
            {
                buttonEditarFila.Enabled = false;
                buttonEliminarFila.Enabled = false;
            }
        }

        private void textBoxToneladas_TextChanged(object sender, EventArgs e)
        {
            float ton, regEnTon, precioUnitario;

            try
            {
                ton = float.Parse(textBoxToneladas.Text.Replace(".", ","));
                regEnTon = float.Parse(textBoxPorReg.Text.Replace(".", ","));
                precioUnitario = float.Parse(textBoxPrecioUnitario.Text.Replace(".", ","));

                textBoxRegEnTon.Text = String.Format("{0:0.000}", (ton * (regEnTon / 100)));
                textBoxTotal.Text = String.Format("{0:0.000}", (ton * (regEnTon / 100)) * precioUnitario);

            }
            catch (Exception ex)
            {
                textBoxRegEnTon.Text = "0";
                textBoxTotal.Text = "0";
            }
        }

        private void buttonEditarFila_Click(object sender, EventArgs e)
        {
            groupBoxEditarFila.Enabled = true;
            groupBoxAgregarFila.Enabled = false;
            dataGridViewTabla.Enabled = false;
            buttonEliminarFila.Enabled = false;
            buttonEditarFila.Enabled = false;

            Fila fila = filas[dataGridViewTabla.CurrentRow.Index];

            textBoxNumGuiaE.Text = fila.numGuia.ToString();
            textBoxM3E.Text = fila.m3.ToString();
            textBoxToneladasE.Text = fila.toneladas.ToString();
            textBoxFInalidadE.Text = fila.finalidad;
            textBoxPorRegE.Text = fila.reg.ToString();
            textBoxRegEnTonE.Text = fila.regEnTon.ToString();
            textBoxPrecioUnitarioE.Text = fila.precioUnitario.ToString();
            textBoxTotalE.Text = fila.totalReg.ToString();
        }

        private void buttonCancelarEdición_Click(object sender, EventArgs e)
        {
            restaurarValores();
        }

        public void restaurarValores()
        {
            groupBoxEditarFila.Enabled = false;
            groupBoxAgregarFila.Enabled = true;
            dataGridViewTabla.Enabled = true;
            buttonEliminarFila.Enabled = true;
            buttonEditarFila.Enabled = true;

            limpiarTextBoxEditar();
        }

        public void limpiarTextBoxEditar()
        {
            textBoxNumGuiaE.Text = "";
            textBoxM3E.Text = "";
            textBoxToneladasE.Text = "";
            textBoxFInalidadE.Text = "";
            textBoxPorRegE.Text = "";
            textBoxRegEnTonE.Text = "";
            textBoxPrecioUnitarioE.Text = "";
            textBoxTotalE.Text = "";
        }

        private void buttonConfirmarEdicion_Click(object sender, EventArgs e)
        {
            Fila filaNueva = new Fila();

            try
            {
                if (!textBoxNumGuiaE.Text.Equals(""))
                    filaNueva.numGuia = int.Parse(textBoxNumGuiaE.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en el campo 'Número de guía'", "Error");
                return;
            }

            try
            {
                if (!textBoxM3E.Text.Equals(""))
                    filaNueva.m3 = float.Parse(textBoxM3E.Text.Replace(".", ","));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en el campo 'M3'", "Error");
                return;
            }

            try
            {
                if (!textBoxToneladasE.Text.Equals(""))
                    filaNueva.toneladas = float.Parse(textBoxToneladasE.Text.Replace(".", ","));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en el campo 'Toneladas'", "Error");
                return;
            }

            filaNueva.finalidad = textBoxFInalidad.Text;

            try
            {
                if (!textBoxPorRegE.Text.Equals(""))
                    filaNueva.reg = float.Parse(textBoxPorRegE.Text.Replace(".", ","));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en el campo 'Porc. Reg.'", "Error");
                return;
            }

            try
            {
                if (!textBoxRegEnTonE.Text.Equals(""))
                    filaNueva.regEnTon = float.Parse(textBoxRegEnTonE.Text.Replace(".", ","));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en el campo 'Reg. en Ton.'", "Error");
                return;
            }

            try
            {
                if (!textBoxPrecioUnitarioE.Text.Equals(""))
                    filaNueva.precioUnitario = float.Parse(textBoxPrecioUnitarioE.Text.Replace(".", ","));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en el campo 'Precio uni.'", "Error");
                return;
            }

            try
            {
                if (!textBoxTotalE.Text.Equals(""))
                    filaNueva.totalReg = float.Parse(textBoxTotalE.Text.Replace(".", ","));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en el campo 'Total'", "Error");
                return;
            }

            Fila fila = filas[dataGridViewTabla.CurrentRow.Index];

            string msg = "¿Realmente desea confirmar estos cambios?" +
            "\n\nNumeroGuia\t " + filaNueva.numGuia + " por " + fila.numGuia +
            "\nFinalidad\t " + filaNueva.finalidad + " por " + fila.finalidad +
            "\nM3\t\t " + filaNueva.m3 + " por " + fila.m3 +
            "\nToneladas\t " + filaNueva.toneladas + " por " + fila.toneladas +
            "\nReg\t\t " + filaNueva.reg + " por " + fila.reg +
            "\nRegEnTon\t " + filaNueva.regEnTon + " por " + fila.regEnTon +
            "\nPrecioUnitario\t " + filaNueva.precioUnitario + " por " + fila.precioUnitario +
            "\nTotalReg\t\t " + filaNueva.totalReg + " por " + fila.totalReg;

            DialogResult result = MessageBox.Show(msg, "Editar filas",
MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                string sql = "UPDATE Filas SET NumeroGuia = " + filaNueva.numGuia +
               ", Finalidad = '" + filaNueva.finalidad + "'" +
               ", M3 = " + filaNueva.m3.ToString().Replace(",", ".") +
               ", Toneladas = " + filaNueva.toneladas.ToString().Replace(",", ".") +
               ", Reg = " + filaNueva.reg.ToString().Replace(",", ".") +
               ", RegEnTon = " + filaNueva.regEnTon.ToString().Replace(",", ".") +
               ", PrecioUnitario = " + filaNueva.precioUnitario.ToString().Replace(",", ".") +
               ", TotalReg = " + filaNueva.totalReg.ToString().Replace(",", ".") +
               " WHERE Id = " + fila.idFila;

                OleDbDataAdapter oledbAdapter = new OleDbDataAdapter();

                oledbAdapter.UpdateCommand = conn.CreateCommand();
                oledbAdapter.UpdateCommand.CommandText = sql;

                int i = oledbAdapter.UpdateCommand.ExecuteNonQuery();

                if (i != 1)
                {
                    MessageBox.Show("Los valores no se han podido modificar.", "Error");
                }
                else
                {
                    // Buscar datos tabla
                    buscar();
                    actualizarTabla();
                    // Buscar y setear valores fijos.
                    setearValoresFijos();
                    restaurarValores();
                }
            }
        }

        private void buttonEliminarFila_MouseClick(object sender, MouseEventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Realmente desea borrar la fila seleccionada?", "Cuidado",
MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                try
                {
                    int i = filas[dataGridViewTabla.CurrentRow.Index].idFila;

                    OleDbCommand oleDbCommand = new OleDbCommand("DELETE FROM Filas WHERE id=" + i, conn);

                    if (oleDbCommand.ExecuteNonQuery() != 1)
                    {
                        MessageBox.Show("No se ha podido eliminar el registro.", "Error");
                    }
                    else
                    {
                        buscar();
                        actualizarTabla();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("No se ha podido eliminar el registro.", "Error");
                }
            }
            else if (result == DialogResult.No)
            {
                //code for No
            }
            else if (result == DialogResult.Cancel)
            {
                //code for Cancel
            }
        }

        private void textBoxToneladasE_TextChanged(object sender, EventArgs e)
        {
            float ton, regEnTon, precioUnitario;

            try
            {
                ton = float.Parse(textBoxToneladasE.Text.Replace(".", ","));
                regEnTon = float.Parse(textBoxPorRegE.Text.Replace(".", ","));
                precioUnitario = float.Parse(textBoxPrecioUnitarioE.Text.Replace(".", ","));

                textBoxRegEnTonE.Text = String.Format("{0:0.000}", (ton * (regEnTon / 100)));
                textBoxTotalE.Text = String.Format("{0:0.000}", (ton * (regEnTon / 100)) * precioUnitario);

            }
            catch (Exception ex)
            {
                textBoxRegEnTonE.Text = "0";
                textBoxTotalE.Text = "0";
            }
        }

        private void textBoxValorFinal_TextChanged(object sender, EventArgs e)
        {
            textBoxOCTotal.Text = textBoxValorFinal.Text;
        }

        private void buttonGenerarDDJJ_Click(object sender, EventArgs e)
        {
            WIP wip = new WIP("Generando declaración jurada...");
            wip.Show();

            try
            {
                String path = Path.GetTempPath() + "DDJJ.docx";
                String _path = Path.GetTempPath() + "DDJJ";

                do
                {
                    try
                    {
                        File.Copy(Program.getActualPath() + @"\data\PlantillaDDJJ.docx", path, true);
                        break;
                    }
                    catch (Exception ex)
                    {
                        path = _path + numeroArchivo + ".docx";
                        numeroArchivo++;
                    }
                } while (numeroArchivo != 12);

                if (numeroArchivo == 12)
                {
                    MessageBox.Show("No se pudo crear la Declaración Jurada porque hay muchos documentos abiertos. Por favor ciérrelos e intente nuevamente.", "Error");
                }

                Application ap = new Application();
                Document document = ap.Documents.Open(path);
                document.Activate();

                FindAndReplace(ap, "{1}", ((Item)comboBoxMes.SelectedItem).name);
                FindAndReplace(ap, "{2}", textBoxAnio.Text);

                Mineral min = (Mineral)comboBoxMineral.SelectedItem;

                FindAndReplace(ap, "{3}", min.yacimiento);
                FindAndReplace(ap, "{4}", min.ToString());

                XmlDocument xml = new XmlDocument();
                xml.Load(Program.getActualPath() + @"\data\config.xml");

                XmlNodeList xmlNode = xml.SelectNodes("gestionMinera");

                String productor = Convert.ToString(xmlNode[0]["productor"].InnerText);
                String num = Convert.ToString(xmlNode[0]["num"].InnerText);

                FindAndReplace(ap, "{5}", productor);
                FindAndReplace(ap, "{6}", num);

                int total = 7;

                foreach (DataGridViewRow row in dataGridViewTabla.Rows)
                {
                    FindAndReplace(ap, "{" + total++ + "}", row.Cells[0].Value.ToString());
                    FindAndReplace(ap, "{" + total++ + "}", row.Cells[1].Value.ToString());
                    FindAndReplace(ap, "{" + total++ + "}", row.Cells[3].Value.ToString());
                    FindAndReplace(ap, "{" + total++ + "}", row.Cells[4].Value.ToString());
                    FindAndReplace(ap, "{" + total++ + "}", row.Cells[5].Value.ToString());
                    FindAndReplace(ap, "{" + total++ + "}", row.Cells[6].Value.ToString());
                    FindAndReplace(ap, "{" + total++ + "}", row.Cells[7].Value.ToString());
                }

                while (total <= 174)
                {
                    FindAndReplace(ap, "{" + total++ + "}", "");
                }

                FindAndReplace(ap, "{175}", textBoxValorFinal.Text);

                if (radioButtonGuiaSi.Enabled)
                {
                    FindAndReplace(ap, "{176}", "SÍ");
                    FindAndReplace(ap, "{177}", ((Item)comboBoxOCGuiaMes.SelectedItem).ToString());
                    FindAndReplace(ap, "{178}", textBoxOCGuiaAnio.Text);
                }
                else
                {
                    FindAndReplace(ap, "{176}", "No");
                    FindAndReplace(ap, "{177}", "");
                    FindAndReplace(ap, "{178}", "");
                }

                if (radioButtonBolSi.Enabled)
                {
                    FindAndReplace(ap, "{179}", "SÍ");
                    FindAndReplace(ap, "{180}", textBoxOCTotal.Text);
                    FindAndReplace(ap, "{181}", ((Item)comboBoxOCBoletaMes.SelectedItem).ToString());
                    FindAndReplace(ap, "{182}", textBoxOCBoletaAnio.Text);

                }
                else
                {
                    FindAndReplace(ap, "{179}", "NO");
                    FindAndReplace(ap, "{180}", "");
                    FindAndReplace(ap, "{181}", "");
                    FindAndReplace(ap, "{182}", "");
                }

                DateTime fecha = dateTimePickerFecha.Value;

                FindAndReplace(ap, "{183}", fecha.Day.ToString() + " de " + dtinfo.GetMonthName(DateTime.Now.Month) + " de " + DateTime.Now.Year.ToString());

                document.Close();

                System.Diagnostics.Process.Start(path);
            }
            catch (Exception ex)
            {
                MessageBox.Show("No se pudo crea el archivo\nError: " + ex.Message, "Error");
            }

            wip.Close();
        }

        private void FindAndReplace(Application doc, object findText, object replaceWithText)
        {
            //options
            object matchCase = false;
            object matchWholeWord = true;
            object matchWildCards = false;
            object matchSoundsLike = false;
            object matchAllWordForms = false;
            object forward = true;
            object format = false;
            object matchKashida = false;
            object matchDiacritics = false;
            object matchAlefHamza = false;
            object matchControl = false;
            object read_only = false;
            object visible = true;
            object replace = 2;
            object wrap = 1;
            //execute find and replace
            doc.Selection.Find.Execute(ref findText, ref matchCase, ref matchWholeWord,
                ref matchWildCards, ref matchSoundsLike, ref matchAllWordForms, ref forward, ref wrap, ref format, ref replaceWithText, ref replace,
                ref matchKashida, ref matchDiacritics, ref matchAlefHamza, ref matchControl);
        }

        private void radioButtonGuiaSi_CheckedChanged(object sender, EventArgs e)
        {
            comboBoxOCGuiaMes.Enabled = radioButtonGuiaSi.Checked;
            textBoxOCGuiaAnio.Enabled = radioButtonGuiaSi.Checked;
        }


        void agregarFila()
        {
            Fila fila = new Fila();

            try
            {
                if (!textBoxNumGuia.Text.Equals(""))
                    fila.numGuia = int.Parse(textBoxNumGuia.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en el campo 'Número de guía'", "Error");
                return;
            }

            try
            {
                if (!textBoxM3.Text.Equals(""))
                    fila.m3 = float.Parse(textBoxM3.Text.Replace(".", ","));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en el campo 'M3'", "Error");
                return;
            }

            try
            {
                if (!textBoxToneladas.Text.Equals(""))
                    fila.toneladas = float.Parse(textBoxToneladas.Text.Replace(".", ","));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en el campo 'Toneladas'", "Error");
                return;
            }

            fila.finalidad = textBoxFInalidad.Text;

            try
            {
                if (!textBoxPorReg.Text.Equals(""))
                    fila.reg = float.Parse(textBoxPorReg.Text.Replace(".", ","));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en el campo 'Porc. Reg.'", "Error");
                return;
            }

            try
            {
                if (!textBoxRegEnTon.Text.Equals(""))
                    fila.regEnTon = float.Parse(textBoxRegEnTon.Text.Replace(".", ","));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en el campo 'Reg. en Ton.'", "Error");
                return;
            }

            try
            {
                if (!textBoxPrecioUnitario.Text.Equals(""))
                    fila.precioUnitario = float.Parse(textBoxPrecioUnitario.Text.Replace(".", ","));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en el campo 'Precio uni.'", "Error");
                return;
            }

            try
            {
                if (!textBoxTotal.Text.Equals(""))
                    fila.totalReg = float.Parse(textBoxTotal.Text.Replace(".", ","));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en el campo 'Total'", "Error");
                return;
            }

            string sql = "INSERT INTO Filas (idMineral, NumeroGuia, Finalidad, M3, Toneladas, Reg, RegEnTon, PrecioUnitario, TotalReg, Anio, Mes) VALUES (";

            sql += idMineral + ", " +
                fila.numGuia + ", " +
                "'" + fila.finalidad + "', " +
                fila.m3.ToString().Replace(",", ".") + ", " +
                fila.toneladas.ToString().Replace(",", ".") + ", " +
                fila.reg.ToString().Replace(",", ".") + ", " +
                fila.regEnTon.ToString().Replace(",", ".") + ", " +
                fila.precioUnitario.ToString().Replace(",", ".") + ", " +
                fila.totalReg.ToString().Replace(",", ".") + ", " +
                anio + ", " +
                mes + ")";

            OleDbCommand oleDbCommand = new OleDbCommand(sql, conn);

            if (oleDbCommand.ExecuteNonQuery() != 1)
            {
                MessageBox.Show("No se ha podido agregar la fila.", "Error");
            }
            else
            {
                buscar();
                actualizarTabla();
                setearValoresFijos();
            }
        }

        private void buttonAgregarFila_Click_3(object sender, EventArgs e)
        {
            agregarFila();
            textBoxNumGuia.Focus();
        }

        private void buttonAgregarFila_KeyPress_2(object sender, KeyPressEventArgs e)
        {
            buttonAgregarFila.PerformClick();
        }

        private void buttonAgregarFila_Click_4(object sender, EventArgs e)
        {
            agregarFila();
            textBoxNumGuia.Focus();
        }

        private void dataGridViewTabla_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
        {
            buttonEditarFila.Enabled = dataGridViewTabla.SelectedRows.Count > 0;
            buttonEliminarFila.Enabled = dataGridViewTabla.SelectedRows.Count > 0;
        }

        private void buttonEliminarFila_Click(object sender, EventArgs e)
        {

        }
    }
}