﻿namespace DDJJ_Regalías_Mineras.Ventanas
{
    partial class ABMMinerales
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ABMMinerales));
            this.error = new System.Windows.Forms.ErrorProvider(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.butAgregar = new System.Windows.Forms.Button();
            this.tbToneladasAM3 = new System.Windows.Forms.TextBox();
            this.tbVecimiento = new System.Windows.Forms.TextBox();
            this.tbRegalias = new System.Windows.Forms.TextBox();
            this.tbFinalidad = new System.Windows.Forms.TextBox();
            this.tbPrecio = new System.Windows.Forms.TextBox();
            this.tbNombre = new System.Windows.Forms.TextBox();
            this.tbYacimiento = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label9 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.butCancelar = new System.Windows.Forms.Button();
            this.cbMineral = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.buteEditar = new System.Windows.Forms.Button();
            this.tbToneladasAM3E = new System.Windows.Forms.TextBox();
            this.tbVencimientoE = new System.Windows.Forms.TextBox();
            this.tbRegaliasE = new System.Windows.Forms.TextBox();
            this.tbFinalidadE = new System.Windows.Forms.TextBox();
            this.tbPrecioE = new System.Windows.Forms.TextBox();
            this.tbNombreE = new System.Windows.Forms.TextBox();
            this.tbYacimientoE = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.tbCoeficiente = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.tbCoeficienteE = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.error)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // error
            // 
            this.error.ContainerControl = this;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.tbCoeficiente);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.butAgregar);
            this.groupBox1.Controls.Add(this.tbToneladasAM3);
            this.groupBox1.Controls.Add(this.tbVecimiento);
            this.groupBox1.Controls.Add(this.tbRegalias);
            this.groupBox1.Controls.Add(this.tbFinalidad);
            this.groupBox1.Controls.Add(this.tbPrecio);
            this.groupBox1.Controls.Add(this.tbNombre);
            this.groupBox1.Controls.Add(this.tbYacimiento);
            this.groupBox1.Controls.Add(this.tableLayoutPanel1);
            this.groupBox1.Location = new System.Drawing.Point(12, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(275, 305);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "AGREGAR MINERAL";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(206, 127);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(15, 13);
            this.label8.TabIndex = 40;
            this.label8.Text = "%";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(153, 202);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 38;
            this.label7.Text = "x TON";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // butAgregar
            // 
            this.butAgregar.Location = new System.Drawing.Point(178, 269);
            this.butAgregar.Name = "butAgregar";
            this.butAgregar.Size = new System.Drawing.Size(75, 23);
            this.butAgregar.TabIndex = 100;
            this.butAgregar.TabStop = false;
            this.butAgregar.Text = "AGREGAR";
            this.butAgregar.UseVisualStyleBackColor = true;
            this.butAgregar.Click += new System.EventHandler(this.button1_Click_1);
            this.butAgregar.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.butAgregar_KeyPress);
            // 
            // tbToneladasAM3
            // 
            this.tbToneladasAM3.Location = new System.Drawing.Point(144, 218);
            this.tbToneladasAM3.Name = "tbToneladasAM3";
            this.tbToneladasAM3.Size = new System.Drawing.Size(56, 20);
            this.tbToneladasAM3.TabIndex = 39;
            this.tbToneladasAM3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbToneladasAM3_KeyPress);
            // 
            // tbVecimiento
            // 
            this.tbVecimiento.Location = new System.Drawing.Point(144, 149);
            this.tbVecimiento.Name = "tbVecimiento";
            this.tbVecimiento.Size = new System.Drawing.Size(109, 20);
            this.tbVecimiento.TabIndex = 35;
            this.tbVecimiento.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbVecimiento_KeyPress);
            // 
            // tbRegalias
            // 
            this.tbRegalias.Location = new System.Drawing.Point(144, 124);
            this.tbRegalias.Name = "tbRegalias";
            this.tbRegalias.Size = new System.Drawing.Size(56, 20);
            this.tbRegalias.TabIndex = 37;
            this.tbRegalias.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbRegalias_KeyPress);
            // 
            // tbFinalidad
            // 
            this.tbFinalidad.Location = new System.Drawing.Point(144, 99);
            this.tbFinalidad.Name = "tbFinalidad";
            this.tbFinalidad.Size = new System.Drawing.Size(109, 20);
            this.tbFinalidad.TabIndex = 36;
            this.tbFinalidad.Text = "VENTA";
            this.tbFinalidad.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbFinalidad_KeyPress);
            // 
            // tbPrecio
            // 
            this.tbPrecio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbPrecio.Location = new System.Drawing.Point(144, 73);
            this.tbPrecio.Name = "tbPrecio";
            this.tbPrecio.Size = new System.Drawing.Size(109, 21);
            this.tbPrecio.TabIndex = 34;
            this.tbPrecio.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbPrecio_KeyPress);
            // 
            // tbNombre
            // 
            this.tbNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNombre.Location = new System.Drawing.Point(144, 23);
            this.tbNombre.Name = "tbNombre";
            this.tbNombre.Size = new System.Drawing.Size(109, 21);
            this.tbNombre.TabIndex = 32;
            this.tbNombre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbNombre_KeyPress);
            // 
            // tbYacimiento
            // 
            this.tbYacimiento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbYacimiento.Location = new System.Drawing.Point(144, 48);
            this.tbYacimiento.Name = "tbYacimiento";
            this.tbYacimiento.Size = new System.Drawing.Size(109, 21);
            this.tbYacimiento.TabIndex = 33;
            this.tbYacimiento.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbYacimiento_KeyPress);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.label19, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.label9, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 7);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(14, 21);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 8;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(124, 207);
            this.tableLayoutPanel1.TabIndex = 30;
            this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(40, 131);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(81, 13);
            this.label9.TabIndex = 26;
            this.label9.Text = "VENCIMIENTO";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(10, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 15);
            this.label3.TabIndex = 4;
            this.label3.Text = "PRECIO UNITARIO";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(42, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "YACIMIENTO";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "NOMBRE MINERAL";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(58, 81);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 13);
            this.label6.TabIndex = 22;
            this.label6.Text = "FINALIDAD";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(61, 106);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 13);
            this.label5.TabIndex = 24;
            this.label5.Text = "REGALÍAS";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(81, 184);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 27;
            this.label4.Text = "1 M3 =";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.White;
            this.groupBox2.Controls.Add(this.tbCoeficienteE);
            this.groupBox2.Controls.Add(this.butCancelar);
            this.groupBox2.Controls.Add(this.cbMineral);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.buteEditar);
            this.groupBox2.Controls.Add(this.tbToneladasAM3E);
            this.groupBox2.Controls.Add(this.tbVencimientoE);
            this.groupBox2.Controls.Add(this.tbRegaliasE);
            this.groupBox2.Controls.Add(this.tbFinalidadE);
            this.groupBox2.Controls.Add(this.tbPrecioE);
            this.groupBox2.Controls.Add(this.tbNombreE);
            this.groupBox2.Controls.Add(this.tbYacimientoE);
            this.groupBox2.Controls.Add(this.tableLayoutPanel2);
            this.groupBox2.Location = new System.Drawing.Point(300, 13);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(273, 305);
            this.groupBox2.TabIndex = 41;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "EDITAR MINERAL";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // butCancelar
            // 
            this.butCancelar.Location = new System.Drawing.Point(39, 269);
            this.butCancelar.Name = "butCancelar";
            this.butCancelar.Size = new System.Drawing.Size(75, 23);
            this.butCancelar.TabIndex = 42;
            this.butCancelar.Text = "CANCELAR";
            this.butCancelar.UseVisualStyleBackColor = true;
            this.butCancelar.Click += new System.EventHandler(this.butCancelar_Click);
            // 
            // cbMineral
            // 
            this.cbMineral.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMineral.Location = new System.Drawing.Point(11, 19);
            this.cbMineral.Name = "cbMineral";
            this.cbMineral.Size = new System.Drawing.Size(124, 21);
            this.cbMineral.TabIndex = 41;
            this.cbMineral.SelectionChangeCommitted += new System.EventHandler(this.cbMineral_SelectionChangeCommitted);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(203, 152);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(15, 13);
            this.label10.TabIndex = 40;
            this.label10.Text = "%";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(150, 227);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(38, 13);
            this.label11.TabIndex = 38;
            this.label11.Text = "x TON";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // buteEditar
            // 
            this.buteEditar.Location = new System.Drawing.Point(120, 269);
            this.buteEditar.Name = "buteEditar";
            this.buteEditar.Size = new System.Drawing.Size(130, 23);
            this.buteEditar.TabIndex = 31;
            this.buteEditar.Text = "CONFIRMAR CAMBIOS";
            this.buteEditar.UseVisualStyleBackColor = true;
            this.buteEditar.Click += new System.EventHandler(this.buteEditar_Click);
            this.buteEditar.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.buteEditar_KeyPress);
            // 
            // tbToneladasAM3E
            // 
            this.tbToneladasAM3E.Location = new System.Drawing.Point(141, 243);
            this.tbToneladasAM3E.Name = "tbToneladasAM3E";
            this.tbToneladasAM3E.Size = new System.Drawing.Size(56, 20);
            this.tbToneladasAM3E.TabIndex = 39;
            this.tbToneladasAM3E.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbToneladasAM3E_KeyPress);
            // 
            // tbVencimientoE
            // 
            this.tbVencimientoE.Location = new System.Drawing.Point(141, 174);
            this.tbVencimientoE.Name = "tbVencimientoE";
            this.tbVencimientoE.Size = new System.Drawing.Size(109, 20);
            this.tbVencimientoE.TabIndex = 35;
            this.tbVencimientoE.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbVencimientoE_KeyPress);
            // 
            // tbRegaliasE
            // 
            this.tbRegaliasE.Location = new System.Drawing.Point(141, 149);
            this.tbRegaliasE.Name = "tbRegaliasE";
            this.tbRegaliasE.Size = new System.Drawing.Size(56, 20);
            this.tbRegaliasE.TabIndex = 37;
            this.tbRegaliasE.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbRegaliasE_KeyPress);
            // 
            // tbFinalidadE
            // 
            this.tbFinalidadE.Location = new System.Drawing.Point(141, 124);
            this.tbFinalidadE.Name = "tbFinalidadE";
            this.tbFinalidadE.Size = new System.Drawing.Size(109, 20);
            this.tbFinalidadE.TabIndex = 36;
            this.tbFinalidadE.Text = "VENTA";
            this.tbFinalidadE.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbFinalidadE_KeyPress);
            // 
            // tbPrecioE
            // 
            this.tbPrecioE.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbPrecioE.Location = new System.Drawing.Point(141, 98);
            this.tbPrecioE.Name = "tbPrecioE";
            this.tbPrecioE.Size = new System.Drawing.Size(109, 21);
            this.tbPrecioE.TabIndex = 34;
            this.tbPrecioE.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbPrecioE_KeyPress);
            // 
            // tbNombreE
            // 
            this.tbNombreE.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNombreE.Location = new System.Drawing.Point(141, 48);
            this.tbNombreE.Name = "tbNombreE";
            this.tbNombreE.Size = new System.Drawing.Size(109, 21);
            this.tbNombreE.TabIndex = 32;
            this.tbNombreE.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbNombreE_KeyPress);
            // 
            // tbYacimientoE
            // 
            this.tbYacimientoE.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbYacimientoE.Location = new System.Drawing.Point(141, 73);
            this.tbYacimientoE.Name = "tbYacimientoE";
            this.tbYacimientoE.Size = new System.Drawing.Size(109, 21);
            this.tbYacimientoE.TabIndex = 33;
            this.tbYacimientoE.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbYacimientoE_KeyPress);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.label20, 0, 6);
            this.tableLayoutPanel2.Controls.Add(this.label12, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.label13, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.label14, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label15, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label16, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.label17, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.label18, 0, 7);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(11, 46);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 8;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(124, 201);
            this.tableLayoutPanel2.TabIndex = 30;
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(40, 131);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(81, 13);
            this.label12.TabIndex = 26;
            this.label12.Text = "VENCIMIENTO";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(10, 55);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(111, 15);
            this.label13.TabIndex = 4;
            this.label13.Text = "PRECIO UNITARIO";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(42, 30);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(79, 15);
            this.label14.TabIndex = 2;
            this.label14.Text = "YACIMIENTO";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(3, 5);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(118, 15);
            this.label15.TabIndex = 0;
            this.label15.Text = "NOMBRE MINERAL";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(58, 81);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(63, 13);
            this.label16.TabIndex = 22;
            this.label16.Text = "FINALIDAD";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label17
            // 
            this.label17.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(61, 106);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(60, 13);
            this.label17.TabIndex = 24;
            this.label17.Text = "REGALÍAS";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(81, 181);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(40, 13);
            this.label18.TabIndex = 27;
            this.label18.Text = "1 M3 =";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbCoeficiente
            // 
            this.tbCoeficiente.Location = new System.Drawing.Point(144, 175);
            this.tbCoeficiente.Name = "tbCoeficiente";
            this.tbCoeficiente.Size = new System.Drawing.Size(109, 20);
            this.tbCoeficiente.TabIndex = 101;
            // 
            // label19
            // 
            this.label19.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(44, 156);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(77, 13);
            this.label19.TabIndex = 102;
            this.label19.Text = "COEFICIENTE";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label20
            // 
            this.label20.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(44, 156);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(77, 13);
            this.label20.TabIndex = 28;
            this.label20.Text = "COEFICIENTE";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbCoeficienteE
            // 
            this.tbCoeficienteE.Location = new System.Drawing.Point(141, 200);
            this.tbCoeficienteE.Name = "tbCoeficienteE";
            this.tbCoeficienteE.Size = new System.Drawing.Size(109, 20);
            this.tbCoeficienteE.TabIndex = 102;
            // 
            // ABMMinerales
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(585, 326);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(601, 322);
            this.Name = "ABMMinerales";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Agregar y editar minerales";
            ((System.ComponentModel.ISupportInitialize)(this.error)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ErrorProvider error;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button butAgregar;
        private System.Windows.Forms.TextBox tbToneladasAM3;
        private System.Windows.Forms.TextBox tbVecimiento;
        private System.Windows.Forms.TextBox tbRegalias;
        private System.Windows.Forms.TextBox tbFinalidad;
        private System.Windows.Forms.TextBox tbPrecio;
        private System.Windows.Forms.TextBox tbNombre;
        private System.Windows.Forms.TextBox tbYacimiento;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cbMineral;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button buteEditar;
        private System.Windows.Forms.TextBox tbToneladasAM3E;
        private System.Windows.Forms.TextBox tbVencimientoE;
        private System.Windows.Forms.TextBox tbRegaliasE;
        private System.Windows.Forms.TextBox tbFinalidadE;
        private System.Windows.Forms.TextBox tbPrecioE;
        private System.Windows.Forms.TextBox tbNombreE;
        private System.Windows.Forms.TextBox tbYacimientoE;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button butCancelar;
        private System.Windows.Forms.TextBox tbCoeficienteE;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox tbCoeficiente;
        private System.Windows.Forms.Label label19;
    }
}