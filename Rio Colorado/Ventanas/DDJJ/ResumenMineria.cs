﻿using DDJJ_Regalías_Mineras.Clases;
using Gestion_Minera.Clases;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Windows.Forms;

namespace Gestion_Minera.Ventanas.DDJJ
{
    public partial class ResumenMineria : Form
    {
        OleDbConnection conn;
        List<TextBox> meses;
        List<Combustible> lista;

        string[] row1 = new string[] { "TN", "", "", "", "", "", "", "", "", "", "", "", "" };
        string[] row2 = new string[] { "M3", "", "", "", "", "", "", "", "", "", "", "", "" };
        string[] row3 = new string[] { "Pago a minería", "", "", "", "", "", "", "", "", "", "", "", "" };
        string[] row4 = new string[] { "Pago a ENTE", "", "", "", "", "", "", "", "", "", "", "", "" };

        public ResumenMineria(OleDbConnection conn)
        {
            InitializeComponent();

            this.conn = conn;

            cbMineral.Items.Clear();

            foreach (Mineral mineral in Utils.getMinerales(conn))
            {
                cbMineral.Items.Add(mineral);
            }

            meses = new List<TextBox>();

            meses.Add(tbEne);
            meses.Add(tbFeb);
            meses.Add(tbMar);
            meses.Add(tbAbr);
            meses.Add(tbMay);
            meses.Add(tbJun);
            meses.Add(tbJul);
            meses.Add(tbAgo);
            meses.Add(tbSep);
            meses.Add(tbOct);
            meses.Add(tbNov);
            meses.Add(tbDic);

            numAnio.Value = numCombustibleAnio.Value = DateTime.Today.Year;

            butImprimirTabla.Enabled = false;
        }

        private void buteBuscar_Click(object sender, EventArgs e)
        {
            actualizarTabla();
        }

        public void actualizarTabla()
        {
            if (cbMineral.SelectedIndex >= 0)
            {
                butImprimirTabla.Enabled = true;

                List<Fila> lista2 = Utils.getTablaDDJJ(conn, ((Mineral)cbMineral.SelectedItem).ID, int.Parse(numAnio.Value.ToString()));

                dgTabla.Rows.Clear();

                row1 = new string[] { "TN", "", "", "", "", "", "", "", "", "", "", "", "" };
                row2 = new string[] { "M3", "", "", "", "", "", "", "", "", "", "", "", "" };
                row3 = new string[] { "Pago a minería", "", "", "", "", "", "", "", "", "", "", "", "" };
                row4 = new string[] { "Pago a ENTE", "", "", "", "", "", "", "", "", "", "", "", "" };

                string[] toolTipRow1 = new string[] { "", "", "", "", "", "", "", "", "", "", "", "", "" };
                string[] toolTipRow2 = new string[] { "", "", "", "", "", "", "", "", "", "", "", "", "" };
                string[] toolTipRow3 = new string[] { "", "", "", "", "", "", "", "", "", "", "", "", "" };
                string[] toolTipRow4 = new string[] { "", "", "", "", "", "", "", "", "", "", "", "", "" };

                float totalTon = 0, totalM3 = 0, totalMineria = 0, totalEnte = 0;

                foreach (Fila fila in lista2)
                {
                    float m3 = fila.toneladas / ((Mineral)cbMineral.SelectedItem).tonAM3;
                    float ente = ((Mineral)cbMineral.SelectedItem).coeficiente * lista[fila.mes - 1].valor * m3;

                    totalTon += fila.toneladas;
                    row1[fila.mes] = fila.toneladas.ToString();

                    if (m3 == 0)
                    {
                        row2[fila.mes] = "0";
                    }
                    else
                    {
                        totalM3 += m3;
                        row2[fila.mes] = String.Format("{0:#,0.00}", m3);
                    }

                    toolTipRow2[fila.mes] = fila.toneladas + " (TON) / " + ((Mineral)cbMineral.SelectedItem).tonAM3;

                    if (fila.totalReg == 0)
                    {
                        row3[fila.mes] = "0";
                    }
                    else
                    {
                        totalMineria += fila.totalReg;
                        row3[fila.mes] = String.Format("{0:#,0.00}", fila.totalReg);
                    }

                    if (ente == 0)
                    {
                        row4[fila.mes] = "0";
                    }
                    else
                    {
                        totalEnte += ente;
                        row4[fila.mes] = String.Format("{0:#,0.00}", ente);
                    }

                    toolTipRow4[fila.mes] = ((Mineral)cbMineral.SelectedItem).coeficiente + " (COEF) * " +
                        lista[fila.mes - 1].valor + " (COMB) * " + m3 + " (M3)";
                }

                dgTabla.Rows.Add(row1);
                dgTabla.Rows.Add(row2);
                dgTabla.Rows.Add(row3);
                dgTabla.Rows.Add(row4);

                tbTotalTon.Text = String.Format("{0:#,0.00}", totalTon);
                tbTotalM3.Text = String.Format("{0:#,0.00}", totalM3);
                tbTotalMineria.Text = String.Format("{0:#,0.00}", totalMineria);
                tbTotalEnte.Text = String.Format("{0:#,0.00}", totalEnte);

                for (int i = 0; i < 12; i++)
                {
                    dgTabla.Rows[1].Cells[i].ToolTipText = toolTipRow2[i];
                    dgTabla.Rows[3].Cells[i].ToolTipText = toolTipRow4[i];
                }
            }
            else
            {
                butImprimirTabla.Enabled = false;
            }
        }

        private void butGuardarValores_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < 12; i++)
            {
                if (lista[i].valor != float.Parse(meses[i].Text.Replace(".", ",")))
                {
                    if (lista[i].ID == 0)
                    {
                        lista[i].valor = float.Parse(meses[i].Text.Replace(".", ","));

                        Utils.agregarCombustible(conn, lista[i]);
                    }
                    else
                    {
                        lista[i].valor = float.Parse(meses[i].Text.Replace(".", ","));

                        Utils.editarCombustible(conn, lista[i]);
                    }
                }
            }

            actualizarTablaCombustible();

            actualizarTabla();
        }

        private void numCombustibleAnio_ValueChanged(object sender, EventArgs e)
        {
            actualizarTablaCombustible();
        }

        public void actualizarTablaCombustible()
        {
            lista = Utils.getCombustible(conn, int.Parse(numCombustibleAnio.Value.ToString()));

            for (int i = 0; i < 12; i++)
            {
                meses[i].Text = lista[i].valor.ToString();
            }
        }

        public void llenarTablaCombustible()
        {

        }

        private void cbMineral_SelectedIndexChanged(object sender, EventArgs e)
        {
            actualizarTabla();
        }

        private void numAnio_ValueChanged(object sender, EventArgs e)
        {
            actualizarTabla();
        }

        private void tbEne_Click(object sender, EventArgs e)
        {
            tbEne.Focus();
        }

        private void butImprimirTabla_Click(object sender, EventArgs e)
        {
            Utils.generarResumenMineria(((Mineral)cbMineral.SelectedItem).ToString(),
                int.Parse(numAnio.Value.ToString()),
                row1,
                row2,
                row3,
                row4);
        }
    }
}
