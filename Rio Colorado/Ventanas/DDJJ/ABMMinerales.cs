﻿using DDJJ_Regalías_Mineras.Clases;
using Gestion_Minera.Clases;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DDJJ_Regalías_Mineras.Ventanas
{
    public partial class ABMMinerales : Form
    {
        OleDbConnection conn;

        Mineral mineralSeleccionado;

        public ABMMinerales(OleDbConnection conn)
        {
            InitializeComponent();

            this.conn = conn;

            actualizarComboMinerales(Utils.getMinerales(conn));

            tbNombre.Focus();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            error.Clear();

            if (tbNombre.Text.Length == 0)
            {
                error.SetError(tbNombre, "Debe especificar obligatoriamente un nombre.");
                tbNombre.Focus();
                return;
            }

            Mineral mineral = new Mineral();

            mineral.nombre = tbNombre.Text;
            mineral.yacimiento = tbYacimiento.Text;

            try
            {
                if (!tbPrecio.Text.Equals(""))
                    mineral.precioUnitario = float.Parse(tbPrecio.Text.Replace(".", ","));
            }
            catch (Exception ex)
            {
                error.SetError(tbPrecio, "Error en el formato del número.");
                tbPrecio.Focus();
                return;
            }

            mineral.finalidad = tbFinalidad.Text;

            try
            {
                if (!tbRegalias.Text.Equals(""))
                    mineral.regalias = float.Parse(tbRegalias.Text.Replace(".", ","));
            }
            catch (Exception ex)
            {
                error.SetError(tbRegalias, "Error en el formato del número.");
                tbRegalias.Focus();
                return;
            }

            // mineral.vencimiento = tbVecimiento.Text;

            try
            {
                if (!tbCoeficiente.Text.Equals(""))
                    mineral.coeficiente = float.Parse(tbCoeficiente.Text.Replace(".", ","));
            }
            catch (Exception ex)
            {
                error.SetError(tbCoeficiente, "Error en el formato del coeficiente.");
                tbCoeficiente.Focus();
                return;
            }

            try
            {
                if (!tbToneladasAM3.Text.Equals(""))
                    mineral.tonAM3 = float.Parse(tbToneladasAM3.Text.Replace(".", ","));
            }
            catch (Exception ex)
            {
                error.SetError(tbToneladasAM3, "Error en el formato del número.");
                tbToneladasAM3.Focus();
                return;
            }

            String msg = "¿Desea agregar el siguiente mineral?\n" +
                "Nombre: " + mineral.nombre + "\n" +
                "Yacimiento: " + mineral.yacimiento + "\n" +
                "Precio unitario: " + mineral.precioUnitario + "\n" +
                "Finalidad: " + mineral.finalidad + "\n" +
                "Regalias: " + mineral.regalias + "\n" +
                "Vencimiento: " + mineral.vencimiento + "\n" +
                "Coeficiente: " + mineral.coeficiente + "\n" +
                "1 M3 = TN / " + mineral.nombre;

            DialogResult result = MessageBox.Show(msg, "Agregar mineral",
MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                agregarMineral(mineral);
            }
        }

        public void agregarMineral(Mineral mineral)
        {
            string sql = "INSERT INTO Mineral (Nombre, Yacimiento, PrecioUnitario, Finalidad, Regalias, Vencimiento, TonAM3, Coeficiente) VALUES (";

            sql += "'" + mineral.nombre + "', " +
                "'" + mineral.yacimiento + "', " +
                mineral.precioUnitario.ToString().Replace(",", ".") + ", " +
                "'" + mineral.finalidad + "', " +
                mineral.regalias.ToString().Replace(",", ".") + ", " +
                "'" + mineral.vencimiento + "', " +
                mineral.tonAM3.ToString().Replace(",", ".") + "," +
                mineral.coeficiente.ToString().Replace(",", ".") + ")";

            OleDbCommand oleDbCommand = new OleDbCommand(sql, conn);

            if (oleDbCommand.ExecuteNonQuery() != 1)
            {
                MessageBox.Show("No se ha podido agregar el mineral", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tbNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                e.Handled = true;
                tbYacimiento.Focus();
            }
        }

        private void tbYacimiento_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                e.Handled = true;
                tbPrecio.Focus();
            }
        }

        private void tbPrecio_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                e.Handled = true;
                tbFinalidad.Focus();
            }
        }

        private void tbFinalidad_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                e.Handled = true;
                tbRegalias.Focus();
            }
        }

        private void tbRegalias_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                e.Handled = true;
                tbVecimiento.Focus();
            }
        }

        private void tbVecimiento_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                e.Handled = true;
                tbToneladasAM3.Focus();
            }
        }

        private void tbToneladasAM3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                e.Handled = true;
                butAgregar.Focus();
            }
        }

        private void butAgregar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                e.Handled = true;
                butAgregar.PerformClick();
            }
        }

        public void actualizarComboMinerales(List<Mineral> minerales)
        {
            cbMineral.Items.Clear();

            foreach (Mineral mineral in minerales)
            {
                cbMineral.Items.Add(mineral);
            }

            enabledEditarBoxes(false);
        }

        private void cbMineral_SelectionChangeCommitted(object sender, EventArgs e)
        {
            mineralSeleccionado = (Mineral)cbMineral.SelectedItem;

            enabledEditarBoxes(true);

            tbNombreE.Text = mineralSeleccionado.nombre;
            tbYacimientoE.Text = mineralSeleccionado.yacimiento;
            tbPrecioE.Text = mineralSeleccionado.precioUnitario.ToString();
            tbFinalidadE.Text = mineralSeleccionado.finalidad;
            tbRegaliasE.Text = mineralSeleccionado.regalias.ToString();
            tbVencimientoE.Text = mineralSeleccionado.vencimiento.ToString();
            tbToneladasAM3E.Text = mineralSeleccionado.tonAM3.ToString();
            tbCoeficienteE.Text = mineralSeleccionado.coeficiente.ToString();

            tbNombreE.Focus();
        }

        private void buteEditar_Click(object sender, EventArgs e)
        {
            error.Clear();

            if (tbNombreE.Text.Length == 0)
            {
                error.SetError(tbNombreE, "Debe especificar obligatoriamente un nombre.");
                tbNombreE.Focus();
                return;
            }

            Mineral mineral = new Mineral();

            mineral.nombre = tbNombreE.Text;
            mineral.yacimiento = tbYacimientoE.Text;

            try
            {
                if (!tbPrecioE.Text.Equals(""))
                    mineral.precioUnitario = float.Parse(tbPrecioE.Text.Replace(".", ","));
            }
            catch (Exception ex)
            {
                error.SetError(tbPrecioE, "Error en el formato del número.");
                tbPrecioE.Focus();
                return;
            }

            mineral.finalidad = tbFinalidadE.Text;

            try
            {
                if (!tbRegaliasE.Text.Equals(""))
                    mineral.regalias = float.Parse(tbRegaliasE.Text.Replace(".", ","));
            }
            catch (Exception ex)
            {
                error.SetError(tbRegaliasE, "Error en el formato del número.");
                tbRegaliasE.Focus();
                return;
            }

            // mineral.vencimiento = tbVecimiento.Text;

            try
            {
                if (!tbCoeficienteE.Text.Equals(""))
                    mineral.coeficiente = float.Parse(tbCoeficienteE.Text.Replace(".", ","));
            }
            catch (Exception ex)
            {
                error.SetError(tbCoeficienteE, "Error en el formato del coeficiente.");
                tbCoeficienteE.Focus();
                return;
            }

            try
            {
                if (!tbToneladasAM3E.Text.Equals(""))
                    mineral.tonAM3 = float.Parse(tbToneladasAM3E.Text.Replace(".", ","));
            }
            catch (Exception ex)
            {
                error.SetError(tbToneladasAM3E, "Error en el formato del número.");
                tbToneladasAM3E.Focus();
                return;
            }

            string msg = "¿Realmente desea confirmar estos cambios?" +
            "\n\nNombre\t\t " + mineralSeleccionado.nombre + " por " + mineral.nombre +
            "\nYacimiento\t " + mineralSeleccionado.yacimiento + " por " + mineral.yacimiento +
            "\nPrecio unitario\t " + mineralSeleccionado.precioUnitario.ToString() + " por " + mineral.precioUnitario.ToString() +
            "\nFinalidad\t " + mineralSeleccionado.finalidad + " por " + mineral.finalidad +
            "\nRegalias\t\t " + mineralSeleccionado.regalias.ToString() + " por " + mineral.regalias.ToString() +
            "\nVencimiento\t " + mineralSeleccionado.vencimiento.ToString() + " por " + mineral.vencimiento.ToString() +
            "\nCoeficiente\t " + mineralSeleccionado.coeficiente.ToString() + " por " + mineral.coeficiente.ToString() +
            "\n1 M3 =\t TN / " + mineralSeleccionado.tonAM3.ToString() + " por TN / " + mineral.tonAM3.ToString();
            DialogResult result = MessageBox.Show(msg, "Editar mineral",
MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                string sql = "UPDATE Mineral SET Nombre = '" + mineral.nombre + "'" +
               ", Yacimiento = '" + mineral.yacimiento + "'" +
               ", PrecioUnitario = " + mineral.precioUnitario.ToString().Replace(",", ".") +
               ", Finalidad = '" + mineral.finalidad + "'" +
               ", Regalias = " + mineral.regalias.ToString().Replace(",", ".") +
               ", Vencimiento = " + mineral.vencimiento.ToString().Replace(",", ".") +
               ", Coeficiente = " + mineral.coeficiente.ToString().Replace(",", ".") +
               ", TonAM3 = " + mineral.tonAM3.ToString().Replace(",", ".") +
               " WHERE Id = " + mineralSeleccionado.ID;

                OleDbDataAdapter oledbAdapter = new OleDbDataAdapter();

                oledbAdapter.UpdateCommand = conn.CreateCommand();
                oledbAdapter.UpdateCommand.CommandText = sql;

                int i = oledbAdapter.UpdateCommand.ExecuteNonQuery();

                if (i != 1)
                {
                    MessageBox.Show("Los valores no se han podido modificar.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    vaciarTextBox();
                    actualizarComboMinerales(Utils.getMinerales(conn));
                }
            }
        }

        public void vaciarTextBox()
        {
            tbNombreE.Clear();
            tbYacimientoE.Clear();
            tbPrecioE.Clear();
            tbRegaliasE.Clear();
            tbVencimientoE.Clear();
            tbToneladasAM3E.Clear();
            tbCoeficiente.Clear();
        }

        public void enabledEditarBoxes(bool en)
        {
            tbNombreE.Enabled = en;
            tbYacimientoE.Enabled = en;
            tbPrecioE.Enabled = en;
            tbRegaliasE.Enabled = en;
            tbVencimientoE.Enabled = en;
            tbToneladasAM3E.Enabled = en;
            tbFinalidadE.Enabled = en;
            tbCoeficiente.Enabled = en;

            buteEditar.Enabled = en;
            butCancelar.Enabled = en;
        }

        private void butCancelar_Click(object sender, EventArgs e)
        {
            vaciarTextBox();
            actualizarComboMinerales(Utils.getMinerales(conn));
            cbMineral.Focus();
        }

        private void tbNombreE_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                e.Handled = true;
                tbYacimientoE.Focus();
            }
        }

        private void tbYacimientoE_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                e.Handled = true;
                tbPrecioE.Focus();
            }
        }

        private void tbPrecioE_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                e.Handled = true;
                tbFinalidadE.Focus();
            }
        }

        private void tbFinalidadE_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                e.Handled = true;
                tbRegaliasE.Focus();
            }
        }

        private void tbRegaliasE_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                e.Handled = true;
                tbVencimientoE.Focus();
            }
        }

        private void tbVencimientoE_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                e.Handled = true;
                tbToneladasAM3E.Focus();
            }
        }

        private void tbToneladasAM3E_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                e.Handled = true;
                buteEditar.Focus();
            }
        }

        private void buteEditar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                e.Handled = true;
                buteEditar.PerformClick();
            }
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label19_Click(object sender, EventArgs e)
        {

        }
    }
}
