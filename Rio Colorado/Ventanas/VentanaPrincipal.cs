﻿using Gestion_Minera.Ventanas.Alquileres;
using Gestion_Minera.Ventanas.DDJJ;
using Gestion_Minera.Ventanas.Sueldos;
using System;
using System.Data.OleDb;
using System.Windows.Forms;

namespace DDJJ_Regalías_Mineras.Ventanas
{
    public partial class VentanaPrincipal : Form
    {
        OleDbConnection connMineria;
        OleDbConnection connAlquiler;

        public VentanaPrincipal(OleDbConnection connMineria, OleDbConnection connAlquiler)
        {
            InitializeComponent();

            this.connMineria = connMineria;
            this.connAlquiler = connAlquiler;
        }

        private void agregarEditarMineralesYValoresFijosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ABMMinerales abmMinerales = new ABMMinerales(connMineria);
            abmMinerales.Show();
        }

        private void DDJJToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GenerarDDJJ generarDDJJ = new GenerarDDJJ(connMineria);
            generarDDJJ.Show();
        }

        private void sueldosToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void agregarYEditarEmpleadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ABMEmpleados abmEmpledos = new ABMEmpleados(connMineria);
            abmEmpledos.Show();
        }

        private void agregarYEditarSindicatosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ABMSindicatos abmSindicatos = new ABMSindicatos(connMineria);
            abmSindicatos.Show();
        }

        private void agregarYEditarNovedadesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Novedades novedades = new Novedades(connMineria);
            novedades.Show();
        }

        private void agregarYEditarInquilinosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ABMInquilinosContratos abmInquilinos = new ABMInquilinosContratos(connAlquiler);
            abmInquilinos.Show();
        }

        private void agregarYEditarInmueblesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ABMInmueble abmInmueble = new ABMInmueble(connAlquiler);
            abmInmueble.Show();
        }

        private void agregarYEditarContratosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ABMContratos aBMContratos = new ABMContratos(connAlquiler);
            aBMContratos.Show();
        }

        private void cargarServiciosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CargaServicios cargarServicios = new CargaServicios(connAlquiler);
            cargarServicios.Show();
        }

        private void generarReciboToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GenerarRecibo generarRecibo = new GenerarRecibo(connAlquiler);
            generarRecibo.Show();
        }

        private void vacacionesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ABMVacaciones aBMVacaciones = new ABMVacaciones(connMineria);
            aBMVacaciones.Show();
        }

        private void resúmenMineríaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ResumenMineria resumenMineria = new ResumenMineria(connMineria);
            resumenMineria.Show();
        }
    }
}
