﻿using Gestion_Minera.Clases;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gestion_Minera.Ventanas.Sueldos
{
    public partial class ABMSindicatos : Form
    {
        OleDbConnection conn;
        Sindicato sindicatoSeleccionado;

        public ABMSindicatos(OleDbConnection conn)
        {
            InitializeComponent();
            this.conn = conn;

            actualizarSindicatos();

            tbNombreE.Enabled = false;
            butCancelar.Enabled = false;
            buteEditar.Enabled = false;
        }

        private void tbNombre_TextChanged(object sender, EventArgs e)
        {

        }

        private void tbCUIL_TextChanged(object sender, EventArgs e)
        {

        }

        private void dateTimeIngreso_ValueChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void tbApellido_TextChanged(object sender, EventArgs e)
        {

        }

        private void butAgregar_Click(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged_1(object sender, EventArgs e)
        {
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void ABMSindicatos_Load(object sender, EventArgs e)
        {

        }

        private void butAgregar_Click_1(object sender, EventArgs e)
        {
            Sindicato sindicato = new Sindicato();

            sindicato.Nombre = tbNombre.Text;

            String msg = "¿Desea agregar el siguiente Sindicato?\n" +
                "Nombre: " + sindicato.Nombre + "\n";

            DialogResult result = MessageBox.Show(msg, "Agregar Sindicato",
                MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                string sql = "INSERT INTO Sindicato (Nombre) VALUES ('" +
                    sindicato.Nombre + "')";

                OleDbCommand oleDbCommand = new OleDbCommand(sql, conn);

                if (oleDbCommand.ExecuteNonQuery() != 1)
                {
                    MessageBox.Show("No se ha podido agregar el Sindicato", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    tbNombre.Text = "";
                    tbNombre.Focus();

                    actualizarSindicatos();
                }
            }
        }

        private void tbNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                e.Handled = true;
                butAgregar.Focus();
            }
            else
            {
                e.KeyChar = Char.ToUpper(e.KeyChar);
            }
        }

        public void actualizarSindicatos()
        {
            cbSindicato.Items.Clear();

            foreach (Sindicato sindicato in Utils.getSindicatos(conn))
            {
                cbSindicato.Items.Add(sindicato);
            }
        }

        private void cbSindicato_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cbSindicato.SelectedIndex >= 0)
            {
                sindicatoSeleccionado = (Sindicato)cbSindicato.SelectedItem;
                tbNombreE.Text = sindicatoSeleccionado.Nombre;

                tbNombreE.Enabled = true;
                butCancelar.Enabled = true;
                buteEditar.Enabled = true;
            }
        }

        private void butCancelar_Click(object sender, EventArgs e)
        {
            cbSindicato.SelectedIndex = -1;

            tbNombreE.Text = "";

            tbNombreE.Enabled = false;
            butCancelar.Enabled = false;
            buteEditar.Enabled = false;
        }

        private void buteEditar_Click(object sender, EventArgs e)
        {
            string msg = "¿Realmente desea confirmar estos cambios?" +
            "\n\nNombre:   " + sindicatoSeleccionado.Nombre + " por " + tbNombreE.Text;

            DialogResult result = MessageBox.Show(msg, "Editar Sindicato",
                MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);

            if (result == DialogResult.Yes)
            {
                string sql = "UPDATE Sindicato SET Nombre = '" + tbNombreE.Text + "'" +
               " WHERE Id = " + sindicatoSeleccionado.ID;

                OleDbDataAdapter oledbAdapter = new OleDbDataAdapter();

                oledbAdapter.UpdateCommand = conn.CreateCommand();
                oledbAdapter.UpdateCommand.CommandText = sql;

                int i = oledbAdapter.UpdateCommand.ExecuteNonQuery();

                if (i != 1)
                {
                    MessageBox.Show("Los valores no se han podido modificar.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    cbSindicato.SelectedIndex = -1;

                    tbNombreE.Text = "";

                    tbNombreE.Enabled = false;
                    butCancelar.Enabled = false;
                    buteEditar.Enabled = false;

                    actualizarSindicatos();
                }
            }
        }
    }
}
