﻿using Gestion_Minera.Clases;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Windows.Forms;

namespace Gestion_Minera.Ventanas.Sueldos
{
    public partial class ABMVacaciones : Form
    {
        OleDbConnection conn;

        List<Vacaciones> listaVacaciones;
        List<NovedadesEmpleados> listaNovedades;

        int idVacaciones = 0;
        int idNovedad = 0;

        int dias1, dias2;

        public ABMVacaciones(OleDbConnection conn)
        {
            InitializeComponent();

            this.conn = conn;

            listaVacaciones = new List<Vacaciones>();
            listaNovedades = new List<NovedadesEmpleados>();

            foreach (Empleado empleado in Utils.getEmpleados(conn))
            {
                cbEmpleado.Items.Add(empleado);
            }

            numAnio.Value = DateTime.Today.Year;

            dgTabla.Enabled = grVacaciones.Enabled = dgNovedades.Enabled = grNovedades.Enabled = false;
            butImprimirComprobante.Enabled = butAgregar.Enabled = butEditar.Enabled = butEliminar.Enabled = false;

            cbEmpleado.Focus();
        }

        private void tbDias_TextChanged(object sender, EventArgs e)
        {
            sumarDiasLaborables();
        }

        public void sumarDiasLaborables()
        {
            DateTime suma = dtDesde.Value;
            dtVacaciones.MaxDate = dtDesde.Value.AddYears(20);
            dtVacaciones.MinDate = dtDesde.Value;

            int dias = 0, feriados = 0;

            try
            {
                dias = int.Parse(tbDias.Text) - 1;
            }
            catch (Exception ex)
            {
            }

            try
            {
                feriados = int.Parse(tbFeriados.Text);
            }
            catch (Exception ex)
            {
            }

            suma = suma.AddDays(dias + feriados);

            DateTime hasta, vuelve;

            hasta = suma;

            tbHasta.Text = hasta.ToShortDateString();
            dtVacaciones.MaxDate = hasta;
            dtVacaciones.Value = dtVacaciones.MaxDate;

            vuelve = hasta.AddDays(1);

            while (vuelve.DayOfWeek == DayOfWeek.Saturday || vuelve.DayOfWeek == DayOfWeek.Sunday)
            {
                vuelve = vuelve.AddDays(1);
            }

            dtVuelve.Value = vuelve;
        }

        private void dtDesde_ValueChanged(object sender, EventArgs e)
        {
            sumarDiasLaborables();
        }

        private void tbFeriados_TextChanged(object sender, EventArgs e)
        {
            sumarDiasLaborables();
        }

        private void butAgregarVacaciones_Click(object sender, EventArgs e)
        {
            int dias = 0;
            int anio = int.Parse(numAnio.Value.ToString());
            String msj, sql;

            try
            {
                dias = int.Parse(tbAgregarDias.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("El número de días debe ser entero.", "ERROR");
            }

            if (idVacaciones > 0)
            {
                sql = "UPDATE Vacaciones SET dias = " + dias +
                    " WHERE Id = " + idVacaciones;

                msj = "Los valores no se han podido modificar.";
            }
            else
            {
                sql = "INSERT INTO Vacaciones (idEmpleado, Anio, Dias) VALUES (" +
                                        ((Empleado)cbEmpleado.SelectedItem).ID + ", " +
                                        anio + ", " +
                                        dias + ")";

                msj = "No se ha podido agregar los días de vacaciones";
            }

            OleDbCommand oleDbCommand = new OleDbCommand(sql, conn);

            if (oleDbCommand.ExecuteNonQuery() != 1)
            {
                MessageBox.Show(msj, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                actualziarTablaVacaciones();
                actualizarDias();

                tbAgregarDias.Text = "";
                numAnio.Value = anio;
            }
        }

        private void cbEmpleado_SelectedIndexChanged(object sender, EventArgs e)
        {
            actualziarTablaNovedades();
            actualziarTablaVacaciones();
            actualizarDias();

            buscarVacaciones();

            cancelar();
        }

        public void actualizarDias()
        {
            tbCantidadDias.Text = (dias1 - dias2).ToString();
        }

        public void actualziarTablaVacaciones()
        {
            dias1 = 0;

            if (cbEmpleado.SelectedIndex > -1)
            {
                dgTabla.Enabled = grVacaciones.Enabled = true;

                listaVacaciones = Utils.getVacaciones(conn, ((Empleado)cbEmpleado.SelectedItem).ID);

                dgTabla.Rows.Clear();

                foreach (Vacaciones vacaciones in listaVacaciones)
                {
                    dgTabla.Rows.Add(new String[] { vacaciones.idVacaciones.ToString(), vacaciones.anio.ToString(), vacaciones.dias.ToString() });

                    dias1 += vacaciones.dias;
                }

                if (dgTabla.Rows.Count > 0)
                {
                    dgTabla.Rows[dgTabla.Rows.Count - 1].Selected = true;
                }
            }
            else
            {
                dgTabla.Enabled = grVacaciones.Enabled = false;
            }

            tbAsignados.Text = dias1.ToString();
        }

        public void actualziarTablaNovedades()
        {
            dias2 = 0;

            butAgregar.Enabled = true;

            if (cbEmpleado.SelectedIndex > -1)
            {
                dgNovedades.Enabled = true;

                listaNovedades = Utils.getNovedadesVacaciones(conn, ((Empleado)cbEmpleado.SelectedItem).ID);

                dgNovedades.Rows.Clear();

                butEditar.Enabled = butEliminar.Enabled = listaNovedades.Count > 0;

                foreach (NovedadesEmpleados novedad in listaNovedades)
                {
                    dgNovedades.Rows.Add(new String[] { novedad.ID.ToString(),
                        novedad.desde.ToShortDateString(),
                        novedad.hasta.ToShortDateString(),
                        novedad.valor,
                        novedad.valor2,
                        novedad.valor3,
                    });

                    dias2 += int.Parse(novedad.valor);
                }

                if (dgNovedades.Rows.Count > 0)
                {
                    dgNovedades.Rows[dgNovedades.Rows.Count - 1].Selected = true;
                }
            }
            else
            {
                dgNovedades.Enabled = true;
                butEditar.Enabled = butEliminar.Enabled = false;
            }

            tbDiasTomados.Text = dias2.ToString();
        }

        private void numAnio_ValueChanged(object sender, EventArgs e)
        {
            buscarVacaciones();
        }

        public void buscarVacaciones()
        {
            idVacaciones = 0;

            tbAgregarDias.Text = "";

            foreach (Vacaciones vacaciones in listaVacaciones)
            {
                if (vacaciones.anio == numAnio.Value)
                {
                    idVacaciones = vacaciones.idVacaciones;
                    tbAgregarDias.Text = vacaciones.dias.ToString();

                    break;
                }
            }
        }

        private void ButAgregarNovedad_Click(object sender, EventArgs e)
        {
            if (tbDias.Text == "")
            {
                tbDias.Text = "0";
            }

            if (tbFeriados.Text == "")
            {
                tbFeriados.Text = "0";
            }

            if (idNovedad > 0)
            {
                string sql = "UPDATE NovedadesEmpleados SET Fecha = '" + DateTime.Now.ToString() + "', " +
                    " Valor = " + int.Parse(tbDias.Text) + ", " +
                    " Valor2 = " + int.Parse(tbFeriados.Text) + ", " +
                    " Valor3 = '" + dtVuelve.Value.ToShortDateString() + "', " +
                    " Desde = '" + dtDesde.Value.ToShortDateString() + "', " +
                    " Hasta = '" + tbHasta.Text + "'" +
                    " WHERE Id = " + idNovedad;

                OleDbDataAdapter oledbAdapter = new OleDbDataAdapter();

                oledbAdapter.UpdateCommand = conn.CreateCommand();
                oledbAdapter.UpdateCommand.CommandText = sql;

                int i = oledbAdapter.UpdateCommand.ExecuteNonQuery();

                if (i != 1)
                {
                    MessageBox.Show("Los valores no se han podido modificar.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    actualziarTablaNovedades();
                    actualizarDias();

                    cancelar();
                }
            }
            else
            {
                /*
                 * Valor = cantidad dias
                 * Valor2 = feriados             
                 */
                String sql = "INSERT INTO NovedadesEmpleados (IdEmpleado, IdNovedad, Fecha, Valor, Valor2, Desde, Hasta, Valor3) VALUES (" +
                                ((Empleado)cbEmpleado.SelectedItem).ID + ", " +
                                "3, " +
                                "'" + DateTime.Now.ToString() + "', " +
                                int.Parse(tbDias.Text) + ", " +
                                int.Parse(tbFeriados.Text) + ", " +
                                "'" + dtDesde.Value.ToShortDateString() + "', " +
                                "'" + tbHasta.Text + "'," +
                                "'" + dtVuelve.Value.ToShortDateString() + "')";

                OleDbCommand oleDbCommand = new OleDbCommand(sql, conn);

                if (oleDbCommand.ExecuteNonQuery() != 1)
                {
                    MessageBox.Show("No se ha podido agregar la Novedad", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    actualziarTablaNovedades();
                    actualizarDias();
                }
            }
        }

        private void butAgregar_Click(object sender, EventArgs e)
        {
            grNovedades.Enabled = true;
            butAgregar.Enabled = butEditar.Enabled = butEliminar.Enabled = false;

            idNovedad = 0;

            tbDias.Focus();
        }

        private void butCancelar_Click(object sender, EventArgs e)
        {
            cancelar();
        }

        public void cancelar()
        {
            dtVacaciones.MinDate = dtVacaciones.MaxDate = DateTime.Today;

            dtDesde.Value = dtVacaciones.Value = dtVuelve.Value = DateTime.Today;
            tbHasta.Text = tbDias.Text = tbFeriados.Text = "";

            grNovedades.Enabled = false;

            butAgregar.Enabled = dgNovedades.Enabled = true;

            butEditar.Enabled = butEliminar.Enabled = listaNovedades.Count > 0;

            butAgregar.Focus();
        }

        private void butImprimirComprobante_Click(object sender, EventArgs e)
        {
            int[,] vacaciones = new int[dgTabla.Rows.Count + 1, 2];
            int[,] vacaciones2 = new int[3, 2];

            int diasTomados = 0, diasATomar = 0;

            for (int i = 0; i < dgTabla.Rows.Count; i++)
            {
                vacaciones[i, 0] = int.Parse(dgTabla[1, i].Value.ToString());
                vacaciones[i, 1] = int.Parse(dgTabla[2, i].Value.ToString());
            }

            for (int i = 0; i < dgNovedades.SelectedRows[0].Index; i++)
            {
                diasTomados += int.Parse(dgNovedades[3, i].Value.ToString());
            }

            diasATomar = int.Parse(dgNovedades[3, dgNovedades.SelectedRows[0].Index].Value.ToString());

            Console.WriteLine("\nDias toamdos: " + diasTomados + "\nDias a tomar: " + diasATomar);

            int a = 0;

            while (diasTomados > 0)
            {
                diasTomados--;
                vacaciones[a, 1]--;

                if (vacaciones[a, 1] == 0)
                {
                    a++;
                }
            }

            int aa = 0;

            String vacaciones3 = "";

            while (diasATomar > 0)
            {
                diasATomar--;
                vacaciones2[aa, 0] = vacaciones[a, 0];
                vacaciones2[aa, 1]++;
                vacaciones[a, 1]--;

                if (vacaciones[a, 1] == 0 || diasATomar == 0)
                {
                    vacaciones3 += " al año " + vacaciones[a, 0] + ", por el periodo de " + vacaciones2[aa, 1] + " días;";

                    a++;
                    aa++;
                }
            }

            if (vacaciones3 == "")
            {
                MessageBox.Show("La persona se toma más días de vacaciones que los que dispone.", "CUIDADO");

                return;
            }

            vacaciones3 = vacaciones3.Remove(vacaciones3.Length - 1);

            vacaciones3 += ".";

            for (int i = 0; i < 3; i++)
            {
                Console.WriteLine(vacaciones2[i, 0] + ": " + vacaciones2[i, 1]);
            }

            String nombre = ((Empleado)cbEmpleado.SelectedItem).Apellido + " " + ((Empleado)cbEmpleado.SelectedItem).Nombre;

            String cuil = ((Empleado)cbEmpleado.SelectedItem).CUIL;

            String desde = dgNovedades[1, dgNovedades.SelectedRows[0].Index].Value.ToString();

            String hasta = dgNovedades[2, dgNovedades.SelectedRows[0].Index].Value.ToString();

            String reincorporar = dgNovedades[5, dgNovedades.SelectedRows[0].Index].Value.ToString();

            String id = dgNovedades[0, dgNovedades.SelectedRows[0].Index].Value.ToString();

            int coun = Utils.getNumberNovedad(conn, int.Parse(id), 3);

            Utils.generarComprobanteVacaciones(coun.ToString(),
                nombre,
                vacaciones3,
                desde,
                hasta,
                reincorporar,
                cuil);
        }

        private void dgNovedades_SelectionChanged(object sender, EventArgs e)
        {
            butImprimirComprobante.Enabled = dgNovedades.SelectedRows.Count != 0;
        }

        private void butEliminar_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Realmente desea borrar la novedad seleccionada?", "Cuidado",
MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                try
                {
                    int i = int.Parse(dgNovedades[0, dgNovedades.SelectedRows[0].Index].Value.ToString());

                    OleDbCommand oleDbCommand = new OleDbCommand("DELETE FROM NovedadesEmpleados WHERE id=" + i, conn);

                    if (oleDbCommand.ExecuteNonQuery() != 1)
                    {
                        MessageBox.Show("No se ha podido eliminar el registro.", "Error");
                    }
                    else
                    {
                        actualziarTablaNovedades();
                        actualziarTablaVacaciones();
                        actualizarDias();

                        buscarVacaciones();

                        cancelar();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("No se ha podido eliminar el registro.", "Error");
                }
            }
            else if (result == DialogResult.No)
            {
                //code for No
            }
            else if (result == DialogResult.Cancel)
            {
                //code for Cancel
            }
        }

        private void butEditar_Click(object sender, EventArgs e)
        {
            if (dgNovedades.SelectedRows.Count > 0)
            {
                grNovedades.Enabled = true;
                butAgregar.Enabled = butEditar.Enabled = butEliminar.Enabled = dgNovedades.Enabled = false;

                int index = dgNovedades.SelectedRows[0].Index;

                dtDesde.Value = listaNovedades[index].desde;
                tbDias.Text = listaNovedades[index].valor;
                tbFeriados.Text = listaNovedades[index].valor2;
                dtVuelve.Value = DateTime.Parse(listaNovedades[index].valor3);

                idNovedad = listaNovedades[index].ID;

                tbDias.Focus();
            }
        }
    }
}
