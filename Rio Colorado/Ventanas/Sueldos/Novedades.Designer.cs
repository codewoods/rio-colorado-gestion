﻿namespace Gestion_Minera.Ventanas.Sueldos
{
    partial class Novedades
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Novedades));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbRango = new System.Windows.Forms.RadioButton();
            this.dtHasta = new System.Windows.Forms.DateTimePicker();
            this.rbFecha = new System.Windows.Forms.RadioButton();
            this.dtDesde = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cbNovedades = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.butBuscar = new System.Windows.Forms.Button();
            this.cbEmpleado = new System.Windows.Forms.ComboBox();
            this.dgTabla = new System.Windows.Forms.DataGridView();
            this.Fecha = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Sindicato = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Empleado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Novedad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Valor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Desde = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Hasta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idNovedad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.butGenerarInforme = new System.Windows.Forms.Button();
            this.cbMes = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.butEliminar = new System.Windows.Forms.Button();
            this.butEditar = new System.Windows.Forms.Button();
            this.butAgregarNovedad = new System.Windows.Forms.Button();
            this.butAnterior = new System.Windows.Forms.Button();
            this.butSiguiente = new System.Windows.Forms.Button();
            this.tbPagActual = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbPagUltima = new System.Windows.Forms.TextBox();
            this.butGenerarComprobante = new System.Windows.Forms.Button();
            this.numAnio = new System.Windows.Forms.NumericUpDown();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTabla)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numAnio)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbRango);
            this.groupBox1.Controls.Add(this.dtHasta);
            this.groupBox1.Controls.Add(this.rbFecha);
            this.groupBox1.Controls.Add(this.dtDesde);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cbNovedades);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.butBuscar);
            this.groupBox1.Controls.Add(this.cbEmpleado);
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(625, 94);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "BUSCAR NOVEDADES";
            // 
            // rbRango
            // 
            this.rbRango.AutoSize = true;
            this.rbRango.Checked = true;
            this.rbRango.Location = new System.Drawing.Point(395, 14);
            this.rbRango.Name = "rbRango";
            this.rbRango.Size = new System.Drawing.Size(107, 17);
            this.rbRango.TabIndex = 67;
            this.rbRango.TabStop = true;
            this.rbRango.Text = "Rango de fechas";
            this.rbRango.UseVisualStyleBackColor = true;
            // 
            // dtHasta
            // 
            this.dtHasta.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtHasta.Location = new System.Drawing.Point(387, 62);
            this.dtHasta.Name = "dtHasta";
            this.dtHasta.Size = new System.Drawing.Size(106, 20);
            this.dtHasta.TabIndex = 50;
            // 
            // rbFecha
            // 
            this.rbFecha.AutoSize = true;
            this.rbFecha.Location = new System.Drawing.Point(334, 14);
            this.rbFecha.Name = "rbFecha";
            this.rbFecha.Size = new System.Drawing.Size(55, 17);
            this.rbFecha.TabIndex = 66;
            this.rbFecha.Text = "Todas";
            this.rbFecha.UseVisualStyleBackColor = true;
            this.rbFecha.CheckedChanged += new System.EventHandler(this.rbFecha_CheckedChanged);
            // 
            // dtDesde
            // 
            this.dtDesde.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDesde.Location = new System.Drawing.Point(387, 36);
            this.dtDesde.Name = "dtDesde";
            this.dtDesde.Size = new System.Drawing.Size(106, 20);
            this.dtDesde.TabIndex = 49;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(346, 64);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 48;
            this.label4.Text = "Hasta";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(343, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 47;
            this.label3.Text = "Desde";
            // 
            // cbNovedades
            // 
            this.cbNovedades.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbNovedades.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbNovedades.Location = new System.Drawing.Point(69, 54);
            this.cbNovedades.Name = "cbNovedades";
            this.cbNovedades.Size = new System.Drawing.Size(244, 23);
            this.cbNovedades.TabIndex = 46;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 45;
            this.label2.Text = "Novedad";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 44;
            this.label1.Text = "Empleado";
            // 
            // butBuscar
            // 
            this.butBuscar.Location = new System.Drawing.Point(513, 28);
            this.butBuscar.Name = "butBuscar";
            this.butBuscar.Size = new System.Drawing.Size(96, 46);
            this.butBuscar.TabIndex = 43;
            this.butBuscar.Text = "BUSCAR";
            this.butBuscar.UseVisualStyleBackColor = true;
            this.butBuscar.Click += new System.EventHandler(this.butBuscar_Click);
            // 
            // cbEmpleado
            // 
            this.cbEmpleado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbEmpleado.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbEmpleado.Location = new System.Drawing.Point(69, 25);
            this.cbEmpleado.Name = "cbEmpleado";
            this.cbEmpleado.Size = new System.Drawing.Size(244, 23);
            this.cbEmpleado.TabIndex = 42;
            // 
            // dgTabla
            // 
            this.dgTabla.AllowUserToAddRows = false;
            this.dgTabla.AllowUserToDeleteRows = false;
            this.dgTabla.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgTabla.BackgroundColor = System.Drawing.Color.MintCream;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgTabla.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgTabla.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgTabla.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Fecha,
            this.Sindicato,
            this.Empleado,
            this.Novedad,
            this.Valor,
            this.Desde,
            this.Hasta,
            this.id,
            this.idNovedad});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgTabla.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgTabla.Location = new System.Drawing.Point(13, 113);
            this.dgTabla.Name = "dgTabla";
            this.dgTabla.ReadOnly = true;
            this.dgTabla.RowHeadersVisible = false;
            this.dgTabla.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dgTabla.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgTabla.Size = new System.Drawing.Size(847, 372);
            this.dgTabla.TabIndex = 1;
            this.dgTabla.RowStateChanged += new System.Windows.Forms.DataGridViewRowStateChangedEventHandler(this.dgTabla_RowStateChanged);
            this.dgTabla.SelectionChanged += new System.EventHandler(this.dgTabla_SelectionChanged);
            // 
            // Fecha
            // 
            this.Fecha.HeaderText = "FECHA INGRESO";
            this.Fecha.Name = "Fecha";
            this.Fecha.ReadOnly = true;
            this.Fecha.Width = 80;
            // 
            // Sindicato
            // 
            this.Sindicato.HeaderText = "SINDICATO";
            this.Sindicato.Name = "Sindicato";
            this.Sindicato.ReadOnly = true;
            this.Sindicato.Width = 85;
            // 
            // Empleado
            // 
            this.Empleado.HeaderText = "EMPLEADO";
            this.Empleado.Name = "Empleado";
            this.Empleado.ReadOnly = true;
            this.Empleado.Width = 160;
            // 
            // Novedad
            // 
            this.Novedad.HeaderText = "NOVEDAD";
            this.Novedad.Name = "Novedad";
            this.Novedad.ReadOnly = true;
            this.Novedad.Width = 240;
            // 
            // Valor
            // 
            this.Valor.HeaderText = "VALOR";
            this.Valor.Name = "Valor";
            this.Valor.ReadOnly = true;
            // 
            // Desde
            // 
            this.Desde.HeaderText = "DESDE";
            this.Desde.Name = "Desde";
            this.Desde.ReadOnly = true;
            this.Desde.Width = 85;
            // 
            // Hasta
            // 
            this.Hasta.HeaderText = "HASTA";
            this.Hasta.Name = "Hasta";
            this.Hasta.ReadOnly = true;
            this.Hasta.Width = 85;
            // 
            // id
            // 
            this.id.HeaderText = "";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Visible = false;
            // 
            // idNovedad
            // 
            this.idNovedad.HeaderText = "";
            this.idNovedad.Name = "idNovedad";
            this.idNovedad.ReadOnly = true;
            this.idNovedad.Visible = false;
            // 
            // butGenerarInforme
            // 
            this.butGenerarInforme.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butGenerarInforme.Location = new System.Drawing.Point(366, 15);
            this.butGenerarInforme.Name = "butGenerarInforme";
            this.butGenerarInforme.Size = new System.Drawing.Size(152, 34);
            this.butGenerarInforme.TabIndex = 53;
            this.butGenerarInforme.Text = "GENERAR INFORME";
            this.butGenerarInforme.UseVisualStyleBackColor = true;
            this.butGenerarInforme.Click += new System.EventHandler(this.butGenerarInforme_Click);
            // 
            // cbMes
            // 
            this.cbMes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMes.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbMes.Location = new System.Drawing.Point(54, 21);
            this.cbMes.Name = "cbMes";
            this.cbMes.Size = new System.Drawing.Size(169, 23);
            this.cbMes.TabIndex = 54;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(18, 26);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(30, 13);
            this.label10.TabIndex = 53;
            this.label10.Text = "MES";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.MintCream;
            this.groupBox3.Controls.Add(this.numAnio);
            this.groupBox3.Controls.Add(this.butGenerarInforme);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.cbMes);
            this.groupBox3.Location = new System.Drawing.Point(15, 532);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(536, 58);
            this.groupBox3.TabIndex = 55;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "INFORME NOVEDADES";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(240, 26);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(30, 13);
            this.label11.TabIndex = 55;
            this.label11.Text = "AÑO";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // butEliminar
            // 
            this.butEliminar.ForeColor = System.Drawing.Color.Red;
            this.butEliminar.Location = new System.Drawing.Point(596, 495);
            this.butEliminar.Name = "butEliminar";
            this.butEliminar.Size = new System.Drawing.Size(126, 23);
            this.butEliminar.TabIndex = 56;
            this.butEliminar.Text = "ELIMINAR NOVEDAD";
            this.butEliminar.UseVisualStyleBackColor = true;
            this.butEliminar.Click += new System.EventHandler(this.butEliminar_Click);
            // 
            // butEditar
            // 
            this.butEditar.Location = new System.Drawing.Point(728, 495);
            this.butEditar.Name = "butEditar";
            this.butEditar.Size = new System.Drawing.Size(126, 23);
            this.butEditar.TabIndex = 57;
            this.butEditar.Text = "EDITAR NOVEDAD";
            this.butEditar.UseVisualStyleBackColor = true;
            this.butEditar.Click += new System.EventHandler(this.butEditar_Click);
            // 
            // butAgregarNovedad
            // 
            this.butAgregarNovedad.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butAgregarNovedad.Location = new System.Drawing.Point(662, 41);
            this.butAgregarNovedad.Name = "butAgregarNovedad";
            this.butAgregarNovedad.Size = new System.Drawing.Size(183, 40);
            this.butAgregarNovedad.TabIndex = 58;
            this.butAgregarNovedad.Text = "AGREGAR NOVEDAD";
            this.butAgregarNovedad.UseVisualStyleBackColor = true;
            this.butAgregarNovedad.Click += new System.EventHandler(this.butAgregarNovedad_Click);
            // 
            // butAnterior
            // 
            this.butAnterior.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butAnterior.Location = new System.Drawing.Point(15, 491);
            this.butAnterior.Name = "butAnterior";
            this.butAnterior.Size = new System.Drawing.Size(30, 23);
            this.butAnterior.TabIndex = 59;
            this.butAnterior.Text = "<";
            this.butAnterior.UseVisualStyleBackColor = true;
            this.butAnterior.Click += new System.EventHandler(this.butAnterior_Click);
            // 
            // butSiguiente
            // 
            this.butSiguiente.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butSiguiente.Location = new System.Drawing.Point(116, 491);
            this.butSiguiente.Name = "butSiguiente";
            this.butSiguiente.Size = new System.Drawing.Size(30, 23);
            this.butSiguiente.TabIndex = 61;
            this.butSiguiente.Text = ">";
            this.butSiguiente.UseVisualStyleBackColor = true;
            this.butSiguiente.Click += new System.EventHandler(this.butSiguiente_Click);
            // 
            // tbPagActual
            // 
            this.tbPagActual.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbPagActual.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbPagActual.Location = new System.Drawing.Point(47, 495);
            this.tbPagActual.Margin = new System.Windows.Forms.Padding(0);
            this.tbPagActual.Name = "tbPagActual";
            this.tbPagActual.Size = new System.Drawing.Size(26, 15);
            this.tbPagActual.TabIndex = 62;
            this.tbPagActual.Text = "1";
            this.tbPagActual.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(74, 495);
            this.label5.Margin = new System.Windows.Forms.Padding(0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(14, 15);
            this.label5.TabIndex = 64;
            this.label5.Text = "-";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbPagUltima
            // 
            this.tbPagUltima.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbPagUltima.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbPagUltima.Location = new System.Drawing.Point(87, 495);
            this.tbPagUltima.Margin = new System.Windows.Forms.Padding(0);
            this.tbPagUltima.Name = "tbPagUltima";
            this.tbPagUltima.Size = new System.Drawing.Size(26, 15);
            this.tbPagUltima.TabIndex = 65;
            this.tbPagUltima.Text = "1";
            this.tbPagUltima.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // butGenerarComprobante
            // 
            this.butGenerarComprobante.Location = new System.Drawing.Point(417, 495);
            this.butGenerarComprobante.Name = "butGenerarComprobante";
            this.butGenerarComprobante.Size = new System.Drawing.Size(173, 23);
            this.butGenerarComprobante.TabIndex = 66;
            this.butGenerarComprobante.Text = "IMPRIMIR COMPROBANTE";
            this.butGenerarComprobante.UseVisualStyleBackColor = true;
            this.butGenerarComprobante.Click += new System.EventHandler(this.butGenerarComprobante_Click);
            // 
            // numAnio
            // 
            this.numAnio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numAnio.Location = new System.Drawing.Point(276, 22);
            this.numAnio.Maximum = new decimal(new int[] {
            2050,
            0,
            0,
            0});
            this.numAnio.Minimum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.numAnio.Name = "numAnio";
            this.numAnio.ReadOnly = true;
            this.numAnio.Size = new System.Drawing.Size(66, 21);
            this.numAnio.TabIndex = 102;
            this.numAnio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numAnio.Value = new decimal(new int[] {
            2010,
            0,
            0,
            0});
            // 
            // Novedades
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(873, 594);
            this.Controls.Add(this.butGenerarComprobante);
            this.Controls.Add(this.tbPagUltima);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbPagActual);
            this.Controls.Add(this.butSiguiente);
            this.Controls.Add(this.butAnterior);
            this.Controls.Add(this.butAgregarNovedad);
            this.Controls.Add(this.butEditar);
            this.Controls.Add(this.butEliminar);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.dgTabla);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Novedades";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Novedades";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTabla)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numAnio)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cbEmpleado;
        private System.Windows.Forms.DataGridView dgTabla;
        private System.Windows.Forms.Button butBuscar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtHasta;
        private System.Windows.Forms.DateTimePicker dtDesde;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbNovedades;
        private System.Windows.Forms.Button butGenerarInforme;
        private System.Windows.Forms.ComboBox cbMes;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button butEliminar;
        private System.Windows.Forms.Button butEditar;
        private System.Windows.Forms.Button butAgregarNovedad;
        private System.Windows.Forms.Button butAnterior;
        private System.Windows.Forms.Button butSiguiente;
        private System.Windows.Forms.TextBox tbPagActual;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbPagUltima;
        private System.Windows.Forms.RadioButton rbRango;
        private System.Windows.Forms.RadioButton rbFecha;
        private System.Windows.Forms.Button butGenerarComprobante;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fecha;
        private System.Windows.Forms.DataGridViewTextBoxColumn Sindicato;
        private System.Windows.Forms.DataGridViewTextBoxColumn Empleado;
        private System.Windows.Forms.DataGridViewTextBoxColumn Novedad;
        private System.Windows.Forms.DataGridViewTextBoxColumn Valor;
        private System.Windows.Forms.DataGridViewTextBoxColumn Desde;
        private System.Windows.Forms.DataGridViewTextBoxColumn Hasta;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn idNovedad;
        private System.Windows.Forms.NumericUpDown numAnio;
    }
}