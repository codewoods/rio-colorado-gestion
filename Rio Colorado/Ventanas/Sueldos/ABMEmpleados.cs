﻿using Gestion_Minera.Clases;
using System;
using System.Data.OleDb;

using System.Windows.Forms;

namespace Gestion_Minera.Ventanas.Sueldos
{
    public partial class ABMEmpleados : Form
    {
        OleDbConnection conn;

        public ABMEmpleados(OleDbConnection conn)
        {
            InitializeComponent();
            this.conn = conn;

            foreach (Sindicato sindicato in Utils.getSindicatos(conn))
            {
                cbSindicato.Items.Add(sindicato);
                cbSindicatoE.Items.Add(sindicato);
            }

            actualizarComboEmpleados();

            butCancelar.Enabled = false;
            buteEditar.Enabled = false;

            limpiar();
            enabledEditar(false);
        }

        private void cbMineral_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbEmpleado.SelectedIndex != -1)
            {
                enabledEditar(true);

                Empleado empleado = (Empleado)cbEmpleado.SelectedItem;

                tbApellidoE.Text = empleado.Apellido;
                tbNombreE.Text = empleado.Nombre;
                tbCUILE.Text = empleado.CUIL;
                dtIngresoE.Value = empleado.FechaIngreso;
                tbCategoriaE.Text = empleado.Categoria;

                int i = 0;

                foreach (Sindicato sindicato in cbSindicatoE.Items)
                {
                    if (sindicato.ID == empleado.Sindicato.ID)
                    {
                        cbSindicatoE.SelectedIndex = i;
                    }

                    i++;
                }

                buteEditar.Enabled = true;
                butCancelar.Enabled = true;

                tbApellidoE.Focus();
            }
        }

        private void butAgregar_Click(object sender, EventArgs e)
        {
            Empleado empleado = new Empleado();

            empleado.Apellido = tbApellido.Text;
            empleado.Nombre = tbNombre.Text;
            empleado.CUIL = tbCUIL.Text;
            empleado.FechaIngreso = dtIngreso.Value;
            empleado.Categoria = tbCategoria.Text;

            if (cbSindicato.SelectedIndex >= 0)
                empleado.Sindicato = (Sindicato)cbSindicato.SelectedItem;
            else
                empleado.Sindicato = new Sindicato(0, "");

            String msg = "¿Desea agregar el siguiente Empleado?\n\n" +
                "Apellido:\t\t" + empleado.Apellido + "\n" +
                "Nombre:\t\t" + empleado.Nombre + "\n" +
                "CUIL:\t\t" + empleado.CUIL + "\n" +
                "Ingreso:\t\t" + empleado.FechaIngreso.ToShortDateString() + "\n" +
                "Categoría:\t" + empleado.Categoria + "\n" +
                "Sindicato:\t" + empleado.Sindicato.ToString() + "\n";

            DialogResult result = MessageBox.Show(msg, "Agregar Empleado",
                MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                string sql = "INSERT INTO Empleado (Apellido, Nombre, CUIL, FechaDeIngreso, Categoria, IdSindicato) VALUES ('" +
                    empleado.Apellido + "', " +
                    "'" + empleado.Nombre + "', " +
                    "'" + empleado.CUIL + "', " +
                    "'" + empleado.FechaIngreso.ToShortDateString() + "', " +
                    "'" + empleado.Categoria + "', " +
                    empleado.Sindicato.ID + ")";


                OleDbCommand oleDbCommand = new OleDbCommand(sql, conn);

                if (oleDbCommand.ExecuteNonQuery() != 1)
                {
                    MessageBox.Show("No se ha podido agregar el Empleado", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    tbApellido.Text = "";
                    tbApellido.Focus();

                    tbNombre.Text = "";
                    tbCUIL.Text = "";
                    tbCategoria.Text = "";

                    cbSindicato.SelectedIndex = -1;

                    actualizarComboEmpleados();
                }
            }
        }

        private void tbApellido_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                e.Handled = true;
                tbNombre.Focus();
            }
            else
            {
                e.KeyChar = Char.ToUpper(e.KeyChar);
            }
        }

        private void tbNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                e.Handled = true;
                tbCUIL.Focus();
            }
            else
            {
                e.KeyChar = Char.ToUpper(e.KeyChar);
            }
        }

        private void tbCUIL_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                e.Handled = true;
                dtIngreso.Focus();
            }
            else
            {
                e.KeyChar = Char.ToUpper(e.KeyChar);
            }
        }

        private void dateTimeIngreso_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                e.Handled = true;
                tbCategoria.Focus();
            }
        }

        private void tbCategoria_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                e.Handled = true;
                cbSindicato.Focus();
            }
            else
            {
                e.KeyChar = Char.ToUpper(e.KeyChar);
            }
        }

        private void cbSindicato_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                e.Handled = true;
                butAgregar.Focus();
            }
        }

        public void actualizarComboEmpleados()
        {
            cbEmpleado.Items.Clear();

            foreach (Empleado empleado in Utils.getEmpleados(conn))
            {
                cbEmpleado.Items.Add(empleado);
            }
        }

        private void butCancelar_Click(object sender, EventArgs e)
        {
            butCancelar.Enabled = false;
            buteEditar.Enabled = false;

            limpiar();
            enabledEditar(false);
        }

        public void limpiar()
        {
            cbEmpleado.SelectedIndex = -1;

            tbApellidoE.Text = "";
            tbNombreE.Text = "";
            tbCUILE.Text = "";
            tbCategoriaE.Text = "";

            cbSindicatoE.SelectedIndex = -1;
        }

        public void enabledEditar(bool b)
        {
            tbApellidoE.Enabled = b;
            tbNombreE.Enabled = b;
            tbCUILE.Enabled = b;
            dtIngresoE.Enabled = b;
            tbCategoriaE.Enabled = b;
            cbSindicatoE.Enabled = b;
        }

        private void buteEditar_Click(object sender, EventArgs e)
        {
            Empleado empleado = (Empleado)cbEmpleado.SelectedItem;
            Sindicato sindicato;

            if (cbSindicatoE.SelectedIndex != -1)
            {
                sindicato = (Sindicato)cbSindicatoE.SelectedItem;
            }
            else
            {
                sindicato = new Sindicato(0, "");
            }


            string msg = "¿Realmente desea confirmar estos cambios?" +
            "\n\nApellido\t\t '" + empleado.Apellido + "' por '" + tbApellidoE.Text + "'" +
            "\nNombre\t\t '" + empleado.Nombre + "' por '" + tbNombreE.Text + "'" +
            "\nCUIL\t\t '" + empleado.CUIL + "' por '" + tbCUILE.Text + "'" +
            "\nIngreso\t\t '" + empleado.FechaIngreso.ToShortDateString() + "' por '" + dtIngresoE.Value.ToShortDateString() + "'" +
            "\nCategoría\t '" + empleado.Categoria + "' por '" + tbCategoriaE.Text + "'" +
            "\nSindicato\t '" + empleado.Sindicato.ToString() + "' por '" + sindicato.ToString() + "'";

            DialogResult result = MessageBox.Show(msg, "Editar Empleado",
MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);

            if (result == DialogResult.Yes)
            {
                string sql = "UPDATE Empleado SET Apellido = '" + tbApellidoE.Text + "'" +
               ", Nombre = '" + tbNombreE.Text + "'" +
               ", CUIL = '" + tbCUILE.Text + "'" +
               ", FechaDeIngreso = '" + dtIngresoE.Value.ToShortDateString() + "'" +
               ", Categoria = '" + tbCategoriaE.Text + "'" +
               ", IdSindicato = " + sindicato.ID +
               " WHERE Id = " + empleado.ID;

                OleDbDataAdapter oledbAdapter = new OleDbDataAdapter();

                oledbAdapter.UpdateCommand = conn.CreateCommand();
                oledbAdapter.UpdateCommand.CommandText = sql;

                int i = oledbAdapter.UpdateCommand.ExecuteNonQuery();

                if (i != 1)
                {
                    MessageBox.Show("Los valores no se han podido modificar.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    actualizarComboEmpleados();

                    limpiar();

                    butCancelar.PerformClick();

                    tbApellido.Focus();
                }
            }
        }

        private void tbApellidoE_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                e.Handled = true;
                tbNombreE.Focus();
            }
            else
            {
                e.KeyChar = Char.ToUpper(e.KeyChar);
            }
        }

        private void tbNombreE_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                e.Handled = true;
                tbCUILE.Focus();
            }
            else
            {
                e.KeyChar = Char.ToUpper(e.KeyChar);
            }
        }

        private void tbCUILE_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                e.Handled = true;
                dtIngresoE.Focus();
            }
            else
            {
                e.KeyChar = Char.ToUpper(e.KeyChar);
            }
        }

        private void dtIngresoE_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                e.Handled = true;
                tbCategoriaE.Focus();
            }
            else
            {
                e.KeyChar = Char.ToUpper(e.KeyChar);
            }
        }

        private void tbCategoriaE_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                e.Handled = true;
                cbSindicatoE.Focus();
            }
            else
            {
                e.KeyChar = Char.ToUpper(e.KeyChar);
            }
        }

        private void cbSindicatoE_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                e.Handled = true;
                buteEditar.Focus();
            }
            else
            {
                e.KeyChar = Char.ToUpper(e.KeyChar);
            }
        }
    }
}
