﻿namespace Gestion_Minera.Ventanas.Sueldos
{
    partial class AgregarNovedad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AgregarNovedad));
            this.dtHasta = new System.Windows.Forms.DateTimePicker();
            this.dtDesde = new System.Windows.Forms.DateTimePicker();
            this.labHasta = new System.Windows.Forms.Label();
            this.labDesde = new System.Windows.Forms.Label();
            this.cbNovedad = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.butAgregar = new System.Windows.Forms.Button();
            this.cbEmpleado = new System.Windows.Forms.ComboBox();
            this.panValor = new System.Windows.Forms.Panel();
            this.cbComprobante = new System.Windows.Forms.CheckBox();
            this.cbMesValor = new System.Windows.Forms.ComboBox();
            this.tbValor = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panKilometraje = new System.Windows.Forms.Panel();
            this.cbMes = new System.Windows.Forms.ComboBox();
            this.tbViaticos = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbControl = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbKm = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panFechas = new System.Windows.Forms.Panel();
            this.rbRango = new System.Windows.Forms.RadioButton();
            this.rbFecha = new System.Windows.Forms.RadioButton();
            this.panValor.SuspendLayout();
            this.panKilometraje.SuspendLayout();
            this.panFechas.SuspendLayout();
            this.SuspendLayout();
            // 
            // dtHasta
            // 
            this.dtHasta.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtHasta.Location = new System.Drawing.Point(82, 55);
            this.dtHasta.Name = "dtHasta";
            this.dtHasta.Size = new System.Drawing.Size(106, 20);
            this.dtHasta.TabIndex = 69;
            this.dtHasta.TabStop = false;
            this.dtHasta.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtHasta_KeyPress);
            // 
            // dtDesde
            // 
            this.dtDesde.CustomFormat = "";
            this.dtDesde.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDesde.Location = new System.Drawing.Point(82, 28);
            this.dtDesde.Name = "dtDesde";
            this.dtDesde.Size = new System.Drawing.Size(106, 20);
            this.dtDesde.TabIndex = 68;
            this.dtDesde.TabStop = false;
            this.dtDesde.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtDesde_KeyPress);
            // 
            // labHasta
            // 
            this.labHasta.AutoSize = true;
            this.labHasta.Location = new System.Drawing.Point(41, 57);
            this.labHasta.Name = "labHasta";
            this.labHasta.Size = new System.Drawing.Size(35, 13);
            this.labHasta.TabIndex = 59;
            this.labHasta.Text = "Hasta";
            // 
            // labDesde
            // 
            this.labDesde.AutoSize = true;
            this.labDesde.Location = new System.Drawing.Point(38, 31);
            this.labDesde.Name = "labDesde";
            this.labDesde.Size = new System.Drawing.Size(38, 13);
            this.labDesde.TabIndex = 58;
            this.labDesde.Text = "Desde";
            this.labDesde.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbNovedad
            // 
            this.cbNovedad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbNovedad.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbNovedad.Location = new System.Drawing.Point(72, 43);
            this.cbNovedad.Name = "cbNovedad";
            this.cbNovedad.Size = new System.Drawing.Size(229, 23);
            this.cbNovedad.TabIndex = 57;
            this.cbNovedad.TabStop = false;
            this.cbNovedad.SelectedIndexChanged += new System.EventHandler(this.cbNovedad_SelectedIndexChanged);
            this.cbNovedad.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbNovedad_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 46);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 13);
            this.label7.TabIndex = 56;
            this.label7.Text = "Novedad";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 19);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 13);
            this.label8.TabIndex = 55;
            this.label8.Text = "Empleado";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // butAgregar
            // 
            this.butAgregar.Location = new System.Drawing.Point(205, 198);
            this.butAgregar.Name = "butAgregar";
            this.butAgregar.Size = new System.Drawing.Size(96, 23);
            this.butAgregar.TabIndex = 70;
            this.butAgregar.TabStop = false;
            this.butAgregar.Text = "AGREGAR";
            this.butAgregar.UseVisualStyleBackColor = true;
            this.butAgregar.Click += new System.EventHandler(this.butAgregar_Click);
            // 
            // cbEmpleado
            // 
            this.cbEmpleado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbEmpleado.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbEmpleado.Location = new System.Drawing.Point(72, 16);
            this.cbEmpleado.Name = "cbEmpleado";
            this.cbEmpleado.Size = new System.Drawing.Size(229, 23);
            this.cbEmpleado.TabIndex = 53;
            this.cbEmpleado.TabStop = false;
            this.cbEmpleado.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbEmpleado_KeyPress);
            // 
            // panValor
            // 
            this.panValor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panValor.Controls.Add(this.cbComprobante);
            this.panValor.Controls.Add(this.cbMesValor);
            this.panValor.Controls.Add(this.tbValor);
            this.panValor.Controls.Add(this.label1);
            this.panValor.Location = new System.Drawing.Point(72, 72);
            this.panValor.Name = "panValor";
            this.panValor.Size = new System.Drawing.Size(229, 120);
            this.panValor.TabIndex = 64;
            // 
            // cbComprobante
            // 
            this.cbComprobante.AutoSize = true;
            this.cbComprobante.Checked = true;
            this.cbComprobante.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbComprobante.Location = new System.Drawing.Point(50, 65);
            this.cbComprobante.Name = "cbComprobante";
            this.cbComprobante.Size = new System.Drawing.Size(129, 17);
            this.cbComprobante.TabIndex = 70;
            this.cbComprobante.Text = "Generar comprobante";
            this.cbComprobante.UseVisualStyleBackColor = true;
            // 
            // cbMesValor
            // 
            this.cbMesValor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMesValor.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbMesValor.FormattingEnabled = true;
            this.cbMesValor.Location = new System.Drawing.Point(50, 35);
            this.cbMesValor.Name = "cbMesValor";
            this.cbMesValor.Size = new System.Drawing.Size(164, 23);
            this.cbMesValor.TabIndex = 69;
            this.cbMesValor.TabStop = false;
            this.cbMesValor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbMesValor_KeyPress);
            // 
            // tbValor
            // 
            this.tbValor.Location = new System.Drawing.Point(50, 9);
            this.tbValor.Name = "tbValor";
            this.tbValor.Size = new System.Drawing.Size(164, 20);
            this.tbValor.TabIndex = 68;
            this.tbValor.TabStop = false;
            this.tbValor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbValor_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Valor";
            // 
            // panKilometraje
            // 
            this.panKilometraje.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panKilometraje.Controls.Add(this.cbMes);
            this.panKilometraje.Controls.Add(this.tbViaticos);
            this.panKilometraje.Controls.Add(this.label4);
            this.panKilometraje.Controls.Add(this.tbControl);
            this.panKilometraje.Controls.Add(this.label3);
            this.panKilometraje.Controls.Add(this.tbKm);
            this.panKilometraje.Controls.Add(this.label2);
            this.panKilometraje.Location = new System.Drawing.Point(72, 72);
            this.panKilometraje.Name = "panKilometraje";
            this.panKilometraje.Size = new System.Drawing.Size(229, 120);
            this.panKilometraje.TabIndex = 67;
            // 
            // cbMes
            // 
            this.cbMes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMes.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbMes.FormattingEnabled = true;
            this.cbMes.Location = new System.Drawing.Point(76, 87);
            this.cbMes.Name = "cbMes";
            this.cbMes.Size = new System.Drawing.Size(138, 23);
            this.cbMes.TabIndex = 69;
            this.cbMes.TabStop = false;
            this.cbMes.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbMes_KeyPress);
            // 
            // tbViaticos
            // 
            this.tbViaticos.Location = new System.Drawing.Point(77, 61);
            this.tbViaticos.Name = "tbViaticos";
            this.tbViaticos.Size = new System.Drawing.Size(137, 20);
            this.tbViaticos.TabIndex = 68;
            this.tbViaticos.TabStop = false;
            this.tbViaticos.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbViaticos_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 64);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 69;
            this.label4.Text = "Viáticos";
            // 
            // tbControl
            // 
            this.tbControl.Location = new System.Drawing.Point(77, 35);
            this.tbControl.Name = "tbControl";
            this.tbControl.Size = new System.Drawing.Size(137, 20);
            this.tbControl.TabIndex = 67;
            this.tbControl.TabStop = false;
            this.tbControl.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbControl_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 38);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 67;
            this.label3.Text = "Control";
            // 
            // tbKm
            // 
            this.tbKm.Location = new System.Drawing.Point(77, 9);
            this.tbKm.Name = "tbKm";
            this.tbKm.Size = new System.Drawing.Size(137, 20);
            this.tbKm.TabIndex = 66;
            this.tbKm.TabStop = false;
            this.tbKm.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbKm_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Kilometraje";
            // 
            // panFechas
            // 
            this.panFechas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panFechas.Controls.Add(this.rbRango);
            this.panFechas.Controls.Add(this.rbFecha);
            this.panFechas.Controls.Add(this.dtDesde);
            this.panFechas.Controls.Add(this.labDesde);
            this.panFechas.Controls.Add(this.labHasta);
            this.panFechas.Controls.Add(this.dtHasta);
            this.panFechas.Location = new System.Drawing.Point(72, 72);
            this.panFechas.Name = "panFechas";
            this.panFechas.Size = new System.Drawing.Size(229, 120);
            this.panFechas.TabIndex = 71;
            // 
            // rbRango
            // 
            this.rbRango.AutoSize = true;
            this.rbRango.Location = new System.Drawing.Point(95, 5);
            this.rbRango.Name = "rbRango";
            this.rbRango.Size = new System.Drawing.Size(107, 17);
            this.rbRango.TabIndex = 63;
            this.rbRango.TabStop = true;
            this.rbRango.Text = "Rango de fechas";
            this.rbRango.UseVisualStyleBackColor = true;
            // 
            // rbFecha
            // 
            this.rbFecha.AutoSize = true;
            this.rbFecha.Location = new System.Drawing.Point(34, 5);
            this.rbFecha.Name = "rbFecha";
            this.rbFecha.Size = new System.Drawing.Size(55, 17);
            this.rbFecha.TabIndex = 62;
            this.rbFecha.TabStop = true;
            this.rbFecha.Text = "Fecha";
            this.rbFecha.UseVisualStyleBackColor = true;
            this.rbFecha.CheckedChanged += new System.EventHandler(this.rbFecha_CheckedChanged);
            // 
            // AgregarNovedad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(313, 230);
            this.Controls.Add(this.panFechas);
            this.Controls.Add(this.panKilometraje);
            this.Controls.Add(this.panValor);
            this.Controls.Add(this.cbNovedad);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.butAgregar);
            this.Controls.Add(this.cbEmpleado);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(72, 72);
            this.Name = "AgregarNovedad";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Agregar Novedad";
            this.panValor.ResumeLayout(false);
            this.panValor.PerformLayout();
            this.panKilometraje.ResumeLayout(false);
            this.panKilometraje.PerformLayout();
            this.panFechas.ResumeLayout(false);
            this.panFechas.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DateTimePicker dtHasta;
        private System.Windows.Forms.DateTimePicker dtDesde;
        private System.Windows.Forms.Label labHasta;
        private System.Windows.Forms.Label labDesde;
        private System.Windows.Forms.ComboBox cbNovedad;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button butAgregar;
        private System.Windows.Forms.ComboBox cbEmpleado;
        private System.Windows.Forms.Panel panValor;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbValor;
        private System.Windows.Forms.Panel panKilometraje;
        private System.Windows.Forms.TextBox tbKm;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbViaticos;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbControl;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panFechas;
        private System.Windows.Forms.RadioButton rbFecha;
        private System.Windows.Forms.RadioButton rbRango;
        private System.Windows.Forms.ComboBox cbMesValor;
        private System.Windows.Forms.ComboBox cbMes;
        private System.Windows.Forms.CheckBox cbComprobante;
    }
}