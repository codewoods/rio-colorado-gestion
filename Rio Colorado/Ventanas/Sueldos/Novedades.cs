﻿using Gestion_Minera.Clases;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using DDJJ_Regalías_Mineras.Clases;
using System.Windows.Forms;
using Application = Microsoft.Office.Interop.Word.Application;
using Microsoft.Office.Interop.Word;
using System.Reflection;

namespace Gestion_Minera.Ventanas.Sueldos
{
    public partial class Novedades : Form
    {
        OleDbConnection conn;
        List<Novedad> listaNovedades;
        List<Empleado> listaEmpleados;

        DateTime desde, hasta;
        int empleadoID, novedadID;

        int total;

        public Novedades(OleDbConnection conn)
        {
            InitializeComponent();

            this.conn = conn;

            cbEmpleado.Items.Add(new Empleado(0));

            listaEmpleados = Utils.getEmpleados(conn);

            foreach (Empleado empleado in listaEmpleados)
            {
                cbEmpleado.Items.Add(empleado);
            }

            cbNovedades.Items.Add(new Novedad(0));

            listaNovedades = Utils.getNovedades(conn);

            foreach (Novedad novedad in listaNovedades)
            {
                cbNovedades.Items.Add(novedad);
            }

            cbEmpleado.SelectedIndex = 0;
            cbNovedades.SelectedIndex = 0;

            cbMes.Items.Add(new Item("ENERO", 1));
            cbMes.Items.Add(new Item("FEBRERO", 2));
            cbMes.Items.Add(new Item("MARZO", 3));
            cbMes.Items.Add(new Item("ABRIL", 4));
            cbMes.Items.Add(new Item("MAYO", 5));
            cbMes.Items.Add(new Item("JUNIO", 6));
            cbMes.Items.Add(new Item("JULIO", 7));
            cbMes.Items.Add(new Item("AGOSTO", 8));
            cbMes.Items.Add(new Item("SEPTIEMBRE", 9));
            cbMes.Items.Add(new Item("OCTUBRE", 10));
            cbMes.Items.Add(new Item("NOVIEMBRE", 11));
            cbMes.Items.Add(new Item("DICIEMBRE", 12));

            cbMes.SelectedIndex = int.Parse(DateTime.Now.Month.ToString("00")) - 1;

            numAnio.Value = DateTime.Today.Year;

            butEliminar.Enabled = false;
            butEditar.Enabled = false;

            butAnterior.Enabled = false;
            butSiguiente.Enabled = false;

            butGenerarComprobante.Visible = false;
        }

        private void butAgregarNovedad_Click(object sender, EventArgs e)
        {
            AgregarNovedad agregarNovedad = new AgregarNovedad(conn, listaNovedades, listaEmpleados);
            agregarNovedad.Show();
        }

        private void butBuscar_Click(object sender, EventArgs e)
        {
            if (rbFecha.Checked)
            {
                desde = hasta = DateTime.MinValue;
            }
            else
            {
                desde = dtDesde.Value;
                hasta = dtHasta.Value;
            }

            empleadoID = ((Empleado)cbEmpleado.SelectedItem).ID;
            novedadID = ((Novedad)cbNovedades.SelectedItem).ID;

            total = Utils.getCountNovedades(conn,
                empleadoID,
                novedadID,
                desde,
                hasta);

            tbPagActual.Text = "1";

            if (total > 8)
            {
                tbPagUltima.Text = Math.Ceiling(Decimal.Parse(total.ToString()) / 8).ToString();
            }
            else
            {
                tbPagUltima.Text = "1";
            }

            butAnterior.Enabled = false;
            butSiguiente.Enabled = total > 8;

            buscar();
        }

        private void rbFecha_CheckedChanged(object sender, EventArgs e)
        {
            dtDesde.Enabled = dtHasta.Enabled = !rbFecha.Checked;
        }

        private void dgTabla_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
        {
            butEditar.Enabled = dgTabla.SelectedRows.Count > 0;
            butEliminar.Enabled = dgTabla.SelectedRows.Count > 0;

            if (dgTabla.SelectedRows.Count > 0)
            {
                butEditar.Enabled = dgTabla.SelectedRows[0].Cells["NOVEDAD"].Value.ToString() != "Vacaciones";
            }
        }

        private void dgTabla_SelectionChanged(object sender, EventArgs e)
        {
            if (dgTabla.SelectedRows.Count > 0)
            {
                butGenerarComprobante.Visible = dgTabla.Rows[dgTabla.CurrentCell.RowIndex].Cells[8].Value.ToString() == "6";
            }
        }

        private void butSiguiente_Click(object sender, EventArgs e)
        {
            tbPagActual.Text = (int.Parse(tbPagActual.Text) + 1).ToString();

            butSiguiente.Enabled = int.Parse(tbPagActual.Text) != int.Parse(tbPagUltima.Text);
            butAnterior.Enabled = int.Parse(tbPagActual.Text) != 1;

            buscar();
        }

        private void butGenerarComprobante_Click(object sender, EventArgs e)
        {
            NovedadesEmpleados novedad = Utils.getNovedadEmpleado(conn, int.Parse(dgTabla.Rows[dgTabla.CurrentCell.RowIndex].Cells[7].Value.ToString()));
            String nombre = dgTabla.Rows[dgTabla.CurrentCell.RowIndex].Cells[2].Value.ToString();

            int coun = Utils.getNumberNovedad(conn, int.Parse(dgTabla.Rows[dgTabla.CurrentCell.RowIndex].Cells[7].Value.ToString()), 6);

            Utils.generarComprobanteHaberes(coun, novedad.valor.ToString(), novedad.desde.ToString("MMMM").ToUpper(), nombre);
        }

        private void butAnterior_Click(object sender, EventArgs e)
        {
            tbPagActual.Text = (int.Parse(tbPagActual.Text) - 1).ToString();

            butSiguiente.Enabled = int.Parse(tbPagActual.Text) != int.Parse(tbPagUltima.Text);
            butAnterior.Enabled = int.Parse(tbPagActual.Text) != 1;

            buscar();
        }

        private void butEliminar_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Realmente desea borrar la fila seleccionada?", "Cuidado",
MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                try
                {
                    int i = int.Parse(dgTabla.Rows[dgTabla.CurrentCell.RowIndex].Cells[7].Value.ToString());

                    OleDbCommand delcmd = new OleDbCommand("DELETE FROM NovedadesEmpleados WHERE Id = " + i, conn);

                    if (delcmd.ExecuteNonQuery() != 1)
                    {
                        MessageBox.Show("No se ha podido eliminar el registro.", "Error");
                    }
                    else
                    {
                        buscar();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("No se ha podido eliminar el registro.", "Error");
                }
            }
        }

        private void butGenerarInforme_Click(object sender, EventArgs e)
        {
            generarNovedades();

            /*object oMissing = Missing.Value;

            Application app = new Microsoft.Office.Interop.Word.Application();
            app.Visible = true;

            Document doc = app.Documents.Add(ref oMissing, ref oMissing, ref oMissing, ref oMissing);

            Range range = doc.Range(0, 0);
            range.Text = "tOTAL DE HORAS: ";
            Range subRange = doc.Range(range.Text.Length -1, range.Text.Length-1);

            subRange.Text = range.Text.Length + "";
            range.InsertParagraphAfter();

            Range subRange = doc.Range(range.StoryLength - 1, range.StoryLength - 1);
            subRange.ListFormat.ApplyBulletDefault();
            subRange.ListFormat.ListIndent();
            subRange.Text = "Alt Birinci";
            subRange.InsertParagraphAfter();
            ListTemplate sublistTemplate = subRange.ListFormat.ListTemplate;

            Range subRange2 = doc.Range(subRange.StoryLength - 1, range.StoryLength - 1);
            subRange2.ListFormat.ApplyListTemplate(sublistTemplate);
            subRange2.ListFormat.ListIndent();
            subRange2.Text = "Alt İkinci";
            subRange2.InsertParagraphAfter();

            Range range2 = doc.Range(range.StoryLength - 1, range.StoryLength - 1);
            range2.ListFormat.ApplyListTemplateWithLevel(listTemplate, true);
            WdContinue isContinue = range2.ListFormat.CanContinuePreviousList(listTemplate);
            range2.Text = "İkinci";
            range2.InsertParagraphAfter();

            Range range3 = doc.Range(range2.StoryLength - 1, range.StoryLength - 1);
            range3.ListFormat.ApplyListTemplate(listTemplate);
            range3.Text = "Üçüncü";
            range3.InsertParagraphAfter();*/
        }

        private void butEditar_Click(object sender, EventArgs e)
        {
            AgregarNovedad agregarNovedad = new AgregarNovedad(conn, listaNovedades, listaEmpleados);
            agregarNovedad.setNovedadEmpleado(Utils.getNovedadEmpleado(conn, int.Parse(dgTabla.Rows[dgTabla.CurrentCell.RowIndex].Cells[7].Value.ToString())));
            agregarNovedad.Show();
        }

        public void buscar()
        {
            dgTabla.Rows.Clear();

            int i;

            if (int.Parse(tbPagActual.Text) == 1)
            {
                i = 1;
            }
            else
            {
                i = ((int.Parse(tbPagActual.Text) - 1) * 8);
            }

            List<NovedadesEmpleados> novedadesEmpleados = Utils.getNovedadesEmpleados(conn,
                empleadoID,
                novedadID,
                desde,
                hasta,
                8,
                i);

            foreach (NovedadesEmpleados novedad in novedadesEmpleados)
            {
                String valor = "", desde = "", hasta = "";

                if (novedad.valor2 != "")
                {
                    valor = "Km.: " + novedad.valor + "\nControl: " + novedad.valor2 + "\nViát.: " + novedad.valor3;
                }
                else
                {
                    valor = novedad.valor;
                }

                if (novedad.novedad.fecha)
                {
                    desde = novedad.desde.ToShortDateString();
                    hasta = novedad.hasta.ToShortDateString();
                }
                else
                {
                    desde = hasta = novedad.desde.ToString("MMMM").ToUpper() + "\n" + novedad.desde.Year;
                }

                string[] row = new string[] {novedad.fecha.ToShortDateString() + "\n" + novedad.fecha.ToShortTimeString() + " hs.",
                    novedad.empleado.Sindicato.Nombre,
                    novedad.empleado.ToString(),
                    novedad.novedad.ToString(),
                valor,
                desde,
                hasta,
                novedad.ID.ToString(),
                novedad.novedad.ID.ToString()};

                dgTabla.Rows.Add(row);
            }

            dgTabla.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        public void generarNovedades()
        {
            List<Sindicato> sindicatos = Utils.getSindicatos(conn);
            List<Empleado> empleados = Utils.getEmpleados(conn);
            List<Novedad> novedades = Utils.getNovedades(conn);

            int anio;

            try
            {
                anio = int.Parse(numAnio.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR", "Error al leer el campo AÑO.");
                return;
            }

            List<NovedadesEmpleados> novedadesEmpleados = Utils.getNovedadesEmpleadosMes(conn, cbMes.SelectedIndex + 1, anio);

            object oMissing = Missing.Value;
            object oEndOfDoc = "\\endofdoc"; /* \endofdoc is a predefined bookmark */

            Application word = new Microsoft.Office.Interop.Word.Application();
            word.Visible = true;


            Document doc = word.Documents.Add(ref oMissing, ref oMissing, ref oMissing, ref oMissing);

            word.Activate();

            bool salto = false;

            foreach (Sindicato sindicato in sindicatos)
            {
                insertarParrafo(doc, "Corralón Río Colorado", 2, 1, 13, true, false, salto);
                insertarParrafo(doc, "Novedades " + cbMes.Text + " " + numAnio.Text, 2, 0, 12, true, false, false);
                insertarParrafo(doc, "Sindicato: " + sindicato.Nombre, 50, 0, 12, true, false, false);

                salto = true;

                foreach (Empleado empleado in empleados)
                {
                    if (empleado.Sindicato.ID == sindicato.ID)
                    {
                        bool nove = false;

                        int ultimoDia = 0;

                        insertarNombreEmpleado(doc, empleado.Apellido + ", " + empleado.Nombre + ":");

                        foreach (Novedad novedad in novedades)
                        {
                            String novedadString = "";
                            List<String> novedadString2 = new List<string>();

                            int cantidad = 0, cantidad2 = 0, cantidad3 = 0;
                            String cantidad4 = "";

                            foreach (NovedadesEmpleados novedadEmpleado in novedadesEmpleados)
                            {
                                if (novedad.ID == novedadEmpleado.novedad.ID && novedadEmpleado.empleado.ID == empleado.ID)
                                {
                                    nove = true;

                                    if (novedad.ID == 10)
                                    {
                                        novedadString = novedad.novedad + ".";
                                    }
                                    else
                                    {
                                        novedadString = novedad.novedad + ": ";
                                    }


                                    if (novedad.fecha)
                                    {
                                        ultimoDia = DateTime.DaysInMonth(anio, cbMes.SelectedIndex + 1);

                                        if (novedadEmpleado.hasta.Month > cbMes.SelectedIndex + 1)
                                        {
                                            novedadEmpleado.hasta = new DateTime(anio, cbMes.SelectedIndex + 1, ultimoDia);
                                        }

                                        if (novedadEmpleado.desde.Month < cbMes.SelectedIndex + 1)
                                        {
                                            novedadEmpleado.desde = new DateTime(anio, cbMes.SelectedIndex + 1, 1);
                                        }

                                        cantidad += (novedadEmpleado.hasta - novedadEmpleado.desde).Days + 1;

                                        if (novedadEmpleado.hasta == novedadEmpleado.desde)
                                        {
                                            novedadString2.Add(novedadEmpleado.hasta.ToShortDateString() + " (1 día).");
                                        }
                                        else
                                        {
                                            novedadString2.Add("Desde " + novedadEmpleado.desde.ToShortDateString() +
                                                " hasta " + novedadEmpleado.hasta.ToShortDateString() +
                                                " (" + ((novedadEmpleado.hasta - novedadEmpleado.desde).Days + 1) + " días).");
                                        }
                                    }
                                    else
                                    {
                                        //novedadString2.Add(novedadEmpleado.desde.ToShortDateString() + ": " +
                                        // novedadEmpleado.novedad.medida + novedadEmpleado.valor);

                                        try
                                        {
                                            cantidad += int.Parse(novedadEmpleado.valor);
                                        }
                                        catch (Exception ex)
                                        {
                                            cantidad4 = novedadEmpleado.valor;
                                        }

                                        if (novedadEmpleado.valor2 != "") cantidad2 += int.Parse(novedadEmpleado.valor2);
                                        if (novedadEmpleado.valor3 != "") cantidad3 += int.Parse(novedadEmpleado.valor3);
                                    }
                                }
                            }

                            if (novedadString != "")
                            {
                                if (nove)
                                {
                                    if (novedad.medida == "$")
                                    {
                                        insertarNovedad(doc, novedadString, novedad.medida + cantidad.ToString(), novedadString2);
                                    }
                                    else if (novedad.medida == "")
                                    {
                                        insertarNovedad(doc, novedadString, novedad.medida + cantidad4, novedadString2);
                                    }
                                    else
                                    {
                                        if (cantidad2 > 0 || cantidad3 > 0)
                                        {
                                            insertarNovedad(doc, "Kilometraje: ", cantidad.ToString() + " " + novedad.medida, new List<string>());
                                            insertarNovedad(doc, "Control: ", cantidad2.ToString() + " " + novedad.medida, new List<string>());
                                            insertarNovedad(doc, "Viáticos: ", cantidad3.ToString() + " " + novedad.medida, new List<string>());
                                        }
                                        else
                                        {
                                            insertarNovedad(doc, novedadString, cantidad.ToString() + " " + novedad.medida, novedadString2);
                                        }
                                    }
                                }
                            }
                        }

                        if (!nove)
                        {
                            insertarNovedad(doc, "Sin novedades.", "", new List<String>());
                        }

                        insertarNovedad(doc, "", "", new List<string>());
                    }
                }
            }
        }

        /* FUNCIONES WORD */

        public void insertarParrafo(Document doc, String text, int space, int negrita, int font, bool aling, bool subrayado, bool salto)
        {
            Paragraph parrafo = doc.Content.Paragraphs.Add(Missing.Value);

            if (salto) parrafo.Range.InsertBreak(Microsoft.Office.Interop.Word.WdBreakType.wdPageBreak);

            parrafo.Range.Text = text;
            parrafo.Range.Font.Bold = negrita;
            parrafo.Range.Font.Size = font;
            parrafo.Format.SpaceAfter = space;

            if (subrayado) parrafo.Range.Font.Underline = Microsoft.Office.Interop.Word.WdUnderline.wdUnderlineSingle;
            else parrafo.Range.Font.Underline = Microsoft.Office.Interop.Word.WdUnderline.wdUnderlineNone;

            if (aling) parrafo.Range.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;
            else parrafo.Range.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphLeft;


            parrafo.Range.InsertParagraphAfter();
        }

        public void insertarNombreEmpleado(Document doc, String text)
        {
            Paragraph parrafo = doc.Content.Paragraphs.Add(Missing.Value);

            parrafo.Range.Text = text;
            parrafo.Range.Font.Bold = 1;
            parrafo.Range.Font.Size = 11;
            parrafo.Format.SpaceAfter = 8;

            parrafo.Range.Font.Underline = Microsoft.Office.Interop.Word.WdUnderline.wdUnderlineSingle;

            parrafo.Range.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphLeft;

            parrafo.Range.InsertParagraphAfter();
        }

        public void insertarNovedad(Document doc, String text1, String value, List<String> text)
        {
            Paragraph parrafo = doc.Content.Paragraphs.Add(Missing.Value);

            parrafo.Range.Font.Bold = 0;
            parrafo.Range.Font.Size = 11;

            if (text.Count == 0)
            {
                parrafo.Format.SpaceAfter = 8;
            }
            else
            {
                parrafo.Format.SpaceAfter = 2;
            }

            parrafo.Range.Font.Underline = Microsoft.Office.Interop.Word.WdUnderline.wdUnderlineNone;
            parrafo.Range.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphLeft;

            parrafo.Range.Text = text1;

            Range subRange = doc.Range(parrafo.Range.End - 1, parrafo.Range.End - 1);

            subRange.Text = value;
            subRange.Font.Bold = 1;
            parrafo.Range.InsertParagraphAfter();

            foreach (String te in text)
            {
                parrafo = doc.Content.Paragraphs.Add(Missing.Value);

                parrafo.Range.Font.Bold = 0;
                parrafo.Range.Font.Size = 9;
                parrafo.Range.Font.Italic = 1;
                parrafo.Format.SpaceAfter = 8;

                parrafo.Range.Font.Underline = Microsoft.Office.Interop.Word.WdUnderline.wdUnderlineNone;
                parrafo.Range.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphLeft;

                parrafo.Range.Text = "     •  " + te;
                parrafo.Range.InsertParagraphAfter();
            }
        }
    }
}
