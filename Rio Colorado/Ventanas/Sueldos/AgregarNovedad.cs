﻿using Gestion_Minera.Clases;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Windows.Forms;

namespace Gestion_Minera.Ventanas.Sueldos
{
    public partial class AgregarNovedad : Form
    {
        OleDbConnection conn;
        NovedadesEmpleados novedadEmpleadoE = null;

        public AgregarNovedad(OleDbConnection conn, List<Novedad> listaNovedades, List<Empleado> listaEmpleados)
        {
            InitializeComponent();

            this.conn = conn;

            foreach (Empleado empleado in listaEmpleados)
            {
                cbEmpleado.Items.Add(empleado);
            }

            listaNovedades = Utils.getNovedades(conn);

            foreach (Novedad novedad in listaNovedades)
            {
                if (novedad.ID != 3)
                {
                    cbNovedad.Items.Add(novedad);
                }
            }

            List<ComboBox> combos = new List<ComboBox>();
            combos.Add(cbMes);
            combos.Add(cbMesValor);

            foreach (ComboBox combo in combos)
            {
                combo.Items.Add("ENERO");
                combo.Items.Add("FEBRERO");
                combo.Items.Add("MARZO");
                combo.Items.Add("ABRIL");
                combo.Items.Add("MAYO");
                combo.Items.Add("JUNIO");
                combo.Items.Add("JULIO");
                combo.Items.Add("AGOSTO");
                combo.Items.Add("SEPTIEMBRE");
                combo.Items.Add("OCTUBRE");
                combo.Items.Add("NOVIEMBRE");
                combo.Items.Add("DICIEMBRE");

                combo.SelectedIndex = DateTime.Today.Month - 1;
            }

            cbEmpleado.SelectedIndex = 0;
            cbNovedad.SelectedIndex = 0;

            tbValor.Focus();
        }

        public void setNovedadEmpleado(NovedadesEmpleados novedadEmpleado)
        {
            this.novedadEmpleadoE = novedadEmpleado;

            int i = 0;

            foreach (Novedad novedad in cbNovedad.Items)
            {
                if (novedad.ID == novedadEmpleado.novedad.ID)
                {
                    cbNovedad.SelectedIndex = i;
                    break;
                }

                i++;
            }

            i = 0;


            foreach (Empleado empleado in cbEmpleado.Items)
            {
                if (empleado.ID == novedadEmpleado.empleado.ID)
                {
                    cbEmpleado.SelectedIndex = i;
                    break;
                }

                i++;
            }


            if (novedadEmpleado.novedad.fecha)
            {

                if (novedadEmpleado.hasta == novedadEmpleado.desde)
                {
                    rbFecha.Checked = true;
                }
                else
                {
                    rbRango.Checked = true;
                }


                dtHasta.Value = novedadEmpleado.hasta;
                dtDesde.Value = novedadEmpleado.desde;
            }
            else if (novedadEmpleado.novedad.valores == 3 && !novedadEmpleado.novedad.fecha)
            {
                tbKm.Text = novedadEmpleado.valor;
                tbControl.Text = novedadEmpleado.valor2;
                tbViaticos.Text = novedadEmpleado.valor3;

                cbMes.SelectedIndex = novedadEmpleado.desde.Month - 1;
            }
            else
            {
                tbValor.Text = novedadEmpleado.valor;
                cbMesValor.SelectedIndex = novedadEmpleado.desde.Month - 1;
            }

        }

        private void cbNovedad_SelectedIndexChanged(object sender, EventArgs e)
        {
            Novedad novedad = (Novedad)cbNovedad.SelectedItem;

            panFechas.Visible = novedad.fecha;

            panKilometraje.Visible = novedad.valores == 3 && !novedad.fecha;
            panValor.Visible = novedad.valores != 3 && !novedad.fecha;

            tbValor.Visible = novedad.ID != 10;

            if (novedad.fecha && novedad.valores == 1)
            {
                rbFecha.Checked = true;
            }
            else
            {
                rbFecha.Checked = false;
                rbRango.Checked = true;
            }

            cbComprobante.Visible = novedad.ID == 6;
        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void butAgregar_Click(object sender, EventArgs e)
        {
            String accion;
            NovedadesEmpleados novedadEmpleado = new NovedadesEmpleados();

            if (novedadEmpleadoE != null)
            {
                accion = "editar";
                novedadEmpleado.ID = novedadEmpleadoE.ID;
            }
            else
            {
                accion = "agregar";
            }

            novedadEmpleado.empleado = (Empleado)cbEmpleado.SelectedItem;
            novedadEmpleado.novedad = (Novedad)cbNovedad.SelectedItem;
            novedadEmpleado.valor = "";
            novedadEmpleado.valor2 = "";
            novedadEmpleado.valor3 = "";

            String sql = "";

            String msg = "¿Desea " + accion + " la siguiente Novedad?\n\n" +
            "Empleado:\t" + novedadEmpleado.empleado + "\n" +
            "Novedad:\t" + novedadEmpleado.novedad + "\n";

            if (panFechas.Visible)
            {
                if (rbFecha.Checked)
                {
                    msg += "Fecha:\t\t" + dtDesde.Value.ToShortDateString() + "\n";

                    sql = "INSERT INTO NovedadesEmpleados (IdEmpleado, IdNovedad, Fecha, Desde, Hasta) VALUES (" +
                            novedadEmpleado.empleado.ID + ", " +
                            novedadEmpleado.novedad.ID + ", " +
                            "'" + DateTime.Now.ToString() + "', " +
                            "'" + dtDesde.Value.ToShortDateString() + "', " +
                            "'" + dtDesde.Value.ToShortDateString() + "')";

                    novedadEmpleado.desde = dtDesde.Value;
                    novedadEmpleado.hasta = dtHasta.Value;
                }
                else
                {
                    msg += "Desde:\t\t" + dtDesde.Value.ToShortDateString() + "\n";
                    msg += "Hasta:\t\t" + dtHasta.Value.ToShortDateString();

                    sql = "INSERT INTO NovedadesEmpleados (IdEmpleado, IdNovedad, Fecha, Desde, Hasta) VALUES (" +
                            novedadEmpleado.empleado.ID + ", " +
                            novedadEmpleado.novedad.ID + ", " +
                            "'" + DateTime.Now.ToString() + "', " +
                            "'" + dtDesde.Value.ToShortDateString() + "', " +
                            "'" + dtHasta.Value.ToShortDateString() + "')";

                    novedadEmpleado.desde = dtDesde.Value;
                    novedadEmpleado.hasta = dtHasta.Value;
                }
            }
            else if (panKilometraje.Visible)
            {
                msg += "Kilometraje:\t" + tbKm.Text + "\n";
                msg += "Control:\t\t" + tbControl.Text + "\n";
                msg += "Viáticos:\t\t" + tbViaticos.Text + "\n";
                msg += "Mes:\t\t" + cbMes.SelectedItem.ToString() + "\n";

                sql = "INSERT INTO NovedadesEmpleados (IdEmpleado, IdNovedad, Fecha, Valor, Valor2, Valor3, Desde, Hasta) VALUES (" +
                        novedadEmpleado.empleado.ID + ", " +
                        novedadEmpleado.novedad.ID + ", " +
                        "'" + DateTime.Now.ToString() + "', " +
                        "'" + tbKm.Text + "', " +
                        "'" + tbControl.Text + "', " +
                        "'" + tbViaticos.Text + "', " +
                        "'" + new DateTime(DateTime.Today.Year, cbMes.SelectedIndex + 1, 1).ToShortDateString() + "', " +
                        "'" + new DateTime(DateTime.Today.Year, cbMes.SelectedIndex + 1, DateTime.DaysInMonth(DateTime.Today.Year, cbMes.SelectedIndex + 1)).ToShortDateString() + "')";

                novedadEmpleado.valor = tbKm.Text;
                novedadEmpleado.valor2 = tbControl.Text;
                novedadEmpleado.valor3 = tbViaticos.Text;
                novedadEmpleado.desde = new DateTime(DateTime.Today.Year, cbMes.SelectedIndex + 1, 1);
                novedadEmpleado.hasta = new DateTime(DateTime.Today.Year, cbMes.SelectedIndex + 1, DateTime.DaysInMonth(DateTime.Today.Year, cbMes.SelectedIndex + 1));
            }
            else if (panValor.Visible)
            {
                msg += "Valor:\t\t" + tbValor.Text + "\n";
                msg += "Mes:\t\t" + cbMesValor.SelectedItem.ToString() + "\n";

                sql = "INSERT INTO NovedadesEmpleados (IdEmpleado, IdNovedad, Fecha, Valor, Desde, Hasta) VALUES (" +
                        novedadEmpleado.empleado.ID + ", " +
                        novedadEmpleado.novedad.ID + ", " +
                        "'" + DateTime.Now.ToString() + "', " +
                        "'" + tbValor.Text + "', " +
                        "'" + new DateTime(DateTime.Today.Year, cbMesValor.SelectedIndex + 1, 1).ToShortDateString() + "', " +
                        "'" + new DateTime(DateTime.Today.Year, cbMesValor.SelectedIndex + 1, DateTime.DaysInMonth(DateTime.Today.Year, cbMesValor.SelectedIndex + 1)).ToShortDateString() + "')";

                novedadEmpleado.valor = tbValor.Text;
                novedadEmpleado.desde = new DateTime(DateTime.Today.Year, cbMesValor.SelectedIndex + 1, 1);
                novedadEmpleado.hasta = new DateTime(DateTime.Today.Year, cbMesValor.SelectedIndex + 1, DateTime.DaysInMonth(DateTime.Today.Year, cbMesValor.SelectedIndex + 1));
            }

            DialogResult result = MessageBox.Show(msg, "Agregar Novedad", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);

            if (result == DialogResult.Yes)
            {
                if (novedadEmpleadoE != null)
                {
                    sql = "UPDATE NovedadesEmpleados SET IdEmpleado = " + novedadEmpleado.empleado.ID +
                   ", IdNovedad = " + novedadEmpleado.novedad.ID +
                   ", Fecha = '" + DateTime.Now.ToString() + "'" +
                   ", Valor = '" + novedadEmpleado.valor + "'" +
                   ", Valor2 = '" + novedadEmpleado.valor2 + "'" +
                   ", Valor3 = '" + novedadEmpleado.valor3 + "'" +
                   ", Desde = '" + novedadEmpleado.desde.ToShortDateString() + "'" +
                   ", Hasta = '" + novedadEmpleado.hasta.ToShortDateString() + "'" +
                   " WHERE Id = " + novedadEmpleado.ID;
                }

                OleDbCommand oleDbCommand = new OleDbCommand(sql, conn);

                if (oleDbCommand.ExecuteNonQuery() != 1)
                {
                    MessageBox.Show("No se ha podido " + accion + " la Novedad", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    if (cbComprobante.Visible && cbComprobante.Checked)
                    {
                        int coun = Utils.getNumberNovedad(conn, Utils.getLastIDNovedad(conn), 6);

                        String nombre = ((Empleado)cbEmpleado.SelectedItem).ToString();
                        Utils.generarComprobanteHaberes(coun, tbValor.Text, cbMesValor.SelectedItem.ToString(), nombre);
                    }

                    Close();
                }
            }
        }

        private void rbFecha_CheckedChanged(object sender, EventArgs e)
        {
            if (rbFecha.Checked)
            {
                labDesde.Text = "Fecha";
                labHasta.Visible = false;
                dtHasta.Visible = false;
            }
            else
            {
                labDesde.Text = "Desde";
                labHasta.Visible = true;
                dtHasta.Visible = true;
            }

            dtDesde.Focus();
        }

        private void tbValor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter || (int)e.KeyChar == (int)Keys.Tab)
            {
                e.Handled = true;
                cbMesValor.Focus();
            }
            else
            {
                e.KeyChar = Char.ToUpper(e.KeyChar);
            }
        }

        private void tbKm_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter || (int)e.KeyChar == (int)Keys.Tab)
            {
                e.Handled = true;
                tbControl.Focus();
            }
            else
            {
                e.KeyChar = Char.ToUpper(e.KeyChar);
            }
        }

        private void tbControl_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter || (int)e.KeyChar == (int)Keys.Tab)
            {
                e.Handled = true;
                tbViaticos.Focus();
            }
            else
            {
                e.KeyChar = Char.ToUpper(e.KeyChar);
            }
        }

        private void cbEmpleado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter || (int)e.KeyChar == (int)Keys.Tab)
            {
                e.Handled = true;
                cbNovedad.Focus();
            }
            else
            {
                e.KeyChar = Char.ToUpper(e.KeyChar);
            }
        }

        private void cbNovedad_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter || (int)e.KeyChar == (int)Keys.Tab)
            {
                e.Handled = true;

                if (panFechas.Visible)
                {
                    dtDesde.Focus();
                }
                else if (panValor.Visible)
                {
                    tbValor.Focus();
                }
                else if (panKilometraje.Visible)
                {
                    tbKm.Focus();
                }
            }
            else
            {
                e.KeyChar = Char.ToUpper(e.KeyChar);
            }
        }

        private void cbMesValor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter || (int)e.KeyChar == (int)Keys.Tab)
            {
                e.Handled = true;

                butAgregar.Focus();
            }
            else
            {
                e.KeyChar = Char.ToUpper(e.KeyChar);
            }
        }

        private void dtDesde_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter || (int)e.KeyChar == (int)Keys.Tab)
            {
                e.Handled = true;

                if (dtHasta.Visible)
                {
                    dtHasta.Focus();
                }
                else
                {
                    butAgregar.Focus();
                }
            }
            else
            {
                e.KeyChar = Char.ToUpper(e.KeyChar);
            }
        }

        private void dtHasta_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter || (int)e.KeyChar == (int)Keys.Tab)
            {
                e.Handled = true;
                butAgregar.Focus();
            }
            else
            {
                e.KeyChar = Char.ToUpper(e.KeyChar);
            }
        }

        private void cbMes_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter || (int)e.KeyChar == (int)Keys.Tab)
            {
                e.Handled = true;
                butAgregar.Focus();
            }
            else
            {
                e.KeyChar = Char.ToUpper(e.KeyChar);
            }
        }

        private void tbViaticos_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter || (int)e.KeyChar == (int)Keys.Tab)
            {
                e.Handled = true;
                cbMes.Focus();
            }
            else
            {
                e.KeyChar = Char.ToUpper(e.KeyChar);
            }
        }
    }
}
