﻿namespace Gestion_Minera.Ventanas.Sueldos
{
    partial class ABMVacaciones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ABMVacaciones));
            this.label8 = new System.Windows.Forms.Label();
            this.cbEmpleado = new System.Windows.Forms.ComboBox();
            this.dgTabla = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AÑO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CANTIDAD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grVacaciones = new System.Windows.Forms.GroupBox();
            this.butAgregarVacaciones = new System.Windows.Forms.Button();
            this.tbAgregarDias = new System.Windows.Forms.TextBox();
            this.numAnio = new System.Windows.Forms.NumericUpDown();
            this.grNovedades = new System.Windows.Forms.GroupBox();
            this.butCancelar = new System.Windows.Forms.Button();
            this.butAceptar = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbHasta = new System.Windows.Forms.TextBox();
            this.tbFeriados = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbDias = new System.Windows.Forms.TextBox();
            this.dtDesde = new System.Windows.Forms.DateTimePicker();
            this.labDesde = new System.Windows.Forms.Label();
            this.labHasta = new System.Windows.Forms.Label();
            this.dtVacaciones = new System.Windows.Forms.DateTimePicker();
            this.tbCantidadDias = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dgNovedades = new System.Windows.Forms.DataGridView();
            this.butEliminar = new System.Windows.Forms.Button();
            this.butEditar = new System.Windows.Forms.Button();
            this.butAgregar = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.tbAsignados = new System.Windows.Forms.TextBox();
            this.tbDiasTomados = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.butImprimirComprobante = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.dtVuelve = new System.Windows.Forms.DateTimePicker();
            this.idd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DESDE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HAST = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DIAS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FERIADOS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VUELVE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgTabla)).BeginInit();
            this.grVacaciones.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numAnio)).BeginInit();
            this.grNovedades.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgNovedades)).BeginInit();
            this.SuspendLayout();
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 15);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 13);
            this.label8.TabIndex = 57;
            this.label8.Text = "Empleado";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbEmpleado
            // 
            this.cbEmpleado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbEmpleado.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbEmpleado.Location = new System.Drawing.Point(72, 12);
            this.cbEmpleado.Name = "cbEmpleado";
            this.cbEmpleado.Size = new System.Drawing.Size(163, 23);
            this.cbEmpleado.TabIndex = 56;
            this.cbEmpleado.TabStop = false;
            this.cbEmpleado.SelectedIndexChanged += new System.EventHandler(this.cbEmpleado_SelectedIndexChanged);
            // 
            // dgTabla
            // 
            this.dgTabla.AllowUserToAddRows = false;
            this.dgTabla.AllowUserToDeleteRows = false;
            this.dgTabla.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgTabla.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgTabla.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgTabla.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgTabla.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.AÑO,
            this.CANTIDAD});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgTabla.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgTabla.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.dgTabla.Location = new System.Drawing.Point(15, 41);
            this.dgTabla.MultiSelect = false;
            this.dgTabla.Name = "dgTabla";
            this.dgTabla.ReadOnly = true;
            this.dgTabla.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dgTabla.RowHeadersVisible = false;
            this.dgTabla.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgTabla.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgTabla.RowTemplate.ReadOnly = true;
            this.dgTabla.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgTabla.Size = new System.Drawing.Size(220, 265);
            this.dgTabla.TabIndex = 104;
            // 
            // ID
            // 
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.ID.Visible = false;
            // 
            // AÑO
            // 
            this.AÑO.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.AÑO.HeaderText = "AÑO";
            this.AÑO.Name = "AÑO";
            this.AÑO.ReadOnly = true;
            // 
            // CANTIDAD
            // 
            this.CANTIDAD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CANTIDAD.HeaderText = "DÍAS ASIGNADOS";
            this.CANTIDAD.Name = "CANTIDAD";
            this.CANTIDAD.ReadOnly = true;
            // 
            // grVacaciones
            // 
            this.grVacaciones.Controls.Add(this.butAgregarVacaciones);
            this.grVacaciones.Controls.Add(this.tbAgregarDias);
            this.grVacaciones.Controls.Add(this.numAnio);
            this.grVacaciones.Location = new System.Drawing.Point(15, 382);
            this.grVacaciones.Name = "grVacaciones";
            this.grVacaciones.Size = new System.Drawing.Size(220, 75);
            this.grVacaciones.TabIndex = 105;
            this.grVacaciones.TabStop = false;
            this.grVacaciones.Text = "AGREGAR O MODIFICAR VACACIONES";
            // 
            // butAgregarVacaciones
            // 
            this.butAgregarVacaciones.Location = new System.Drawing.Point(119, 46);
            this.butAgregarVacaciones.Name = "butAgregarVacaciones";
            this.butAgregarVacaciones.Size = new System.Drawing.Size(87, 23);
            this.butAgregarVacaciones.TabIndex = 103;
            this.butAgregarVacaciones.Text = "ACEPTAR";
            this.butAgregarVacaciones.UseVisualStyleBackColor = true;
            this.butAgregarVacaciones.Click += new System.EventHandler(this.butAgregarVacaciones_Click);
            // 
            // tbAgregarDias
            // 
            this.tbAgregarDias.Location = new System.Drawing.Point(106, 20);
            this.tbAgregarDias.Name = "tbAgregarDias";
            this.tbAgregarDias.Size = new System.Drawing.Size(100, 20);
            this.tbAgregarDias.TabIndex = 102;
            // 
            // numAnio
            // 
            this.numAnio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numAnio.Location = new System.Drawing.Point(15, 19);
            this.numAnio.Maximum = new decimal(new int[] {
            2050,
            0,
            0,
            0});
            this.numAnio.Minimum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.numAnio.Name = "numAnio";
            this.numAnio.ReadOnly = true;
            this.numAnio.Size = new System.Drawing.Size(85, 21);
            this.numAnio.TabIndex = 101;
            this.numAnio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numAnio.Value = new decimal(new int[] {
            2010,
            0,
            0,
            0});
            this.numAnio.ValueChanged += new System.EventHandler(this.numAnio_ValueChanged);
            // 
            // grNovedades
            // 
            this.grNovedades.Controls.Add(this.dtVuelve);
            this.grNovedades.Controls.Add(this.butCancelar);
            this.grNovedades.Controls.Add(this.butAceptar);
            this.grNovedades.Controls.Add(this.label5);
            this.grNovedades.Controls.Add(this.label4);
            this.grNovedades.Controls.Add(this.tbHasta);
            this.grNovedades.Controls.Add(this.tbFeriados);
            this.grNovedades.Controls.Add(this.label3);
            this.grNovedades.Controls.Add(this.label2);
            this.grNovedades.Controls.Add(this.tbDias);
            this.grNovedades.Controls.Add(this.dtDesde);
            this.grNovedades.Controls.Add(this.labDesde);
            this.grNovedades.Controls.Add(this.labHasta);
            this.grNovedades.Controls.Add(this.dtVacaciones);
            this.grNovedades.Location = new System.Drawing.Point(241, 294);
            this.grNovedades.Name = "grNovedades";
            this.grNovedades.Size = new System.Drawing.Size(345, 163);
            this.grNovedades.TabIndex = 106;
            this.grNovedades.TabStop = false;
            this.grNovedades.Text = "NOVEDADES";
            // 
            // butCancelar
            // 
            this.butCancelar.Location = new System.Drawing.Point(180, 134);
            this.butCancelar.Name = "butCancelar";
            this.butCancelar.Size = new System.Drawing.Size(84, 23);
            this.butCancelar.TabIndex = 114;
            this.butCancelar.Text = "CANCELAR";
            this.butCancelar.UseVisualStyleBackColor = true;
            this.butCancelar.Click += new System.EventHandler(this.butCancelar_Click);
            // 
            // butAceptar
            // 
            this.butAceptar.Location = new System.Drawing.Point(270, 134);
            this.butAceptar.Name = "butAceptar";
            this.butAceptar.Size = new System.Drawing.Size(69, 23);
            this.butAceptar.TabIndex = 104;
            this.butAceptar.Text = "ACEPTAR";
            this.butAceptar.UseVisualStyleBackColor = true;
            this.butAceptar.Click += new System.EventHandler(this.ButAgregarNovedad_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 110);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(109, 13);
            this.label5.TabIndex = 113;
            this.label5.Text = "Día vuelta empleado:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(214, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 13);
            this.label4.TabIndex = 111;
            this.label4.Text = "Total vacaciones:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbHasta
            // 
            this.tbHasta.Location = new System.Drawing.Point(217, 19);
            this.tbHasta.Name = "tbHasta";
            this.tbHasta.ReadOnly = true;
            this.tbHasta.Size = new System.Drawing.Size(106, 20);
            this.tbHasta.TabIndex = 110;
            // 
            // tbFeriados
            // 
            this.tbFeriados.Location = new System.Drawing.Point(110, 75);
            this.tbFeriados.Name = "tbFeriados";
            this.tbFeriados.Size = new System.Drawing.Size(71, 20);
            this.tbFeriados.TabIndex = 109;
            this.tbFeriados.TextChanged += new System.EventHandler(this.tbFeriados_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 13);
            this.label3.TabIndex = 108;
            this.label3.Text = "Cantidad feriados:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 108;
            this.label2.Text = "Cantidad días:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbDias
            // 
            this.tbDias.Location = new System.Drawing.Point(110, 49);
            this.tbDias.Name = "tbDias";
            this.tbDias.Size = new System.Drawing.Size(71, 20);
            this.tbDias.TabIndex = 108;
            this.tbDias.TextChanged += new System.EventHandler(this.tbDias_TextChanged);
            // 
            // dtDesde
            // 
            this.dtDesde.CustomFormat = "";
            this.dtDesde.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDesde.Location = new System.Drawing.Point(56, 19);
            this.dtDesde.Name = "dtDesde";
            this.dtDesde.Size = new System.Drawing.Size(106, 20);
            this.dtDesde.TabIndex = 72;
            this.dtDesde.TabStop = false;
            this.dtDesde.ValueChanged += new System.EventHandler(this.dtDesde_ValueChanged);
            // 
            // labDesde
            // 
            this.labDesde.AutoSize = true;
            this.labDesde.Location = new System.Drawing.Point(12, 22);
            this.labDesde.Name = "labDesde";
            this.labDesde.Size = new System.Drawing.Size(38, 13);
            this.labDesde.TabIndex = 70;
            this.labDesde.Text = "Desde";
            this.labDesde.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labHasta
            // 
            this.labHasta.AutoSize = true;
            this.labHasta.Location = new System.Drawing.Point(177, 22);
            this.labHasta.Name = "labHasta";
            this.labHasta.Size = new System.Drawing.Size(35, 13);
            this.labHasta.TabIndex = 71;
            this.labHasta.Text = "Hasta";
            // 
            // dtVacaciones
            // 
            this.dtVacaciones.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtVacaciones.Location = new System.Drawing.Point(217, 75);
            this.dtVacaciones.Name = "dtVacaciones";
            this.dtVacaciones.Size = new System.Drawing.Size(106, 20);
            this.dtVacaciones.TabIndex = 73;
            this.dtVacaciones.TabStop = false;
            // 
            // tbCantidadDias
            // 
            this.tbCantidadDias.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCantidadDias.Location = new System.Drawing.Point(121, 355);
            this.tbCantidadDias.Name = "tbCantidadDias";
            this.tbCantidadDias.ReadOnly = true;
            this.tbCantidadDias.Size = new System.Drawing.Size(84, 21);
            this.tbCantidadDias.TabIndex = 107;
            this.tbCantidadDias.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(46, 358);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 74;
            this.label1.Text = "Días a favor:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dgNovedades
            // 
            this.dgNovedades.AllowUserToAddRows = false;
            this.dgNovedades.AllowUserToDeleteRows = false;
            this.dgNovedades.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgNovedades.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgNovedades.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgNovedades.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgNovedades.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idd,
            this.DESDE,
            this.HAST,
            this.DIAS,
            this.FERIADOS,
            this.VUELVE});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgNovedades.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgNovedades.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.dgNovedades.Location = new System.Drawing.Point(241, 41);
            this.dgNovedades.MultiSelect = false;
            this.dgNovedades.Name = "dgNovedades";
            this.dgNovedades.ReadOnly = true;
            this.dgNovedades.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dgNovedades.RowHeadersVisible = false;
            this.dgNovedades.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgNovedades.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dgNovedades.RowTemplate.ReadOnly = true;
            this.dgNovedades.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgNovedades.Size = new System.Drawing.Size(345, 189);
            this.dgNovedades.TabIndex = 108;
            this.dgNovedades.SelectionChanged += new System.EventHandler(this.dgNovedades_SelectionChanged);
            // 
            // butEliminar
            // 
            this.butEliminar.ForeColor = System.Drawing.Color.Red;
            this.butEliminar.Location = new System.Drawing.Point(516, 236);
            this.butEliminar.Name = "butEliminar";
            this.butEliminar.Size = new System.Drawing.Size(70, 23);
            this.butEliminar.TabIndex = 114;
            this.butEliminar.Text = "ELIMINAR";
            this.butEliminar.UseVisualStyleBackColor = true;
            this.butEliminar.Click += new System.EventHandler(this.butEliminar_Click);
            // 
            // butEditar
            // 
            this.butEditar.Location = new System.Drawing.Point(452, 236);
            this.butEditar.Name = "butEditar";
            this.butEditar.Size = new System.Drawing.Size(58, 23);
            this.butEditar.TabIndex = 115;
            this.butEditar.Text = "EDITAR";
            this.butEditar.UseVisualStyleBackColor = true;
            this.butEditar.Click += new System.EventHandler(this.butEditar_Click);
            // 
            // butAgregar
            // 
            this.butAgregar.Location = new System.Drawing.Point(241, 265);
            this.butAgregar.Name = "butAgregar";
            this.butAgregar.Size = new System.Drawing.Size(345, 23);
            this.butAgregar.TabIndex = 116;
            this.butAgregar.Text = "AGREGAR NUEVA NOVEDAD";
            this.butAgregar.UseVisualStyleBackColor = true;
            this.butAgregar.Click += new System.EventHandler(this.butAgregar_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(28, 310);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 13);
            this.label6.TabIndex = 117;
            this.label6.Text = "Total asignados:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbAsignados
            // 
            this.tbAsignados.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAsignados.Location = new System.Drawing.Point(20, 328);
            this.tbAsignados.Name = "tbAsignados";
            this.tbAsignados.ReadOnly = true;
            this.tbAsignados.Size = new System.Drawing.Size(101, 21);
            this.tbAsignados.TabIndex = 118;
            this.tbAsignados.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbDiasTomados
            // 
            this.tbDiasTomados.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbDiasTomados.Location = new System.Drawing.Point(127, 328);
            this.tbDiasTomados.Name = "tbDiasTomados";
            this.tbDiasTomados.ReadOnly = true;
            this.tbDiasTomados.Size = new System.Drawing.Size(101, 21);
            this.tbDiasTomados.TabIndex = 119;
            this.tbDiasTomados.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(139, 310);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 13);
            this.label7.TabIndex = 120;
            this.label7.Text = "Total tomados:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // butImprimirComprobante
            // 
            this.butImprimirComprobante.Location = new System.Drawing.Point(241, 236);
            this.butImprimirComprobante.Name = "butImprimirComprobante";
            this.butImprimirComprobante.Size = new System.Drawing.Size(169, 23);
            this.butImprimirComprobante.TabIndex = 121;
            this.butImprimirComprobante.Text = "IMPRIMIR COMPROBANTE";
            this.butImprimirComprobante.UseVisualStyleBackColor = true;
            this.butImprimirComprobante.Click += new System.EventHandler(this.butImprimirComprobante_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(241, 25);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(109, 13);
            this.label9.TabIndex = 122;
            this.label9.Text = "Vacaciones tomadas:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dtVuelve
            // 
            this.dtVuelve.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtVuelve.Location = new System.Drawing.Point(123, 108);
            this.dtVuelve.Name = "dtVuelve";
            this.dtVuelve.Size = new System.Drawing.Size(200, 20);
            this.dtVuelve.TabIndex = 115;
            // 
            // idd
            // 
            this.idd.HeaderText = "id";
            this.idd.Name = "idd";
            this.idd.ReadOnly = true;
            this.idd.Visible = false;
            // 
            // DESDE
            // 
            this.DESDE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DESDE.FillWeight = 93.27411F;
            this.DESDE.HeaderText = "DESDE";
            this.DESDE.Name = "DESDE";
            this.DESDE.ReadOnly = true;
            // 
            // HAST
            // 
            this.HAST.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.HAST.FillWeight = 93.27411F;
            this.HAST.HeaderText = "HASTA";
            this.HAST.Name = "HAST";
            this.HAST.ReadOnly = true;
            // 
            // DIAS
            // 
            this.DIAS.FillWeight = 126.9036F;
            this.DIAS.HeaderText = "DÍAS";
            this.DIAS.Name = "DIAS";
            this.DIAS.ReadOnly = true;
            this.DIAS.Width = 40;
            // 
            // FERIADOS
            // 
            this.FERIADOS.FillWeight = 93.27411F;
            this.FERIADOS.HeaderText = "FERIADOS";
            this.FERIADOS.Name = "FERIADOS";
            this.FERIADOS.ReadOnly = true;
            this.FERIADOS.Width = 65;
            // 
            // VUELVE
            // 
            this.VUELVE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.VUELVE.FillWeight = 93.27411F;
            this.VUELVE.HeaderText = "VUELVE";
            this.VUELVE.Name = "VUELVE";
            this.VUELVE.ReadOnly = true;
            // 
            // ABMVacaciones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(597, 464);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.butImprimirComprobante);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tbDiasTomados);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tbAsignados);
            this.Controls.Add(this.butAgregar);
            this.Controls.Add(this.butEditar);
            this.Controls.Add(this.butEliminar);
            this.Controls.Add(this.dgNovedades);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbCantidadDias);
            this.Controls.Add(this.grNovedades);
            this.Controls.Add(this.grVacaciones);
            this.Controls.Add(this.dgTabla);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.cbEmpleado);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ABMVacaciones";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Vacaciones";
            ((System.ComponentModel.ISupportInitialize)(this.dgTabla)).EndInit();
            this.grVacaciones.ResumeLayout(false);
            this.grVacaciones.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numAnio)).EndInit();
            this.grNovedades.ResumeLayout(false);
            this.grNovedades.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgNovedades)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbEmpleado;
        private System.Windows.Forms.DataGridView dgTabla;
        private System.Windows.Forms.GroupBox grVacaciones;
        private System.Windows.Forms.Button butAgregarVacaciones;
        private System.Windows.Forms.TextBox tbAgregarDias;
        private System.Windows.Forms.NumericUpDown numAnio;
        private System.Windows.Forms.GroupBox grNovedades;
        private System.Windows.Forms.TextBox tbFeriados;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbDias;
        private System.Windows.Forms.DateTimePicker dtDesde;
        private System.Windows.Forms.Label labDesde;
        private System.Windows.Forms.Label labHasta;
        private System.Windows.Forms.DateTimePicker dtVacaciones;
        private System.Windows.Forms.TextBox tbCantidadDias;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbHasta;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button butAceptar;
        private System.Windows.Forms.DataGridView dgNovedades;
        private System.Windows.Forms.Button butCancelar;
        private System.Windows.Forms.Button butEliminar;
        private System.Windows.Forms.Button butEditar;
        private System.Windows.Forms.Button butAgregar;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbAsignados;
        private System.Windows.Forms.TextBox tbDiasTomados;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button butImprimirComprobante;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn AÑO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CANTIDAD;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dtVuelve;
        private System.Windows.Forms.DataGridViewTextBoxColumn idd;
        private System.Windows.Forms.DataGridViewTextBoxColumn DESDE;
        private System.Windows.Forms.DataGridViewTextBoxColumn HAST;
        private System.Windows.Forms.DataGridViewTextBoxColumn DIAS;
        private System.Windows.Forms.DataGridViewTextBoxColumn FERIADOS;
        private System.Windows.Forms.DataGridViewTextBoxColumn VUELVE;
    }
}